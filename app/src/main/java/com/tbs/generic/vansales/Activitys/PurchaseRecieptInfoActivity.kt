package com.tbs.generic.vansales.Activitys

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.tbs.generic.vansales.Adapters.CreatePaymentAdapter
import com.tbs.generic.vansales.Adapters.InvoiceAdapter
import com.tbs.generic.vansales.Adapters.PurchaseRecieptInfoAdapter
import com.tbs.generic.vansales.Adapters.ScheduledPickupProductsAdapter
import com.tbs.generic.vansales.Model.ActiveDeliveryDO
import com.tbs.generic.vansales.Model.CustomerDo
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.CreateLotRequest
import com.tbs.generic.vansales.Requests.CreatePurchaseRecieptRequest
import com.tbs.generic.vansales.Requests.UnPaidInvoicesListRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.AlphaNumeric
import com.tbs.generic.vansales.utils.LogUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.purchase_stock_data.*
import java.lang.Double
import java.util.*


class PurchaseRecieptInfoActivity : BaseActivity() {

    lateinit var invoiceAdapter: InvoiceAdapter
    var recieptDos: ArrayList<ActiveDeliveryDO> = ArrayList()
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    var serialLotDO: ActiveDeliveryDO = ActiveDeliveryDO()
    lateinit var view: LinearLayout
    lateinit var btnAddSerialLot: Button
    lateinit var btnConfirm: Button

    var fromId = 0
    var productId = ""
    var productDescription = ""
    private lateinit var siteListRequest: UnPaidInvoicesListRequest
    var activeDeliveryDO: ActiveDeliveryDO = ActiveDeliveryDO()
    var infoAdapter: PurchaseRecieptInfoAdapter = PurchaseRecieptInfoAdapter(activeDeliveryDO, this@PurchaseRecieptInfoActivity, ArrayList())

    // This is my number set it contains 62 symbols
    private val numberset = Arrays.asList(
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    )
    private var initAL: String? = null // keep starting AN
    private var BASE: Int = 0 // base of numberset is 62 so, addition = (anynumber %
    // BASE) and spare = (anynumber / 62)

    override fun onResume() {
        super.onResume()
//        selectInvoiceList()
    }


    @Synchronized
    fun nextAN(): String {

        var number = initAL!!.toCharArray()
        var spare = 0
        val addition = 1

        /*
         * Here what i'm doing is i'm looping the given string backwards and get
         * char by char
         */

        for (i in number.indices.reversed()) {

            val lastnumber = numberset.indexOf(number[i])
            /*
             * abov i'm getting number associated with the last char, example
             * letter "A" means 10 and in the below line i'm adding +1, also if
             * there is number left in the previous calculation I'm adding it
             * too
             */

            val newnumb = lastnumber + addition + spare

            /*
             * now i'm checking whether the new number is exceeding the base,
             * example, in normal number set is 9+1 = 10
             */
            if (newnumb >= BASE) {
                // calculate spare and the addition
                number[i] = numberset[newnumb % BASE]
                spare = newnumb / BASE
            } else {
                /*
				if the addition is not exceeding the base then we can just
				add them
				and stop there
				*/
                number[i] = numberset[newnumb]
                break
            }
            /*
			checkin a special situation, spare can be 1 but sometimes number
			might have end in the next iteration, in this case i'm adding the
			spare to the front
			and stoping
			*/
            if (spare > 0 && i - 1 < 0) {
                number = (numberset[spare] + String(number)).toCharArray()
                break
            }
        }

        return number.toString()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.reciept_info_list, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        this.initAL = initAL
        this.BASE = numberset.size
        if (intent.hasExtra("P_ID")) {
            productId = intent.extras?.getString("P_ID")!!
        }
        if (intent.hasExtra("PRODUCTDO")) {
            activeDeliveryDO = intent.extras!!.getSerializable("PRODUCTDO") as ActiveDeliveryDO
        }
        if (intent.hasExtra("PRODUCT")) {
            productDescription = intent.extras?.getString("PRODUCT")!!
        }
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            showToast(getString(R.string.lotmanagement))

        }
    }

    override fun initializeControls() {
//        tvScreenTitle.setText("Enter Details")
        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
        ivAdd.visibility = View.VISIBLE
        ivAdd.setOnClickListener {
            Util.preventTwoClick(it)
            showAddItemDialog("", "", "", "", 0, "")
        }

        btnAddSerialLot = findViewById<Button>(R.id.btnAddSerialLot)
        btnAddSerialLot.visibility = View.GONE

        btnAddSerialLot.setOnClickListener {
            Util.preventTwoClick(it)
            showAddItemDialog("", "", "", "", 0, "")
        }
        btnConfirm = findViewById<Button>(R.id.btnConfirm)
        btnConfirm.setOnClickListener {
            Util.preventTwoClick(it)
            var locqty=0
            showLoader()
            for (i in recieptDos.indices) {
                locqty=locqty+recieptDos.get(i).orderedQuantity
            }
            if (locqty == activeDeliveryDO.totalQuantity) {
                hideLoader()
//                ScheduledPickupProductsAdapter.MyViewHolder.btnSerialLot.visibility=View.GONE
                showAppCompatAlert("", getString(R.string.are_you_sure_confirm_cant_modify), getString(R.string.ok), getString(R.string.cancel), getString(R.string.success), true)

            }else{
                hideLoader()
                showAlert(resources.getString(R.string.lotmanagementt))
            }

        }
        recycleview.layoutManager = linearLayoutManager
        var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

        tvScreenTitle.text = activeDeliverySavedDo.customerDescription

        infoAdapter = PurchaseRecieptInfoAdapter(activeDeliveryDO, this@PurchaseRecieptInfoActivity, ArrayList())
        recycleview.adapter = infoAdapter


    }


    fun showAddItemDialog(position: String, lot: String, serialStart: String, serialEnd: String, quantity: Int, statusSelected: String) {
        try {


            var list_of_items = arrayOf("Stock Status", "A")
//            var list_of_items = arrayOf("Select Status", "A", "Q", "R")

            var dialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(this).inflate(R.layout.reciept_info_item_details, null)
//            applyFont(view, AppConstants.MONTSERRAT_REGULAR_TYPE_FACE)
            var etLotNumber = view.findViewById<View>(R.id.etLotNumber) as EditText
            var etSerialStart = view.findViewById<View>(R.id.etSerialStart) as EditText
            var etSerialEnd = view.findViewById<View>(R.id.etSerialEnd) as EditText
            var etQuantity = view.findViewById<View>(R.id.etQuantity) as EditText
            var spStatus = view.findViewById<View>(R.id.etStatus) as Spinner

            if (quantity > 0) {
//                etQuantity.removeTextChangedListener(this)
                etQuantity.setText("" + quantity)
//                etQuantity.addTextChangedListener(this)


            }
            etQuantity.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                    if (quantity > 0) {
                        etQuantity.removeTextChangedListener(this)
                        etQuantity.setText("" + quantity)
                        etQuantity.addTextChangedListener(this)


                    }
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (quantity > 0) {
                        etQuantity.removeTextChangedListener(this)
                        etQuantity.setText("" + quantity)
                        etQuantity.addTextChangedListener(this)


                    }
                }

                override fun afterTextChanged(s: Editable) {
                    if (quantity == 0) {
                        if (!s.toString().equals("", ignoreCase = true)) {
                            if (Double.parseDouble(s.toString()).toInt() <= activeDeliveryDO.totalQuantity && Double.parseDouble(s.toString()).toInt() > 0) {
                                var totalQty = 0

                                for (i in recieptDos.indices) {//10
                                    totalQty = totalQty + recieptDos.get(i).purchaseOrderQuantity

                                }
                                totalQty = totalQty + Double.parseDouble(s.toString()).toInt()
                                if (totalQty > activeDeliveryDO.totalQuantity) {
                                    etQuantity.removeTextChangedListener(this)
                                    etQuantity.setText("")
                                    etQuantity.addTextChangedListener(this)
                                    showToast(getString(R.string.please_enter_less_than_qty))

                                } else {

                                }
                            } else {
                                etQuantity.removeTextChangedListener(this)
                                etQuantity.setText("")
                                etQuantity.addTextChangedListener(this)
                                showToast(getString(R.string.please_enter_less_than_qty))

                            }
                        } else {
                            etQuantity.removeTextChangedListener(this)
                            etQuantity.setText("")
                            etQuantity.addTextChangedListener(this)
                        }
                    } else {
                        etQuantity.removeTextChangedListener(this)
                        etQuantity.setText("" + quantity)
                        etQuantity.addTextChangedListener(this)
                    }
                }
            })
//            etSerialStart.addTextChangedListener(object : TextWatcher {
//                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
//
//                }
//
//                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//
//                }
//
//                override fun afterTextChanged(s: Editable) {
//                    if (!s.toString().equals("", ignoreCase = true)) {
//                        var enteredQty=0
//                        if(etQuantity.text.toString().trim().length>0){
//                            enteredQty = etQuantity.text.toString().trim().toInt()
//
//                        }
//                        if (etSerialStart.text.toString().trim().length > 0) {
//                            var start = etSerialStart.text.toString().trim()
////                               var numberOnly= start.replace("[^0-9]", "").toInt();
//                            if (enteredQty > 1) {
//                                var counter = 0
//
//                                while (true) {
//                                    var docID = AlphaNumeric(start)
//
//                                    counter++
//                                    if (counter == enteredQty) {
//                                        break
//                                    }
//                                    etSerialEnd.removeTextChangedListener(this)
//                                    etSerialEnd.setText("" + docID.nextAN())
//                                    etSerialEnd.addTextChangedListener(this)
////                                   showToast( "ID: " + docID.nextAN() + " Counter :" + counter)
//
//                                }
//                            }else{
//                                etSerialEnd.removeTextChangedListener(this)
//                                etSerialEnd.setText("" + start)
//                                etSerialEnd.addTextChangedListener(this)
//
//                            }
//
//                        }
//
//
//                    } else {
//                        etQuantity.removeTextChangedListener(this)
//                        etQuantity.setText("")
//                        etQuantity.addTextChangedListener(this)
//                    }
//
//                }
//            })

            val btnSubmit = view.findViewById<View>(R.id.btnConfirm) as Button
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            if (lot.isNotEmpty()) {
                etLotNumber.setText(lot)
            }
            if (serialStart.isNotEmpty()) {
                etSerialStart.setText(serialStart)
            }
            if (serialEnd.isNotEmpty()) {
                etSerialEnd.setText(serialEnd)
            }

            if (statusSelected.isNotEmpty()) {
//                if (statusSelected.equals("A")) {
//                    list_of_items = arrayOf("A", "Q", "R")
//
//                } else if (statusSelected.equals("Q")) {
//                    list_of_items = arrayOf("Q", "A", "R")
//
//                } else if (statusSelected.equals("R")) {
//                    list_of_items = arrayOf("R", "A", "Q")
//
//                }

                if (statusSelected.equals("A")) {
                    list_of_items = arrayOf("A")

                }

            }
            var status = ""

            val array_adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, list_of_items)
            array_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spStatus.adapter = array_adapter
            spStatus.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    if (position == 0) {
                        if (list_of_items[0].equals("Stock Status")) {
                            status = ""

                        } else {
                            status = list_of_items[position]

                        }


                    } else {
                        status = list_of_items[position]

                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }


            btnSubmit.setOnClickListener {
                Util.preventTwoClick(it)
                tvNoDataFound.visibility = View.GONE
                val lotNumber = etLotNumber.text.toString().trim()
                val serialStart = etSerialStart.text.toString().trim()
                val serialEnd = etSerialEnd.text.toString().trim()
                val quantity = etQuantity.text.toString().trim()

                if (lotNumber.equals("", ignoreCase = true)) {
                    showToast(getString(R.string.please_enter_lot_no))
                }
//                else if (serialStart.equals("", ignoreCase = true)) {
//                    showToast(getString(R.string.please_enter_serial_start_no))
//                } else if (serialEnd.equals("", ignoreCase = true)) {
//                    showToast(getString(R.string.please_enter_serial_end_no))
//                }
                else if (quantity.equals("", ignoreCase = true)) {
                    showToast(getString(R.string.please_enter_quantity))
                } else if (status.equals("", ignoreCase = true)) {
                    showToast(getString(R.string.please_enter_status))
                } else {
                    var serialNumber = serialStart

                    if (position.isNotEmpty()) {
                        var pos = Integer.valueOf(position)
                        for (i in recieptDos.indices) {//10
                            if (i == pos) {
                                recieptDos.get(pos).purchaseOrderLotNumber = lotNumber
                                recieptDos.get(pos).purchaseOrderSerialNumber = serialNumber
                                recieptDos.get(pos).purchaseOrderSerialNumberStart = serialStart
                                recieptDos.get(pos).purchaseOrderSerialNumberEnd = serialEnd

                                recieptDos.get(pos).purchaseOrderQuantity = quantity.toInt()
                                recieptDos.get(pos).purchaseOrderStatus = status
                                recieptDos.get(pos).product = activeDeliveryDO.product
                                recieptDos.get(pos).productDescription = activeDeliveryDO.productDescription
                                var qty = Integer.valueOf(quantity)
                                recieptDos.get(pos).orderedQuantity = qty
                                recieptDos.get(pos).price = activeDeliveryDO.price
                                recieptDos.get(pos).linenumber = activeDeliveryDO.linenumber
                                recieptDos.get(pos).unit = activeDeliveryDO.unit
                                recieptDos.set(pos, recieptDos.get(pos))
                            }
                        }

                    } else {
                        serialLotDO = ActiveDeliveryDO()
                        serialLotDO.purchaseOrderLotNumber = lotNumber
                        serialLotDO.purchaseOrderSerialNumber = serialNumber
                        serialLotDO.purchaseOrderSerialNumberStart = serialStart
                        serialLotDO.purchaseOrderSerialNumberEnd = serialEnd

                        serialLotDO.purchaseOrderQuantity = Double.parseDouble(quantity).toInt()
                        serialLotDO.purchaseOrderStatus = status
                        serialLotDO.product = activeDeliveryDO.product
                        serialLotDO.productDescription = activeDeliveryDO.productDescription
                        serialLotDO.orderedQuantity =  Double.parseDouble(quantity).toInt()
                        serialLotDO.price = activeDeliveryDO.price
                        serialLotDO.linenumber = activeDeliveryDO.linenumber
                        serialLotDO.unit = activeDeliveryDO.unit

                        recieptDos.add(serialLotDO)

                    }
                    var totalQty = 0
                    for (i in recieptDos.indices) {//10
                        totalQty = totalQty + recieptDos.get(i).purchaseOrderQuantity

                    }
                    if (totalQty >= activeDeliveryDO.totalQuantity) {
                        btnAddSerialLot.isEnabled = false
                        btnAddSerialLot.isClickable = false
                        btnAddSerialLot.setBackgroundColor(resources.getColor(R.color.md_gray_light))

                        ivAdd.isEnabled = false
                        ivAdd.isClickable = false

                    } else {
                        btnAddSerialLot.isEnabled = true
                        btnAddSerialLot.isClickable = true
                        btnAddSerialLot.setBackgroundColor(resources.getColor(R.color.md_green))

                        ivAdd.isEnabled = true
                        ivAdd.isClickable = true
                    }
                    infoAdapter.refreshAdapter(activeDeliveryDO, recieptDos)

                    dialog.dismiss()
                }

            }
            dialog.setContentView(view)
            if (!dialog.isShowing)
                dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
//            selectInvoiceList()
        }
    }

    override fun onBackPressed() {
        showToast(getString(R.string.lotmanagement))
    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            val siteListRequest = CreateLotRequest(recieptDos, this@PurchaseRecieptInfoActivity)
            siteListRequest.setOnResultListener { isError, approveDO, msg ->
                hideLoader()

                if (isError) {
                    if (msg.isNotEmpty()) {
                        showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)

                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)

                    }
                } else {
                    if (approveDO.flag == 2) {
                        val intent = Intent()
                        intent.putExtra("RecieptDOS", recieptDos)
                        setResult(10, intent)
                        finish()
                    }else{
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)
                    }

                }

            }
            siteListRequest.execute()

        } else
            if (getString(R.string.failure).equals(from, ignoreCase = true)) {

            }

    }

}