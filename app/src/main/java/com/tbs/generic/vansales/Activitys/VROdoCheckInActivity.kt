package com.tbs.generic.vansales.Activitys

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.Model.PickUpDo
import com.tbs.generic.vansales.Model.VehicleCheckInDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.StartRouteService
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.Constants
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.configuration_layout.*
import kotlinx.android.synthetic.main.reading_dialog.*
import kotlinx.android.synthetic.main.vr_odo_check_in_screen.*
import java.math.BigDecimal
import java.text.DecimalFormat


class VROdoCheckInActivity : BaseActivity() {
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200
    lateinit var vehicleCheckInDo: VehicleCheckInDo
    private var pickUpDos: ArrayList<PickUpDo> = ArrayList()
    var isDepartureExisted = false
    lateinit var preferenceUtilss: PreferenceUtils


    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.vr_odo_check_in_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        preferenceUtilss = PreferenceUtils(this@VROdoCheckInActivity)
        flToolbar.visibility = View.GONE
        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        changeLocale()
        tv_title.text = getString(R.string.select_vr)
        val startRoute = preferenceUtils.getIntFromPreference(PreferenceUtils.START_ROUTE, 0)

        if (startRoute==2) {
            ll_startRoute.setBackgroundResource(R.drawable.card_white_green_boarder)

        }

    }

    override fun onResume() {
        super.onResume()
        initializeControls()
    }

    override fun initializeControls() {


        ll_vehicle_route.setBackgroundResource(R.drawable.card_white)
        ll_odo_meter_read.setBackgroundResource(R.drawable.card_white)
        ll_check_in_odo.setBackgroundResource(R.drawable.card_white)
        val odoreading = preferenceUtils.getIntFromPreference(PreferenceUtils.ODO_READ, 0)

        val routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
        if (!TextUtils.isEmpty(routeId)) {
            ll_vehicle_route.setBackgroundResource(R.drawable.card_white_green_boarder)
            tvVR.setText(routeId)
            tvVR.visibility = View.VISIBLE


        } else {
            tvVR.visibility = View.GONE


        }
        ll_vehicle_route.setOnClickListener {
            val intent = Intent(this, VRselectionScreen::class.java)
            startActivity(intent)
        }
        val read = preferenceUtilss.getStringFromPreference(PreferenceUtils.OPENING_READING, "")
        val unit = preferenceUtilss.getStringFromPreference(PreferenceUtils.ODO_UNIT, "")

        if (read.isNotEmpty()) {
            ll_odo_meter_read.setBackgroundResource(R.drawable.card_white_green_boarder)
            tvEdit.visibility = View.VISIBLE
            if(read.isNullOrEmpty()){
                tvEdit.setText("" + odoreading+" "+unit)

            }else{
                tvEdit.setText("" + read+" "+unit)

            }



        } else {
            if(read.isNullOrEmpty()){
                tvEdit.setText("" + odoreading+" "+unit)
                tvEdit.visibility = View.VISIBLE

            }else{
                tvEdit.visibility = View.GONE

            }


        }
        ll_startRoute.setOnClickListener {
            startRouteTimeCapture()
        }
        ll_odo_meter_read.setOnClickListener {
            if (TextUtils.isEmpty(routeId)) {
                showAppCompatAlert("", getString(R.string.please_select_vr_selection), getString(R.string.ok), "", getString(R.string.failure), false)
                return@setOnClickListener;
            }
            var customDialog = Dialog(this);
            customDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
            customDialog.setContentView(R.layout.reading_dialog);
            customDialog.show();
            val unit = preferenceUtilss.getStringFromPreference(PreferenceUtils.ODO_UNIT, "")
            customDialog.tvUnit.setText(""+unit)

            var odoReading = preferenceUtils.getStringFromPreference(PreferenceUtils.OPENING_READING, "")
            if (odoReading.isNotEmpty()) {
                customDialog.etReading.setText(odoReading+"")
                val pos: Int = customDialog.etReading.getText().length
                customDialog.etReading.setSelection(pos)
            }else{
                customDialog.etReading.setText(odoreading.toString())
                val pos: Int = customDialog.etReading.getText().length
                customDialog.etReading.setSelection(pos)
            }
            customDialog.button1.setOnClickListener {
                customDialog.dismiss();
            }
            customDialog.button2.setOnClickListener {
                customDialog.dismiss();
                if (customDialog.etReading.text.toString().isNotEmpty()) {
                    var reading = customDialog.etReading.text.toString()
                    if(reading.toDouble()>=odoreading.toDouble()){
                        preferenceUtils.saveString(PreferenceUtils.OPENING_READING, reading)

                    }else{
                        showToast(getString(R.string.greater_alert))
                    }

                }

//                        if(reading!=null&&reading.isNaN())
                onResume()
            }

        }
        val vehicleCheckInDo = StorageManager.getInstance(this@VROdoCheckInActivity).getVehicleCheckInData(this@VROdoCheckInActivity)
        if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
            ll_check_in_odo.setBackgroundResource(R.drawable.card_white_green_boarder)
        } else {

            ll_check_in_odo.setOnClickListener {
                if (TextUtils.isEmpty(routeId)) {
                    showAppCompatAlert("", getString(R.string.please_select_vr_selection), getString(R.string.ok), "", getString(R.string.failure), false)
                    return@setOnClickListener;
                }
//                if (odoreading==null&&read.isEmpty()) {
//                    showAppCompatAlert("", getString(R.string.please_enter_odo_reading), getString(R.string.ok), "", getString(R.string.failure), false)
//                    return@setOnClickListener;
//                }

                val intent = Intent(this, ScheduleNonScheduleActivity::class.java)
                intent.putExtra(Constants.SCREEN_TYPE, getString(R.string.confirm_stock))
                startActivity(intent)
            }
        }


    }
    private fun startRouteTimeCapture() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
        val date = CalendarUtils.getDate()
        val time = CalendarUtils.getTime()
        val num = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

        val siteListRequest = StartRouteService(id, date, time, num, this)
        siteListRequest.setOnResultListener { isError, loginDo ->

            if (isError) {
                Util.showToast(this, getString(R.string.server_error))
            } else {

                if (loginDo != null) {
                    if (loginDo.flag == 20) {
                        ll_startRoute.setBackgroundResource(R.drawable.card_white_green_boarder)
                        preferenceUtils.saveInt(PreferenceUtils.START_ROUTE, 2)

                        Util.showToast(this, getString(R.string.updated_successfully))


                    } else {
                        Util.showToast(this, getString(R.string.server_error))

                    }
                } else {
                    Util.showToast(this, getString(R.string.server_error))


                }
            }

        }

        siteListRequest.execute()
    }


}