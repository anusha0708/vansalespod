package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class StockItemDo implements Serializable {

    public int stockType;
    public String shipmentNumber = "";
    public String productName = "";
    public String productDescription = "";
    public String stockUnit = "";
    public String weightUnit = "";
    public double productVolume = 0.0;
    public String totalStockUnit = "";
    public String totalStockVolume = "";
    public double productWeight = 0.0;
    public Double quantity = 0.0;
    public Double updateQty = 0.0;
    public String productUnit = "";
    public int itemCount = 0;
    public int status;
    public int productGroupType = 0;
    public String other = "";
    public int pickUpOrDrop = 0;

    @Override
    public String toString() {
        return "StockItemDo{" +
                "stockType=" + stockType +
                ", shipmentNumber='" + shipmentNumber + '\'' +
                ", productName='" + productName + '\'' +
                ", productDescription='" + productDescription + '\'' +
                ", stockUnit='" + stockUnit + '\'' +
                ", weightUnit='" + weightUnit + '\'' +
                ", productVolume=" + productVolume +
                ", totalStockUnit='" + totalStockUnit + '\'' +
                ", totalStockVolume='" + totalStockVolume + '\'' +
                ", productWeight=" + productWeight +
                ", quantity='" + quantity + '\'' +
                ", productUnit='" + productUnit + '\'' +
                ", itemCount=" + itemCount +
                ", status=" + status +
                ", productGroupType=" + productGroupType +
                ", other='" + other + '\'' +
                '}';
    }

}
