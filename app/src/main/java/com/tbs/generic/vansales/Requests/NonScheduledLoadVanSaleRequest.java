package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.Model.LoadStockMainDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class NonScheduledLoadVanSaleRequest extends AsyncTask<String, Void, Boolean> {

    private LoadStockMainDO loadStockMainDO;
    private LoadStockDO loadStockDO;
    private Context mContext;
    String routingId;

    public NonScheduledLoadVanSaleRequest(String routingId, Context mContext) {
        this.mContext = mContext;
        this.routingId = routingId;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, LoadStockMainDO loadStockMainDO);

    }

    public boolean runRequest() {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YUNROU", routingId);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.NON_SCHEDULED_LOAD_VANSALES, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            loadStockMainDO = new LoadStockMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        loadStockMainDO.loadStockDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        loadStockDO = new LoadStockDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                         if (attribute.equalsIgnoreCase("O_XFINVOL")) {
                            loadStockMainDO.volume = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_XVNMASUN")) {
                            loadStockMainDO.vehicleCapacityUnit = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_XVNVOLUN")) {
                            loadStockMainDO.volumeUnit = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_YTWI")) {
                             if(text.length()>0){

                                 loadStockMainDO.totalNonScheduledStock = text;
                             }


                        }
                        else if (attribute.equalsIgnoreCase("O_XVANMASS")) {
                            loadStockMainDO.vehicleCapacity = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_XVANVOL")) {
                            loadStockMainDO.vanVolume = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_YSDHNUM")) {
                          //  loadStockDO.shipmentNumber = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_YITMREF")) {
                            loadStockDO.product = text;


                        } else if (attribute.equalsIgnoreCase("O_YDES")) {
                            loadStockDO.productDescription = text;


                        } else if (attribute.equalsIgnoreCase("O_YSUNI")) {
                            loadStockDO.stockUnit = text;

                        } else if (attribute.equalsIgnoreCase("O_YWUNI")) {
                            loadStockDO.weightUnit = text;

                        } else if (attribute.equalsIgnoreCase("O_YWI")) {
                            loadStockDO.productWeight = Double.parseDouble(text);

                        } else if (attribute.equalsIgnoreCase("O_YVO")) {
                            loadStockDO.productVolume = Double.parseDouble(text);

                        }  else if (attribute.equalsIgnoreCase("O_YSQTY")) {
                            loadStockDO.quantity = Integer.parseInt(text);


                        }

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        loadStockMainDO.loadStockDOS.add(loadStockDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

       // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, loadStockMainDO);
        }
    }
}