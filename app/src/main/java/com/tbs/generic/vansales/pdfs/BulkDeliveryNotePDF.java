package com.tbs.generic.vansales.pdfs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.pdfs.utils.PDFConstants;
import com.tbs.generic.vansales.pdfs.utils.PDFOperations;
import com.tbs.generic.vansales.pdfs.utils.PreviewActivity;
import com.tbs.generic.vansales.utils.CalendarUtils;
import com.tbs.generic.vansales.utils.LogUtils;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;

import java.io.File;
import java.io.FileOutputStream;

/*
 * Created by developer on 25/1/19.
 */
public class BulkDeliveryNotePDF {

    private Context context;
    private ActiveDeliveryMainDO activeDeliveryMainDO;
    private PdfPTable parentTable;
    private Font normalFont;
    private Font boldFont;
    private Document document;

    public BulkDeliveryNotePDF(Context context) {
        this.context = context;
    }


    public BulkDeliveryNotePDF createDeliveryPDF(ActiveDeliveryMainDO activeDeliveryMainDO, String type) {
        this.activeDeliveryMainDO = activeDeliveryMainDO;

        Log.d("ActiveDeliveryMainDO-->", activeDeliveryMainDO + "");

        boldFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);
        normalFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.NORMAL, BaseColor.BLACK);
        if(type.equalsIgnoreCase("Preview")){
            normalFont = boldFont;
        }
//        new CreateDeliveryPDF().execute();

        return this;
    }

    @SuppressLint("StaticFieldLeak")
    public class CreateDeliveryPDF extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Generating Delivery Note Please wait...");
//            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                preparePDF();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            progressDialog.cancel();
            sendPDfewDoc();
        }
    }
    @SuppressLint("StaticFieldLeak")
    public class CreatePDF extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Preparing Delivery Note Please wait...");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                preparePDF();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.cancel();
            Intent intent = new Intent(context.getApplicationContext(), PreviewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("KEY",PDFConstants.BULK_DELIVERY_NOTE_PDF_NAME);
            context.startActivity(intent);
        }
    }
    //Pre pare pdf
    private void preparePDF() {
        parentTable = new PdfPTable(1);
        parentTable.setWidthPercentage(100);
        initFile();

        addHeaderLogo();
        addHeaderLabel();

        addDeliveryDetails();

        addProductsDetailsTableLables();
        addProductsDetailsTableValues();
        addSignature();
        addFooterToPdf();
        document.close();
    }

    private void addHeaderLogo() {

        try {
            document.add(PDFOperations.getInstance().
                    getHeadrLogoIOSCertificateLogo(context, document,activeDeliveryMainDO.companyCode));
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }


    //Create file and document

    private void initFile() {
        File file = new File(Util.getAppPath(context) + PDFConstants.BULK_DELIVERY_NOTE_PDF_NAME);
        if (file.exists()) {
            file.delete();
        }
        try {
            document = new Document();
            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));
            // Open to write
            document.open();
            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor(PDFConstants.AUTHOR);
            document.addCreator(PDFConstants.CREATOR);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void addHeaderLabel() {
        try {
            Font headerLabelFont = new Font(Font.FontFamily.COURIER, 11.0f, Font.BOLD, BaseColor.BLACK);
            Paragraph headerLabel = new Paragraph(activeDeliveryMainDO.companyDescription, headerLabelFont);

            headerLabel.setAlignment(Element.ALIGN_CENTER);
            headerLabel.setPaddingTop(10);
            document.add(headerLabel);

            Paragraph headerLabel1 = new Paragraph(PDFConstants.HEADER_2, headerLabelFont);
            headerLabel1.setAlignment(Element.ALIGN_CENTER);
            headerLabel1.setPaddingTop(10);
            document.add(headerLabel1);
            addEmptySpaceLine(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addDeliveryDetails() {
        try {

            PdfPTable headerParentTable = new PdfPTable(1);
            headerParentTable.setPaddingTop(10f);
            headerParentTable.setWidthPercentage(100);

            float[] columnWidths = {2.2f, 0.3f, 6, 1.5f, 0.3f, 3f};
            PdfPTable headerTable = new PdfPTable(columnWidths);
            headerTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellOne1 = new PdfPCell();
            cellOne1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellTwo = new PdfPCell();
            cellTwo.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree = new PdfPCell();
            cellThree.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree1 = new PdfPCell();
            cellThree1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellFour = new PdfPCell();
            cellFour.setBorder(Rectangle.NO_BORDER);


            cellOne.addElement(new Phrase(PDFConstants.D_R_NO, boldFont));
//            cellOne.addElement(new Phrase(PDFConstants.PAYMENT_TERM, boldFont));

            cellOne.addElement(new Phrase(PDFConstants.DELIVERY_TO, boldFont));
            cellOne.addElement(new Phrase(PDFConstants.ADDRESS, boldFont));

            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellOne1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));

            cellTwo.addElement(new Phrase(activeDeliveryMainDO.shipmentNumber, normalFont));
//            cellTwo.addElement(new Phrase(activeDeliveryMainDO.paymentTerm, normalFont));

            cellTwo.addElement(new Phrase(activeDeliveryMainDO.customerDescription, normalFont));
            cellTwo.addElement(new Phrase(getAddress(), normalFont));

            cellThree.addElement(new Phrase(PDFConstants.DATE, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.TIME, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_ID, boldFont));
            cellThree.addElement(new Phrase(PDFConstants.USER_NAME, boldFont));

            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));
            cellThree1.addElement(new Phrase(PDFConstants.COLOUMN, normalFont));

            if(activeDeliveryMainDO.createdDate.length()>0){
                String dMonth = activeDeliveryMainDO.createdDate.substring(4, 6);
                String dyear = activeDeliveryMainDO.createdDate.substring(0, 4);
                String dDate = activeDeliveryMainDO.createdDate.substring(Math.max(activeDeliveryMainDO.createdDate.length() - 2, 0));

                cellFour.addElement(new Phrase(dDate + "-" + dMonth + "-" + dyear, normalFont));
            }else {
                cellFour.addElement(new Phrase(CalendarUtils.getPDFDate(context), normalFont));

            }
            cellFour.addElement(new Phrase(activeDeliveryMainDO.createdTime, normalFont));
//            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, ""), normalFont));
//            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, ""), normalFont));
            cellFour.addElement(new Phrase(activeDeliveryMainDO.createUserID, normalFont));
            cellFour.addElement(new Phrase(activeDeliveryMainDO.createUserName, normalFont));
            headerTable.addCell(cellOne);
            headerTable.addCell(cellOne1);
            headerTable.addCell(cellTwo);
            headerTable.addCell(cellThree);
            headerTable.addCell(cellThree1);
            headerTable.addCell(cellFour);

            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.addElement(headerTable);
            headerParentTable.addCell(pdfPCell);
            document.add(headerParentTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addProductsDetailsTableLables() {
        try {
            float[] productColumnsWidth = {2.5f, 4, 3.5f, 3.5f, 3.5f};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setPaddingTop(10);
            productsTable.setWidthPercentage(100);

            PdfPCell cellOne = new PdfPCell();
            PdfPCell cellTwo = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            PdfPCell cellFour = new PdfPCell();
            PdfPCell cellFive = new PdfPCell();

            Paragraph pOne = new Paragraph(PDFConstants.SR_NO, boldFont);
            pOne.setAlignment(Element.ALIGN_CENTER);
            cellOne.addElement(pOne);

            Paragraph pTwo = new Paragraph(PDFConstants.ITEM, boldFont);
            pTwo.setAlignment(Element.ALIGN_LEFT);
            cellTwo.addElement(pTwo);

            Paragraph pThree = new Paragraph(PDFConstants.OPENING_READING, boldFont);
            pThree.setAlignment(Element.ALIGN_CENTER);
            cellThree.addElement(pThree);

            Paragraph pFour = new Paragraph(PDFConstants.ENDING_READING, boldFont);
            pFour.setAlignment(Element.ALIGN_CENTER);
            cellFour.addElement(pFour);

            Paragraph pFive = new Paragraph(PDFConstants.NET_QUANTITY, boldFont);
            pFive.setAlignment(Element.ALIGN_CENTER);
            cellFive.addElement(pFive);

            productsTable.addCell(cellOne);
            productsTable.addCell(cellTwo);
            productsTable.addCell(cellThree);
            productsTable.addCell(cellFour);
            productsTable.addCell(cellFive);

            document.add(productsTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addProductsDetailsTableValues() {
        try {
            float[] productColumnsWidth = {2.5f, 4, 3.5f, 3.5f, 3.5f};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            LogUtils.INSTANCE.info("activeDeliveryMainDO", activeDeliveryMainDO.activeDeliveryDOS.size() + "--->");

            if (activeDeliveryMainDO.activeDeliveryDOS != null && activeDeliveryMainDO.activeDeliveryDOS.size() > 0) {
                for (int i = 0; i < activeDeliveryMainDO.activeDeliveryDOS.size(); i++) {
                    ActiveDeliveryDO actDelDo = activeDeliveryMainDO.activeDeliveryDOS.get(i);
                    PdfPCell cellOne = new PdfPCell();
                    PdfPCell cellTwo = new PdfPCell();
                    PdfPCell cellThree = new PdfPCell();
                    PdfPCell cellFour = new PdfPCell();
                    PdfPCell cellFive = new PdfPCell();

                    cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prOne = new Paragraph("" + (i + 1), normalFont);
                    prOne.setAlignment(Element.ALIGN_CENTER);
                    cellOne.addElement(prOne);

                    cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cellTwo.addElement(new Phrase("" + actDelDo.productDescription, normalFont));

                    cellThree.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prThree = new Paragraph("" + actDelDo.openingQuantity , normalFont);
                    prThree.setAlignment(Element.ALIGN_CENTER);
                    cellThree.addElement(prThree);

                    cellFour.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prFour = new Paragraph("" + actDelDo.endingQuantity , normalFont);
                    prFour.setAlignment(Element.ALIGN_CENTER);
                    cellFour.addElement(prFour);

                    cellFive.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prFive = new Paragraph("" + actDelDo.orderedQuantity + " " + actDelDo.unit, normalFont);
                    prFive.setAlignment(Element.ALIGN_CENTER);
                    cellFive.addElement(prFive);


                    productsTable.addCell(cellOne);
                    productsTable.addCell(cellTwo);
                    productsTable.addCell(cellThree);
                    productsTable.addCell(cellFour);
                    productsTable.addCell(cellFive);

                }
                try {
                    document.add(productsTable);
                    PdfPTable noteTable = new PdfPTable(1);
                    noteTable.setWidthPercentage(100);

                    PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
                    String note = "";
                    if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
                        note = podDo.getNotes();
                    }
                    PdfPCell noteCell = new PdfPCell(new Phrase("Remarks : " + activeDeliveryMainDO.remarks, boldFont));
//                PdfPCell noteCell = new PdfPCell(new Phrase("AED : Eighteen Thousand Eight Hundred Forty Seven and 50/100 Only-", boldFont));
                    noteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    noteCell.setPadding(10);
                    noteTable.addCell(noteCell);

                    document.add(noteTable);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
//            addEmptySpaceLine(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addSignature() {
        try {
//
            PdfPTable custTable = new PdfPTable(1);
            custTable.setWidthPercentage(100);

            Image image = PDFOperations.getInstance().getSignatureFromFile(activeDeliveryMainDO.signature);
            if (image != null) {
                document.add(image);
            }

            PdfPCell pdfPCell = new PdfPCell(new Phrase("Customer's Signature:", normalFont));
            pdfPCell.setBorder(Rectangle.NO_BORDER);
            pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            custTable.addCell(pdfPCell);
            document.add(custTable);


        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }

    private void addFooterToPdf() {
        try {
            //document.add(PDFOperations.getInstance().addFooterImage(context, document));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addEmptySpaceLine(int noOfLines) {
        try {
            PdfPTable emptyTable = new PdfPTable(1);
            emptyTable.setWidthPercentage(100);
            for (int i = 0; i < noOfLines; i++) {
                PdfPCell emptyCell = new PdfPCell();
                emptyCell.setBorder(Rectangle.NO_BORDER);
                emptyCell.addElement(new Paragraph("\n"));
                emptyTable.addCell(emptyCell);
            }
            document.add(emptyTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private PdfPCell getBorderlessCell(String elementName, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(elementName, normalFont));
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    private void sendPDfewDoc() {
        String shipmentType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "");
        if (shipmentType.equalsIgnoreCase("Scheduled")) {
            ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(context).getActiveDeliveryMainDo(context);
            if(activeDeliverySavedDo.deliveryEmail.length()>0){
                sendpdfMail(activeDeliverySavedDo.deliveryEmail, activeDeliverySavedDo.customerDescription);
            }else {
                ((BaseActivity)context).showToast(context.getString(R.string.please_provide_email_address));
                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS");

            }

        }
        else {
            CustomerDo customerDo = StorageManager.getInstance(context).getCurrentSpotSalesCustomer(context);
            if(customerDo.deliveryEmail.length()>0){
                sendpdfMail(customerDo.deliveryEmail, customerDo.customerName);
            }else {
                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS");
                ((BaseActivity)context).showToast(context.getString(R.string.please_provide_email_address));
            }
        }
    }

    private void sendpdfMail(String email, String userName) {
        /*TBSMailBG.newBuilder(context, "CYL Delivert Note")
                .withUsername("online@brothersgas.ae")
                .withPassword("Cam52743@2018")
                .withMailto("tema.vansales@gmail.com")
//                .withMailto("anusha.v@tbs-systems.com")
                .withSubject("Delivery Note")
                .withBody("Dear " + userName + ", Please find the attached Delivery Note for your reference.")
                .withAttachments(Util.getAppPath(context) + "CYLDeliveryNote.pdf")

                .send();*/

        PDFOperations.getInstance().sendpdfMail(context,
                email,
                userName,
                PDFConstants.BULK_DELIVERY_NOTE_PDF_NAME, PDFConstants.CYL_NAME);
        ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS");


    }


    private String getAddress() {


        String street      = activeDeliveryMainDO.customerStreet;
        String landMark    = activeDeliveryMainDO.customerLandMark;
        String town        = activeDeliveryMainDO.customerTown;
        String postal      = activeDeliveryMainDO.customerPostalCode;
        String city        = activeDeliveryMainDO.customerCity;
        String countryName = activeDeliveryMainDO.countryName;

        String finalString = "";

        if (!TextUtils.isEmpty(street)) {
            finalString += street + ", ";
        }
        if (!TextUtils.isEmpty(landMark)) {
            finalString += landMark + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
//        if (!TextUtils.isEmpty(countryName)) {
//            finalString += countryName;
//        }

        return finalString;
    }
}
