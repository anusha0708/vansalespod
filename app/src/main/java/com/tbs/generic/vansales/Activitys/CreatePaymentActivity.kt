package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.tbs.generic.vansales.Adapters.BankListAdapter
import com.tbs.generic.vansales.Adapters.CreatePaymentAdapter
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.pdfs.ReceiptPDF
import com.tbs.generic.vansales.prints.PrinterConnection
import com.tbs.generic.vansales.prints.PrinterConstants
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.LogUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.io.*
import java.util.*


class CreatePaymentActivity : BaseActivity() {
    lateinit var customerDo: CustomerDo
    lateinit var activeDeliverySavedDo: ActiveDeliveryMainDO
    lateinit var shipmentType :String
    var customer = ""
    lateinit var etChequeNo: EditText
    lateinit var tvBankSelection: TextView
    lateinit var dialog: Dialog
    lateinit var bankDialog: Dialog
    lateinit var etAmount: EditText
    private var calender: Calendar = Calendar.getInstance()
    lateinit var podDo: PodDo
    var encImage = ""
    lateinit var createPDFpaymentDO: PaymentPdfDO
    private lateinit var llBankList: LinearLayout

    private lateinit var ivCapturedPic: ImageView
    private lateinit var ivCamera: ImageView
    private lateinit var llChequeDate: LinearLayout
    private lateinit var llChequeNo: LinearLayout
    private lateinit var llView: LinearLayout

    lateinit var btnPayments: Button
    public lateinit var tvSelection: TextView
    private lateinit var cheque: String
    private lateinit var bank: String
    private lateinit var amount: String
    private lateinit var tvCurrency: TextView
    private lateinit var tvSelectDate: TextView
    private lateinit var createPaymentDo: CreatePaymentDO
    private lateinit var unpaidMainDo: UnPaidInvoiceMainDO
    private lateinit var llCapture: LinearLayout
    private lateinit var siteListRequest: UnPaidInvoicesListRequest

    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    private var type: Int = 10
    private var paymentType: Int = 0

    private var fromId = 0
    private var totalAmount = ""

    private lateinit var bitmap: Bitmap
    private lateinit var chequeDate: String

    override fun initialize() {
        val llCategories = getLayoutInflater().inflate(R.layout.create_payments_screen, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
        disableMenuWithBackButton()
        initializeControls()
        totalAmount = preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_AMOUNT, "")

        activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")

        if (shipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
            customer = customerDo.customer
        } else {
            customer = activeDeliverySavedDo.customer
        }
        if (intent.hasExtra("Sales")) {
            fromId = intent!!.extras!!.getInt("Sales")
        }
        if (fromId == 1) {
            llView.setVisibility(View.GONE)
            etAmount.setText(totalAmount)

        } else {
            llView.setVisibility(View.VISIBLE)
            val data = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "")
            tvSelection.setText("" + data)
            etAmount.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_AMOUNT, ""))

        }
        podDo = StorageManager.getInstance(this).getDepartureData(this);
        if (!podDo.payment.equals("")) {

//            btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            btnPayments.setClickable(false)
//            btnPayments.setEnabled(false)

            var selectedId = tvSelection.text.toString().trim()


            var invoiceId = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID, "")
            if (invoiceId.length > 0 && selectedId.equals(invoiceId)) {
//                btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//                btnPayments.setClickable(false)
//                btnPayments.setEnabled(false)

            } else {
                btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
                btnPayments.setClickable(true)
                btnPayments.setEnabled(true)
            }
        } else {
            btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
            btnPayments.setClickable(true)
            btnPayments.setEnabled(true)
        }
//
//        if (!podDo.payment.equals("")) {
//
//            btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            btnPayments.setClickable(false)
//            btnPayments.setEnabled(false)
//            var selectedId= tvSelection.text.toString().trim()
//
//            var deliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID, "")
//            if(deliveryId.length>0&&selectedId.equals(deliveryId)){
//                btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//                btnPayments.setClickable(false)
//                btnPayments.setEnabled(false)
//
//            }else{
//                btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
//                btnPayments.setClickable(true)
//                btnPayments.setEnabled(true)
//            }
//        }else{
//            btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
//            btnPayments.setClickable(true)
//            btnPayments.setEnabled(true)
//        }


        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.gallary);
        cmonth = calender.get(Calendar.MONTH)
        cday = calender.get(Calendar.YEAR)
        cday = calender.get(Calendar.DAY_OF_MONTH)
        ivCamera.setOnClickListener {
            Util.preventTwoClick(it)
            try {
                val file = File(Environment.getExternalStorageDirectory(), "BrothersGas_" + System.currentTimeMillis().toString() + ".png");
                if (file.exists()) {
                    file.delete()
                }
                file.createNewFile()
                val camIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                val uri = Uri.fromFile(file)
                camIntent.putExtra("return-data", true)
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)

                    val camIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    AppConstants.file2 = File(Environment.getExternalStorageDirectory(), "vansales_" + System.currentTimeMillis().toString() + ".png")
                    val uri = Uri.fromFile(AppConstants.file2)
                    camIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)

                    startActivityForResult(camIntent, 111)
                } else {
                    val file = File(uri.path)
                    val photoUri = FileProvider.getUriForFile(applicationContext, applicationContext.packageName + ".fileprovider", file)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivityForResult(camIntent, 199)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }


        }
    }

    private fun selectPhotoFromCamera(requestCode: Int) {
        try {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                val camIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                var file = File(Environment.getExternalStorageDirectory(), "vansales_" + System.currentTimeMillis().toString() + ".png")
                val uri = Uri.fromFile(file)
                camIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
                camIntent.putExtra("return-data", true)
                startActivityForResult(camIntent, requestCode)
            } else {
//                val file = File(uri.getPath())
//                val photoUri = FileProvider.getUriForFile(applicationContext, applicationContext.packageName + ".fileprovider", file!!)
//                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)

                val dir = Environment.getExternalStorageDirectory().toString() + "/picFolder/"
                val newdir = File(dir)
                newdir.mkdirs()

                val file = dir + System.currentTimeMillis() + ".jpg"
                val newfile = File(file)
                try {
                    newfile.createNewFile()
                } catch (e: IOException) {
                }


                val outputFileUri = Uri.fromFile(newfile)

                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)

                startActivityForResult(cameraIntent, requestCode)

            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
//    try {
//        val file = File(Environment.getExternalStorageDirectory(), "BrothersGas_" + System.currentTimeMillis().toString() + ".png");
//        if (file.exists()) {
//            file.delete()
//        }
//        file.createNewFile()
//        val camIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        val uri = Uri.fromFile(file)
//        camIntent.putExtra("return-data", true)
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
//        } else {
//            val file = File(uri.path)
////                    val photoUri = FileProvider.getUriForFile(applicationContext, applicationContext.packageName + ".fileprovider", file)
//
//            val photoUri = FileProvider.getUriForFile(applicationContext, "com.tbs.generic.vansales.fileprovider", file)
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
//        }
//        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//        startActivityForResult(camIntent, 199)
//    } catch (e: Exception) {
//        e.stackTrace
//
//    }


    //    try
//    {
////                val ivCamera = findViewById<ImageView>(R.id.ivCamera)
//        val fis = FileInputStream(file)
//        val bitmap = BitmapFactory.decodeStream(fis)
//        val baos = ByteArrayOutputStream()
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
//        val bytes = baos.toByteArray()
//        val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
//        podDo.capturedImagesList = encImage;
////                ivCamera.setImageBitmap(bm)// in xml file remove tint if you want to display the image
//        StorageManager.getInstance(this).saveDepartureData(this, podDo)
//    }
//    catch (e:FileNotFoundException) {
//        e.printStackTrace()
//    }
//}
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 111 && resultCode == Activity.RESULT_OK) {

            if (data!!.data == null) {
                val fis = FileInputStream(AppConstants.file2)
                val bitmap = BitmapFactory.decodeStream(fis)
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val bytes = baos.toByteArray()
                val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                ivCapturedPic.setImageBitmap(bitmap)
                preferenceUtils.saveString(PreferenceUtils.CAPTURE, "CAPTURE")
                LogUtils.debug("PIC", encImage)
            } else {

                try {
                    val fis = FileInputStream(AppConstants.file2)
                    val bitmap = BitmapFactory.decodeStream(fis)
                    val baos = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    val bytes = baos.toByteArray()
                    val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                    ivCapturedPic.setImageBitmap(bitmap)
                    preferenceUtils.saveString(PreferenceUtils.CAPTURE, encImage)
//                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                    LogUtils.debug("PIC", encImage)

                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }

            }
        } else if (requestCode == 199 && resultCode == Activity.RESULT_OK) {
            if (data!!.data == null) {
                bitmap = data.extras?.get("data") as Bitmap
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 75, baos)
                val b = baos.toByteArray()
                val encImage = Base64.encodeToString(b, Base64.DEFAULT)
                preferenceUtils.saveString(PreferenceUtils.CAPTURE, encImage)

                ivCapturedPic.setImageBitmap(bitmap)
            } else {
                bitmap = MediaStore.Images.Media.getBitmap(contentResolver, data.data)
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 75, baos)
                val b = baos.toByteArray()
                val encImage = Base64.encodeToString(b, Base64.DEFAULT)
                preferenceUtils.saveString(PreferenceUtils.CAPTURE, encImage)

                ivCapturedPic.setImageBitmap(bitmap)
            }
        }
    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.payment)
        tvSelection = findViewById(R.id.tvSelection) as TextView
        llView = findViewById(R.id.llView) as LinearLayout
        tvBankSelection = findViewById(R.id.tvBankSelection) as TextView
        etChequeNo = findViewById(R.id.etChequeNo) as EditText
        etAmount = findViewById(R.id.etAmount) as EditText
        tvCurrency = findViewById(R.id.tvCurrency) as TextView
        tvSelectDate = findViewById(R.id.tvSelectDate) as TextView
        btnPayments = findViewById(R.id.btnPayments) as Button
        ivCapturedPic = findViewById(R.id.ivCapturedPic) as ImageView
        llBankList = findViewById(R.id.llBankList) as LinearLayout
        ivCamera = findViewById(R.id.ivCamera) as ImageView
        llChequeDate = findViewById(R.id.llChequeDate) as LinearLayout
        llChequeNo = findViewById(R.id.llChequeNo) as LinearLayout
        llCapture = findViewById(R.id.llCapture) as LinearLayout
        val yourRadioGroup = findViewById(R.id.radioGroup) as RadioGroup
        val rbCash = findViewById(R.id.rbCash) as RadioButton
        val rbCheque = findViewById(R.id.rbCheque) as RadioButton
        rbCash.isChecked = true
        clickOnCash(rbCash)
        ivCapturedPic.setOnClickListener {
            Util.preventTwoClick(it)
            if (bitmap != null) {
                showChequePreview(bitmap)
            }
        }
        paymentType = preferenceUtils.getIntFromPreference(PreferenceUtils.PAYMENT_TYPE, 0)
        var currency = preferenceUtils.getStringFromPreference(PreferenceUtils.CURRENCY, "")
        tvCurrency.setText("" + currency)
        if (paymentType == 1) {
            rbCash.setChecked(true)
            rbCheque.isClickable = false
            rbCheque.setEnabled(false)
            clickOnCash(rbCash)

        } else if (paymentType == 2) {
            rbCheque.setChecked(true)
            rbCash.isClickable = false
            rbCash.setEnabled(false)
            clickOnCheque(rbCheque)

        } else {
            rbCash.setChecked(true)
            rbCash.isClickable = true
            rbCash.setEnabled(true)
            rbCheque.isClickable = true
            rbCheque.setEnabled(true)
        }

        yourRadioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.rbCash -> {
                        clickOnCash(rbCash)
                    }
                    R.id.rbCheque -> {
                        clickOnCheque(rbCheque)
                    }

                }
            }
        })


        val chequeValue = preferenceUtils.getIntFromPreference(PreferenceUtils.CHEQUE_DATE, 0)
        val datePicker = DatePickerDialog(this@CreatePaymentActivity, datePickerListener, cyear, cmonth, cday)
        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        tvSelectDate.setOnClickListener {
            Util.preventTwoClick(it)
            if (!datePicker.isShowing) {
                val c1 = Calendar.getInstance()

                c1.set(Calendar.DAY_OF_MONTH, c1.get(Calendar.DAY_OF_MONTH))
                c1.add(Calendar.MONTH, -6)
                c1.set(Calendar.YEAR, c1.get(Calendar.YEAR))
                datePicker.getDatePicker().setMinDate(c1.time.time);
                datePicker.show()
            }
        }

        cheque = etChequeNo.text.toString().trim()
        bank = tvBankSelection.text.toString().trim()
        amount = etAmount.text.toString().trim()

        btnPayments.setOnClickListener {
            Util.preventTwoClick(it)
            amount = etAmount.text.toString().trim()
            cheque = etChequeNo.text.toString().trim()
            bank = tvBankSelection.text.toString().trim()
            val data = tvSelection.text.toString()
            if (type == 10) {
                when {
//                    data.isEmpty() -> showToast("please select invoice")
                    amount.isEmpty() -> showToast("please enter amount")
                    else -> customerAuthorization(0)
                }

            } else {
                val value = preferenceUtils.getStringFromPreference(PreferenceUtils.CAPTURE, "")

                when {

//                    data.isEmpty() -> showToast("please select invoice")
                    cheque.isEmpty() -> showToast("please enter cheque number")
                    chequeDate.isEmpty() -> showToast("please select date")
                    amount.isEmpty() -> showToast("please enter amount")
                    bank.isEmpty() -> showToast("please select bank")
                    value.isEmpty() -> showToast("please capture image")


                    else -> customerAuthorization(1)
                }

            }
        }

//        tvSelection.setOnClickListener {
//            selectInvoiceList()
//        }
        tvBankSelection.setOnClickListener {
            Util.preventTwoClick(it)
            selectBankList()
        }
    }

    private fun clickOnCash(rbCash: RadioButton) {
        type = 10
        rbCash.isChecked = true
        etChequeNo.isEnabled = false
        etChequeNo.isClickable = false
        llChequeDate.setVisibility(View.GONE)
        llChequeNo.setVisibility(View.GONE)
        llCapture.setVisibility(View.GONE)
        llBankList.setVisibility(View.GONE)

        etChequeNo.setText("")
        etChequeNo.setHint(getString(R.string.cheque_number))
        tvSelectDate.isEnabled = false
        tvSelectDate.isClickable = false
        tvSelectDate.setText("")
        chequeDate = ""
        tvSelectDate.setHint(getString(R.string.cheque_date))
        tvBankSelection.isEnabled = true
        tvBankSelection.isClickable = true
        tvBankSelection.setText("")
        tvBankSelection.setHint(getString(R.string.select_bank))
        ivCapturedPic.setBackgroundColor(resources.getColor(android.R.color.transparent))
        ivCapturedPic.setImageResource(R.drawable.gallary)
        ivCamera.isClickable = false
        ivCamera.isEnabled = false
        ivCapturedPic.isClickable = false
        ivCapturedPic.isEnabled = false
    }

    private fun customerAuthorization(type: Int) {

        if (type == 0) {
            customerAuthorization(customer, 25, ResultListner { `object`, isSuccess ->

                if (isSuccess) {
                    paymentCreation()
                } else {
                    showAlert(resources.getString(R.string.not_authorized))

                }
            })
        } else {
            customerAuthorization(customer, 26, ResultListner { `object`, isSuccess ->

                if (isSuccess) {
                    paymentCreation()
                } else {
                    showAlert(resources.getString(R.string.not_authorized))

                }
            })
        }
    }


    private fun clickOnCheque(rbCheque: RadioButton) {
        rbCheque.isChecked = true
        type = 20
        tvSelectDate.isEnabled = true
        tvSelectDate.isClickable = true
        tvSelectDate.setText("")
        chequeDate = ""
        tvSelectDate.setHint(getString(R.string.cheque_date))
        llChequeDate.setVisibility(View.VISIBLE)
        llCapture.setVisibility(View.VISIBLE)
        llBankList.setVisibility(View.VISIBLE)

        llChequeNo.setVisibility(View.VISIBLE)
        etChequeNo.isEnabled = true
        etChequeNo.isClickable = true
        etChequeNo.setText("")
        etChequeNo.setHint(getString(R.string.cheque_number))
        tvBankSelection.isEnabled = true
        tvBankSelection.isClickable = true
        tvBankSelection.setText("")
        tvBankSelection.setHint(getString(R.string.select_bank))
        ivCamera.isClickable = true
        ivCamera.isEnabled = true
        ivCapturedPic.isClickable = true
        ivCapturedPic.isEnabled = true
    }

    private fun showChequePreview(bitmap: Bitmap) {
        if (bitmap != null) {
            val dialog = Dialog(this, R.style.NewDialog)
            dialog.setContentView(R.layout.cheque_img_preview_dialog)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
            val tvCloseable = dialog.findViewById(R.id.tvClose) as TextView
            val ivChequePreview = dialog.findViewById(R.id.ivChequePreview) as ImageView

            ivChequePreview.setImageBitmap(bitmap)
            tvCloseable.setOnClickListener {
                Util.preventTwoClick(it)
                dialog.dismiss()
            }

            dialog.show()
        } else {
            showToast(getString(R.string.please_capture_pic_for_cheque))
        }
    }

    private val datePickerListener: DatePickerDialog.OnDateSetListener = OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        chequeDate = "" + cyear + monthString + dayString;
        tvSelectDate.setText("" + dayString + " - " + monthString + " - " + cyear)

    }

    private fun paymentCreation() {
        var payId = "";
        var id = tvSelection.text.toString()
        amount = etAmount.text.toString().trim()
        if (intent.hasExtra("SINGLE")) {
            payId = intent!!.extras!!.getString("SINGLE").toString()
        }
        if (payId.length > 0) {
            val siteListRequest = CreateSinglePaymentRequest(id, cheque, chequeDate, bank, java.lang.Double.parseDouble(amount), type, encImage, this@CreatePaymentActivity)
            siteListRequest.setOnResultListener { isError, createPaymentDO,msg ->
                hideLoader()
                if (createPaymentDO != null) {
                    createPaymentDo = createPaymentDO
                    if (isError) {
                        if (msg.isNotEmpty()) {
                            showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                        } else {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.error), false)

                        }
                    } else {
                        if (createPaymentDO.paymentNumber.length > 0) {
                            preferenceUtils.saveString(PreferenceUtils.PAYMENT_INVOICE_ID, createPaymentDO.invoiceNumber)
                            preferenceUtils.saveString(PreferenceUtils.PAYMENT_ID, createPaymentDO.paymentNumber)
//                            showAppCompatAlert("Success", "Payment Created - " + createPaymentDO.paymentNumber, "Ok", "", "SUCCESS", false)
                            showToast(getString(R.string.payment_created)+" - " + createPaymentDO.paymentNumber)

                            preparePaymentCreation();

                            podDo.payment = CalendarUtils.getCurrentDate(this);
                            StorageManager.getInstance(this).saveDepartureData(this, podDo)
                            btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnPayments.setClickable(false)
                            btnPayments.setEnabled(false)

                        } else if (createPaymentDO.message.length > 0) {
                            showAppCompatAlert(getString(R.string.info), " " + createPaymentDO.message, getString(R.string.ok), "", "", false)

                        } else {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.payment_not_created), getString(R.string.ok), "", getString(R.string.falied), false)

                        }
                        // Toast.makeText(this@CreatePaymentActivity, "Payment Created", Toast.LENGTH_SHORT).show()

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                    }
                } else {
                    if (msg.isNotEmpty()) {
                        showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.error), false)

                    }                }

            }

            siteListRequest.execute()
        } else {
            val siteListRequest = CreatePaymentRequest(id, cheque, chequeDate, bank, java.lang.Double.parseDouble(amount), type, encImage, this@CreatePaymentActivity)
            siteListRequest.setOnResultListener { isError, createPaymentDO,msg ->
                hideLoader()
                if (createPaymentDO != null) {
                    createPaymentDo = createPaymentDO
                    if (isError) {
                        if (msg.isNotEmpty()) {
                            showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                        } else {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.error), false)

                        }                    } else {
                        if (createPaymentDO.paymentNumber.length > 0) {
                            preferenceUtils.saveString(PreferenceUtils.PAYMENT_INVOICE_ID, createPaymentDO.invoiceNumber)
                            preferenceUtils.saveString(PreferenceUtils.PAYMENT_ID, createPaymentDO.paymentNumber)
//                            showAppCompatAlert("Success", "Payment Created - " + createPaymentDO.paymentNumber, "Ok", "", "SUCCESS", false)
                            showToast(getString(R.string.payment_created)+" - " + createPaymentDO.paymentNumber)
                            preparePaymentCreation();
                            podDo.payment = CalendarUtils.getCurrentDate(this);
                            StorageManager.getInstance(this).saveDepartureData(this, podDo)
                            btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnPayments.setClickable(false)
                            btnPayments.setEnabled(false)

                        } else if (createPaymentDO.message.length > 0) {
                            showAppCompatAlert(getString(R.string.info), " " + createPaymentDO.message, getString(R.string.ok), "", "", false)

                        } else {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.payment_not_created), getString(R.string.ok), "", getString(R.string.falied), false)

                        }
                        // Toast.makeText(this@CreatePaymentActivity, "Payment Created", Toast.LENGTH_SHORT).show()

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                    }
                } else {
                    if (msg.isNotEmpty()) {
                        showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.error), false)

                    }                }

            }

            siteListRequest.execute()
        }

    }


    private fun selectInvoiceList() {
        dialog = Dialog(this, R.style.NewDialog)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.payment_list_dialog)
        val window = dialog.getWindow()
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        }
        var tvInvoiceId = dialog.findViewById(R.id.tvInvoiceId) as TextView
        var tvNoData = dialog.findViewById(R.id.tvNoDataFound) as TextView
        var tvAmount = dialog.findViewById(R.id.tvAmount) as TextView

        var data = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "")
        if (data.isEmpty()) {
            tvInvoiceId.setVisibility(View.GONE)
        } else {
            tvInvoiceId.setVisibility(View.VISIBLE)

        }
        tvInvoiceId.setText("" + data)

        tvInvoiceId.setOnClickListener({
            preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, tvInvoiceId.text.toString())
            tvSelection.text = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "")
            dialog.dismiss()
        })
        var recyclerView = dialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));
        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
        if (Util.isNetworkAvailable(this)) {

            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType == getResources().getString(R.string.checkin_non_scheduled)) {

                siteListRequest = UnPaidInvoicesListRequest(customerDo.customer, this@CreatePaymentActivity)
            } else {
                siteListRequest = UnPaidInvoicesListRequest(activeDeliverySavedDo.customer, this@CreatePaymentActivity)
            }
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
                    if (data.isEmpty()) {
                        tvInvoiceId.setVisibility(View.GONE)
                        tvAmount.setVisibility(View.GONE)

                    } else {
                        tvInvoiceId.setVisibility(View.VISIBLE)
                        tvAmount.setVisibility(View.VISIBLE)
                        tvAmount.setText(getString(R.string.total_outstanding_amount)+" " + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)

                    }

                    tvNoData.setVisibility(View.GONE)
                    recyclerView.setVisibility(View.VISIBLE)
                    unpaidMainDo = unPaidInvoiceMainDO
                    if (isError) {
                        Toast.makeText(this@CreatePaymentActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        tvAmount.setVisibility(View.VISIBLE)

                        var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, unpaidMainDo.unPaidInvoiceDOS)
                        recyclerView.setAdapter(siteAdapter)
                        tvAmount.setText(getString(R.string.total_outstanding_amount)+" "+ String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency)

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)f

                    }
                } else {
                    tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.setVisibility(View.GONE)

                    tvNoData.setVisibility(View.VISIBLE)
                    recyclerView.setVisibility(View.GONE)
                    hideLoader()

                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

        }




        dialog.show()

    }

    fun selectBankList() {
        bankDialog = Dialog(this, R.style.NewDialog)
        bankDialog.setCancelable(true)
        bankDialog.setCanceledOnTouchOutside(true)
        bankDialog.setContentView(R.layout.simple_list_dialog)
        val window = bankDialog.getWindow()
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        }
        var data = preferenceUtils.getStringFromPreference(PreferenceUtils.Reason_Payment, "")
//            tvInvoiceId.setText("" + data)
//            tvInvoiceId.setOnClickListener({
//                preferenceUtils.saveString(PreferenceUtils.SITE_NAME, tvInvoiceId.text.toString())
//                tvSelection.text = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SITE_NAME, "")
//                dialog.dismiss()
//            })
        var site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")

        var recyclerView = bankDialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this));
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = BankListRequest(site, this@CreatePaymentActivity)
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null) {
                    if (isError) {
                        bankDialog.dismiss()
                        showToast(getString(R.string.server_error))

                    } else {
                        if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.bankDOS.size > 0) {
                            var siteAdapter = BankListAdapter(this@CreatePaymentActivity, unPaidInvoiceMainDO.bankDOS, 3)
                            recyclerView.setAdapter(siteAdapter)
                        } else {
                            showToast(getString(R.string.no_banks_found))
                            bankDialog.dismiss()
                        }
                    }
                } else {
                    hideLoader()
                    bankDialog.dismiss()
                    showToast(getString(R.string.server_error))                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }




        bankDialog.show()

    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            finish()
        }
    }

    override fun onButtonNoClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            finish()
        }
    }

    private fun preparePaymentCreation() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
//        id = "SD-U101-19000886";
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = PDFPaymentDetailsRequest(id, this);
                val listener = PDFPaymentDetailsRequest.OnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {

                        if (isError) {
                            finish()
                            showToast(getString(R.string.unable_to_send_email))

//                        showAppCompatAlert("Error", "Unable to send Email", "Ok", "", "", false)
                        } else {
                            createPDFpaymentDO = createPDFInvoiceDO
                            if (createPaymentDo.email == 10) {
                                createPaymentPDF(createPDFInvoiceDO)
                            }
//                            if (createPaymentDo.print == 10) {
//                                printDocument(createPDFpaymentDO, PrinterConstants.PrintPaymentReport)
//                            }

//                        if(from.equals("Print", true)){
//                        }
//                        else{
//                        }
                        }
                    } else {
                        showToast(getString(R.string.unable_to_send_email))
                    }
                    val intent = Intent()
                    setResult(1, intent)
                    finish()
                }
                siteListRequest.setOnResultListener(listener)
                siteListRequest.execute()
            } else {
                showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

            }


        } else {
            showToast(getString(R.string.unable_to_send_email))

        }

    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@CreatePaymentActivity)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast(getString(R.string.please_enable_your_bluetooth))
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        AppConstants.file2 = null

        super.onDestroy()

    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun createPaymentPDF(createPDFInvoiceDO: PaymentPdfDO) {
        if (createPDFInvoiceDO != null) {
            ReceiptPDF(this).createReceiptPDF(createPDFInvoiceDO, "Email")

            return

        } else {
            //Display error message
        }
    }

    override fun onBackPressed() {

        val intent = Intent()
        setResult(1, intent)
        finish()
        super.onBackPressed()

    }


}