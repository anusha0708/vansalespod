package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.OnResultListener;
import com.tbs.generic.vansales.Model.CylinderIssueDO;
import com.tbs.generic.vansales.Model.CylinderIssueMainDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CylinderIssueRequest extends AsyncTask<String, Void, Boolean> {

    private CylinderIssueMainDO cylinderIssueMainDO;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    CylinderIssueDO cylinderIssueDO;

    public CylinderIssueRequest(String id, Context mContext) {

        this.mContext = mContext;
        this.id = id;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_XSDHNUM", id);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.CYLINDER_ISSUE_REQUEST, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }


    }


    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            cylinderIssueMainDO = new CylinderIssueMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        cylinderIssueMainDO.cylinderIssueDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        cylinderIssueDO = new CylinderIssueDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("I_XSDHNUM")) {
                            if (text.length() > 0) {
                                cylinderIssueMainDO.shipmentNumber = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITE")) {
                            cylinderIssueMainDO.site = text;

                        } else if (attribute.equalsIgnoreCase("O_XSITEDES")) {
                            cylinderIssueMainDO.siteDescription = text;
                        }

                        else if (attribute.equalsIgnoreCase("O_XSITADDLIG0")) {
                            cylinderIssueMainDO.siteAddress1 = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XSITADDLIG1")) {
                            cylinderIssueMainDO.siteAddress2 = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_XSITADDLIG2")) {
                            cylinderIssueMainDO.siteAddress3 = text;

                        }

                        else if (attribute.equalsIgnoreCase("O_XBPCNUM")) {
                            cylinderIssueMainDO.customer = text;

                        } else if (attribute.equalsIgnoreCase("O_XBPCSHO")) {
                            cylinderIssueMainDO.customerDescription = text;
                        }

                       else if (attribute.equalsIgnoreCase("O_XCUSCRY")) {
                            cylinderIssueMainDO.customercountryName = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_XBPADDLIG0")) {


                            cylinderIssueMainDO.customerStreet = text;
                        } else if (attribute.equalsIgnoreCase("O_XBPADDLIG1")) {


                            cylinderIssueMainDO.customerLandMark = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XCPYDES")) {


                            cylinderIssueMainDO.company = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XCPYNAM")) {


                            cylinderIssueMainDO.companyCode = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XBPADDLIG2")) {


                            cylinderIssueMainDO.customerTown = text;
                        } else if (attribute.equalsIgnoreCase("O_XCUSCTY")) {


                            cylinderIssueMainDO.customerCity = text;
                        } else if (attribute.equalsIgnoreCase("O_XCUSPOS")) {


                            cylinderIssueMainDO.customerPostalCode = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XCUSLD")) {
                            cylinderIssueMainDO.landLine = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_XCUSMOB")) {
                            cylinderIssueMainDO.mobile = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_XCUSFAX")) {
                            cylinderIssueMainDO.fax = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_XCUSEML1")) {
                            cylinderIssueMainDO.email = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XCUSEML2")) {
                            cylinderIssueMainDO.email2 = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XCPYIMG")) {
                            if(text.length()>0){
                                cylinderIssueMainDO.logo = text;

                            }
                        }


                    else if (attribute.equalsIgnoreCase("O_XSITCRY")) {
                            if(text.length()>0){
                                cylinderIssueMainDO.siteCountry = text;

                            }
                        }else if (attribute.equalsIgnoreCase("O_XSITCTY")) {
                            if(text.length()>0){
                                cylinderIssueMainDO.siteCity = text;

                            }
                        }else if (attribute.equalsIgnoreCase("O_XSITPOS")) {
                            if(text.length()>0){
                                cylinderIssueMainDO.sitePostalCode = text;

                            }
                        }else if (attribute.equalsIgnoreCase("O_XSITLND")) {
                            if(text.length()>0){
                                cylinderIssueMainDO.siteLandLine = text;

                            }
                        }else if (attribute.equalsIgnoreCase("O_XSITMOB")) {
                            if(text.length()>0){
                                cylinderIssueMainDO.siteMobile = text;

                            }
                        }else if (attribute.equalsIgnoreCase("O_XSITFAX")) {
                            if(text.length()>0){
                                cylinderIssueMainDO.siteFax = text;

                            }
                        }else if (attribute.equalsIgnoreCase("O_XSITEML1")) {
                            if(text.length()>0){
                                cylinderIssueMainDO.siteEmail1 = text;

                            }
                        }else if (attribute.equalsIgnoreCase("O_XSITEML2")) {
                            if(text.length()>0){
                                cylinderIssueMainDO.siteEmail2 = text;

                            }
                        }else if (attribute.equalsIgnoreCase("O_XSITWEB")) {
                            if(text.length()>0){
                                cylinderIssueMainDO.siteWebEmail = text;

                            }
                        }

                        else if (attribute.equalsIgnoreCase("O_XSITWEB")) {
                            cylinderIssueMainDO.webSite = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XITM")) {
                            cylinderIssueDO.product = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XITMDES")) {
                            cylinderIssueDO.productDescription = text;
                        }

                        else if (attribute.equalsIgnoreCase("O_XOQTY")) {
                            cylinderIssueDO.orderedQuantity = Integer.parseInt(text);
                        }
                        else if (attribute.equalsIgnoreCase("O_XOUNI")) {
                            cylinderIssueDO.orderedQuantityunit = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XISQTY")) {
                            cylinderIssueDO.issuedQuantity = Integer.parseInt(text);
                        }
                        else if (attribute.equalsIgnoreCase("O_XISUNI")) {
                            cylinderIssueDO.issuedQuantityunit = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XRCVDQTY")) {
                            cylinderIssueDO.recievedQuantity = Integer.parseInt(text);
                        }
                        else if (attribute.equalsIgnoreCase("O_XRCVDUN")) {
                            cylinderIssueDO.recievedQuantityunit = text;
                        }
                        else if (attribute.equalsIgnoreCase("O_XBALQTY")) {
                            cylinderIssueDO.balanceQuantity = Integer.parseInt(text);
                        }
                        else if (attribute.equalsIgnoreCase("O_XBALUN")) {
                            cylinderIssueDO.balanceQuantityunit = text;
                        }

                        text = "";


                    }


                    if (endTag.equalsIgnoreCase("GRP3")) {
                        //   customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        cylinderIssueMainDO.cylinderIssueDOS.add(cylinderIssueDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {

                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, cylinderIssueMainDO);
        }
    }
}