//package com.tbs.generic.vansales.Activitys;
//
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.tbs.generic.vansales.Adapters.SiteAdapter;
//import com.tbs.generic.vansales.Model.SiteDO;
//import com.tbs.generic.vansales.R;
//import com.tbs.generic.vansales.Requests.SiteListRequest;
//import com.tbs.generic.vansales.database.StorageManager;
//
//import java.util.ArrayList;
//
//public class SiteActivity extends BaseActivity {
//    private RecyclerView recycleview;
//    private SiteAdapter siteAdapter;
//    private LinearLayout llOrderHistory;
//    private TextView tvScreenTitles, tvNoItems;
//    private ArrayList<SiteDO> siteDOS;
//    private LinearLayout llSearch;
//    private EditText etSearch;
//    private ImageView ivClearSearch, ivSearch, ivGoBack;
//
//
//
//    @Override
//    public void initialize() {
//        llOrderHistory = (LinearLayout) getLayoutInflater().inflate(R.layout.site_screen, null);
//        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        flToolbar.setVisibility(View.GONE);
//        changeLocale();
//        disableMenuWithBackButton();
//        initializeControls();
//        tvScreenTitles.setText(R.string.site_list);
//        tvScreenTitles.setVisibility(View.VISIBLE);
//        recycleview.setLayoutManager(new LinearLayoutManager(this));
//        siteDOS = StorageManager.getInstance(SiteActivity.this).getSiteListData(SiteActivity.this);
//        loadData(siteDOS);
//
//        ivGoBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//        ivSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                if(llSearch.visibility == View.VISIBLE){
////                llSearch.visibility = View.INVISIBLE
////            }
////            else{
////            }
//                tvScreenTitles.setVisibility(View.GONE);
//                llSearch.setVisibility(View.VISIBLE);
//            }
//        });
//
//        ivClearSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                etSearch.setText("");
//                if(siteDOS!=null &&siteDOS.size()>0){
//                    siteAdapter= new SiteAdapter(SiteActivity.this, siteDOS);
//                    recycleview.setAdapter(siteAdapter);
//                    tvNoItems.setVisibility(View.GONE);
//                    recycleview.setVisibility(View.VISIBLE);
//                }
//                else{
//                    tvNoItems.setVisibility(View.VISIBLE);
//                    recycleview.setVisibility(View.GONE);
//                }
//
//            }
//        });
//
//        etSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if(etSearch.getText().toString().trim().equalsIgnoreCase("")){
//                    if(siteDOS!=null &&siteDOS.size()>0){
//                        siteAdapter= new SiteAdapter(SiteActivity.this, siteDOS);
//                        recycleview.setAdapter(siteAdapter);
//                        tvNoItems.setVisibility(View.GONE);
//                        recycleview.setVisibility(View.VISIBLE);
//                    }
//                    else{
//                        tvNoItems.setVisibility(View.VISIBLE);
//                        recycleview.setVisibility(View.GONE);
//                    }
//                }
//                else if(etSearch.getText().toString().trim().length()>2){
//                    filter(etSearch.getText().toString().trim());
//                }
//            }
//        });
//
//        SiteListRequest siteListRequest = new SiteListRequest(SiteActivity.this);
//        showLoader();
//        siteListRequest.setOnResultListener(new SiteListRequest.OnResultListener() {
//            @Override
//            public void onCompleted(boolean isError, ArrayList<SiteDO> siteDoS) {
//                hideLoader();
//                siteDOS =siteDoS;
//                if (isError) {
//                    Toast.makeText(SiteActivity.this, R.string.error_site_list, Toast.LENGTH_SHORT).show();
//                } else {
//                    loadData(siteDOS);
//                    saveSiteListData(siteDOS);
//                }
//            }
//        });
//        siteListRequest.execute();
//    }
//
//    private ArrayList<SiteDO> filter(String filtered) {
//        ArrayList<SiteDO> siteDOs = new ArrayList();
//        for (int i=0; i<siteDOS.size(); i++){
//            if(siteDOS.get(i).siteName.contains(filtered) || siteDOS.get(i).siteId.contains(filtered)){
//                siteDOs.add(siteDOS.get(i));
//            }
//        }
//        if(siteDOs.size()>0){
//            siteAdapter.refreshAdapter(siteDOs);
//            tvNoItems.setVisibility(View.GONE);
//            recycleview.setVisibility(View.VISIBLE);
//        }
//        else{
//            tvNoItems.setVisibility(View.VISIBLE);
//            recycleview.setVisibility(View.GONE);
//        }
//        return siteDOs;
//    }
//
//    private void loadData(ArrayList<SiteDO> siteDOS){
//        if(siteDOS!=null && siteDOS.size()>0){
//            recycleview.setVisibility(View.VISIBLE);
//            tvNoItems.setVisibility(View.GONE);
//            siteAdapter= new SiteAdapter(SiteActivity.this,siteDOS);
//            recycleview.setAdapter(siteAdapter);
//        }
//        else {
//            recycleview.setVisibility(View.GONE);
//            tvNoItems.setVisibility(View.VISIBLE);
//        }
//    }
//
//    private void saveSiteListData(ArrayList<SiteDO> siteDOS){
//        StorageManager.getInstance(SiteActivity.this).saveSiteListData(SiteActivity.this, siteDOS);
//    }
//    @Override
//    public void initializeControls() {
//        recycleview             = findViewById(R.id.recycleview);
//        tvNoItems               = findViewById(R.id.tvNoOrders);
//        tvScreenTitles           = findViewById(R.id.tvScreenTitles);
//        llSearch                = findViewById(R.id.llSearch);
//        etSearch                = findViewById(R.id.etSearch);
//        ivClearSearch           = findViewById(R.id.ivClearSearch);
//        ivGoBack                = findViewById(R.id.ivGoBack);
//        ivSearch                = findViewById(R.id.ivSearchs);
//    }
//
//}
