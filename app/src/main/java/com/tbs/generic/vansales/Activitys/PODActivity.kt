@file:Suppress("NAME_SHADOWING")

package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.content.Intent
import android.text.Html
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.RelativeLayout
import com.tbs.generic.vansales.Adapters.VehicleRouteAdapter
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO
import com.tbs.generic.vansales.Model.CustomerDo
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.PodDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.DeliveryRoutingIdRequest
import com.tbs.generic.vansales.Requests.ForceSkipRequest
import com.tbs.generic.vansales.Requests.NonScheduledLoadVanSaleRequest
import com.tbs.generic.vansales.Requests.PODDepartureTimeCaptureRequest
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.LogUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.pod_screen_1.*
import java.util.*

class PODActivity : BaseActivity() {

    lateinit var podDo: PodDo
    lateinit var btnConfirmDeparture: LinearLayout
    lateinit var btnConfirmArrival: LinearLayout
    lateinit var btnDeliveryDetails: LinearLayout
    lateinit var btnGenerate: LinearLayout
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()
    var docType = 0
    var pickupFlag = 0
    lateinit var btnStartUnloading: LinearLayout
    lateinit var btnEndUnloading: LinearLayout
    lateinit var customerDo: CustomerDo
    lateinit var shipmentId: String
    lateinit var shipmentType: String
    lateinit var customerName: String
    lateinit var activeDeliverySavedDo: ActiveDeliveryMainDO

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.pod_screen_1, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        disableMenuWithBackButton()
        initializeControls()
        tvScreenTitle.setText("POD")
        docType = preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0)
        pickupFlag = preferenceUtils.getIntFromPreference(PreferenceUtils.PICKUP_DROP_FLAG, 0)

        podDo = StorageManager.getInstance(this).getDepartureData(this);
        if (pickupFlag == 1) {
            tvScreenTitle.setText("POC")

            tvCustomerLabel.setText(resources.getString(R.string.supplier))
            tvStartLoadingDateTime.setText(resources.getString(R.string.startload_pickup))
            tvEndUnLoadingDateTime.setText(resources.getString(R.string.endload_pickup))
            //pickup
        } else {
            //drop
            tvScreenTitle.setText("POD")

            tvCustomerLabel.setText(resources.getString(R.string.customer))

            tvStartLoadingDateTime.setText(resources.getString(R.string.startload_drop))
            tvEndUnLoadingDateTime.setText(resources.getString(R.string.endload_drop))
        }
        if (docType == 1) {
            tvLoadStock.setText(resources.getString(R.string.delivery_details_pod))
            llInvPayment.visibility = View.VISIBLE
            ivCustomerLocation.visibility = View.VISIBLE
        } else if (docType == 2) {
            tvLoadStock.setText(resources.getString(R.string.receipt_details_pod))
            llInvPayment.visibility = View.GONE
            ivCustomerLocation.visibility = View.VISIBLE
        } else if (docType == 3) {
            tvLoadStock.setText(resources.getString(R.string.retun_details_pod))
            llInvPayment.visibility = View.GONE
            ivCustomerLocation.visibility = View.VISIBLE
        } else if (docType == 4) {
            tvLoadStock.setText(resources.getString(R.string.pick_ticket_pod))
            llInvPayment.visibility = View.VISIBLE
            ivCustomerLocation.visibility = View.VISIBLE
        } else if (docType == 5) {
            tvLoadStock.setText(resources.getString(R.string.retun_details_pod))
            llInvPayment.visibility = View.GONE
            ivCustomerLocation.visibility = View.VISIBLE
        } else if (docType == 6) {
            tvLoadStock.setText(resources.getString(R.string.misc_details_pod))
            llInvPayment.visibility = View.GONE
            ivCustomerLocation.visibility = View.VISIBLE
        } else {
            tvLoadStock.setText(resources.getString(R.string.delivery_details_pod))
            llInvPayment.visibility = View.VISIBLE
            ivCustomerLocation.visibility = View.VISIBLE
        }

        shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        tvScreenTitle.visibility = View.VISIBLE
        var etaETD = preferenceUtils.getStringFromPreference(PreferenceUtils.ETA_ETD, "")
        if (etaETD.isNotEmpty()) {
            tvETAETD.setText("" + etaETD)
            tvETAETD.visibility = View.VISIBLE
        } else {
            tvETAETD.visibility = View.GONE
        }

        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
        customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
        activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        tvW8Unit.setText(activeDeliverySavedDo.totalWeight.toString()+" "+activeDeliverySavedDo.weightUnit)
        tvVolumeUnit.setText(activeDeliverySavedDo.totalVolume.toString()+" "+activeDeliverySavedDo.volumeUnit)

        if (shipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
            customerName = customerDo.customerName
            if (customerName.isNotEmpty()) {
                tvCustomer.visibility = View.VISIBLE
                tvCustomer.setText(customerDo.customer + " \n" +  customerDo.customerName)
            }
//            loadNonVehicleStockData()
            llWeight.visibility=View.GONE
        } else {
            llWeight.visibility=View.VISIBLE

            llDocument.visibility = View.VISIBLE
            tvDocument.setText(activeDeliverySavedDo.shipmentNumber)
            customerName = activeDeliverySavedDo.customerDescription
            if (customerName.isNotEmpty()) {
                tvCustomer.visibility = View.VISIBLE
                tvCustomer.setText(activeDeliverySavedDo.customer + " \n" + activeDeliverySavedDo.customerDescription)
            }
        }
//
        podDo = StorageManager.getInstance(this).getDepartureData(this);
        if (podDo != null) {
            if (!podDo.arrivalTime.equals("")) {
                btnConfirmArrival.setBackgroundResource(R.drawable.card_white_green_boarder)
                btnConfirmArrival.setClickable(false)
                btnConfirmArrival.setEnabled(false)
                v1.setBackgroundResource(R.drawable.done_tick_green)
                tvConfirmArrivalDT.visibility = View.VISIBLE
                tvConfirmArrivalDT.setText(Html.fromHtml(resources.getString(R.string.arrival) + podDo.arrivalTime))
            }
            if (!podDo.podTimeCaptureStartLoadingDate.equals("")) {
                btnStartUnloading.setBackgroundResource(R.drawable.card_white_green_boarder)
                btnStartUnloading.setClickable(false)
                btnStartUnloading.setEnabled(false)
                v2.setBackgroundResource(R.drawable.done_tick_green)
                tvStartLoadingDT.visibility = View.VISIBLE
                tvStartLoadingDT.setText(Html.fromHtml(resources.getString(R.string.startload) + podDo.unLoadingTime))
            }
            if (!podDo.podTimeCaptureEndLoadingDate.equals("")) {
                btnEndUnloading.setBackgroundResource(R.drawable.card_white_green_boarder)
                btnEndUnloading.setClickable(false)
                btnEndUnloading.setEnabled(false)
                v4.setBackgroundResource(R.drawable.done_tick_green)

                tvEndUnloadDT.visibility = View.VISIBLE
                tvEndUnloadDT.setText(Html.fromHtml(resources.getString(R.string.endload) + podDo.endUnLoadingTime))
            }
            if (!podDo.capturedProductListTime.equals("")) {
                btnDeliveryDetails.setBackgroundResource(R.drawable.card_white_green_boarder)
                btnDeliveryDetails.setClickable(false)
                btnDeliveryDetails.setEnabled(false)
                tvDeliveryDT.visibility = View.VISIBLE
                tvDeliveryDT.setText(Html.fromHtml(resources.getString(R.string.delivery_details) + podDo.capturedProductListTime))
                saveDepartureData()
                v3.setBackgroundResource(R.drawable.done_tick_green)
            }
        } else {
            podDo = PodDo();
        }
        if(pickupFlag==1){
            btnType.setBackground(resources.getDrawable(R.drawable.pick_48))
            ivendload.setBackground(resources.getDrawable(R.drawable.endload48))
            ivstartload.setBackground(resources.getDrawable(R.drawable.load_48))

        } else {
            btnType.setBackground(resources.getDrawable(R.drawable.drop_48))
            ivendload.setBackground(resources.getDrawable(R.drawable.endunload_48))
            ivstartload.setBackground(resources.getDrawable(R.drawable.unload48))
        }
        btnConfirmDeparture.setOnClickListener {
            Util.preventTwoClick(it)
            if (shipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                if (podDo.arrivalTime.equals("")) {
                    showToast(resources.getString(R.string.check_arrival))
                } else if (podDo.unLoadingTime!!.isNotEmpty() && podDo.capturedProductListTime!!.isEmpty()) {
                    showToast(resources.getString(R.string.check_document))

                } else if (podDo.capturedProductListTime!!.isNotEmpty() && podDo.endUnLoadingTime!!.isEmpty()) {
                    showToast(resources.getString(R.string.check_endload))

                } else {
                    showAppCompatAlert("", resources.getString(R.string.departurelabel) + " " + customerName + " on " + CalendarUtils.getCurrentDate(this) + " ?", resources.getString(R.string.confirm), resources.getString(R.string.hint_cancel), resources.getString(R.string.departure_from), false)

                }
            } else {
                if (podDo.arrivalTime.equals("")) {
                    showToast(resources.getString(R.string.check_arrival))
                } else if (podDo.capturedProductListTime.equals("", true)) {
                    showToast(resources.getString(R.string.check_document))
                } else if (podDo.podTimeCaptureEndLoadingDate.equals("", true)) {
                    showToast(resources.getString(R.string.check_endload))
                } else {
                    showAppCompatAlert("", resources.getString(R.string.departure_confirm) + " " + customerName + " on " + CalendarUtils.getCurrentDate(this) + " ?", resources.getString(R.string.confirm), resources.getString(R.string.hint_cancel), resources.getString(R.string.departure_from), false)
                }
            }
        }
        btnConfirmArrival.setOnClickListener {
            Util.preventTwoClick(it)
            clickOnConfirmArrival(resources.getString(R.string.confrmlabel) + " "+CalendarUtils.getCurrentDate(this) + " ?")
        }
        btnStartUnloading.setOnClickListener {
            Util.preventTwoClick(it)
            if (pickupFlag == 1) {
                clickOnStartUnloading(resources.getString(R.string.startloadonlabel)+" "+ CalendarUtils.getCurrentDate(this) + " ?")
                //pickup
            } else  {
                //drop
                clickOnStartUnloading(resources.getString(R.string.startunloadonlabel)+" "+ CalendarUtils.getCurrentDate(this) + " ?")

            }
        }
        btnEndUnloading.setOnClickListener {
            Util.preventTwoClick(it)
            if (pickupFlag == 1) {
                clickOnEndUnloading(resources.getString(R.string.endloadonlabel)+ " "+CalendarUtils.getCurrentDate(this) + " ?")
                //pickup
            } else  {
                //drop
                clickOnEndUnloading(resources.getString(R.string.endunloadonlabel)+ " "+CalendarUtils.getCurrentDate(this) + " ?")

            }
        }
        btnGenerate.setOnClickListener {
            Util.preventTwoClick(it)
            if (podDo.arrivalTime.equals("")) {
                showToast(resources.getString(R.string.check_arrival))
            } else {
                val intent = Intent(this@PODActivity, PaymentsActivity::class.java);
                startActivityForResult(intent, 2);
            }
        }
        llnoteFab.setOnClickListener {
            var intent = Intent(this@PODActivity, PODNotesActivity::class.java);
            startActivityForResult(intent, 140);
        }
        ivCustomerLocation.setOnClickListener {
            Util.preventTwoClick(it)
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT)
            } else {
                var shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
                val popup = PopupMenu(this@PODActivity, ivCustomerLocation)
                if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                   if(docType==1){
                       popup.getMenuInflater().inflate(R.menu.pod_scheduled_menu, popup.getMenu())

                   }else{
                       popup.getMenuInflater().inflate(R.menu.pod_menu, popup.getMenu())

                   }
                } else {
                    popup.getMenuInflater().inflate(R.menu.non_schedule_damage_menu, popup.getMenu())
                }
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        if (item.title.equals(getString(R.string.sales_return))) {
                            if (podDo.arrivalTime.equals("")) {
                                showToast(resources.getString(R.string.check_arrival))
                            } else {
                                val intent = Intent(this@PODActivity, CaptureSalesReturnDetailsActivity::class.java);
                                startActivityForResult(intent, 11);
                            }
                        } else if (item.title.equals(resources.getString(R.string.menu_del_remarks))) {
                            if (podDo.arrivalTime.equals("")) {
                                showToast(resources.getString(R.string.check_arrival))
                            } else {
                                val intent = Intent(this@PODActivity, DeliveryNotesActivity::class.java);
                                startActivityForResult(intent, 120)
                            }
                        } else if (item.title.equals(resources.getString(R.string.menu_inv_remarks))) {
                            if (podDo.arrivalTime.equals("")) {
                                showToast(resources.getString(R.string.check_arrival))
                            } else {
                                val intent = Intent(this@PODActivity, InvoiceNotesActivity::class.java);
                                startActivityForResult(intent, 130)
                            }
                        } else if (item.title.equals(resources.getString(R.string.menu_cancel))) {

                            if (podDo.arrivalTime.equals("")) {
                                showToast(resources.getString(R.string.check_arrival))
                            } else if (!podDo.capturedProductListTime.equals("")) {
                                showToast(resources.getString(R.string.cant_use))
                            } else {
                                val intent = Intent(this@PODActivity, Cancel_RescheduleActivity::class.java);
                                startActivityForResult(intent, 511);
                            }

                        } else if (item.title.equals(resources.getString(R.string.menu_print))) {
                            if (podDo.arrivalTime.equals("")) {
                                showToast(resources.getString(R.string.check_arrival))
                            } else {
                                val intent = Intent(this@PODActivity, PrintDocumentsActivity::class.java);
                                startActivityForResult(intent, 3);
                            }
                        }
                        else if (item.title.equals(resources.getString(R.string.force_skip))) {
                            if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
                                skipShipment(1)
                            } else {
                                if (!preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "").isEmpty()) {
                                    if (!podDo.capturedProductListTime.isNullOrEmpty()) {
                                        skipShipment(2)
                                        //online 1 offline 2
                                    } else {
                                        skipShipment(1)
                                    }
                                } else {
                                    skipShipment(2)
                                }
                            }
                        }
                        else {

                        }
                        return true
                    }
                })
                popup.show()
            }
        }
        btnDeliveryDetails.setOnClickListener {
            Util.preventTwoClick(it)
            if (podDo.arrivalTime.equals("")) {
                showToast(resources.getString(R.string.check_arrival))
            } else if (podDo.podTimeCaptureStartLoadingDate.equals("")) {
                showToast(resources.getString(R.string.check_startload))
            } else {
                if (docType == 1) {
                    var intent = Intent(this@PODActivity, ScheduledCaptureDeliveryActivity::class.java);
                    startActivityForResult(intent, 1);
                } else if (docType == 2) {
                    var intent = Intent(this@PODActivity, RecieptScheduledCaptureDeliveryActivity::class.java);
                    startActivityForResult(intent, 2);
                } else if (docType == 3) {
                    var intent = Intent(this@PODActivity, CustomerReturnScheduledCaptureDeliveryActivity::class.java);
                    startActivityForResult(intent, 3);
                } else if (docType == 4) {
                    var intent = Intent(this@PODActivity, PickTicketScheduledCaptureDeliveryActivity::class.java);
                    startActivityForResult(intent, 4);
                } else if (docType == 5) {
                    var intent = Intent(this@PODActivity, PurchaseReturnScheduledCaptureDeliveryActivity::class.java);
                    startActivityForResult(intent, 5);
                } else if (docType == 6) {
                    if (pickupFlag == 1) {
                        var intent = Intent(this@PODActivity, MiscStopScheduledCaptureDeliveryActivity::class.java);
                        startActivityForResult(intent, 6);
                    } else {
                        var intent = Intent(this@PODActivity, MiscStopScheduledCaptureDeliveryActivity::class.java);
                        startActivityForResult(intent, 6);
                    }

                } else {
                    var intent = Intent(this@PODActivity, ScheduledCaptureDeliveryActivity::class.java);
                    startActivityForResult(intent, 1);
                }
            }
        }
    }

    private fun skipShipment(type: Int) {
        if (type == 1) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = ForceSkipRequest(this@PODActivity)
                siteListRequest.setOnResultListener { isError, loginDo ->
                    hideLoader()
                    if (loginDo != null) {
                        if (isError) {
                            hideLoader()
                            showAppCompatAlert("Alert!", resources.getString(R.string.server_error), "OK", "", "", false)
                        } else {
                            hideLoader()
                            if (loginDo.flag == 20) {
                                showToast("Delivery has skipped")
                                deletePOD()
                                val intent = Intent();
                                intent.putExtra("SkipShipment", "Skip")
                                setResult(52, intent)
                                finish()

                            } else {
                                showToast("Delivery not skipped")
                            }

                        }
                    } else {
                        hideLoader()
                    }

                }

                siteListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "", false)

            }
        } else {
            showToast("Delivery has skipped")
            deletePOD()
            val intent = Intent();
            intent.putExtra("SkipShipment", "Skip")
            setResult(52, intent)
            finish()

        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (requestCode == 1 && resultCode == 1 || requestCode == 4 && resultCode == 1) {
            podDo.capturedProductListTime = CalendarUtils.getCurrentDate(this);
            if (preferenceUtils.getIntFromPreference(PreferenceUtils.VALIDATED_FLAG, 0) == 2) {
                btnDeliveryDetails.setBackgroundResource(R.drawable.card_white_green_boarder)
                btnDeliveryDetails.setClickable(false)
                btnDeliveryDetails.setClickable(false)
                var CRELAT = data!!.getStringExtra("CRELAT")
                var CRELON = data!!.getStringExtra("CRELON")
                var rating = data!!.getStringExtra("RATING")
                var notes = data!!.getStringExtra("NOTES")
                podDo.notes = notes

                podDo.departureRating = rating
                podDo.creDocLattitude = CRELAT
                podDo.creDOCLongitude = CRELON

                v3.setBackgroundResource(R.drawable.done_tick_green)
                tvDeliveryDT.visibility = View.VISIBLE
                tvDeliveryDT.setText(resources.getString(R.string.delivery_details) + podDo.capturedProductListTime)
                saveDepartureData()
            }


        } else if (requestCode == 2 && resultCode == 11) {
            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, resources.getString(R.string.from_success))
            podDo.capturedProductListTime = CalendarUtils.getCurrentDate(this);
            podDo.podTimeCaptureCaptureDeliveryTime = CalendarUtils.getTime()
            podDo.podTimeCaptureCaptureDeliveryDate = CalendarUtils.getDate()
            podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
            podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
            var CRELAT = data!!.getStringExtra("CRELAT")
            var CRELON = data!!.getStringExtra("CRELON")
            var rating = data!!.getStringExtra("RATING")
            var notes = data!!.getStringExtra("NOTES")
            podDo.notes = notes

            podDo.departureRating = rating
            podDo.creDocLattitude = CRELAT
            podDo.creDOCLongitude = CRELON
            btnDeliveryDetails.setBackgroundResource(R.drawable.card_white_green_boarder)
            btnDeliveryDetails.setClickable(false)
            btnDeliveryDetails.setClickable(false)
            tvDeliveryDT.visibility = View.VISIBLE
            tvDeliveryDT.setText(resources.getString(R.string.receipt_details) + podDo.capturedProductListTime)
            saveDepartureData()
            v3.setBackgroundResource(R.drawable.done_tick_green)
        } else if (requestCode == 3 && resultCode == 11) {
            var notes = data!!.getStringExtra("NOTES")
            podDo.notes = notes

            var CRELAT = data!!.getStringExtra("CRELAT")
            var CRELON = data!!.getStringExtra("CRELON")
            var rating = data!!.getStringExtra("RATING")
            podDo.departureRating = rating

            podDo.creDocLattitude = CRELAT
            podDo.creDOCLongitude = CRELON
            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, resources.getString(R.string.from_success))
            btnDeliveryDetails.setBackgroundResource(R.drawable.card_white_green_boarder)
            btnDeliveryDetails.setBackgroundResource(R.drawable.card_white_green_boarder)
            btnDeliveryDetails.isClickable = false
            btnDeliveryDetails.isClickable = false
            podDo.capturedProductListTime = CalendarUtils.getCurrentDate(this)
            podDo.podTimeCaptureCaptureDeliveryTime = CalendarUtils.getTime()
            podDo.podTimeCaptureCaptureDeliveryDate = CalendarUtils.getDate()
            podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
            podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
            tvDeliveryDT.visibility = View.VISIBLE
            tvDeliveryDT.setText(resources.getString(R.string.return_details) + podDo.capturedProductListTime)
            saveDepartureData()
            v3.setBackgroundResource(R.drawable.done_tick_green)
        } else if (requestCode == 5 && resultCode == 11) {
            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, resources.getString(R.string.from_success))
            btnDeliveryDetails.setBackgroundResource(R.drawable.card_white_green_boarder)
            btnDeliveryDetails.setBackgroundResource(R.drawable.card_white_green_boarder)
            btnDeliveryDetails.setClickable(false)
            btnDeliveryDetails.setClickable(false)
            var notes = data!!.getStringExtra("NOTES")
            podDo.notes = notes

            var CRELAT = data!!.getStringExtra("CRELAT")
            var CRELON = data!!.getStringExtra("CRELON")
            var rating = data!!.getStringExtra("RATING")
            podDo.departureRating = rating
            podDo.creDocLattitude = CRELAT
            podDo.creDOCLongitude = CRELON

            podDo.capturedProductListTime = CalendarUtils.getCurrentDate(this);
            podDo.podTimeCaptureCaptureDeliveryTime = CalendarUtils.getTime()
            podDo.podTimeCaptureCaptureDeliveryDate = CalendarUtils.getDate()
            podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
            podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
            tvDeliveryDT.visibility = View.VISIBLE
            tvDeliveryDT.setText(resources.getString(R.string.return_details) + podDo.capturedProductListTime)
            saveDepartureData()
            v3.setBackgroundResource(R.drawable.done_tick_green)
        } else if (requestCode == 6 && resultCode == 11) {
            var CRELAT = data!!.getStringExtra("CRELAT")
            var CRELON = data!!.getStringExtra("CRELON")

            var rating = data!!.getStringExtra("RATING")
            var notes = data!!.getStringExtra("NOTES")
            podDo.notes = notes

            podDo.departureRating = rating
            podDo.creDocLattitude = CRELAT
            podDo.creDOCLongitude = CRELON
            podDo.capturedProductListTime = CalendarUtils.getCurrentDate(this);
            podDo.podTimeCaptureCaptureDeliveryTime = CalendarUtils.getTime()
            podDo.podTimeCaptureCaptureDeliveryDate = CalendarUtils.getDate()
            podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
            podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
            btnDeliveryDetails.setBackgroundResource(R.drawable.card_white_green_boarder)
            btnDeliveryDetails.setClickable(false)
            tvDeliveryDT.visibility = View.VISIBLE
            tvDeliveryDT.setText(resources.getString(R.string.misc_details) + podDo.capturedProductListTime)
            saveDepartureData()
            v3.setBackgroundResource(R.drawable.done_tick_green)
        } else if (requestCode == 120 && resultCode == Activity.RESULT_OK) {
            val returnString = data!!.getStringExtra(resources.getString(R.string.del_remarks))
            LogUtils.debug(getString(R.string.del_remarks), returnString)
            podDo.deliveryNotes = returnString;
            saveDepartureData()
        } else if (requestCode == 130 && resultCode == Activity.RESULT_OK) {
            val returnString = data!!.getStringExtra(resources.getString(R.string.inv_remarks))
            LogUtils.debug(resources.getString(R.string.inv_remarks), returnString)
            podDo.invoiceNotes = returnString;
            saveDepartureData()
        } else if (requestCode == 140 && resultCode == Activity.RESULT_OK) {
            val returnString = data!!.getStringExtra(resources.getString(R.string.notes_remarks))
            LogUtils.debug(resources.getString(R.string.notes_remarks), returnString)
            podDo.notes = returnString;
            saveDepartureData()
        } else if (requestCode == 511 && resultCode == 123) {
            val data = data!!.getStringExtra(resources.getString(R.string.data))
            val array = data.split(",")
            podDo.rescheduleStatus = array
            StorageManager.getInstance(this).saveDepartureData(this, podDo)
        } else if (requestCode == 511 && resultCode == 511) {
            deletePOD()
        }
    }

    override fun initializeControls() {
        btnConfirmArrival = findViewById(R.id.btnConfirmArrival) as LinearLayout
        btnStartUnloading = findViewById(R.id.btnStartUnLoading) as LinearLayout
        btnDeliveryDetails = findViewById(R.id.btnDeliveryDetails) as LinearLayout
        btnEndUnloading = findViewById(R.id.btnEndUnloading) as LinearLayout
        btnGenerate = findViewById(R.id.btnGenerate) as LinearLayout
        btnConfirmDeparture = findViewById(R.id.btnConfirmDeparture) as LinearLayout
    }

    private fun clickOnConfirmArrival(message: String) {
        showAppCompatAlert(resources.getString(R.string.alert_message), message, resources.getString(R.string.confirm), resources.getString(R.string.hint_cancel), resources.getString(R.string.arrival_from), false);
    }

    private fun clickOnStartUnloading(message: String) {
        if (podDo.arrivalTime.equals("")) {
            showToast(resources.getString(R.string.check_arrival))
        } else {
            showAppCompatAlert(resources.getString(R.string.alert_message), message, resources.getString(R.string.confirm), resources.getString(R.string.hint_cancel), resources.getString(R.string.startload_from), false);
        }
    }

    private fun clickOnEndUnloading(message: String) {
        if (podDo.arrivalTime.equals("")) {
            showToast(resources.getString(R.string.check_arrival))
        } else if (podDo.podTimeCaptureStartLoadingDate.equals("")) {
            showToast(resources.getString(R.string.check_startload))
        } else if (podDo.capturedProductListTime.equals("")) {
            showToast(getString(R.string.please_finish))
        } else {
            showAppCompatAlert(resources.getString(R.string.alert_message), message, resources.getString(R.string.confirm), resources.getString(R.string.hint_cancel), resources.getString(R.string.endload_from), false);
        }
    }


    override fun onButtonYesClick(from: String) {

        if (from.equals(resources.getString(R.string.arrival_from))) {

            location(ResultListner { `object`, isSuccess ->
                if (isSuccess) {
                    hideLoader()
                    stopLocationUpdates()
                    podDo.arrivalLattitude = lattitudeFused
                    podDo.arrivalLongitude = longitudeFused
                    podDo.arrivalTime = CalendarUtils.getCurrentDate(this);
                    podDo.podTimeCaptureArrivalTime = CalendarUtils.getTime()
                    podDo.podTimeCaptureArrivalDate = CalendarUtils.getDate()
                    btnConfirmArrival.setBackgroundResource(R.drawable.card_white_green_boarder)
                    btnConfirmArrival.setClickable(false)
                    btnConfirmArrival.setEnabled(false)
                    tvConfirmArrivalDT.visibility = View.VISIBLE
                    tvConfirmArrivalDT.setText(resources.getString(R.string.arrival) + podDo.arrivalTime)
                    v1.setBackgroundResource(R.drawable.done_tick_green)
                    saveDepartureData()
                    deliveryStatus(3)

                }else{
                    hideLoader()
                    showToast(getString(R.string.cur_loc))
                }
            })

        } else if (from.equals(resources.getString(R.string.departure_from))) {

                showLoader()
                stopLocationUpdates()
                location(ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        hideLoader()
                        stopLocationUpdates()
                        podDo.departureLattitude = lattitudeFused
                        podDo.departureLongitude = longitudeFused
                        podDo.departureTime = CalendarUtils.getCurrentDate(this);
                        podDo.podTimeCapturepodDepartureTime = CalendarUtils.getTime()
                        podDo.podTimeCapturpodDepartureDate = CalendarUtils.getDate()
                        saveDepartureData()
                        if (shipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
                            podTimeCapture(shipmentId)
                        } else {
                            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                            podTimeCapture(shipmentId)
                        }


                    }
                })


        } else if (from.equals(resources.getString(R.string.endload_from))) {
            podDo.podTimeCaptureEndLoadingDate = CalendarUtils.getDate();
            podDo.podTimeCaptureEndLoadingTime = CalendarUtils.getTime()
            btnEndUnloading.setBackgroundResource(R.drawable.card_white_green_boarder)
            podDo.endUnLoadingTime = CalendarUtils.getCurrentDate(this)
            btnEndUnloading.setClickable(false)
            btnEndUnloading.setEnabled(false)
            saveDepartureData()
            v4.setBackgroundResource(R.drawable.done_tick_green)

            tvEndUnloadDT.visibility = View.VISIBLE
            tvEndUnloadDT.setText(resources.getString(R.string.endload) + podDo.endUnLoadingTime)

        } else if (from.equals(resources.getString(R.string.startload_from))) {
            podDo.podTimeCaptureStartLoadingDate = CalendarUtils.getDate();
            podDo.podTimeCaptureStartLoadingTime = CalendarUtils.getTime()
            podDo.unLoadingTime = CalendarUtils.getCurrentDate(this)
            btnStartUnloading.setBackgroundResource(R.drawable.card_white_green_boarder)
            btnStartUnloading.setClickable(false)
            btnStartUnloading.setEnabled(false)
            tvStartLoadingDT.visibility = View.VISIBLE
            tvStartLoadingDT.setText(resources.getString(R.string.startload) + podDo.unLoadingTime)
            saveDepartureData()
            v2.setBackgroundResource(R.drawable.done_tick_green)

        } else
            super.onButtonYesClick(from)
    }

    fun deletePOD() {
        StorageManager.getInstance(this).saveDepartureData(this, podDo);
        StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
        StorageManager.getInstance(this).deleteDepartureData(this)
        StorageManager.getInstance(this).deleteCurrentSpotSalesCustomer(this)
        StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
        StorageManager.getInstance(this).deleteReturnCylinders(this)
        preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.PICK_TICKET);

        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_SHIPMENT_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.VALIDATED_FLAG);
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION_INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_AMOUNT);
        preferenceUtils.removeFromPreference(PreferenceUtils.NONBG);
        preferenceUtils.removeFromPreference(PreferenceUtils.PICKUP_DROP_FLAG)
        preferenceUtils.removeFromPreference(PreferenceUtils.DOC_TYPE)
        preferenceUtils.removeFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.EMIRATES_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.COMMENT_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.DAMAGE);
        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL);
        preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL);
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId);
        preferenceUtils.removeFromPreference(PreferenceUtils.ETA_ETD)
        v6.setBackgroundResource(R.drawable.done_tick_green)

        btnConfirmDeparture.setBackgroundResource(R.drawable.card_white_green_boarder)
        btnConfirmDeparture.setClickable(false)
        btnConfirmDeparture.setEnabled(false)
        tvDepartureDT.visibility = View.VISIBLE
        tvDepartureDT.setText(resources.getString(R.string.departure) + podDo.departureTime)
        v6.setBackgroundResource(R.drawable.done_tick_green)
        moveBack()

    }


    private fun moveBack() {
        val intent = Intent();
        intent.putExtra(resources.getString(R.string.captured_returns), true)
        setResult(12, intent)
        finish()
    }

    private fun saveDepartureData() {
        StorageManager.getInstance(this).saveDepartureData(this, podDo);
    }

    private fun podTimeCapture(id: String) {
        showLoader()
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var doctype = ""
        if (id.isEmpty()) {
            doctype = ""
        } else {
            doctype = "4"
        }
        val siteListRequest = PODDepartureTimeCaptureRequest(driverId, date, doctype, id, this)
        siteListRequest.setOnResultListener { isError, loginDo, msg ->
            hideLoader()
            if (isError) {
                hideLoader()
                if (msg.isNotEmpty()) {
                    showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                } else {
                    showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.success), false)

                }
            } else {
                hideLoader()
                if (loginDo != null) {
                    if (loginDo.flag == 2) {
                        deliveryStatus(4)
                        deletePOD()
                    } else {
                        showToast(resources.getString(R.string.unable_departure))
                    }
                } else {
                    hideLoader()
                    if (msg.isNotEmpty()) {
                        showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.success), false)

                    }
                }
            }
        }

        siteListRequest.execute()
    }


    override fun onBackPressed() {
        AppConstants.CapturedReturns = false;
        super.onBackPressed()
    }


    private fun loadNonVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {

            var nonScheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")
            if (nonScheduledRootId.length > 0) {
                val loadVanSaleRequest = NonScheduledLoadVanSaleRequest(nonScheduledRootId, this@PODActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()

                    if (isError) {
                        showToast(resources.getString(R.string.server_error))
                    } else {
                        hideLoader()
                        nonScheduleDos = loadStockMainDo.loadStockDOS;
                        if (nonScheduleDos == null || nonScheduleDos.size < 1) {
                            btnDeliveryDetails.setBackgroundResource(R.drawable.card_white_green_boarder)
                            btnDeliveryDetails.setClickable(false)
                            btnDeliveryDetails.setEnabled(false)
                        }

                    }
                }
                loadVanSaleRequest.execute()
            }


        } else {
            showToast(resources.getString(R.string.internet_connection))
        }
    }

    fun deliveryStatus(status: Int) {
        showLoader()
        if (Util.isNetworkAvailable(this)) {
            var vrId = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            var deliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

            val siteListRequest = DeliveryRoutingIdRequest(this@PODActivity, vrId, deliveryId, status)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                if (loginDo != null) {
                    if (isError) {
                        hideLoader()
                    } else {
                        hideLoader()
                        if (loginDo.flag == 2) {
                            if (status == 3) {
                                // showToast(resources.getString(R.string.status_progress))
                            } else if (status == 4) {
                                  showToast(resources.getString(R.string.status_departure))
                            }
                        }
                    }
                } else {
                    hideLoader()
                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert(resources.getString(R.string.alert_message), resources.getString(R.string.internet_connection), resources.getString(R.string.ok), "", resources.getString(R.string.from_failure), false)

        }
    }

}