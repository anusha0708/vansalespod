package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.ActiveDeliveryActivity;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.Cancel_RescheduleActivity;
import com.tbs.generic.vansales.Model.ReasonDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.listeners.ResultListner;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;

import java.util.ArrayList;

public class ReasonListAdapter extends RecyclerView.Adapter<ReasonListAdapter.MyViewHolder>  {

    private ArrayList<ReasonDO> reasonDoS;
    private Context context;
    int type;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);



        }
    }


    public ReasonListAdapter(int typ,Context context, ArrayList<ReasonDO> reasonDOS) {
        this.context = context;
        this.reasonDoS = reasonDOS;
        this.type = typ;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reasons_text, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final ReasonDO reasonDO = reasonDoS.get(position);

        holder.tvName.setText(""+reasonDO.reason);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.preventTwoClick(view);

                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                if(reasonDO.code.equalsIgnoreCase("OTH")){
                    ((BaseActivity)context).showAddItemDialog(new ResultListner() {
                        @Override
                        public void onResultListner(Object object, boolean isSuccess) {
                            if(isSuccess){
                                holder.tvName.setText(""+object.toString());

                                preferenceUtils.saveString(PreferenceUtils.Reason_CODE, String.valueOf(reasonDoS.get(pos).code));
                                preferenceUtils.saveString(PreferenceUtils.Reason_OTH,object.toString());

                                if(type==5){
                                ((ActiveDeliveryActivity)context).skipShipment(reasonDoS.get(pos).code);

                                }else {

                                    ((Cancel_RescheduleActivity)context).updateReqApprButton(object.toString());

                                }
                            }
                        }
                    });

                }else {
                    preferenceUtils.saveString(PreferenceUtils.Reason_CODE, String.valueOf(reasonDoS.get(pos).code));
                    preferenceUtils.saveString(PreferenceUtils.Reason_Payment, String.valueOf(reasonDoS.get(pos).reason));
                    if(type==5){

                        ((ActiveDeliveryActivity)context).skipShipment(reasonDoS.get(pos).code);

                    }else {
                        ((Cancel_RescheduleActivity)context).updateReqApprButton(""+reasonDoS.get(pos).reason);

                    }
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return reasonDoS.size();
    }

}
