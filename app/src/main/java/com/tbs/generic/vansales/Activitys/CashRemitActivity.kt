package com.tbs.generic.vansales.Activitys

import android.content.Intent
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.utils.Util

//
class CashRemitActivity : BaseActivity() {

    override fun initialize() {
      var  llCategories = layoutInflater.inflate(R.layout.cash_remit, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.cash_remit)

        var btnConfirm = findViewById<Button>(R.id.btnConfirm)
        btnConfirm.setOnClickListener {
            Util.preventTwoClick(it)
            var intent = Intent(this@CashRemitActivity,DashBoardActivity::class.java)
            startActivity(intent)
        }


    }
}