package com.tbs.generic.vansales.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 *Created by kishoreganji on 21-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
data class VehicleCheckOutDo(
        @SerializedName("id") var id: String? = "",
        @SerializedName("user_id") var user_id: String? = "",
        @SerializedName("checkinTime") var checkInDate: String? = "",
        @SerializedName("checkOutTime") var checkOutDate: String? = "",
        @SerializedName("eta") var eta: String? = "",
        @SerializedName("etd") var etd: String? = "",
        @SerializedName("site") var site: String? = "",
        @SerializedName("vehicle") var vehicle: String? = "",
        @SerializedName("status") var status: String? = "",

        @SerializedName("vrID") var vrID: String? = "",
        @SerializedName("startRead") var startRead: String? = "",
        @SerializedName("endRead") var endRead: String? = "",
        @SerializedName("checkoutFlag") var checkoutFlag: String? = ""



        ) : Serializable