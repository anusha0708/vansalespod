package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.AddressListActivity;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class SpotSalesCustomerAdapter extends RecyclerView.Adapter<SpotSalesCustomerAdapter.MyViewHolder> {

    private ArrayList<CustomerDo> customerDos;
    private Context context;
    private String from;


    public void refreshAdapter(@NotNull ArrayList<CustomerDo> customerDos) {
        this.customerDos = customerDos;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCustomerName, tvCustomerCode, tvBuisinessLine, tvAddressCode, tvDescription, tvCity, tvEmirates;
        public TextView btnPay;
        private LinearLayout llDetails;
        public TextView tvOrderId;

        public MyViewHolder(View view) {
            super(view);
            llDetails = view.findViewById(R.id.llDetails);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvCustomerCode = itemView.findViewById(R.id.tvCustomerCode);
            tvBuisinessLine = itemView.findViewById(R.id.tvBuisinessLine);
            tvAddressCode = itemView.findViewById(R.id.tvAddressCode);


        }
    }


    public SpotSalesCustomerAdapter(Context context, ArrayList<CustomerDo> listOrderDos, String froM) {
        this.context = context;
        this.customerDos = listOrderDos;
        this.from = froM;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.spotsales_customer_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final CustomerDo customerDo = customerDos.get(position);
        customerDo.isCreditAvailable = customerDo.flag == 1;
//        if (customerDo.isCreditAvailable) {
//            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.gray));
//        } else {
//            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
//        }
//        holder.tvCustomerName.setText("Name                : " + customerDo.customerName + "\n"
//                + "Credit Amount : " + customerDo.amount +
//                "\n" + "Business line   : " + customerDo.businessLine);
//        holder.tvCustomerId.setText("Code                 : " + customerDo.customer);
        holder.tvCustomerCode.setText(customerDo.customer);
        holder.tvCustomerName.setText(customerDo.customerName);
        holder.tvBuisinessLine.setText(customerDo.businessLine);
        holder.tvAddressCode.setText(customerDo.amount);
//        if (customerDo.postBox.length() > 0 || customerDo.landmark.length() > 0 || customerDo.town.length() > 0) {
//            holder.tvCustomerAddress.setVisibility(View.VISIBLE);
//
//            holder.tvCustomerAddress.setText("Address            : " + customerDo.postBox + "  \n" + customerDo.landmark + customerDo.town);
//        } else {
//            holder.tvCustomerAddress.setVisibility(View.GONE);
//        }
        if (customerDo.isDelivered != null && customerDo.isDelivered.equalsIgnoreCase("Delivered")) {
            holder.llDetails.setBackgroundColor(context.getResources().getColor(R.color.gray_new));
            holder.llDetails.setClickable(false);
            holder.llDetails.setEnabled(false);
        } else {
//            if (customerDo.isCreditAvailable) {
//                holder.llDetails.setBackgroundColor(context.getResources().getColor(R.color.white));
//            } else {
//                holder.llDetails.setBackgroundColor(context.getResources().getColor(R.color.gray_new));
//            }
            holder.llDetails.setClickable(true);
            holder.llDetails.setEnabled(true);
        }
        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!from.isEmpty() && from.equalsIgnoreCase("MASTER")) {
//                    if (StorageManager.getInstance(context).saveCurrentSpotSalesCustomer(((BaseActivity) context), customerDo)) {
                    Intent intent = new Intent(context, AddressListActivity.class);
                    intent.putExtra("CODE", customerDo.customer);
                    intent.putExtra("MASTER", "MASTER");
                    intent.putExtra("CUSTOMER_DO", customerDo);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
//                    }


//                    Intent intent = new Intent(context, CustomerDetailsActivity.class);
//                    intent.putExtra("CODE", customerDo.customer);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    context.startActivity(intent);
                } else {
////                    CustomerDo customerDo = StorageManager.getInstance(context).getCurrentSpotSalesCustomer(context);
//                    CustomerDo custDo = StorageManager.getInstance(context).getCurrentSpotSalesCustomer(context);
//                    if (custDo != null && !custDo.customer.equalsIgnoreCase("")) {
//                        Intent intent = new Intent(context, AddressListActivity.class);
//                        intent.putExtra("CODE", customerDo.customer);
//
//                        ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
//                        ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
//                        context.startActivity(intent);
////                        if (custDo.customer.equalsIgnoreCase(customerDo.customer)) {
////                            //
////                            Intent intent = new Intent(context, AddressListActivity.class);
////                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
////                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
////                            context.startActivity(intent);
////                        }
////                        else {
////                            ((BaseActivity) context).showAppCompatAlert("", "Please process the current customer " + custDo.customer + "'s non schedule shipment", "OK", "", "", false);
////                        }
//                    } else {
//
//                        StorageManager.getInstance(context).deleteActiveDeliveryMainDo(((BaseActivity) context));
//                        StorageManager.getInstance(context).deleteDepartureData(((BaseActivity) context));
//                        ((BaseActivity) context).preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId);
//                        StorageManager.getInstance(context).deleteCurrentSpotSalesCustomer(((BaseActivity) context));
//                        StorageManager.getInstance(context).deleteCurrentDeliveryItems(((BaseActivity) context));
//                        StorageManager.getInstance(context).deleteReturnCylinders(((BaseActivity) context));
//
//                        if (StorageManager.getInstance(context).saveCurrentSpotSalesCustomer(((BaseActivity) context), customerDo)) {
//                            Intent intent = new Intent(context, AddressListActivity.class);
//                            intent.putExtra("CODE", customerDo.customer);
//                            intent.putExtra("CUSTOMERDO", customerDo);
//
//                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
//                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
//                            context.startActivity(intent);
//
//                        }
//
//                    }


                    Intent intent = new Intent(context, AddressListActivity.class);
                    intent.putExtra("CODE", customerDo.customer);
                    intent.putExtra("CUSTOMER_DO", customerDo);
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CURRENCY, "" +customerDo.currency);

                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
                    context.startActivity(intent);
                }

//                intent.putExtra("CustomerId", customerDo.customer);
//                intent.putExtra("Mobile", customerDo.mobile);
//                intent.putExtra("Email", customerDo.email);
//                intent.putExtra("Lattitude", customerDo.lattitude);
//                intent.putExtra("Longitude", customerDo.longitude);

            }
        });


    }

    @Override
    public int getItemCount() {
//        if (filterList != null && filterList.size() > 0) {
//            return filterList.size();
//
//        } else {
//        }
        return customerDos != null ? customerDos.size() : 0;

    }

}
