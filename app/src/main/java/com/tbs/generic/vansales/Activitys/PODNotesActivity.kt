package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.R
import android.content.Intent
import android.text.Selection
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.Util
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.ContextCompat.getSystemService






//
class PODNotesActivity : BaseActivity() {

    lateinit var btnSave : Button
    lateinit var etNotes : EditText

    override fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.notes_screen, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()
        tvScreenTitle.text = getString(R.string.add_notes)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        initializeControls()
        var podDo = StorageManager.getInstance(this).getDepartureData(this)
        etNotes.setText(podDo.notes)
        val position = podDo.notes!!.length
        val etext = etNotes.getText()
        Selection.setSelection(etext, position)
        btnSave.setOnClickListener {
            Util.preventTwoClick(it)
            if(etNotes.text.toString().isNotEmpty()){
                var data = etNotes.text.toString()
                val intent = Intent()
                intent.putExtra(getString(R.string.notes_remarks), data)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
            else{
                showToast(getString(R.string.please_enter_remarsk))
            }
        }
    }

    override fun initializeControls() {
        etNotes         = findViewById<EditText>(R.id.etNotes)
        btnSave         = findViewById<Button>(R.id.btnSave)
    }
}