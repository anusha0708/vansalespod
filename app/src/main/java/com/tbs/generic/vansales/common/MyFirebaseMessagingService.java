package com.tbs.generic.vansales.common;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.tbs.generic.vansales.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;


/**
 * The type My firebase messaging service.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * The constant arr_group_ids.
     */
    public static ArrayList<String> arr_group_ids = new ArrayList<>();
    /**
     * The constant arr_msg_body.
     */
    public static ArrayList<String> arr_msg_body = new ArrayList<>();

    String body;
    private String title;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        Map<String, String> data = remoteMessage.getData();

        Gson gson = new Gson();
        String json = gson.toJson(data);

        Log.i(TAG, "json response-->" + remoteMessage.getData());

        try {
            JSONObject objt = new JSONObject(json);
            body = objt.optString("body");
            title = objt.optString("title");


        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendNormalNotification(MyFirebaseMessagingService.this, body);
    }

    public void sendNormalNotification(Context ctx, String msgbody) {

        if (ctx == null) {
            return;
        }


        String name = getPackageName();
        String id = "my_package_channel_1"; // The user-visible name of the channel.
        String description = "my_package_first_channel"; // The user-visible description of the channel.
        NotificationChannel mChannel = null;
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {
                mChannel = notificationManager.getNotificationChannel(id);
                if (mChannel == null) {
                    mChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_HIGH);
                    mChannel.setDescription(description);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    notificationManager.createNotificationChannel(mChannel);
                }

            }
        }


        NotificationCompat.Builder notificationBuilder;
        if (mChannel != null) {
            notificationBuilder = new NotificationCompat.Builder(this, mChannel.getId());
        } else {
            notificationBuilder = new NotificationCompat.Builder(this);
        }

        int notificationId = new Random().nextInt(); // just use a counter in some util class...

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder.setSmallIcon(getNotificationIcon());
        notificationBuilder.setColor(ContextCompat.getColor(ctx, R.color.colorPrimary));
        notificationBuilder.setContentTitle(ctx.getResources().getString(R.string.app_name));
        notificationBuilder.setContentText(msgbody);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setPriority(Notification.PRIORITY_MAX);
        notificationBuilder.setWhen(0);
        notificationBuilder.setSound(defaultSoundUri);
        notificationBuilder.setPriority(Notification.PRIORITY_HIGH);
        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msgbody));
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.tema_logo : R.drawable.tema_logo;
    }

}
