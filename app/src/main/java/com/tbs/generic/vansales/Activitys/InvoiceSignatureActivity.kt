package com.tbs.generic.vansales.Activitys

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.*
import android.os.Build
import android.os.Environment
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Model.CreateInvoiceDO
import com.tbs.generic.vansales.Model.CreateInvoicePaymentDO
import com.tbs.generic.vansales.Model.CustomerManagementDO
import com.tbs.generic.vansales.Model.PodDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.pdfs.BGInvoicePdf
import com.tbs.generic.vansales.prints.PrinterConnection
import com.tbs.generic.vansales.prints.PrinterConstants
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

/**
 *Created by VenuAppasani on 22-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
class InvoiceSignatureActivity : BaseActivity(), View.OnClickListener {
    lateinit var mClear: Button
    lateinit var mGetSign: Button
    lateinit var mCancel: Button
    lateinit var file: File
    lateinit var mContent: LinearLayout
    lateinit var view: View
    lateinit var mSignature: signature
    var bitmap: Bitmap? = null
    lateinit var data: String

    lateinit var savedPath: String
    lateinit var encodedImage: String
    private val permissionRequestCode = 34

    lateinit var createPDFInvoiceDo: CreateInvoicePaymentDO
    lateinit var createInvoiceDO: CreateInvoiceDO
    lateinit var podDo: PodDo
    lateinit var customerManagementDO: CustomerManagementDO


    // Creating Separate Directory for saving Generated Images
    var DIRECTORY = Environment.getExternalStorageDirectory().path + "/UserSignature/"
    var pic_name = /*SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())*/"signature"
    var StoredPath = DIRECTORY + pic_name + ".png"

    override fun initializeControls() {
        mContent = findViewById<View>(R.id.canvasLayout) as LinearLayout
        mSignature = signature(applicationContext, null)
        mSignature.setBackgroundColor(Color.WHITE)

        // Dynamically generating Layout through java code
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        mClear = findViewById<View>(R.id.clear) as Button
        mGetSign = findViewById<View>(R.id.getsign) as Button
        podDo = StorageManager.getInstance(this).getDepartureData(this)

//        tvCustomerName.setText("Customer : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

        if (intent.hasExtra("DATA")) {
            data = intent.extras?.getString("DATA")!!
        }



//        mGetSign.isEnabled = false
        mCancel = findViewById<View>(R.id.cancel) as Button
        view = mContent
        mGetSign.setOnClickListener(this@InvoiceSignatureActivity)
        mClear.setOnClickListener(this@InvoiceSignatureActivity)
        mCancel.setOnClickListener(this@InvoiceSignatureActivity)

        // Method to create Directory, if the Directory doesn't exists
        file = File(DIRECTORY)
        if (!file.exists()) {
            file.mkdir()
        }
//         podDo = StorageManager.getInstance(this).getDepartureData(this);

        if (podDo.signature.equals("")) {
            tvScreenTitle.text = getString(R.string.sign_and_save_here)
        } else {
            tvScreenTitle.text = getString(R.string.view_sign)
            tvScreenTitle.setOnClickListener {
                Util.preventTwoClick(it)
                val intent = Intent(this@InvoiceSignatureActivity, ViewImageActivity::class.java)

                startActivity(intent)
            }
        }


    }


    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.activity_invoice_signature, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        disableMenuWithBackButton()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            //            showToast("Please save signature")
            finish()
        }
    }


    override fun onClick(v: View?) {
        // TODO Auto-generated method stub
        if (v === mClear) {
            mSignature.clear()
//            mGetSign.isEnabled = false
        } else if (v === mGetSign) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (isStoragePermissionGranted()) {
                    view.isDrawingCacheEnabled = true
                    mSignature.save(view, StoredPath)
                    // Toast.makeText(applicationContext, "Successfully Saved", Toast.LENGTH_SHORT).show()
                    // Calling the same class
                    // recreate()
                }
            } else {
                view.isDrawingCacheEnabled = true
                mSignature.save(view, StoredPath)
                // Toast.makeText(applicationContext, "Successfully Saved", Toast.LENGTH_SHORT).show()
                // Calling the same class
                //  recreate()
            }
        } else if (v === mCancel) {
            showToast(getString(R.string.please_save_Sig))
        }
    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (applicationContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            view.isDrawingCacheEnabled = true
            mSignature.save(view, StoredPath)
//            Toast.makeText(applicationContext, "Successfully Saved", Toast.LENGTH_SHORT).show()
            // Calling the same class
            recreate()
        }
        if (requestCode == permissionRequestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createInvoicePDF(createPDFInvoiceDo)
            } else {
//                Log.e(TAG, "WRITE_PERMISSION_DENIED")
            }
        } else {
            Toast.makeText(this, getString(R.string.the_app_not_allowed), Toast.LENGTH_LONG).show()
        }
    }

    inner class signature(context: Context, attrs: AttributeSet?) : View(context, attrs) {
        private val paint = Paint()
        private val path = Path()

        private var lastTouchX: Float = 0.toFloat()
        private var lastTouchY: Float = 0.toFloat()
        private val dirtyRect = RectF()

        init {
            paint.isAntiAlias = true
            paint.color = Color.BLACK
            paint.style = Paint.Style.STROKE
            paint.strokeJoin = Paint.Join.ROUND
            paint.strokeWidth = STROKE_WIDTH
        }

        @SuppressLint("WrongThread")
        fun save(v: View, StoredPath: String) {
            Log.v("log_tag", "Width: " + v.width)
            Log.v("log_tag", "Height: " + v.height)
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(mContent.width, mContent.height, Bitmap.Config.RGB_565)
            }
            val canvas = bitmap?.let { Canvas(it) }
            try {
//                // Output the file
                val mFileOutStream = FileOutputStream(StoredPath)
                v.draw(canvas)
//
//                // Convert the output file to Image such as .png
                bitmap!!.compress(Bitmap.CompressFormat.PNG, 2, mFileOutStream)
////                val intent = Intent(this@SignatureActivity, CaptureReturnDetailsActivity::class.java)
////                intent.putExtra("imagePath", StoredPath)
////                startActivity(intent)
//
                mFileOutStream.flush()
                mFileOutStream.close()
//
//                val baos = ByteArrayOutputStream();
//                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 2, baos); //bm is the bitmap object
//
//                val byteArrayImage = baos.toByteArray();
//                encodedImage = android.util.Base64.encodeToString(byteArrayImage, android.util.Base64.DEFAULT)
//                var podDo = StorageManager.getInstance(context).getDepartureData(context);
//
//                intent.putExtra("SIGNATURE", StoredPath)
//                intent.putExtra("SignatureEncode", encodedImage)
//                podDo.signatureEncode = encodedImage;
//                savedPath = StoredPath
//                StorageManager.getInstance(context).saveDepartureData(this@SignatureActivity, podDo)
//                var scaledBitmap = scaleDown(bitmap!!, 2f, true)
//
//                val baoos = ByteArrayOutputStream();
//                scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, 2, baoos); //bm is the bitmap object
//
//                val byteArrayImagee = baos.toByteArray();
//                var encodedImagee = android.util.Base64.encodeToString(byteArrayImagee, android.util.Base64.DEFAULT)

                val bos = ByteArrayOutputStream()
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 2, bos)
                val bitmapdata = bos.toByteArray()
                Log.e("Signature Size : ", "Size : " + bitmapdata)
                encodedImage = android.util.Base64.encodeToString(bitmapdata, android.util.Base64.DEFAULT)
                podDo.signatureEncode = encodedImage
                savedPath = StoredPath
                StorageManager.getInstance(context).saveDepartureData(this@InvoiceSignatureActivity, podDo)
                if (Util.isNetworkAvailable(context)) {
                    if (!path.isEmpty) {
                        invoiceCreation()
                    } else {
                        showToast(context.getString(R.string.please_do_sig))
                    }
                } else {
                    showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

                }


            } catch (e: Exception) {
                Log.v("log_tag", e.toString())
            }

        }

        fun clear() {
            path.reset()
            invalidate()
//            mGetSign.isEnabled = false
        }

        fun scaleDown(realImage: Bitmap, maxImageSize: Float,
                      filter: Boolean): Bitmap {
            val ratio = Math.min(
                    maxImageSize.toFloat() / realImage.width,
                    maxImageSize.toFloat() / realImage.height)
            val width = Math.round(ratio.toFloat() * realImage.width)
            val height = Math.round(ratio.toFloat() * realImage.height)
            val newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter)
            return newBitmap
        }

        override fun onDraw(canvas: Canvas) {
            canvas.drawPath(path, paint)
        }

        override fun onTouchEvent(event: MotionEvent): Boolean {
            val eventX = event.x
            val eventY = event.y
//            mGetSign.isEnabled = true

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    path.moveTo(eventX, eventY)
                    lastTouchX = eventX
                    lastTouchY = eventY
                    return true
                }

                MotionEvent.ACTION_MOVE,

                MotionEvent.ACTION_UP -> {

                    resetDirtyRect(eventX, eventY)
                    val historySize = event.historySize
                    for (i in 0 until historySize) {
                        val historicalX = event.getHistoricalX(i)
                        val historicalY = event.getHistoricalY(i)
                        expandDirtyRect(historicalX, historicalY)
                        path.lineTo(historicalX, historicalY)
                    }
                    path.lineTo(eventX, eventY)
                }

                else -> {
                    debug("Ignored touch event: " + event.toString())
                    return false
                }
            }

            invalidate((dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt())

            lastTouchX = eventX
            lastTouchY = eventY

            return true
        }

        private fun debug(string: String) {

            Log.v("log_tag", string)

        }

        private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY
            }
        }

        private fun resetDirtyRect(eventX: Float, eventY: Float) {
            dirtyRect.left = Math.min(lastTouchX, eventX)
            dirtyRect.right = Math.max(lastTouchX, eventX)
            dirtyRect.top = Math.min(lastTouchY, eventY)
            dirtyRect.bottom = Math.max(lastTouchY, eventY)
        }

    }

    companion object {
        private val STROKE_WIDTH = 5f
        private val HALF_STROKE_WIDTH = STROKE_WIDTH / 2
    }


    private fun invoiceCreation() {
        if (Util.isNetworkAvailable(this)) {
            if (data.length > 0) {
                val siteListRequest = CreateInvoiceRequest(encodedImage,podDo.invoiceNotes, data, this@InvoiceSignatureActivity)
                siteListRequest.setOnResultListener { isError, createPaymentDO, msg ->
                    hideLoader()
                    if (createPaymentDO != null) {
                        createInvoiceDO = createPaymentDO
                        if (isError) {
                            if (msg.isNotEmpty()) {
                                showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)

                            } else {
                                showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)

                            }
                        } else {
                            if (createInvoiceDO.flag == 2) {
                                showToast("Invoice Created - " + createInvoiceDO.salesInvoiceNumber)

//                            showAppCompatAlert("Success", "Invoice Created - " + createInvoiceDO.salesInvoiceNumber, "Ok", "", "SUCCESS", false)
                                //Toast.makeText(this@CreateInvoiceActivity, "Invoice Created", Toast.LENGTH_SHORT).show()
                                preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, createPaymentDO.salesInvoiceNumber)
                                preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, createPaymentDO.amount)
                                preferenceUtils.saveString(PreferenceUtils.INVOICE_SHIPMENT_ID, createPaymentDO.o_shipmentNumber)
                                preferenceUtils.saveString(PreferenceUtils.PREVIEW_INVOICE_ID, createPaymentDO.salesInvoiceNumber)
                                podDo.invoice = createPaymentDO.salesInvoiceNumber
                                StorageManager.getInstance(this).saveDepartureData(this, podDo)
//                                mGetSign.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//                                mGetSign.isClickable = false
//                                mGetSign.isEnabled = false

                                val intent = Intent()
                                intent.putExtra("INVOICE", createPaymentDO.salesInvoiceNumber)
                                setResult(10, intent)
                                finish()

                                prepareInvoiceCreation()


                            } else if (createInvoiceDO.flag == 4) {
                                if (createInvoiceDO.message.length > 0) {
                                    showAppCompatAlert(getString(R.string.info), "" + createInvoiceDO.message, getString(R.string.ok), "", getString(R.string.failure), false)

                                } else {
                                    showAppCompatAlert(getString(R.string.error), getString(R.string.invoice_not_created), getString(R.string.ok), "", getString(R.string.failure), false)

                                }

                            } else {
                                if (createInvoiceDO.message.length > 0) {
                                    showAppCompatAlert(getString(R.string.info), "" + createInvoiceDO.message, getString(R.string.ok), "", getString(R.string.failure), false)
//                                    val intent = Intent()
//                                    intent.putExtra("INVOICE", createPaymentDO.salesInvoiceNumber)
//                                    setResult(10, intent)
//                                    finish()
                                } else {
                                    if(msg.isNotEmpty()){
                                        showAppCompatAlert(getString(R.string.error),msg, getString(R.string.ok), "", getString(R.string.failure), false)

                                    }else{
                                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)

                                    }
                                }

                            }

                        }
                    } else {
                        if (msg.isNotEmpty()) {
                            showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)

                        } else {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)

                        }

                    }
                }

                siteListRequest.execute()
            } else {
                //   mGetSign.isClickable=false
            }
        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }

    }

    private fun prepareInvoiceCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "")
//        var id="CDC-U101-19000091"//
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = InvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showToast(getString(R.string.unable_to_send_email))
//                        showAppCompatAlert("Error", "Unable to send Email", "Ok", "", "", false)
                        } else {
                            createPDFInvoiceDo = createPDFInvoiceDO
//                        if(createPDFInvoiceDo.email==10){
//                            createPaymentPDF(createPDFInvoiceDO)
//                        }
//                        if(createPaymentDo.print==10){
//                            printDocument(createPDFpaymentDO, PrinterConstants.PrintPaymentReport)
//                        }
                            if (createInvoiceDO.email == 10) {
                                createInvoicePDF(createPDFInvoiceDO)
                            }
//                            if (createInvoiceDO.print == 10) {
//                                if (checkFilePermission()) {
//                                    printDocument(createPDFInvoiceDO, PrinterConstants.PrintInvoiceReport)
//                                }
//                            }


                        }
                    } else {
                        showToast(getString(R.string.unable_to_send_email))

//                    showAppCompatAlert("Error", "Unable to send Email!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
//            showAppCompatAlert("Error", "Unable to send Email", "Ok", "", "Failure", false)
            }
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }


    }

    private fun checkFilePermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this@InvoiceSignatureActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@InvoiceSignatureActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), permissionRequestCode)
        } else {
            return true
        }
        return false
    }

    private fun createInvoicePDF(createPDFInvoiceDO: CreateInvoicePaymentDO) {
        BGInvoicePdf.getBuilder(this).build(createPDFInvoiceDO, "Email")
    }


    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@InvoiceSignatureActivity)
        if (printConnection.isBluetoothEnabled) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast(getString(R.string.please_enable_your_bluetooth))
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().bondedDevices
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.name
                val mac = device.address // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac)
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    override fun onBackPressed() {
        finish()

        super.onBackPressed()
    }
}