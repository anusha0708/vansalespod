package com.tbs.generic.vansales.Requests;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.alert.rajdialogs.ProgressDialog;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.PickUpDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressDialogTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.Util;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vijay Dhas on 26/05/16.
 */
public class DisplayPickupListRequest extends AsyncTask<String, Void, Boolean> {
    ArrayList<PickUpDo> pods;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    Context mContext;
    String id;
    private ProgressDialog progressDialog;


    public DisplayPickupListRequest(Context mContext, String date) {

        this.mContext = mContext;
        this.id = date;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, ArrayList<PickUpDo> pods);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String sequence = preferenceUtils.getStringFromPreference(PreferenceUtils.SEQUENCE, "");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_XLOGINSEQNO", "" + sequence);

            jsonObject.put("I_YVEHROU", id);
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.TRANSACTION_LIST, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("Delivery List xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            PickUpDo pod = null;
            pods = new ArrayList<>();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        pod = new PickUpDo();
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YSEQ")) {
                            if (text.length() > 0) {
                                pod.sequenceId = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YSHPNUM")) {
                            if (text.length() > 0) {
                                pod.shipmentNumber = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YVALFLG")) {
                            if (text.length() > 0) {
                                pod.validatedFlag = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YPRVLAT")) {
                            if (text.length() > 0) {
                                pod.prevLattitude = Double.parseDouble(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YPRVLON")) {
                            if (text.length() > 0) {
                                pod.prevLongitude = Double.parseDouble(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YAFTERLAT")) {
                            if (text.length() > 0) {
                                pod.afterLattitude = Double.parseDouble(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YAFTERLON")) {
                            if (text.length() > 0) {
                                pod.afterLongitude = Double.parseDouble(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YDEPFLG")) {
                            if (text.length() > 0) {
                                pod.departuredFlag = Integer.parseInt(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_XDOCTYP")) {
                            if (text.length() > 0) {
                                pod.docType = Integer.parseInt(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YSKIPFLG")) {
                            if (text.length() > 0) {
                                pod.skipFlag = Integer.parseInt(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YSDHCANFLG")) {
                            if (text.length() > 0) {
                                pod.cancelFlag = Integer.parseInt(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_XTRANSTYP")) {
                            if (text.length() > 0) {
                                pod.pickupDropFlag = Integer.parseInt(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YCITY")) {
                            if (text.length() > 0) {

                                pod.city = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XFLG")) {
                            if (text.length() > 0) {

                                pod.sequenceFlag = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YBP")) {
                            if (text.length() > 0) {
                                pod.customer = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YOCUR")) {
                            if (text.length() > 0) {
                                pod.currency = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YPROTYP")) {
                            if (text.length() > 0) {

                                pod.productType = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPDES")) {
                            if (text.length() > 0) {

                                pod.description = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XRDEPDATE")) {
                            if (text.length() > 0) {

                                pod.revisedDepDate = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YLEFTFLG")) {
                            if (text.length() > 0) {

                                pod.leftFlag = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XRDEPTIME")) {
                            if (text.length() > 0) {

                                pod.revisedDepTime = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XRARRDATE")) {
                            if (text.length() > 0) {

                                pod.revisedArrDate = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XRARRTIME")) {
                            if (text.length() > 0) {

                                pod.revisedArrTime = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YWGT")) {
                            if (text.length() > 0) {

                                pod.netWeight = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YVOL")) {
                            if (text.length() > 0) {

                                pod.volume = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XAPPVERSION")) {
                            if (text.length() > 0) {

                                pod.version = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YLON")) {
                            if (text.length() > 0) {

                                pod.lattitude = Double.valueOf(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YLAT")) {
                            if (text.length() > 0) {

                                pod.longitude = Double.valueOf(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YNOP")) {
                            if (text.length() > 0) {

                                pod.packages = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YDPDT")) {
                            if (text.length() > 0) {

                                pod.departureDate = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YDPTM")) {
                            if (text.length() > 0) {

                                pod.departureTime = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YARDT")) {
                            if (text.length() > 0) {

                                pod.arrivalDate = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YARTM")) {
                            if (text.length() > 0) {

                                pod.arrivalTime = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YEDIS")) {
                            if (text.length() > 0) {
                                pod.distance = Double.parseDouble(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YDISUN")) {
                            if (text.length() > 0) {

                                pod.unit = text;
                            }
                        }
                        text = "";
                    }
                    if (endTag.equalsIgnoreCase("LIN")) {
                        pods.add(pod);
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }
                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            Log.e(this.getClass().getCanonicalName(), "parseXML() : " + e.getMessage());

            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, pods);
        }
    }
}