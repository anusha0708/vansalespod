package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class RouteDO implements Serializable {

    public String routeId = "";
    public String routeName = "";
    public String date = "";
    public String routing = "";
    public String vehicleRoutingId = "";
    public String carrier = "";
    public String site = "";
    public String jobId = "";
    public String optimization = "";
    public String validated = "";
    public String trip = "";

}
