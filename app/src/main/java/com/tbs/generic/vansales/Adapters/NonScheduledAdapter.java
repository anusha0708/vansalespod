package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.CustomProductDO;
import com.tbs.generic.vansales.Model.NonSheduledProductDO;
import com.tbs.generic.vansales.R;

import java.util.ArrayList;
import java.util.List;

public class NonScheduledAdapter extends RecyclerView.Adapter<NonScheduledAdapter.MyViewHolder> {

    private List<NonSheduledProductDO> productDOS;
    private Context context;
    private ArrayList<String> customProductDOS;

    List<CustomProductDO> list;
    CustomProductDO aa;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvNumber;
        private LinearLayout llDetails;
        public RelativeLayout rlRemove, rlAdd;
        public ImageView ivRemove, ivAdd;

        public MyViewHolder(View view) {
            super(view);
            llDetails = view.findViewById(R.id.llDetails);
            tvProductName = view.findViewById(R.id.tvProductName);
            ivRemove = view.findViewById(R.id.ivRemove);
            ivAdd = view.findViewById(R.id.ivAdd);
            tvNumber = view.findViewById(R.id.tvNumber);

        }
    }


    public NonScheduledAdapter(Context context, List<NonSheduledProductDO> productDOS) {
        this.context = context;
        this.productDOS = productDOS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.non_scheduled_product_data, parent, false);
        list = new ArrayList<CustomProductDO>();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final NonSheduledProductDO productDO = productDOS.get(position);
        holder.tvProductName.setText(productDO.item+"\n"+productDO.itemDescription);
        holder.tvNumber.setText("Qty : " + productDO.quantity);


    }

    @Override
    public int getItemCount() {
        return 10;
    }

}
