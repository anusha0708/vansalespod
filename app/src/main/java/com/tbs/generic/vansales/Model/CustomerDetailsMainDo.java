package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class CustomerDetailsMainDo implements Serializable {

    public String name = "";
    public String descrption = "";
    public String number = "";

    public String address = "";
    public String currency = "";
    public String bill = "";
    public String billAddress = "";
    public String pay = "";
    public String payByAddress = "";
    public String code = "";
    public String rule = "";
    public String term = "";
    public String buisinessLine = "";
    public String lattitude = "";
    public String longitude = "";


    public void descrption(String text) {
        this.descrption = text;
    }

    public void term(String text) {
        this.term = text;
    }

    public void name(String text) {
        this.name = text;
    }

    public void address(String text) {
        this.address = text;
    }

    public void currency(String text) {
        this.currency = text;
    }

    public void bill(String text) {
        this.bill = text;
    }

    public void billAddress(String text) {
        this.billAddress = text;
    }

    public void pay(String text) {
        this.pay = text;
    }

    public void payByAddress(String text) {
        this.payByAddress = text;
    }

    public void code(String text) {
        this.code = text;
    }

    public void rule(String text) {
        this.rule = text;
    }
    public ArrayList<CustomerDetailsDo> customerDetailsDos = new ArrayList<>();


}
