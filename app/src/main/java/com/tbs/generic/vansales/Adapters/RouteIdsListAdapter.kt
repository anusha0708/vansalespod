package com.tbs.generic.vansales.Adapters

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView

import com.tbs.generic.vansales.Model.DriverDO
import com.tbs.generic.vansales.Model.LoadStockMainDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.listeners.RouteIdSelectionListener
import com.tbs.generic.vansales.utils.PreferenceUtils

import java.util.ArrayList

class RouteIdsListAdapter(private val context: Context, private val pickUpDos: ArrayList<DriverDO>, private val routeIDSelectionListener: RouteIdSelectionListener) : androidx.recyclerview.widget.RecyclerView.Adapter<RouteIdsListAdapter.MyViewHolder>() {


    inner class MyViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        var tvSiteName: TextView
//        var tvSiteId: TextView
        var tvShipments: TextView
        var tvVehicleCode: TextView
        var tvPlate: TextView
        var tvADT: TextView
        lateinit var loadStockMainDO: LoadStockMainDO

        var tvDDT: TextView
       var llDetails: LinearLayout
        lateinit var rb: RadioButton

        init {
            llDetails = view.findViewById<View>(R.id.llDetails) as LinearLayout
            tvSiteName = view.findViewById<View>(R.id.tvSiteName) as TextView
//            tvSiteId = view.findViewById<View>(R.id.tvSiteId) as TextView
            tvShipments = view.findViewById<View>(R.id.tvShipments) as TextView
            tvVehicleCode = view.findViewById<View>(R.id.tvVehicleCode) as TextView
            tvPlate = view.findViewById<View>(R.id.tvPlate) as TextView
            tvADT = view.findViewById<View>(R.id.tvADT) as TextView
            tvDDT = view.findViewById<View>(R.id.tvDDT) as TextView
            rb = view.findViewById<View>(R.id.rb) as RadioButton

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.route_id_data, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val preferenceUtils = PreferenceUtils(context)

        val siteDO = pickUpDos[position]
        holder.tvSiteName.text = context.getString(R.string.vr_id)+ " : " + siteDO.vehicleRouteId
        //  holder.tvSiteId.setText("" + siteDO.siteId);
        holder.tvVehicleCode.text =context.getString(R.string.site)+ " : " + siteDO.site
        holder.tvPlate.text = "" + siteDO.plate
        holder.tvShipments.text = context.getString(R.string.no_of_shipments)+" : " + siteDO.nShipments
        holder.tvADT.text = "" + siteDO.aDate + "  " + siteDO.aTime
        holder.tvDDT.text = "" + siteDO.dDate + "  " + siteDO.dTime



    }


    override fun getItemCount(): Int {
        return pickUpDos.size
    }

}
