package com.tbs.generic.vansales.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.ArrayList

/**
 *Created by kishoreganji on 21-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
data class UpdateDocumentDO(

        @SerializedName("addnotes") var addNotes: String? = "",


        @SerializedName("signatureEncode") var signatureEncode: String? = "",
        @SerializedName("capturedImagesList") var capturedImagesList:String? = "",
        @SerializedName("capturedImagesListBulk") var capturedImagesListBulk: ArrayList<String>? =ArrayList()

) : Serializable