package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class ActiveDeliveryMainDO implements Serializable {

    public Double vehicleCapacity = 0.0;

    public String shipmentNumber = "";
    public String customer = "";
    public String weightUnit = "";
    public String volumeUnit = "";
    public double totalWeight = 0;
    public double totalVolume = 0;
    public double totalQuantity = 0;
    public String paymentTerm = "";

    public String capturedName = "";
    public String capturedNumber = "";
    public String customerDescription = "";
    public String countryName = "";
    public String webSite = "";
    public String landLine = "";
    public String email = "";
    public String fax = "";
    public String mobile = "";
    public String lattitude = "";
    public String longitude = "";
    public String companyDescription = "";
    public String companyCode = "";
    public String remarks = "";
    public int doctype = 0;
    public int priceApproval = 0;
    public int qtyApproval = 0;

    public String signature = "";
    public String createUserName;
    public String createUserID;
    public String createdTime;
    public String createdDate="";
    public String startRouteTime = "";
    public String podTime = "";
    public String CaptureReturnsTime = "";
    public String arrivalTime = "";
    public String arraivalDate = "";
    public String departureTime = "";
    public String departureDate = "";
    public String deliveryDate = "";
    public String shipmentDate = "";

    public String timeCaptureStartRouteTime = "";
    public String timeCaptureStartRouteDate = "";
    public String logo = "";

    public String siteDescription = "";
    public String siteAddress1 = "";
    public String siteAddress2 = "";
    public String siteAddress3 = "";
    public String siteCountry = "";
    public String siteCity = "";
    public String sitePostalCode = "";
    public String siteLandLine = "";
    public String siteMobile = "";
    public String siteFax = "";
    public String siteEmail1 = "";
    public String siteEmail2 = "";
    public String siteWebEmail = "";
    public String customerStreet = "";
    public String customerLandMark = "";
    public String customerTown = "";
    public String customerCity = "";
    public Double prevLattitude =0.0;
    public Double prevLongitude =0.0;
    public Double afterLattitude =0.0;
    public Double afterLongitude =0.0;
    public String customerPostalCode = "";

    public String deliveryEmail = "";
    public String invoiceEmail = "";
    public String notes = "";

    public String paymentEmail = "";
    public String cylinderIssueEmail = "";
    public ArrayList<ActiveDeliveryDO> activeDeliveryDOS = new ArrayList<>();
    public ArrayList<ImageDO> imageDOS = new ArrayList<>();


    @Override
    public String toString() {
        return "ActiveDeliveryMainDO{" +
                "shipmentNumber='" + shipmentNumber + '\'' +
                ", customer='" + customer + '\'' +
                ", customerDescription='" + customerDescription + '\'' +
                ", countryName='" + countryName + '\'' +
                ", webSite='" + webSite + '\'' +
                ", landLine='" + landLine + '\'' +
                ", email='" + email + '\'' +
                ", fax='" + fax + '\'' +
                ", mobile='" + mobile + '\'' +
                ", lattitude='" + lattitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", startRouteTime='" + startRouteTime + '\'' +
                ", podTime='" + podTime + '\'' +
                ", CaptureReturnsTime='" + CaptureReturnsTime + '\'' +
                ", arrivalTime='" + arrivalTime + '\'' +
                ", arraivalDate='" + arraivalDate + '\'' +
                ", deliveryDate='" + deliveryDate + '\'' +
                ", timeCaptureStartRouteTime='" + timeCaptureStartRouteTime + '\'' +
                ", timeCaptureStartRouteDate='" + timeCaptureStartRouteDate + '\'' +
                ", activeDeliveryDOS=" + activeDeliveryDOS +
                '}';
    }
}
