package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class ProductDetailsDO implements Serializable {


    public String address = "";
    public String country = "";
    public String countryName = "";
    public String addressLine = "";
    public String postalCode = "";
    public String city = "";

    public String telephone = "";
    public String mobile = "";

}
