package com.tbs.generic.pod.Activitys

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ScrollView
import com.tbs.generic.vansales.Activitys.*
import com.tbs.generic.vansales.R


class MasterDataActivity : BaseActivity() {


    override fun initialize() {
      var  llCategories = layoutInflater.inflate(R.layout.master_data_layout, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
//            overridePendingTransition(R.anim.exit, R.anim.enter)
        }
        initializeControls()

    }


    override fun initializeControls() {

        tvScreenTitle.text = getString(R.string.master_data)
        var  llProducts = findViewById<View>(R.id.llProducts) as LinearLayout
        var  llCustomers = findViewById<View>(R.id.llCustomers) as LinearLayout


        llCustomers.setOnClickListener {
            val intent = Intent(this@MasterDataActivity, MasterDataCustomerActivity::class.java)
            startActivity(intent)
        }

        llProducts.setOnClickListener {
//            val intent = Intent(this@MasterDataActivity, ProductActivity::class.java)
//            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()
    }


}