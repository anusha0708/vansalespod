package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.CustomProductDO;
import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.listeners.LoadStockListener;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class CustomLoadStockAdapter extends RecyclerView.Adapter<CustomLoadStockAdapter.MyViewHolder> implements Filterable {
    private ArrayList<CustomProductDO> loadStockDOS;
    private String imageURL;
    private double weight=0;
    private double volume=0;
    ValueFilter valueFilter;
    private Context context;
    private PreferenceUtils preferenceUtils;
    private LoadStockListener loadStockListener;
    double    mass =0.0;
//
//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String charString = charSequence.toString();
//                if (charString.isEmpty()) {
//                    loadStockDOS=loadStockDOS;
//                } else {
//                    ArrayList<LoadStockDO> filteredList = new ArrayList<>();
//                    for (LoadStockDO row : loadStockDOS) {
//
//                        // name match condition. this might differ depending on your requirement
//                        // here we are looking for name or phone number match
//                        if (row.product.toLowerCase().contains(charString.toLowerCase())) {
//                            filteredList.add(row);
//                        }
//                    }
//
//                    loadStockDOS = filteredList;
//                }
//
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = loadStockDOS;
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                loadStockDOS = (ArrayList<LoadStockDO>) filterResults.values;
//                notifyDataSetChanged();
//
//            }
//        };
//    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<CustomProductDO> filterList = new ArrayList<>();
                for (int i = 0; i < loadStockDOS.size(); i++) {
                    if ((loadStockDOS.get(i).productName.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(loadStockDOS.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = loadStockDOS.size();
                results.values = loadStockDOS;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            loadStockDOS = (ArrayList<CustomProductDO>) results.values;
//            notifyDataSetChanged();
            notifyDataSetChanged();
        }

    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvDescription,tvNumber;
        public RelativeLayout rlRemove,rlAdd;
        public ImageView ivRemove,ivAdd;

        public MyViewHolder(View view) {
            super(view);
            tvProductName   = view.findViewById(R.id.tvName);
            tvDescription   = view.findViewById(R.id.tvDescription);
//            rlRemove        = (RelativeLayout) view.findViewById(R.id.rlRemove);
//            rlAdd           = (RelativeLayout) view.findViewById(R.id.rlAdd);
            ivRemove        = view.findViewById(R.id.ivRemove);
            ivAdd           = view.findViewById(R.id.ivAdd);
            tvNumber        = view.findViewById(R.id.tvNumber);
        }
    }

    public void refreshAdapter(ArrayList<CustomProductDO> loadStockDoS){

        this.loadStockDOS = loadStockDoS;

    }
    public CustomLoadStockAdapter(Context context, ArrayList<CustomProductDO> loadStockDoS) {
        this.context = context;
        this.loadStockDOS = loadStockDoS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_stock_data, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final CustomProductDO loadStockDO = loadStockDOS.get(position);
        holder.tvProductName.setText(loadStockDO.productName);
        holder.tvDescription.setText(loadStockDO.productId + " Stock:" +loadStockDO.itemCount);
        holder.tvNumber.setText(""+loadStockDO.itemCount);


        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadStockDO.itemCount  =   loadStockDO.itemCount + 1;

                holder.tvNumber.setText(""+   loadStockDO.itemCount);
              //                          mass = mass +  AppConstants.listStockDO.get(position).itemCount*AppConstants.listStockDO.get(position).stockUnit;


//                int cartCount = getCount();
//                 weight = weight+(loadStockDO.productWeight*loadStockDO.itemCount);
//                volume = volume+(loadStockDO.productWeight*loadStockDO.itemCount);
//
//                preferenceUtils.saveInt(PreferenceUtils.STOCK_COUNT,cartCount);
//
//
//                preferenceUtils.saveDouble(PreferenceUtils.WEIGHT,weight);
//                preferenceUtils.saveDouble(PreferenceUtils.VOLUME,volume);
//                loadStockListener.updateCount();


//                         count= count+1;
//                holder.tvNumber.setText(""+count);

            }
        });
        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(  loadStockDO.itemCount > 0){
                    loadStockDO.itemCount=   loadStockDO.itemCount- 1;

                    holder.tvNumber.setText(""+   loadStockDO.itemCount);
                    int cartCount = getCount();
//                    preferenceUtils.saveInt(PreferenceUtils.STOCK_COUNT,cartCount);
//                    weight = weight-(loadStockDO.productWeight*loadStockDO.itemCount);
//                    volume = volume-(loadStockDO.productWeight*loadStockDO.itemCount);
//
//                    preferenceUtils.saveInt(PreferenceUtils.STOCK_COUNT,cartCount);
//
//
//                    preferenceUtils.saveDouble(PreferenceUtils.WEIGHT,weight);
//                    preferenceUtils.saveDouble(PreferenceUtils.VOLUME,volume);
//                    loadStockListener.updateCount();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return loadStockDOS.size();
    }

    private int getCount(){

        int count = 0;
        for(LoadStockDO loadStockDO : AppConstants.listStockDO){
            if(loadStockDO.itemCount > 0){
                count = count + 1;
            }
        }
        return count;
    }

    public void filterList(ArrayList<CustomProductDO> filterdNames) {
        this.loadStockDOS = filterdNames;
        notifyDataSetChanged();
    }


}
