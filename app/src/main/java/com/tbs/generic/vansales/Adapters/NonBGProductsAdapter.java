package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.Model.ReasonMainDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.listeners.LoadStockListener;

import java.util.ArrayList;

public class NonBGProductsAdapter extends RecyclerView.Adapter<NonBGProductsAdapter.MyViewHolder> implements Filterable {
    public ArrayList<ActiveDeliveryDO> loadStockDOS;
    private String imageURL;
    private double weight = 0;
    private double volume = 0;
    ValueFilter valueFilter;
    private Context context;
    private ReasonMainDO reasonMainDo;
    private LoadStockListener loadStockListener;
    double mass = 0.0;


    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<ActiveDeliveryDO> filterList = new ArrayList<>();
                for (int i = 0; i < loadStockDOS.size(); i++) {
                    if ((loadStockDOS.get(i).product.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(loadStockDOS.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = loadStockDOS.size();
                results.values = loadStockDOS;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            loadStockDOS = (ArrayList<ActiveDeliveryDO>) results.values;
            notifyDataSetChanged();
        }

    }

    public void refreshAdapter(ArrayList<ActiveDeliveryDO> loadStockDoS) {
        this.loadStockDOS = loadStockDoS;
        notifyDataSetChanged();
    }

    public ArrayList<ActiveDeliveryDO> selectedLoadStockDOs = new ArrayList<>();
    private String from = "";
    private String damage = "";

    public ArrayList<ActiveDeliveryDO> getSelectedLoadStockDOs() {
        return selectedLoadStockDOs;
    }

    public NonBGProductsAdapter(Context context, ArrayList<ActiveDeliveryDO> loadStockDoS, String from, String damagE) {
        this.context = context;
        this.loadStockDOS = loadStockDoS;
        this.from = from;
        this.damage = damagE;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_stock_data, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ActiveDeliveryDO loadStockDO = loadStockDOS.get(position);
        holder.tvProductName.setText(loadStockDO.product);
        holder.tvDescription.setText(loadStockDO.productDescription);
        holder.tvNumberET.setText("0" );
        holder.tvAvailableQty.setText(context.getString(R.string.ordered_quantity)+" : " + loadStockDO.totalQuantity + " " + loadStockDO.unit);
        holder.cbSelected.setOnCheckedChangeListener(null);


        holder.cbSelected.setChecked(loadStockDO.isProductAdded);
        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                loadStockDO.isProductAdded = isChecked;
                if (isChecked) {
                    selectedLoadStockDOs.add(loadStockDO);
                } else {
                    selectedLoadStockDOs.remove(loadStockDO);
                }
            }
        });
        loadStockDO.orderedQuantity=0;
        int maxCount = loadStockDO.totalQuantity;//Integer.parseInt(holder.tvNumberET.getText().toString());
        final int[] quantity = {0};//{loadStockDO.orderedQuantity};//{Integer.parseInt(holder.tvNumberET.getText().toString())};
        selectedLoadStockDOs.add(loadStockDO);

        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quantity[0] < maxCount) {
                    quantity[0] = quantity[0] + 1;
                    holder.tvNumberET.setText("" + quantity[0]++);
                    loadStockDO.orderedQuantity = quantity[0];

                }
            }
        });
        holder.ivRemove.setOnClickListener(view -> {
            if (quantity[0] > 0) {
                quantity[0] = quantity[0] - 1;
                holder.tvNumberET.setText("" + quantity[0]);
                loadStockDO.orderedQuantity = quantity[0];

            }
        });
        holder.tvNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equalsIgnoreCase("")) {
                    if (Integer.parseInt(s.toString()) <= maxCount && Integer.parseInt(s.toString()) >= 0) {
                        quantity[0] = Integer.parseInt(s.toString());
                        loadStockDO.orderedQuantity = quantity[0];

                    }
                    else {
                        holder.tvNumberET.setText(""+loadStockDO.orderedQuantity);
                        if(s.length()>1){
                            final String newText = s.toString().substring(0, s.length() - 1) + "";
                            holder.tvNumberET.setText("" + newText);
                        }
                    }
                }
                else {
                    holder.tvNumberET.setText("0");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return loadStockDOS != null ? loadStockDOS.size() : 0;
    }

    private int getCount() {

        int count = 0;
        for (LoadStockDO loadStockDO : AppConstants.listStockDO) {
            if (loadStockDO.quantity > 0) {
                count = count + 1;
            }
        }
        return count;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvDescription, tvAvailableQty, tvNumber, etTotalMeterQty, tvSelection;
        public RelativeLayout rlRemove, rlAdd;
        public ImageView ivRemove, ivAdd, ivDelete;
        public LinearLayout llAddRemove, llMeterReadings, llSelectReason;
        public EditText tvNumberET, etMeterReading1, etMeterReading2;

        private CheckBox cbSelected;

        public MyViewHolder(View view) {
            super(view);
            tvProductName = view.findViewById(R.id.tvName);
            tvDescription = view.findViewById(R.id.tvDescription);
            tvNumber = view.findViewById(R.id.tvNumber);
            tvNumberET = view.findViewById(R.id.tvNumberET);
            tvAvailableQty = view.findViewById(R.id.tvAvailableQty);
//            rlRemove              = (RelativeLayout) view.findViewById(R.id.rlRemove);
//            rlAdd                 = (RelativeLayout) view.findViewById(R.id.rlAdd);
            ivRemove         = view.findViewById(R.id.ivRemove);
            ivAdd            = view.findViewById(R.id.ivAdd);
            etMeterReading1  = view.findViewById(R.id.etMeterReading1);
            etMeterReading2  = view.findViewById(R.id.etMeterReading2);
            etTotalMeterQty  = view.findViewById(R.id.etTotalMeterQty);
            ivDelete         = view.findViewById(R.id.ivDelete);
            cbSelected       = view.findViewById(R.id.cbSelected);
            llAddRemove      = view.findViewById(R.id.llAddRemove);
            llMeterReadings  = view.findViewById(R.id.llMeterReadings);
            llSelectReason   = view.findViewById(R.id.llSelectReason);
            tvSelection      = view.findViewById(R.id.tvSelection);
            if (from.equalsIgnoreCase("NONBG")){
                cbSelected.setVisibility(View.GONE);

            }else {
                cbSelected.setVisibility(View.GONE);

            }


        }
    }


}
