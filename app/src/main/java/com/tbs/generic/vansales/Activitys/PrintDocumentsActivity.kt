package com.tbs.generic.vansales.Activitys

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.pdfs.*
import com.tbs.generic.vansales.pdfs.utils.PDFConstants
import com.tbs.generic.vansales.print.FileHelper
import com.tbs.generic.vansales.print.PrintActivity
import com.tbs.generic.vansales.prints.PrinterConnection
import com.tbs.generic.vansales.prints.PrinterConstants
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.io.File
import java.io.InputStream
import java.io.OutputStream


//
class PrintDocumentsActivity : BaseActivity() {

    lateinit var btnCompleted: Button
    lateinit var btnShow: Button
    lateinit var `in`: InputStream
    lateinit var out: OutputStream
    val fileName = "data.txt"
    var company = ""
    var companyDescription = ""
    val path = Environment.getExternalStorageDirectory().absolutePath + "/instinctcoder/readwrite/"
    val TAG = FileHelper::class.java.name
    private val permissionRequestCode = 34
    lateinit var cbDeliveryEmail: ImageView
    lateinit var cbDeliveryPrint: ImageView
    lateinit var cbInvoiceEmail: ImageView
    lateinit var cbInvoicePrint: ImageView
    lateinit var cbPaymentEmail: ImageView
    lateinit var cbPaymentPrint: ImageView
    lateinit var cbCylinderEmail: ImageView
    lateinit var cbCylinderPrint: ImageView

    lateinit var cbDeliveryPreview: ImageView
    lateinit var cbInvoicePreview: ImageView
    lateinit var cbPaymentPreview: ImageView
    lateinit var cbCylinderPreview: ImageView

    lateinit var img_qr_code_image: ImageView


    lateinit var customerDo: CustomerDo
    lateinit var activeDeliverySavedDo: ActiveDeliveryMainDO
    lateinit var ShipmentType: String
    var customer = ""
    var docType = 0
    lateinit var createPDFInvoiceDo: CreateInvoicePaymentDO
    lateinit var createPDFpaymentDO: PaymentPdfDO

    override fun initialize() {
        val llCategories = layoutInflater.inflate(R.layout.print_documents, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
        }
        disableMenuWithBackButton()
        tvScreenTitle.setText(R.string.print_documents)
        activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
        ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
        docType = preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0)
        if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
            customer = customerDo.customer
        } else {
            customer = activeDeliverySavedDo.customer
        }
        initializeControls()
        company = preferenceUtils.getStringFromPreference(PreferenceUtils.COMPANY, "")
        companyDescription = preferenceUtils.getStringFromPreference(PreferenceUtils.COMPANY_DES, "")

        btnCompleted.setOnClickListener {
            Util.preventTwoClick(it)

            val value = preferenceUtils.getStringFromPreference(PreferenceUtils.DELIVERY_NOTE, "")
            val value2 = preferenceUtils.getStringFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "")

            if (value.equals("", true) && !value.equals("SUCCESS", true)) {
                if (!value2.equals("", true)) {
                    showAppCompatAlert("", getString(R.string.are_you_sure_you_want_to_exit), getString(R.string.ok), getString(R.string.cancel), getString(R.string.exit), true)
                } else {

                    showAppCompatAlert("", getString(R.string.please_complete_delivery_note), getString(R.string.ok), getString(R.string.cancel), "", false)
                }
            } else {
                if (!value2.equals("", true)) {
                    showAppCompatAlert("", getString(R.string.are_you_sure_you_want_to_exit), getString(R.string.ok), getString(R.string.cancel), getString(R.string.exit), true)

                } else if (value.equals("", true) && !value.equals("SUCCESS", true)) {
//
                    showAppCompatAlert("", getString(R.string.please_complete_delivery_note), getString(R.string.ok), getString(R.string.cancel), "", false)

                } else {
                    showAppCompatAlert("", getString(R.string.are_you_sure_you_want_to_exit), getString(R.string.ok), getString(R.string.cancel), getString(R.string.exit), true)

                }
            }
        }
    }

    override fun initializeControls() {
        btnCompleted = findViewById<View>(R.id.btnCompleted) as Button
        cbDeliveryEmail = findViewById<View>(R.id.cbDeliveryEmail) as ImageView
        cbDeliveryPrint = findViewById<View>(R.id.cbDeliveryPrint) as ImageView
        cbInvoiceEmail = findViewById<View>(R.id.cbInvoiceEmail) as ImageView
        cbInvoicePrint = findViewById<View>(R.id.cbInvoicePrint) as ImageView
        cbPaymentEmail = findViewById<View>(R.id.cbPaymentEmail) as ImageView
        cbPaymentPrint = findViewById<View>(R.id.cbPaymentPrint) as ImageView
        cbCylinderEmail = findViewById<View>(R.id.cbCylinderEmail) as ImageView
        cbCylinderPrint = findViewById<View>(R.id.cbCylinderPrint) as ImageView

        cbDeliveryPreview = findViewById<View>(R.id.cbDeliveryPreview) as ImageView
        cbInvoicePreview = findViewById<View>(R.id.cbInvoicePreview) as ImageView
        cbPaymentPreview = findViewById<View>(R.id.cbPaymentPreview) as ImageView
        cbCylinderPreview = findViewById<View>(R.id.cbCylinderPreview) as ImageView
        img_qr_code_image = findViewById<View>(R.id.img_qr_code_image) as ImageView

        btnShow = findViewById<View>(R.id.btnShow) as Button

        btnShow.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this@PrintDocumentsActivity, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        }

        cbDeliveryPreview.setOnClickListener {
            Util.preventTwoClick(it)
            if (Util.isNetworkAvailable(this)) {
//                prePrepareDeliveryNoteCreation()
                val intent = Intent(this@PrintDocumentsActivity, DocumentPreviewActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
            } else {
                showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

            }

        }

        cbInvoicePreview.setOnClickListener {
            Util.preventTwoClick(it)
            if (Util.isNetworkAvailable(this)) {

                prePrepareInvoiceCreation()
            } else {
                showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

            }

        }
        cbPaymentPreview.setOnClickListener {
            Util.preventTwoClick(it)
            if (Util.isNetworkAvailable(this)) {

                preParePaymentCreation()
            } else {
                showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

            }

        }
        cbCylinderPreview.setOnClickListener {
            Util.preventTwoClick(it)
            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.FixedQuantityProduct)) {
                if (Util.isNetworkAvailable(this)) {
                    prePareCylinderIssueNoteCreation()

                } else {
                    showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

                }

            }
        }

        cbDeliveryEmail.setOnClickListener {
            Util.preventTwoClick(it)
            if (docType == 1) {
                authorization(6)
            } else if (docType == 2) {
                authorization(15)
            } else if (docType == 3) {
                authorization(12)
            } else if (docType == 4) {
                authorization(3)
            } else if (docType == 5) {
                authorization(18)
            } else if (docType == 6) {
                authorization(21)
            } else {
                authorization(0)
            }
        }
        cbDeliveryPrint.setOnClickListener {
            Util.preventTwoClick(it)
            if (docType == 1) {
                authorization(5)
            } else if (docType == 2) {
                authorization(14)
            } else if (docType == 3) {
                authorization(11)
            } else if (docType == 4) {
                authorization(2)
            } else if (docType == 5) {
                authorization(17)
            } else if (docType == 6) {
                authorization(20)
            } else {
                authorization(0)
            }
        }
        cbInvoiceEmail.setOnClickListener {
            Util.preventTwoClick(it)
            authorization(9)
        }
        cbInvoicePrint.setOnClickListener {
            Util.preventTwoClick(it)
            authorization(8)
        }

        cbPaymentEmail.setOnClickListener {
            Util.preventTwoClick(it)
            authorization(24)
        }
        cbPaymentPrint.setOnClickListener {
            Util.preventTwoClick(it)
            authorization(23)
        }
        cbCylinderEmail.setOnClickListener {
            Util.preventTwoClick(it)
            authorization(0)
        }
        cbCylinderPrint.setOnClickListener {
            Util.preventTwoClick(it)
            authorization(0)
        }

    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@PrintDocumentsActivity)
        if (printConnection.isBluetoothEnabled) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showAppCompatAlert("", getString(R.string.please_enable_your_bluetooth), getString(R.string.enable), getString(R.string.cancel), "EnableBluetooth", false)
        }
    }


    fun authorization(type: Int) {
        customerAuthorization(customer, type, ResultListner { `object`, isSuccess ->
            if (isSuccess) {
                if (type == 3 || type == 21 || type == 6 || type == 12 || type == 15 || type == 18) {
                    //delivery email
                    prepareDeliveryNoteCreation("Email")
                } else if (type == 5 || type == 11 || type == 14 || type == 17 || type == 20 || type == 2) {
                    //delivery print
                    prepareDeliveryNoteCreation("Print")

                } else if (type == 24) {
                    //payment email
                    preparePaymentCreation("Email")

                } else if (type == 9) {
                    //sales invoice email
                    prepareInvoiceCreation("Email")

                } else if (type == 8) {
                    //sales invoice print
                    prepareInvoiceCreation("Print")

                } else if (type == 23) {
                    //payment print
                    preparePaymentCreation("Print")

                } else {
                    showAlert(resources.getString(R.string.not_authorized))

                }

            } else {
                showAlert(resources.getString(R.string.not_authorized))
            }
        })
    }

    private fun checkFilePermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this@PrintDocumentsActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@PrintDocumentsActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), permissionRequestCode)
        } else {
            return true
        }
        return false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == permissionRequestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createInvoicePDF(createPDFInvoiceDo)
            } else {
                Log.e(TAG, "WRITE_PERMISSION_DENIED")
            }
        }

    }

    private fun printDocuments(view: View) {
        startActivity(Intent(this@PrintDocumentsActivity, PrintActivity::class.java))
    }

    private fun createInvoicePDF(createPDFInvoiceDO: CreateInvoicePaymentDO) {
        BGInvoicePdf.getBuilder(this).build(createPDFInvoiceDO, "Email")
    }

    private fun prepareDeliveryNoteCreation(from: String) {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {

                val driverListRequest = ActiveDeliveryRequest(id, this@PrintDocumentsActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (activeDeliveryDo != null) {
                        if (isError) {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.delivery_note_pdf_error), getString(R.string.ok), "", "", false)
                        } else {
                            if (from.equals("Print", true)) {
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                    printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes)
                                } else {
                                    printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes)
                                }
                            } else {
                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                    BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                } else {
                                    CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                }
                            }
                        }
                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)
                    }
                }
                driverListRequest.execute()

            } else {
                showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

            }

        } else {
            if (spotSaleDeliveryId.length > 0) {
                if (Util.isNetworkAvailable(this)) {

                    val driverListRequest = ActiveDeliveryRequest(spotSaleDeliveryId, this@PrintDocumentsActivity)
                    driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                        hideLoader()
                        if (activeDeliveryDo != null) {
                            if (isError) {
                                showAppCompatAlert(getString(R.string.error), getString(R.string.delivery_note_pdf_error), getString(R.string.ok), "", "", false)
                            } else {
                                if (from.equals("Print", true)) {
                                    if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                        printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes)
                                    } else {
                                        printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes)
                                    }
                                } else {
                                    if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                        BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                    } else {
                                        CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                    }
                                }
                            }
                        } else {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)
                        }
                    }
                    driverListRequest.execute()
                } else {
                    showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

                }

            } else {
                showAppCompatAlert(getString(R.string.error), getString(R.string.please_create_del), getString(R.string.ok), "", "", false)

            }

        }
    }

    private fun prepareInvoiceCreation(from: String) {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID, "")
//        var id="CDC-U101-19000091"//
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = InvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.invoice_pdf_error), getString(R.string.ok), "", "", false)
                        } else {
                            createPDFInvoiceDo = createPDFInvoiceDO
                            if (from.equals("Print", true)) {

                                printDocument(createPDFInvoiceDO, PrinterConstants.PrintInvoiceReport)
                            } else {
                                if (checkFilePermission()) {
                                    createInvoicePDF(createPDFInvoiceDO)
                                }
                            }
                        }
                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
                showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

            }


        } else {
            showAppCompatAlert(getString(R.string.error), getString(R.string.no_invoice_id_found), getString(R.string.ok), "", getString(R.string.failure), false)
        }
    }

    private fun preparePaymentCreation(from: String) {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
//        id = "SD-U101-19000886";
        if (id.length > 0) {
            val siteListRequest = PDFPaymentDetailsRequest(id, this)
            val listener = PDFPaymentDetailsRequest.OnResultListener { isError, createPDFInvoiceDO ->
                hideLoader()
                if (createPDFInvoiceDO != null) {
                    if (isError) {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.payment_pdf_id_error), getString(R.string.ok), "", "", false)
                    } else {
                        createPDFpaymentDO = createPDFInvoiceDO
                        if (from.equals("Print", true)) {
                            printDocument(createPDFpaymentDO, PrinterConstants.PrintPaymentReport)
                        } else {
                            createPaymentPDF(createPDFInvoiceDO)
                        }
                    }
                } else {
                    showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)
                }
            }
            siteListRequest.setOnResultListener(listener)
            siteListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.error), getString(R.string.no_payment_id_found), getString(R.string.ok), "", "", false)

        }

    }

    private fun prepareCylinderIssueNoteCreation(from: String) {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

//        val id="SD-U101-19000847"
        if (id.length > 0) {
            val driverListRequest = CylinderIssueRequest(id, this@PrintDocumentsActivity)
            val onResultListener = OnResultListener(from)
            driverListRequest.setOnResultListener(onResultListener)
            driverListRequest.execute()
        } else {
            if (spotSaleDeliveryId.length > 0) {
                val driverListRequest = CylinderIssueRequest(spotSaleDeliveryId, this@PrintDocumentsActivity)
                val onResultListener = OnResultListener(from)
                driverListRequest.setOnResultListener(onResultListener)
                driverListRequest.execute()
            } else {
                showAppCompatAlert(getString(R.string.error), getString(R.string.please_create_del), getString(R.string.ok), "", "", false)

            }

        }
    }

    private fun OnResultListener(from: String): OnResultListener {
        return OnResultListener { isError, cylinderIssueMainDO ->
            if (cylinderIssueMainDO != null) {
                if (isError) {
                    showAppCompatAlert(getString(R.string.error),getString(R.string.delivery_note_pdf_error), getString(R.string.ok), "", "", false)
                } else {
                    if (from.equals("Print", true)) {
                        printDocument(cylinderIssueMainDO, PrinterConstants.PrintCylinderIssue)
                    } else {
                        CylinderIssRecPdfBuilder.getBuilder(this@PrintDocumentsActivity).build(cylinderIssueMainDO, "Email")
                    }
                }
            } else {
                showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)

            }
        }

    }

    private fun createPaymentPDF(createPDFInvoiceDO: PaymentPdfDO) {
        if (createPDFInvoiceDO != null) {
            ReceiptPDF(this).createReceiptPDF(createPDFInvoiceDO, "Email")
            return
        } else {
            //Display error message
        }
    }

    private fun createCylinderIssuePDF(createPDFInvoiceDO: ActiveDeliveryMainDO) {
        val loanReturnDOS = StorageManager.getInstance(this).getReturnCylinders(this@PrintDocumentsActivity)
        val deliveryDos = StorageManager.getInstance(this).getCurrentDeliveryItems(this@PrintDocumentsActivity)

        if (deliveryDos != null && deliveryDos.size > 0) {
            createPDFInvoiceDO.activeDeliveryDOS.addAll(deliveryDos)
        }
        if (loanReturnDOS != null && loanReturnDOS.size > 0) {
            for (i in loanReturnDOS.indices) {
                val activeDeliveryD0 = ActiveDeliveryDO()
                activeDeliveryD0.productDescription = loanReturnDOS.get(i).productDescription
                activeDeliveryD0.totalQuantity = loanReturnDOS.get(i).qty as Int
                activeDeliveryD0.receivedQuantity = loanReturnDOS.get(i).qty as Int

                createPDFInvoiceDO.activeDeliveryDOS.add(activeDeliveryD0)
            }
        }

        //val builder = CylinderIssRecPdfBuilder.getBuilder(this@PrintDocumentsActivity).build(createPDFInvoiceDO)
    }

    override fun onButtonNoClick(from: String) {

    }

    override fun onButtonYesClick(from: String) {
        if (getString(R.string.exit).equals(from, ignoreCase = true)) {
            setResult(3, null)
            finish()
        } else if (getString(R.string.failure).equals(from, ignoreCase = true)) {

        } else if (from.equals("EnableBluetooth", true)) {
//            val printConnection = PrinterConnection.getInstance(this@PrintDocumentsActivity);
//            printConnection.enableBluetooth()
//            cbDeliveryPrint.isChecked = false;
            startActivity(Intent(Settings.ACTION_BLUETOOTH_SETTINGS))
        }
    }

    private fun prePrepareDeliveryNoteCreation() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

        if (id.length > 0) {
            val driverListRequest = ActiveDeliveryRequest(id, this@PrintDocumentsActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                if (activeDeliveryDo != null) {
                    if (isError) {
                        showAppCompatAlert(getString(R.string.error),getString(R.string.delivery_note_pdf_error), getString(R.string.ok), "", "", false)
                    } else {
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                            BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Preview").CreatePDF().execute()
                        } else {
                            CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Preview").CreatePDF().execute()
                        }
                    }
                } else {
                    showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)

                }
            }
            driverListRequest.execute()
        } else {
            if(spotSaleDeliveryId.isNotEmpty()){
                val driverListRequest = ActiveDeliveryRequest(spotSaleDeliveryId, this@PrintDocumentsActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (activeDeliveryDo != null) {
                        if (isError) {
                            showAppCompatAlert(getString(R.string.error),getString(R.string.delivery_note_pdf_error), getString(R.string.ok), "", "", false)
                        } else {
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Preview").CreatePDF().execute()
                            } else {
                                CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Preview").CreatePDF().execute()
                            }
                        }
                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)

                    }
                }
                driverListRequest.execute()
            }else{
                showAppCompatAlert(getString(R.string.error),getString(R.string.document_not_found), getString(R.string.ok), "", getString(R.string.failure), false)

            }

        }
    }

    private fun prePrepareInvoiceCreation() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID, "")
//        var id="CNV-U102-19000144"
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {

                val siteListRequest = InvoiceDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showAppCompatAlert(getString(R.string.error),getString(R.string.invoice_pdf_error), getString(R.string.ok), "", "", false)

                        } else {
                            createPDFInvoiceDo = createPDFInvoiceDO

                            if (checkFilePermission()) {
                                BGInvoicePdf.getBuilder(this).createPDF(createPDFInvoiceDO, "Preview")
                            }

                        }
                    } else {

                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
                showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), "OK", "", "", false)

            }

        } else {
            showAppCompatAlert(getString(R.string.error),getString(R.string.no_invoice_id_found), getString(R.string.ok), "", getString(R.string.failure), false)

        }

    }

    private fun preParePaymentCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
//        var id ="RCSH-U1L1900138"
        if (id.length > 0) {
            val siteListRequest = PDFPaymentDetailsRequest(id, this)
            siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                hideLoader()
                if (createPDFInvoiceDO != null) {
                    if (isError) {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.no_payment_id_found),getString(R.string.ok), "", "", false)
                    } else {
                        createPDFpaymentDO = createPDFInvoiceDO
                        if (createPDFInvoiceDO != null) {
                            ReceiptPDF(this).preparePDF(createPDFInvoiceDO, "Preview")
                        } else {
                            //Display error message
                        }
                    }
                } else {
                    showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)


                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.error), getString(R.string.no_payment_id_found),getString(R.string.ok), "", "", false)

        }

    }


    private fun prePareCylinderIssueNoteCreation() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

//        var id="SD-U101-19000847"
        if (id.length > 0) {
            val driverListRequest = CylinderIssueRequest(id, this@PrintDocumentsActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (activeDeliveryDo != null) {
                    if (isError) {
                        showAppCompatAlert(getString(R.string.error),getString(R.string.delivery_note_pdf_error), getString(R.string.ok), "", "", false)
                    } else {
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.FixedQuantityProduct)) {
                            CylinderIssRecPdfBuilder.getBuilder(this@PrintDocumentsActivity).createPDF(activeDeliveryDo, "Preview")
                        }

                    }
                } else {
                    showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)

                }
            }
            driverListRequest.execute()

        } else {
            if (spotSaleDeliveryId.length > 0) {
                val driverListRequest = CylinderIssueRequest(spotSaleDeliveryId, this@PrintDocumentsActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (activeDeliveryDo != null) {
                        if (isError) {
                            showAppCompatAlert(getString(R.string.error),getString(R.string.delivery_note_pdf_error), getString(R.string.ok), "", "", false)
                        } else {

                            CylinderIssRecPdfBuilder.getBuilder(this@PrintDocumentsActivity).createPDF(activeDeliveryDo, "Preview")

                        }
                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)

                    }
                }
                driverListRequest.execute()

            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().bondedDevices
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.name
                val mac = device.address // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac)
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

}