package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tbs.generic.vansales.Activitys.ActiveDeliveryActivity;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.Cancel_RescheduleActivity;
import com.tbs.generic.vansales.Activitys.EditDocumentScreen;
import com.tbs.generic.vansales.Activitys.LoginActivity;
import com.tbs.generic.vansales.Activitys.TransactionList;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.PickUpDo;
import com.tbs.generic.vansales.Model.UpdateDocumentDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.Requests.ActiveDeliveryRequest;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.CalendarUtils;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;

import java.util.ArrayList;

import static com.itextpdf.awt.geom.Point2D.distance;

public class VehicleRouteAdapter extends RecyclerView.Adapter<VehicleRouteAdapter.MyViewHolder> implements LocationListener {
    private static Activity context;

    private ArrayList<PickUpDo> productDOS;
    Double lat, lng;
    double dist;
    String arrivalTime2, departureTime2;
    String aMonth, ayear, aDate, dMonth, dyear, dDate;
    private ArrayList<ActiveDeliveryDO> activeDeliveryDOs;

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();
        ((BaseActivity) context).preferenceUtils = new PreferenceUtils(context);


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvCount, tvTop, tvAddress, tvTime, tvDistance, tvShipmentNumber, tvTimeDifference;
        private LinearLayout llDetails, llback;
        private ImageView ivLocation, ivDone;
        private Button btnReschedule;
        ImageView btnType;

        public MyViewHolder(View view) {
            super(view);
            llDetails = view.findViewById(R.id.llDetails);
            tvName = view.findViewById(R.id.tvName);
            tvCount = view.findViewById(R.id.tvCount);
            tvDistance = view.findViewById(R.id.tvDistance);
            tvTimeDifference = view.findViewById(R.id.tvTimeDifference);

            tvShipmentNumber = view.findViewById(R.id.tvShipmentNumber);
            tvTop = view.findViewById(R.id.tvTop);
            tvAddress = view.findViewById(R.id.tvAddress);
            tvTime = view.findViewById(R.id.tvTime);
            ivDone = view.findViewById(R.id.ivDone);
            ivLocation = view.findViewById(R.id.ivLocation);
            llback = view.findViewById(R.id.ll_back);
            btnReschedule = view.findViewById(R.id.btnReschedule);
            btnType = view.findViewById(R.id.btnType);


        }
    }


    public VehicleRouteAdapter(Activity context, ArrayList<PickUpDo> pickUpDos) {
        this.context = context;
        this.productDOS = pickUpDos;
        for (int i = 0; i < pickUpDos.size(); i++) {
            if (pickUpDos.get(i).departuredFlag != 2
                    && pickUpDos.get(i).skipFlag != 2 && pickUpDos.get(i).cancelFlag != 30 && pickUpDos.get(i).leftFlag != 2) {
                AppConstants.EnableShipments = pickUpDos.get(i).shipmentNumber;
                break;
            }
        }
        productDOS = pickUpDos;
    }

    public void refreshAdapter(ArrayList<PickUpDo> pickUpDos) {
        for (int i = 0; i < pickUpDos.size(); i++) {
            if (pickUpDos.get(i).departuredFlag != 2
                    && pickUpDos.get(i).skipFlag != 2 && pickUpDos.get(i).cancelFlag != 30 && pickUpDos.get(i).leftFlag != 2) {
                AppConstants.EnableShipments = pickUpDos.get(i).shipmentNumber;
                break;
            }
        }
        productDOS = pickUpDos;
        notifyDataSetChanged();
    }

    @SuppressLint("MissingPermission")
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_data, parent, false);
//        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//        lat = location.getLongitude();
//        lng = location.getLatitude();
//        double lat2 = Double.valueOf(((BaseActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE, 0.0));
//        double lng3 = Double.valueOf(((BaseActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE, 0.0));
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final PickUpDo productDO = productDOS.get(position);

        holder.tvName.setText(productDO.customer + "\n" + productDO.city);
        holder.tvAddress.setText(productDO.shipmentNumber);
        holder.tvShipmentNumber.setText("" + productDO.description);


        holder.tvDistance.setText("" + productDO.distance + " " + productDO.unit);

        String mins = CalendarUtils.revisedTimeDifference(productDO.arrivalDate, productDO.arrivalTime, productDO.revisedArrDate, productDO.revisedArrTime);
        if (!mins.isEmpty()) {
            if (mins.contains("Delay")) {
                holder.tvTimeDifference.setTextColor(context.getResources().getColor(R.color.red));
            } else {
                holder.tvTimeDifference.setTextColor(context.getResources().getColor(R.color.md_green));

            }
            holder.tvTimeDifference.setText("" + mins);
            holder.tvTimeDifference.setVisibility(View.VISIBLE);

        } else {
            holder.tvTimeDifference.setVisibility(View.GONE);
        }

        PreferenceUtils preferenceUtils = new PreferenceUtils(context);
        if (productDO.pickupDropFlag == 1) {
            holder.btnType.setBackground(context.getResources().getDrawable(R.drawable.pick_48));

        } else {
            holder.btnType.setBackground(context.getResources().getDrawable(R.drawable.drop_48));


        }

        if (productDO.arrivalTime.length() > 0 && productDO.departureTime.length() > 0) {
            String arrivalTime = productDO.arrivalTime.substring(Math.max(productDO.arrivalTime.length() - 2, 0));
            String departureTime = productDO.departureTime.substring(Math.max(productDO.departureTime.length() - 2, 0));

            arrivalTime2 = productDO.arrivalTime.substring(0, 2);
            departureTime2 = productDO.departureTime.substring(0, 2);
            holder.tvTime.setText("ETA : " + arrivalTime2 + ":" + arrivalTime + "  ETD : " + departureTime2 + ":" + departureTime);

            preferenceUtils.saveString(PreferenceUtils.DOC_DD, dDate + "-" + dMonth + "-" + dyear);
            preferenceUtils.saveString(PreferenceUtils.DOC_DT, departureTime2 + ":" + departureTime);


            preferenceUtils.saveString(PreferenceUtils.SHIPMENT_DT, dDate + "-" + dMonth + "-" + dyear + "  " + departureTime2 + ":" + departureTime);
            preferenceUtils.saveString(PreferenceUtils.SHIPMENT_AT, aDate + "-" + aMonth + "-" + ayear + "  " + arrivalTime2 + ":" + arrivalTime);

        }
        if (productDOS.get(position).arrivalDate.length() > 0) {
            aMonth = productDOS.get(position).arrivalDate.substring(4, 6);
            ayear = productDOS.get(position).arrivalDate.substring(0, 4);
            aDate = productDOS.get(position).arrivalDate.substring(Math.max(productDOS.get(position).arrivalDate.length() - 2, 0));

        }
        if (productDOS.get(position).departureDate.length() > 0) {

            dMonth = productDOS.get(position).departureDate.substring(4, 6);
            dyear = productDOS.get(position).departureDate.substring(0, 4);
            dDate = productDOS.get(position).departureDate.substring(Math.max(productDOS.get(position).departureDate.length() - 2, 0));
        }
        preferenceUtils.saveDouble(PreferenceUtils.LATTITUDE, productDOS.get(position).lattitude);
        preferenceUtils.saveDouble(PreferenceUtils.LONGITUDE, productDOS.get(position).longitude);
        preferenceUtils.saveString(PreferenceUtils.PHONE, String.valueOf(productDOS.get(position).sequenceId));
        preferenceUtils.saveString(PreferenceUtils.EMAIL, String.valueOf(productDOS.get(position).netWeight));

        int pos = position + 1;
        if (pos == 1) {
            //   holder.tvTop.setVisibility(View.GONE);
        } else
            holder.tvTop.setVisibility(View.VISIBLE);

        String shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "");
        holder.llback.setBackground(ContextCompat.getDrawable(context, R.drawable.card_white));
        if (shipmentId.equalsIgnoreCase(productDO.shipmentNumber) ||
                (shipmentId.equalsIgnoreCase("") && (productDO.skipFlag == 2
                        || AppConstants.EnableShipments.equalsIgnoreCase(productDO.shipmentNumber)))) {
            if (AppConstants.EnableShipments.equalsIgnoreCase(productDO.shipmentNumber)) {
                holder.llback.setBackground(ContextCompat.getDrawable(context, R.drawable.card_white_green_boarder));
            }

        }

        if (productDO.departuredFlag == 2 || productDO.cancelFlag == 30) {
            holder.ivDone.setImageResource(R.drawable.done_tick_green);
            holder.btnReschedule.setVisibility(View.GONE);
            holder.tvTimeDifference.setVisibility(View.GONE);

        } else if (productDO.skipFlag == 2) {
            holder.ivDone.setImageResource(R.drawable.skip_icon);
            holder.llback.setBackground(ContextCompat.getDrawable(context, R.drawable.card_white));
            holder.btnReschedule.setVisibility(View.VISIBLE);
            holder.tvTimeDifference.setVisibility(View.GONE);


        } else if (productDO.leftFlag == 2) {
            holder.ivDone.setImageResource(R.drawable.skip_icon);
            holder.ivDone.setColorFilter(ContextCompat.getColor(context,
                    R.color.red));


        } else {
            holder.btnReschedule.setVisibility(View.VISIBLE);
            holder.tvTimeDifference.setVisibility(View.VISIBLE);

            if (AppConstants.EnableShipments.equalsIgnoreCase(productDO.shipmentNumber)) {//preferenceUtils.getStringFrom(PreferenceUtils.SHIPMENT, "");
                Animation vibrateAnimation = AnimationUtils.loadAnimation(context, R.anim.shake_animation);
                holder.ivLocation.setImageResource(R.drawable.marker_green);
                holder.ivLocation.startAnimation(vibrateAnimation);
                holder.ivDone.setImageResource(R.drawable.check_mark);
            } else {
                holder.ivLocation.clearAnimation();
                holder.ivLocation.setImageResource(R.drawable.markers);
                holder.ivDone.setImageResource(R.drawable.check_mark);
            }
        }
        holder.tvCount.setText(productDO.sequenceId + "");
        pos++;
        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productDO.version.equals(context.getResources().getString(R.string.app_version))) {
                    if (productDO.sequenceFlag == 30) {
                        ((BaseActivity) context).showToast(context.getResources().getString(R.string.force_logout));
                        preferenceUtils.removeFromPreference(PreferenceUtils.DRIVER_ID);
                        preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_SUCCESS);
                        StorageManager.getInstance(context).deleteSpotSalesCustomerList((TransactionList) context);
                        AppPrefs.clearPref(context);
                        clearDataLocalData();

                    } else {
                        String shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "");
                        if (productDO.departuredFlag == 2 || productDO.cancelFlag == 30) {
                            showShipmentDetails(productDO);
                        } else if (shipmentId.equalsIgnoreCase(productDO.shipmentNumber) ||
                                (shipmentId.equalsIgnoreCase("") && (productDO.skipFlag == 2
                                        || productDO.leftFlag == 2 || AppConstants.EnableShipments.equalsIgnoreCase(productDO.shipmentNumber)))) {
                            PreferenceUtils preferenceUtils = ((BaseActivity) context).preferenceUtils;
                            preferenceUtils.saveDouble(PreferenceUtils.LATTITUDE, productDO.lattitude);
                            preferenceUtils.saveDouble(PreferenceUtils.LONGITUDE, productDO.longitude);
                            Intent intent = new Intent(context, ActiveDeliveryActivity.class);
                            intent.putExtra("SHIPMENT_ID", productDO.shipmentNumber);
//                  intent.putExtra("Code", productDO.vehicleRoutingId);
                            preferenceUtils.saveString(PreferenceUtils.CUSTOMER, productDO.customer);
                            preferenceUtils.saveString(PreferenceUtils.CUSTOMER_NAME, productDO.description);
                            intent.putExtra("Scheduled_customer", productDO.customer);
                            preferenceUtils.saveDouble(PreferenceUtils.DISTANCE, productDO.distance);
                            intent.putExtra("DISTANCE", productDO.distance);
                            preferenceUtils.saveString(PreferenceUtils.CURRENCY, "" + productDO.currency);
                            preferenceUtils.saveInt(PreferenceUtils.PICKUP_DROP_FLAG, productDO.pickupDropFlag);
                            preferenceUtils.saveString(PreferenceUtils.ETA_ETD, holder.tvTime.getText().toString());


                            preferenceUtils.saveInt(PreferenceUtils.DOC_TYPE, productDO.docType);
                            preferenceUtils.saveString(PreferenceUtils.SHIPMENT, productDO.shipmentNumber);
                            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentProductsType, productDO.productType);
                            preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_scheduled));
                            ((BaseActivity) context).startActivityForResult(intent, 19);


                        } else {
                            ((BaseActivity) context).showToast(context.getString(R.string.please_process_the_current_shipment));
                        }
                    }


                } else {
                    ((TransactionList) context).showAppCompatAlert("" + context.getResources().getString(R.string.login_info), "" + context.getResources().getString(R.string.playstore_messsage), "" + context.getResources().getString(R.string.ok), "Cancel", "PLAYSTORE", true);
                }
            }
        });
        if (position == (getItemCount() - 1)) {
            ((TransactionList) context).hideLoader();
        }
        holder.btnReschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shipmentId.equalsIgnoreCase(productDO.shipmentNumber) ||
                        (shipmentId.equalsIgnoreCase("") && (productDO.skipFlag == 2
                                || productDO.leftFlag == 2 || AppConstants.EnableShipments.equalsIgnoreCase(productDO.shipmentNumber)))) {
                    preferenceUtils.saveInt(PreferenceUtils.DOC_TYPE, productDO.docType);
                    preferenceUtils.saveString(PreferenceUtils.SHIPMENT, productDO.shipmentNumber);
                    Intent intent = new Intent(context, Cancel_RescheduleActivity.class);
                    intent.putExtra("TYPE", 1);
                    context.startActivityForResult(intent, 511);
                } else {
                    ((BaseActivity) context).showToast(context.getString(R.string.please_process_the_current_shipment));

                }

            }
        });
    }

    @Override
    public int getItemCount() {
        if (productDOS.size() > 0) {
            return productDOS.size();
        } else {

            return 0;
        }
    }

    private void showShipmentDetails(PickUpDo productDO) {
        Dialog shipmentDetailsDialog = new Dialog(context);
        shipmentDetailsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        shipmentDetailsDialog.setContentView(R.layout.shipment_details_dialog_layout);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((BaseActivity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        shipmentDetailsDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        shipmentDetailsDialog.setCancelable(true);
        shipmentDetailsDialog.setCanceledOnTouchOutside(true);
        TextView tvShipmentNumber = shipmentDetailsDialog.findViewById(R.id.tvShipmentNumber);
        TextView tvCustomerName = shipmentDetailsDialog.findViewById(R.id.tvCustomerName);
        ProgressBar progressBar = shipmentDetailsDialog.findViewById(R.id.progress_bar);
        ListView lvItems = shipmentDetailsDialog.findViewById(R.id.lvItems);
//        TextView tvDistance = shipmentDetailsDialog.findViewById(R.id.tvDistance);
//        TextView tvProductType = shipmentDetailsDialog.findViewById(R.id.tvProductType);
//        TextView tvDescription = shipmentDetailsDialog.findViewById(R.id.tvDescription);
        TextView tvClose = shipmentDetailsDialog.findViewById(R.id.tvClose);
        TextView tvStatus = shipmentDetailsDialog.findViewById(R.id.tvStatus);

//
        tvShipmentNumber.setText("" + productDO.shipmentNumber);
        tvCustomerName.setText("" + productDO.description);
//        tvCity.setText("" + productDO.city);
//        tvDistance.setText("" + productDO.distance);
//        tvProductType.setText("" + productDO.productType);
//        tvDescription.setText("" + productDO.description);
        if(productDO.cancelFlag==30){
            tvClose.setVisibility(View.GONE);
            tvStatus.setText(context.getResources().getString(R.string.this_doc_has_been_recheduled));

        }else {
            tvClose.setVisibility(View.VISIBLE);

            tvStatus.setText(context.getResources().getString(R.string.this_doc_has_been_proceed));

        }
        ((BaseActivity) context).preferenceUtils.saveInt(PreferenceUtils.DOC_TYPE, productDO.docType);

        if (Util.isNetworkAvailable(context)) {
            progressBar.setVisibility(View.VISIBLE);
            lvItems.setVisibility(View.GONE);
            ActiveDeliveryRequest driverListRequest = new ActiveDeliveryRequest(productDO.shipmentNumber, context);
            driverListRequest.setOnResultListener(new ActiveDeliveryRequest.OnResultListener() {
                @Override
                public void onCompleted(boolean isError, ActiveDeliveryMainDO activeDeliveryMainDO) {
                    progressBar.setVisibility(View.GONE);
                    if (isError) {
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                    } else {
                        lvItems.setVisibility(View.VISIBLE);
                        activeDeliveryDOs = activeDeliveryMainDO.activeDeliveryDOS;
                        ProductListAdapter productListAdapter = new ProductListAdapter(context, activeDeliveryMainDO.activeDeliveryDOS);
                        lvItems.setAdapter(productListAdapter);
                        StorageManager.getInstance(context).saveUpdateActiveDeliveryMainDo((TransactionList) context, activeDeliveryMainDO);
                        UpdateDocumentDO updateDocumentDO = StorageManager.getInstance(context).getUpdateData(context);
                        updateDocumentDO.setAddNotes(activeDeliveryMainDO.notes);
                        ;
                        updateDocumentDO.setSignatureEncode(activeDeliveryMainDO.signature);
                        ;
                        StorageManager.getInstance(context).saveUpdateData((TransactionList) context, updateDocumentDO);
                    }
                }
            });
            driverListRequest.execute();
        } else {
            ((BaseActivity) context).showToast(context.getString(R.string.no_internet));
        }
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                shipmentDetailsDialog.dismiss();
//
                if (productDO.docType == 2) {
                    ((BaseActivity) context).showToast(context.getString(R.string.sta_progress));
                } else {
                    Intent intent = new Intent(context, EditDocumentScreen.class);
                    intent.putExtra("UPDATE_DOC", productDO.shipmentNumber);
                    context.startActivity(intent);
                }


            }
        });
        shipmentDetailsDialog.show();
    }

    private class ProductListAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<ActiveDeliveryDO> activeDeliveoS;

        private ProductListAdapter(Context context, ArrayList<ActiveDeliveryDO> activeDeliveryDOS) {
            this.context = context;
            this.activeDeliveoS = activeDeliveryDOS;
        }

        @Override
        public int getCount() {
            return activeDeliveoS != null ? activeDeliveoS.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = LayoutInflater.from(context).inflate(R.layout.shipment_detail_data, null);
            TextView tvProductName = view.findViewById(R.id.tvName);
            TextView tvDescription = view.findViewById(R.id.tvDescription);
            TextView tvNumber = view.findViewById(R.id.tvNumber);
            TextView tvAvailableQty = view.findViewById(R.id.tvAvailableQty);
//            tvProductName.setText(activeDeliveoS.get(position).product);
            tvDescription.setText("" + activeDeliveoS.get(position).productDescription);
            tvNumber.setText("" + activeDeliveoS.get(position).totalQuantity + " " + activeDeliveoS.get(position).unit);
            tvAvailableQty.setVisibility(View.GONE);

            return view;
        }

    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private void clearDataLocalData() {
        PreferenceUtils preferenceUtils = new PreferenceUtils(context);

        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE);
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER);
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE);
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE);
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.CARRIER_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.NOTES);
        preferenceUtils.removeFromPreference(PreferenceUtils.OPENING_READING);
        preferenceUtils.removeFromPreference(PreferenceUtils.ODO_UNIT);

        preferenceUtils.removeFromPreference(PreferenceUtils.CLOSING_READING);
        preferenceUtils.removeFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT);
        preferenceUtils.removeFromPreference(PreferenceUtils.SITE_NAME);
        preferenceUtils.removeFromPreference(PreferenceUtils.VOLUME);
        preferenceUtils.removeFromPreference(PreferenceUtils.WEIGHT);
        preferenceUtils.removeFromPreference(PreferenceUtils.CV_PLATE);
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_DATE);
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_TIME);
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_DATE);
        preferenceUtils.removeFromPreference(PreferenceUtils.CVEHICLE_CODE);
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_TIME);
        preferenceUtils.removeFromPreference(PreferenceUtils.CN_SHIPMENTS);
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECK_IN_STATUS);
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_LOAD);
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CAPACITY);
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION);
        AppPrefs.clearPref(context);
        StorageManager.getInstance(context).deleteCheckInData(context);
        StorageManager.getInstance(context).deleteCompletedShipments(((TransactionList) context));
        StorageManager.getInstance(context).deleteSkipShipmentList(((TransactionList) context));
        StorageManager.getInstance(context).deleteVanNonScheduleProducts(((TransactionList) context));
        StorageManager.getInstance(context).deleteVanScheduleProducts(((TransactionList) context));
        StorageManager.getInstance(context).deleteSpotSalesCustomerList(((TransactionList) context));
        StorageManager.getInstance(context).deleteVehicleInspectionList(((TransactionList) context));
        StorageManager.getInstance(context).deleteGateInspectionList(((TransactionList) context));
        StorageManager.getInstance(context).deleteSiteListData(((TransactionList) context));
        StorageManager.getInstance(context).deleteShipmentListData(((TransactionList) context));
        StorageManager.getInstance(context).deleteCheckOutData(context);
        StorageManager.getInstance(context).deleteDepartureData(((TransactionList) context));
        StorageManager.getInstance(context).deleteUpdateData(((TransactionList) context));

        StorageManager.getInstance(context).deleteScheduledNonScheduledReturnData();// clearing all tables
//        preferenceUtils.saveString(PreferenceUtils.STATUS, "" + resources.getString(R.string.logged_in))
//        preferenceUtils.saveString(PreferenceUtils.CHECK_IN_STATUS, resources.getString(R.string.logged_in))
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

}
