package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class ActiveDeliveryDO implements Serializable {
    public String image = "";

    public String shipmentId = "";
    public String product = "";
    public String productType = "";
    public String pType = "";
    public double productWeight = 0.0;
    public Double actualQuantity = 0.0;
    public String reasonFOC = "";
    public int focFlag = 0;
    public String shipmentProductType = "";
    public String productDescription = "";
    public int totalQuantity = 0;
    public int orderedQuantity = 0;
    public Double openingQuantity = 0.0;
    public Double endingQuantity = 0.0;
    public String stockUnit = "";
    public String weightUnit = "";
    public int quantity = 0;
    public String unit = "";
    public String linenumber = "";
    public int serialLotFlag = 0;


    public int receivedQuantity = 0;//return qty in cyllinder issue pdf
    public boolean isProductAdded;
    public boolean isScheduled;
    public Double price = 0.0;
    public String reason = "";
    public int reasonId = 0;
    public String productUnit = "";
    public String location = "";
    public int line = 0;
    public Double priceTag = 0.00;
    public String purchaseOrderrecievingSite = "";
    public String purchaseOrderSupplier = "";
    public String purchaseOrderNumber = "";
    public String purchaseOrderProduct = "";
    public int purchaseOrderQuantity = 0;
    public String purchaseOrderStatus = "";
    public String purchaseOrderLocation = "";
    public String purchaseOrderLotNumber = "";
    public String purchaseOrderSupplierLOT = "";
    public String purchaseOrderSerialNumber = "";
    public String purchaseOrderSerialNumberStart = "";
    public String purchaseOrderSerialNumberEnd = "";


    @Override
    public String toString() {
        return "ActiveDeliveryDO{" +
                "shipmentId='" + shipmentId + '\'' +
                ", product='" + product + '\'' +
                ", productType='" + productType + '\'' +
                ", shipmentProductType='" + shipmentProductType + '\'' +
                ", productDescription='" + productDescription + '\'' +
                ", totalQuantity=" + totalQuantity +
                ", orderedQuantity=" + orderedQuantity +
                ", unit='" + unit + '\'' +
                ", isProductAdded=" + isProductAdded +
                ", isScheduled=" + isScheduled +
                ", price=" + price +
                ", linenumber=" + linenumber +
                ", receivedQuantity=" + receivedQuantity +
                '}';
    }
}
