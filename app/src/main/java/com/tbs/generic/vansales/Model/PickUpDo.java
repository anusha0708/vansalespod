package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class PickUpDo implements Serializable {


    public String sequenceId = "";
    public String shipmentNumber = "";
    public String city = "";
    public String customer = "";
    public String productType = "";
    public String currency = "";
    public int sequenceFlag = 0;
    public String arra = "";
    public String description = "";
    public String revisedDepDate = "";
    public String revisedArrDate = "";

    public String revisedArrTime = "";
    public String revisedDepTime = "";
    public String netWeight = "";
    public String volume = "";
    public Double lattitude =0.0;
    public Double longitude =0.0;
    public String packages = "";
    public String departureDate = "";
    public String departureTime = "";
    public String arrivalDate = "";
    public String arrivalTime = "";
    public double distance = 0.0;
    public String status = "";
    public String unit = "";
    public int departuredFlag=0;
    public int docType=0;
    public int leftFlag   =0;

    public int validatedFlag=0;
    public int cancelFlag   =0;
    public int skipFlag     =0;
    public int pickupDropFlag   =0;
    public String version = "";
    public Double prevLattitude =0.0;
    public Double prevLongitude =0.0;
    public Double afterLattitude =0.0;
    public Double afterLongitude =0.0;
    public String city() {
        return city;
    }
}
