package com.tbs.generic.vansales.Activitys

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.tbs.generic.vansales.Adapters.ScheduledProductsAdapter
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.ActiveDeliveryRequest
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.include_toolbar.*

//
class ShipmentDetailsActivity : BaseActivity() {
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView

    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.shipment_details, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        disableMenuWithBackButton()
        /* toolbar.setNavigationIcon(R.drawable.back)
         toolbar.setNavigationOnClickListener {
             setResult(14, null)
             finish()
         }
 */

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)
        act_toolbar.setNavigationOnClickListener {
            setResult(14, null)
            finish()
        }

    }

    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))
        var idd = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        var tvCustomerName = findViewById<View>(R.id.tvCustomerName) as TextView
        var tvReference = findViewById<View>(R.id.tvReference) as TextView
        var tvShipmentNumber = findViewById<View>(R.id.tvShipmentNumber) as TextView

        var dropFlag = preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0)

        if (dropFlag != 6) {

            if (idd.length > 0) {
                if (Util.isNetworkAvailable(this)) {
                    val driverListRequest = ActiveDeliveryRequest(idd, this@ShipmentDetailsActivity)
                    driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                        hideLoader()
                        if (isError) {

                            Toast.makeText(this@ShipmentDetailsActivity, R.string.server_error, Toast.LENGTH_SHORT).show()
                        } else {
                            var aMonth: String = ""
                            var ayear: String = ""
                            var aDate: String = ""
                            try {
                                var date = activeDeliveryDo.deliveryDate
                                aMonth = date.substring(4, 6)
                                ayear = date.substring(0, 4)
                                aDate = date.substring(Math.max(date.length - 2, 0))
                            } catch (e: Exception) {
                            }

                            tvReference.setText(getString(R.string.date) + " : " + aDate + "-" + aMonth + "-" + ayear)

                            var loadStockAdapter = ScheduledProductsAdapter(this@ShipmentDetailsActivity, activeDeliveryDo.activeDeliveryDOS, "Shipments")
                            recycleview.setAdapter(loadStockAdapter)
                        }
                    }
                    driverListRequest.execute()

                } else {
                    showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

                }


            } else {

            }
        }
        if (dropFlag == 2) {
            tv_title.setText(getString(R.string.receipt_details_pod))

            tvCustomerName.setText(getString(R.string.supplier) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

            tvShipmentNumber.setText(getString(R.string.receipt) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

        } else if (dropFlag == 1) {
            tv_title.setText(R.string.shipment_details)

            tvCustomerName.setText(getString(R.string.customer) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

            tvShipmentNumber.setText(getString(R.string.shipment) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

        } else if (dropFlag == 3) {
            tv_title.setText(getString(R.string.retun_details_pod))

            tvCustomerName.setText(getString(R.string.customer) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

            tvShipmentNumber.setText(getString(R.string.returns) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

        } else if (dropFlag == 4) {
            tv_title.setText(getString(R.string.picking_ticket_details))

            tvCustomerName.setText(getString(R.string.customer) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

            tvShipmentNumber.setText(getString(R.string.picking_tickets) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

        } else if (dropFlag == 5) {
            tv_title.setText(getString(R.string.purchase_return_detail))

            tvCustomerName.setText(getString(R.string.supplier) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

            tvShipmentNumber.setText(getString(R.string.purchase_return) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

        } else if (dropFlag == 6) {
            var pickupDropFlag = preferenceUtils.getIntFromPreference(PreferenceUtils.PICKUP_DROP_FLAG, 0)

            tv_title.setText(getString(R.string.misc_stop_details))
            if (pickupDropFlag == 1) {
                tvCustomerName.setText(getString(R.string.customer) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

                tvShipmentNumber.setText(getString(R.string.mis_stoo) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

            } else {
                tvCustomerName.setText(getString(R.string.supplier) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

                tvShipmentNumber.setText(getString(R.string.mis_stoo) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

            }
            //pickupsupp


        } else {
            tv_title.setText(R.string.shipment_details)

            tvCustomerName.setText(getString(R.string.customer) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))

            tvShipmentNumber.setText(getString(R.string.shipment) + " : " + preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, ""))

        }


    }

}