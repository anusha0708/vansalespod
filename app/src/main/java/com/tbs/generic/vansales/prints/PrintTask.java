package com.tbs.generic.vansales.prints;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.honeywell.mobility.print.LinePrinter;
import com.honeywell.mobility.print.LinePrinterException;
import com.honeywell.mobility.print.PrintProgressEvent;
import com.honeywell.mobility.print.PrintProgressListener;
import com.honeywell.mobility.print.PrinterException;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.CreateInvoicePaymentDO;
import com.tbs.generic.vansales.Model.CylinderIssueDO;
import com.tbs.generic.vansales.Model.CylinderIssueMainDO;
import com.tbs.generic.vansales.Model.PaymentPdfDO;
import com.tbs.generic.vansales.Model.PdfInvoiceDo;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.Model.TransactionCashDO;
import com.tbs.generic.vansales.Model.TransactionCashMainDo;
import com.tbs.generic.vansales.Model.TransactionChequeDO;
import com.tbs.generic.vansales.Model.TransactionChequeMainDo;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.pdfs.utils.PDFOperations;
import com.tbs.generic.vansales.utils.CalendarUtils;
import com.tbs.generic.vansales.utils.NumberToWord;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class PrintTask extends AsyncTask<String, Integer, String> {

    private static final String TAG = "PrintTask";
    private Context context;
    private PrintConfigDo printConfigDo;
    private String sResult = "";
    private Object object;
    private String driverId = "", driverName = "";
    private static final String printData1Start = "\u001BEZ{PRINT:\n";
    private static final String printDataEnd = "}{LP}";// + "\n" + "\n" + "\n";


    public PrintTask(Context context, PrintConfigDo printConfigDo) {
        this.context = context;
        this.driverId = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        this.driverName = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "");
        driverId = driverId.substring(0, Math.min(driverId.length(), 30));
        driverName = driverName.substring(0, Math.min(driverName.length(), 15));
        this.printConfigDo = printConfigDo;
        this.object = printConfigDo.printData;

        if (printConfigDo.printerID.contains(":")) {

        }
    }

    @Override
    protected void onPreExecute() {
        // Shows a progress icon on the title bar to indicate
        // it is working on something.

        ((BaseActivity) context).showLoader();
    }

    @Override
    protected String doInBackground(String... args) {

        String sPrinterURI = "bt://" + printConfigDo.printerID;
//			String sPrinterURI = "bt://" + "00:06:66:82:C3:09";

        LinePrinter.ExtraSettings exSettings = new LinePrinter.ExtraSettings();

        exSettings.setContext(context);

        PrintProgressListener progressListener = new PrintProgressListener() {
            @Override
            public void receivedStatus(PrintProgressEvent aEvent) {
                // Publishes updates on the UI thread.
                publishProgress(aEvent.getMessageType());
            }
        };

        if (printConfigDo.printFrom == PrinterConstants.PrintCylinderDeliveryNotes) {
            ActiveDeliveryMainDO activeDeliveryMainDO = (ActiveDeliveryMainDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = activeDeliveryMainDO.logo;
            printDeliveryNotes(sPrinterURI, exSettings, progressListener, activeDeliveryMainDO);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintBulkDeliveryNotes) {
            ActiveDeliveryMainDO activeDeliveryMainDO = (ActiveDeliveryMainDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = activeDeliveryMainDO.logo;
            printDeliveryNotes(sPrinterURI, exSettings, progressListener, activeDeliveryMainDO);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintInvoiceReport) {
            CreateInvoicePaymentDO invoiceDo = (CreateInvoicePaymentDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = invoiceDo.logo;
            printInvoiceReport(sPrinterURI, exSettings, progressListener, invoiceDo);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintCylinderIssue) {
            CylinderIssueMainDO cylinderIssueMainDO = (CylinderIssueMainDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = cylinderIssueMainDO.logo;
            printCylinderIssue(sPrinterURI, exSettings, progressListener, cylinderIssueMainDO);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintPaymentReport) {
            PaymentPdfDO paymentDO = (PaymentPdfDO) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = paymentDO.logo;
            printPaymentReport(sPrinterURI, exSettings, progressListener, paymentDO);
        } else if (printConfigDo.printFrom == PrinterConstants.PrintActivityReport) {
            ArrayList<Serializable> transLists = (ArrayList<Serializable>) printConfigDo.printData;
//            PrinterConstants.base64LogoPng = paymentDO.logo;
            printActivityReport(sPrinterURI, exSettings, progressListener, transLists);
        }
        return sResult;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        // Access the values array.
        int progress = values[0];

        switch (progress) {
            case PrintProgressEvent.MessageTypes.CANCEL:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_CANCEL_MSG);
                break;
            case PrintProgressEvent.MessageTypes.COMPLETE:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_COMPLETE_MSG);
                break;
            case PrintProgressEvent.MessageTypes.ENDDOC:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_ENDDOC_MSG);
                break;
            case PrintProgressEvent.MessageTypes.FINISHED:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_FINISHED_MSG);
                break;
            case PrintProgressEvent.MessageTypes.STARTDOC:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_STARTDOC_MSG);
                break;
            default:
                Log.e("" + this.getClass().getName(), PrinterConstants.PROGRESS_NONE_MSG);
                break;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        // Displays the result (number of bytes sent to the printer or
        // exception message) in the Progress and Status text box.
        ((BaseActivity) context).hideLoader();
        if (result != null) {
            Log.e("" + this.getClass().getName(), "onPostExecute() : " + result);
        }
    }

    private void printDeliveryNotes(String sPrinterURI, LinePrinter.ExtraSettings exSettings,
                                    PrintProgressListener progressListener, ActiveDeliveryMainDO activeDeliveryMainDO) {
        LinePrinter lp = null;
        try {

            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }


            if (activeDeliveryMainDO.companyCode.equalsIgnoreCase("U2") || activeDeliveryMainDO.companyCode.equalsIgnoreCase("U3")) {
                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            } else {
                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            }
            Thread.sleep(100);
            lp.newLine(1);
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int topSpace = 0;
            String printData = "";
            String startCommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";

            int leftSpace = 0;//225
            if (activeDeliveryMainDO.companyDescription.toLowerCase().contains("taqat")) {
//                leftSpace = 320 - (activeDeliveryMainDO.companyDescription.length()/2);
                leftSpace = 320;

            } else {
                leftSpace = 250;
            }

            printData = startCommand + "@" + topSpace + "," + leftSpace + ":PE203,HMULT1,VMULT2|" + activeDeliveryMainDO.companyDescription + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand + "@" + topSpace + ",350:PE203,HMULT1,VMULT2|Delivery Note|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|D.N. No|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.shipmentNumber + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFDate(context) + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            activeDeliveryMainDO.customerDescription = activeDeliveryMainDO.customerDescription.substring(0, Math.min(activeDeliveryMainDO.customerDescription.length(), 30));
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Delivered To|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + activeDeliveryMainDO.customerDescription + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Time|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFTime(context) + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|User ID|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + driverId + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + driverName + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + getAddress(activeDeliveryMainDO) + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            String shipmentProductsType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "");
            if (shipmentProductsType.equals(AppConstants.MeterReadingProduct)) {
                topSpace = 30;
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
                        "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|Item|" +
                        "@" + (topSpace + 5) + ",360:PE203,HMULT1,VMULT1|Opening Reading|" +
                        "@" + (topSpace + 5) + ",540:PE203,HMULT1,VMULT1|Ending Reading|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|Quantity|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < activeDeliveryMainDO.activeDeliveryDOS.size(); i++) {
                    ActiveDeliveryDO activeDeliveryDO = activeDeliveryMainDO.activeDeliveryDOS.get(i);
                    activeDeliveryDO.productDescription = activeDeliveryDO.productDescription.substring(0, Math.min(activeDeliveryDO.productDescription.length(), 25));
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + (i + 1) + "|" +
                            "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|" + activeDeliveryDO.productDescription + "|" +
                            "@" + (topSpace + 5) + ",360:PE203,HMULT1,VMULT1|" + activeDeliveryDO.openingQuantity + "|" +
                            "@" + (topSpace + 5) + ",540:PE203,HMULT1,VMULT1|" + activeDeliveryDO.endingQuantity + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + activeDeliveryDO.orderedQuantity + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    topSpace = 0;
                }

//                printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :- |" + endCommand;
//                PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
//                String note = "";
                if (activeDeliveryMainDO.remarks != null ) {
//                    note = podDo.getNotes();
                    topSpace = topSpace + 40;
                    String remarks1="";
                    String remarks2="";
                    if (activeDeliveryMainDO.remarks.length() > 70) {
                        remarks1 = activeDeliveryMainDO.remarks.substring(0, 70);
                        remarks2 = activeDeliveryMainDO.remarks.substring(70);
                    } else {
                        remarks1 = activeDeliveryMainDO.remarks;

                    }
                    printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" +"Remarks :  "+remarks1+ "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    if(remarks2.length()>0){
                        printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" +""+remarks2+ "|" + endCommand;
                        lp.write(printData.getBytes());
                        Thread.sleep(100);
                    }
                }

            } else {
                topSpace = 30;
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
                        "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|Item|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|Quantity|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
                for (int i = 0; i < activeDeliveryMainDO.activeDeliveryDOS.size(); i++) {
                    ActiveDeliveryDO activeDeliveryDO = activeDeliveryMainDO.activeDeliveryDOS.get(i);
                    activeDeliveryDO.productDescription = activeDeliveryDO.productDescription.substring(0, Math.min(activeDeliveryDO.productDescription.length(), 50));
                    printData = startCommand +
                            "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + (i + 1) + "|" +
                            "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|" + activeDeliveryDO.productDescription + "|" +
                            "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + activeDeliveryDO.totalQuantity + "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                    topSpace = 0;
                }

                printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :- |" + endCommand;
                PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
                String note = "";
                if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
                    note = podDo.getNotes();
                    printData = startCommand + "@" + (topSpace + 5) + ",180:PE203,HMULT1,VMULT1|Remarks :- |";
                    topSpace = topSpace + 40;
                    printData = printData + "@" + (topSpace + 5) + ",180:PE203,HMULT1,VMULT1|" + note + "|" + endCommand;
                }
                lp.write(printData.getBytes());
                Thread.sleep(100);
            }

            printSignature(printConfigDo.signatureBase64, lp, progressListener);
            printDynamicFooter(activeDeliveryMainDO, lp);
            lp.newLine(7);

            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
        } catch (BadPrinterStateException ex) {
            // Stop listening for printer events.
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                    lp.close();  // Releases resources
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS");

                } catch (Exception ex) {
                }
            }
        }
    }

    private void printCylinderIssue(String sPrinterURI, LinePrinter.ExtraSettings exSettings, PrintProgressListener progressListener, CylinderIssueMainDO cylinderIssueMainDO) {
        LinePrinter lp = null;
        try {
            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            // Check the state of the printer and abort printing if there are
            // any critical errors detected.
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        // Paper out.
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        // Lid open.
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }

            int topSpace = 0;
            String printData = "";
            String startCommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";

            if (cylinderIssueMainDO.companyCode.equalsIgnoreCase("U2") || cylinderIssueMainDO.companyCode.equalsIgnoreCase("U3")) {
                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            } else {
                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            }
            Thread.sleep(100);
            lp.newLine(1);
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int leftSpace = 0;//225
            if (cylinderIssueMainDO.company.toLowerCase().contains("taqat")) {
//                leftSpace = 320 - (cylinderIssueMainDO.company.length()/2);
                leftSpace = 320;
            } else {
                leftSpace = 245;
            }
            printData = startCommand + "@" + topSpace + "," + leftSpace + ":PE203,HMULT1,VMULT2|" + cylinderIssueMainDO.company + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand + "@" + topSpace + ",250:PE203,HMULT1,VMULT2|Cylinder Issuance/Receipt Document|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|D.N. No|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.shipmentNumber + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFDate(context) + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            cylinderIssueMainDO.customerDescription = cylinderIssueMainDO.customerDescription.substring(0, Math.min(cylinderIssueMainDO.customerDescription.length(), 30));
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Delivered To|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + cylinderIssueMainDO.customerDescription + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Time|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFTime(context) + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|User ID|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + driverId + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + driverName + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +

                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + getAddress(cylinderIssueMainDO) + "|" +
                    "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;

            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
                    "@" + (topSpace + 5) + ",90:PE203,HMULT1,VMULT1|Item|" +
                    "@" + (topSpace + 5) + ",390:PE203,HMULT1,VMULT1|Open QTY|" +
                    "@" + (topSpace + 5) + ",490:PE203,HMULT1,VMULT1|Issued Qty|" +
                    "@" + (topSpace + 5) + ",600:PE203,HMULT1,VMULT1|Received Qty|" +
                    "@" + (topSpace + 5) + ",730:PE203,HMULT1,VMULT1|Balance|" + "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            for (int i = 0; i < cylinderIssueMainDO.cylinderIssueDOS.size(); i++) {
                CylinderIssueDO cylinderIssueDO = cylinderIssueMainDO.cylinderIssueDOS.get(i);
                cylinderIssueDO.productDescription = cylinderIssueDO.productDescription.substring(0, Math.min(cylinderIssueDO.productDescription.length(), 25));
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + (i + 1) + "|" +
                        "@" + (topSpace + 5) + ",90:PE203,HMULT1,VMULT1|" + cylinderIssueDO.productDescription + "|" +
                        "@" + (topSpace + 5) + ",390:PE203,HMULT1,VMULT1|" + cylinderIssueDO.orderedQuantity + "|" +
                        "@" + (topSpace + 5) + ",490:PE203,HMULT1,VMULT1|" + cylinderIssueDO.issuedQuantity + "|" +
                        "@" + (topSpace + 5) + ",600:PE203,HMULT1,VMULT1|" + cylinderIssueDO.recievedQuantity + "|" +
                        "@" + (topSpace + 5) + ",730:PE203,HMULT1,VMULT1|" + cylinderIssueDO.balanceQuantity + "|";// +
                lp.write((printData + endCommand).getBytes());
                Thread.sleep(100);
                topSpace = 0;
            }
            PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
            String note = "";
            if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
                note = podDo.getNotes();
            }
            printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :-  " + note + "|" + endCommand;
            topSpace = topSpace + 120;//tvs = 50+40 = 90,
            lp.write(printData.getBytes());
            Thread.sleep(100);
            printSignature(printConfigDo.signatureBase64, lp, progressListener);
            printDynamicFooter(cylinderIssueMainDO, lp);
            lp.newLine(7);
            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
        } catch (BadPrinterStateException ex) {
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                    lp.close();  // Releases resources
                } catch (Exception ex) {
                }
            }
        }
    }

    private static void printSignature(String signatureBase64, LinePrinter lp, PrintProgressListener progressListener) {
        try {
            if (signatureBase64 != null && !signatureBase64.equalsIgnoreCase("")) {
                lp.writeGraphicBase64(signatureBase64, LinePrinter.GraphicRotationDegrees.DEGREE_0, 520, 250, 80);
                Thread.sleep(100);
                lp.newLine(1);
            }
            lp.setUnderline(true);
            String printData = printData1Start + "@0,540:PE203,HMULT1,VMULT1|Signature|" + printDataEnd;
            lp.write(printData.getBytes());
            lp.setUnderline(false);
            lp.newLine(1);
        } catch (Exception e) {
            e.printStackTrace();
            lp.removePrintProgressListener(progressListener);
            Log.e(TAG, "Printer error detected: " + e.getMessage() + ". Please correct the error and try again.");
        }
    }

    private void printDynamicFooter(Object object, LinePrinter lp) {
        try {
            String site = "", siteDescr = "", street = "", landmark = "", town = "", country = "", website = "";
            String city = "", pinCode = "", landLine = "", mobile = "", fax = "", siteEmail1 = "", siteEmail2 = "";

            if (object instanceof ActiveDeliveryMainDO) {
                ActiveDeliveryMainDO activeDeliveryMainDO = (ActiveDeliveryMainDO) object;
                site = activeDeliveryMainDO.siteDescription;//
                siteDescr = activeDeliveryMainDO.siteAddress1;//
                street = activeDeliveryMainDO.siteAddress2;//
                landmark = activeDeliveryMainDO.siteAddress3;
                town = activeDeliveryMainDO.customerTown;
                country = activeDeliveryMainDO.siteCountry;//
                city = activeDeliveryMainDO.siteCity;//
                pinCode = activeDeliveryMainDO.sitePostalCode;
                landLine = activeDeliveryMainDO.siteLandLine;
                mobile = activeDeliveryMainDO.siteMobile;
                fax = activeDeliveryMainDO.siteFax;
                siteEmail1 = activeDeliveryMainDO.siteEmail1;
                siteEmail2 = activeDeliveryMainDO.siteEmail2;
                website = activeDeliveryMainDO.siteWebEmail;
            } else if (object instanceof CylinderIssueMainDO) {
                CylinderIssueMainDO cylinderIssueMainDO = (CylinderIssueMainDO) object;
                site = cylinderIssueMainDO.siteDescription;//
                siteDescr = cylinderIssueMainDO.siteAddress1;//
//				street = cylinderIssueMainDO.street;//
//				landmark = cylinderIssueMainDO.landMark;
//				town = cylinderIssueMainDO.town;
                country = cylinderIssueMainDO.siteCountry;//
                city = cylinderIssueMainDO.siteCity;//
                pinCode = cylinderIssueMainDO.sitePostalCode;
                landLine = cylinderIssueMainDO.siteLandLine;
                mobile = cylinderIssueMainDO.siteMobile;
                fax = cylinderIssueMainDO.siteFax;
                siteEmail1 = cylinderIssueMainDO.siteEmail1;
                siteEmail2 = cylinderIssueMainDO.siteEmail2;
                website = cylinderIssueMainDO.siteWebEmail;
            } else if (object instanceof CreateInvoicePaymentDO) {
                CreateInvoicePaymentDO invoiceDo = (CreateInvoicePaymentDO) object;
                site = invoiceDo.siteDescription;//
                siteDescr = invoiceDo.siteAddress1;//
                street = invoiceDo.siteStreet;//
                landmark = invoiceDo.siteLandMark;
                town = invoiceDo.siteTown;
                country = invoiceDo.siteCountry;//
                city = invoiceDo.siteCity;//
                pinCode = invoiceDo.sitePostalCode;
                landLine = invoiceDo.siteLandLine;
                mobile = invoiceDo.siteMobile;
                fax = invoiceDo.siteFax;
                siteEmail1 = invoiceDo.siteEmail1;
                siteEmail2 = invoiceDo.siteEmail2;
                website = invoiceDo.siteWebEmail;
            } else if (object instanceof PaymentPdfDO) {
                PaymentPdfDO paymentPdfDO = (PaymentPdfDO) object;
                site = paymentPdfDO.siteDescription;//
                siteDescr = paymentPdfDO.siteAddress1;//
                street = paymentPdfDO.siteAddress2;//
                landmark = paymentPdfDO.siteLandLine;
                town = paymentPdfDO.customerTown;
                country = paymentPdfDO.siteCountry;//
                city = paymentPdfDO.siteCity;//
                pinCode = paymentPdfDO.sitePostalCode;
                landLine = paymentPdfDO.siteLandLine;
                mobile = paymentPdfDO.siteMobile;
                fax = paymentPdfDO.siteFax;
                siteEmail1 = paymentPdfDO.siteEmail1;
                siteEmail2 = paymentPdfDO.siteEmail2;
                website = paymentPdfDO.siteWebEmail;
            }

            int topSpace = 10;
            siteEmail1 = siteEmail1.substring(0, Math.min(siteEmail1.length(), 34));
            String printData = "\u001BEZ{PRINT:\n";
            printData = printData + "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Registered office|\n";
            topSpace = topSpace + 40;
            printData = printData + "@" + topSpace + ",10:PE203,HMULT1,VMULT1|" + site + "|\n";
            topSpace = topSpace + 40;
            printData = printData + "@" + topSpace + ",10:PE203,HMULT1,VMULT1|" + siteDescr + ", P.O Box " + pinCode + ", " + city + ", " + country + "|\n";
            topSpace = topSpace + 40;
            printData = printData + "@" + topSpace + ",10:PE203,HMULT1,VMULT1|T: " + mobile + "|\n";
            printData = printData + "@" + topSpace + ",410:PE203,HMULT1,VMULT1|F: " + fax + "|\n";
            topSpace = topSpace + 40;
            printData = printData + "@" + topSpace + ",10:PE203,HMULT1,VMULT1|E: " + siteEmail1 + "|\n";
            printData = printData + "@" + topSpace + ",410:PE203,HMULT1,VMULT1|website: " + website + "|\n";
            printData = printData + "}{LP}\n";
            lp.write(printData.getBytes());
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
            sResult = "Printer error detected: " + e.getMessage() + ". Please correct the error and try again.";
        }
    }

    private void printInvoiceReport(String sPrinterURI, LinePrinter.ExtraSettings exSettings, PrintProgressListener progressListener, CreateInvoicePaymentDO invoiceDo) {
        LinePrinter lp = null;
        try {
            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        // Paper out.
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        // Lid open.
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }
            if (invoiceDo.companyCode.equalsIgnoreCase("U2") || invoiceDo.companyCode.equalsIgnoreCase("U3")) {
                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            } else {
                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);

            }
            Thread.sleep(100);
            lp.newLine(1);
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int topSpace = 0;
            String printData = "";
            String startCommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";
            int leftSpace = 0;//225
            if (invoiceDo.supplierName.toLowerCase().contains("taqat")) {
//                leftSpace = 320 - (invoiceDo.company.length()/2);
                leftSpace = 320;
            } else {
                leftSpace = 245;
            }
            printData = startCommand + "@" + topSpace + "," + leftSpace + ":PE203,HMULT1,VMULT2|" + invoiceDo.supplierName + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand + "@" + topSpace + ",290:PE203,HMULT1,VMULT2|Commercial and Tax Invoice|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Invoice  No|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.invoiceNumber + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFDate(context) + "|" + endCommand;
            lp.write(printData.getBytes());
            topSpace = 0;
            invoiceDo.customerDescription = invoiceDo.customerDescription.substring(0, Math.min(invoiceDo.customerDescription.length(), 30));
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Customer Name|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.customerDescription + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Time|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFTime(context) + "|" + endCommand;
            lp.write(printData.getBytes());
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Customer TRN|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.customerTrn + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User ID|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + driverId + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            String address = getAddress(invoiceDo);
            String addresses1 = "";
            String addresses2 = "";
            if (address.length() > 32) {
                addresses1 = address.substring(0, 32);
                addresses2 = address.substring(32);
            } else {
                addresses1 = address;
            }

            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Cust Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + addresses1 + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + driverName + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            if (addresses2.length() > 0) {
                printData = startCommand +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1||" +
                        "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + addresses2 + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;
            }
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Supplier Name|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.supplierName + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Supplier TRN|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + invoiceDo.supplierTrn + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Reg Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + getRegisteredAddress(invoiceDo) + "|" + "" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 30;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Sr.No.|" +
                    "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|Particulars|" +
                    "@" + (topSpace + 5) + ",360:PE203,HMULT1,VMULT1|Quantity|" +
                    "@" + (topSpace + 5) + ",540:PE203,HMULT1,VMULT1|Unit Price|" +
                    "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|Amount(AED)|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            for (int i = 0; i < invoiceDo.pdfInvoiceDos.size(); i++) {
                PdfInvoiceDo pdfInvoiceDo = invoiceDo.pdfInvoiceDos.get(i);
                pdfInvoiceDo.productDesccription = pdfInvoiceDo.productDesccription.substring(0, Math.min(pdfInvoiceDo.productDesccription.length(), 23));
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + (i + 1) + "|" +
                        "@" + (topSpace + 5) + ",110:PE203,HMULT1,VMULT1|" + pdfInvoiceDo.productDesccription + "|" +
                        "@" + (topSpace + 5) + ",360:PE203,HMULT1,VMULT1|" + pdfInvoiceDo.deliveredQunatity + "|" +
                        "@" + (topSpace + 5) + ",540:PE203,HMULT1,VMULT1|" + pdfInvoiceDo.grossPrice + "|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + pdfInvoiceDo.amount + "|";
                lp.write((printData + endCommand).getBytes());
                Thread.sleep(100);
                topSpace = 0;
            }

            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Discount|" +
                    "@" + (topSpace + 5) + ",720:PE203,HMULT1,VMULT1|" + invoiceDo.totalDiscount + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Total Excluding VAT|" +
                    "@" + (topSpace + 5) + ",720:PE203,HMULT1,VMULT1|" + invoiceDo.excludingTax + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|VAT|" +
                    "@" + (topSpace + 5) + ",720:PE203,HMULT1,VMULT1|" + invoiceDo.totalTax + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Total|" +
                    "@" + (topSpace + 5) + ",720:PE203,HMULT1,VMULT1|" + invoiceDo.includingTax + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            //
            double totalAmount = getTotalAmount(invoiceDo);
            int afterDcimalVal = PDFOperations.getInstance().anyMethod(totalAmount);
//            String printWords = startCommand + "@" + topSpace + ",1:PE203,HMULT1,VMULT1|AED : |"+getAmountInLetters(totalAmount, false) + " and " + getAmountInLetters(afterDcimalVal, true) + "" + endCommand;
//            lp.write(printWords.getBytes());
//            Thread.sleep(100);

            printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|AED Total:  " +getAmountInLetters(totalAmount, false) + " and " + getAmountInLetters(afterDcimalVal, true) + "" +  "|" + endCommand;
            topSpace = 0;
            lp.write(printData.getBytes());
            Thread.sleep(100);
//            double totalAmount = getTotalAmount(invoiceDo);
//
//            int afterDcimalVal = PDFOperations.getInstance().anyMethod(totalAmount);
//            String printWords = startCommand + "@" + topSpace + ",1:PE203,HMULT1,VMULT1|AED : |"+getAmountInLetters(totalAmount, false) + " and " + getAmountInLetters(afterDcimalVal, true) + "" + endCommand;
//            lp.write(printWords.getBytes());
//            Thread.sleep(100);


            PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
            String note = "";
            if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
                note = podDo.getNotes();
            }

            if (invoiceDo.remarks != null ) {
//                    note = podDo.getNotes();
                topSpace = topSpace + 40;
                String remarks1="";
                String remarks2="";
                if (invoiceDo.remarks.length() > 70) {
                    remarks1 = invoiceDo.remarks.substring(0, 70);
                    remarks2 = invoiceDo.remarks.substring(70);
                } else {
                    remarks1 = invoiceDo.remarks;

                }
                printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" +"Remarks :  "+remarks1+ "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                if(remarks2.length()>0){
                    printData = startCommand + "@" + (5) + ",12:PE203,HMULT1,VMULT1|" +""+remarks2+ "|" + endCommand;
                    lp.write(printData.getBytes());
                    Thread.sleep(100);
                }
            }

//
//            printData = startCommand + "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :-  " + invoiceDo.remarks + "|" + endCommand;
//            topSpace = topSpace + 120;
//            lp.write(printData.getBytes());
//            Thread.sleep(100);

            String printNote = startCommand + "@" + topSpace + ",1:PE203,HMULT1,VMULT1|THIS IS COMPUTER GENERATED DOCUMENT DOES NOT REQUIRE SIGNATURE|" + endCommand;
            lp.write(printNote.getBytes());
            topSpace = 0;
            printDynamicFooter(invoiceDo, lp);
            printQRCode(context, lp, invoiceDo.invoiceNumber);
            lp.newLine(7);
            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
        } catch (BadPrinterStateException ex) {
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                } catch (Exception ex) {
                }
            }
        }
    }

    private void printActivityReport(String sPrinterURI, LinePrinter.ExtraSettings exSettings,
                                     PrintProgressListener progressListener, ArrayList<Serializable> transactionList) {
        LinePrinter lp = null;
        try {
            String name = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "");
            String id = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");

            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }
//
//
//            if(transactionMainDo.companyCode.equalsIgnoreCase("U2")||transactionMainDo.companyCode.equalsIgnoreCase("U3")){
//                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 280, 250,250);
//
//            }else {
//                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 280, 250,250);
//
//            }
            TransactionCashMainDo transactionCashMainDo = (TransactionCashMainDo) transactionList.get(0);
            TransactionChequeMainDo transactionChequeMainDo = (TransactionChequeMainDo) transactionList.get(1);

            lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 280, 250, 250);

            Thread.sleep(100);
            lp.newLine(1);
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int topSpace = 0;
            String printData = "";
            String startCommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";
            int leftSpace = 0;//225
            printData = startCommand + "@" + topSpace + ",225:PE203,HMULT1,VMULT2|Brothers Gas Bottling & Distribution Co.LLC|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand + "@" + topSpace + ",340:PE203,HMULT1,VMULT2|Payment History|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|User Id|" +
                    "@" + topSpace + ",250:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",270:PE203,HMULT1,VMULT1|" + id + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + name + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Total Cash Reciepts|" +
                    "@" + topSpace + ",250:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",270:PE203,HMULT1,VMULT1|" + transactionCashMainDo.cashReciepts + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFDate(context) + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Total Cheque Reciepts|" +
                    "@" + topSpace + ",250:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",270:PE203,HMULT1,VMULT1|" + transactionChequeMainDo.chequeReciepts + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Time|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFTime(context) + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;

            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|From Date|" +
                    "@" + topSpace + ",250:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",270:PE203,HMULT1,VMULT1|" + AppConstants.Trans_From_Date + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|To Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + AppConstants.Trans_To_Date + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 30;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Rec.No.|" +
                    "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT2|Customer Name|" +
                    "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Cash|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            for (int i = 0; i < transactionCashMainDo.transactionCashDOS.size(); i++) {
                TransactionCashDO transactionCashDO = transactionCashMainDo.transactionCashDOS.get(i);
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + transactionCashDO.cashPayment + "|" +
                        "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT1|" + transactionCashDO.cashCustomer + "|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + transactionCashDO.cashAmount + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(100);
                topSpace = 0;

            }
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total Amount Paid|" +
                    "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                    "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + transactionCashMainDo.totalAmount + " AED" + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startCommand +
                    "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT2|Rec.No.|" +
                    "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT2|Customer Name|" +
                    "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT2|Cheque|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            for (int i = 0; i < transactionChequeMainDo.transactionChequeDOS.size(); i++) {
                TransactionChequeDO transactionChequeDO = transactionChequeMainDo.transactionChequeDOS.get(i);
                printData = startCommand +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|" + transactionChequeDO.chequePayment + "|" +
                        "@" + (topSpace + 5) + ",220:PE203,HMULT1,VMULT1|" + transactionChequeDO.chequeCustomer + "|" +
                        "@" + (topSpace + 5) + ",710:PE203,HMULT1,VMULT1|" + transactionChequeDO.chequeAmount + "|" + endCommand;
                lp.write(printData.getBytes());
                Thread.sleep(150);
                topSpace = 0;
            }
            printData = startCommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT2|Total Amount Paid|" +
                    "@" + topSpace + ",240:PE203,HMULT1,VMULT2|:|" +
                    "@" + topSpace + ",650:PE203,HMULT1,VMULT2|" + transactionChequeMainDo.totalAmount + " AED" + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);

            lp.newLine(7);
            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
        } catch (BadPrinterStateException ex) {
            // Stop listening for printer events.
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                    lp.close();  // Releases resources
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS");

                } catch (Exception ex) {
                }
            }
        }
    }


    private static void printQRCode(Context context, LinePrinter lp, String invoiceNumber) {

        try {
            Bitmap qrCode = Util.encodeAsBitmap(context, invoiceNumber);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Bitmap b = Bitmap.createScaledBitmap(qrCode, 120, 120, false);
            b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            String encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);

            if (encodedImage != null) {
                lp.writeGraphicBase64(encodedImage, LinePrinter.GraphicRotationDegrees.DEGREE_0,
                        280,  // Offset in printhead dots from the left of the page
                        150, // Desired graphic width on paper in printhead dots
                        150); // Desired graphic height on paper in printhead dots
            }
            Thread.sleep(100);
        } catch (Exception e) {

        }
    }

    private void printPaymentReport(String sPrinterURI, LinePrinter.ExtraSettings exSettings, PrintProgressListener progressListener, PaymentPdfDO paymentPdfDO) {
        LinePrinter lp = null;
        try {
            lp = new LinePrinter(printConfigDo.jsonCmdAttribStr, printConfigDo.printerName, sPrinterURI, exSettings);
            lp.addPrintProgressListener(progressListener);
            int numtries = 0;
            int maxretry = 2;
            while (numtries < maxretry) {
                try {
                    lp.connect();  // Connects to the printer
                    break;
                } catch (LinePrinterException ex) {
                    numtries++;
                    Thread.sleep(1000);
                }
            }
            if (numtries == maxretry) lp.connect();//Final retry
            int[] results = lp.getStatus();
            if (results != null) {
                for (int err = 0; err < results.length; err++) {
                    if (results[err] == 223) {
                        throw new BadPrinterStateException("Paper out");
                    } else if (results[err] == 227) {
                        throw new BadPrinterStateException("Printer lid open");
                    }
                }
            }
            if (paymentPdfDO.companyCode.equalsIgnoreCase("U2") || paymentPdfDO.companyCode.equalsIgnoreCase("U3")) {
                lp.writeGraphicBase64(PrinterConstants.base64Footer, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);
            } else {
                lp.writeGraphicBase64(PrinterConstants.base64LogoPng, LinePrinter.GraphicRotationDegrees.DEGREE_0, 300, 250, 250);
            }
            Thread.sleep(100);
            lp.newLine(1);

            int topSpace = 0;
            String printData = "";
            String startACommand = "\u001BEZ{PRINT:\n";
            String endCommand = "}{LP}\n";
            byte[] easymode = new byte[]{0x1b, 0x45, 0x5a}; // [ESC]EZ
            lp.write(easymode);
            int leftSpace = 0;//225
            if (paymentPdfDO.company.toLowerCase().contains("taqat")) {
//                leftSpace = 320 - (paymentPdfDO.company.length()/2);
                leftSpace = 320;
            } else {
                leftSpace = 245;
            }
            printData = startACommand + "@" + topSpace + "," + leftSpace + ":PE203,HMULT1,VMULT2|" + paymentPdfDO.company + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startACommand + "@" + topSpace + ",330:PE203,HMULT1,VMULT2|Receipt Voucher|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startACommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Receipt No.|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + paymentPdfDO.paymentNumber + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Date|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFDate(context) + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            paymentPdfDO.customerDescription = paymentPdfDO.customerDescription.substring(0, Math.min(paymentPdfDO.customerDescription.length(), 30));
            printData = startACommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Received From|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + paymentPdfDO.customerDescription + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|Time|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + CalendarUtils.getPDFTime(context) + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startACommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|User ID|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + driverId + "|" +
                    "@" + topSpace + ",510:PE203,HMULT1,VMULT1|User Name|" +
                    "@" + topSpace + ",600:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",630:PE203,HMULT1,VMULT1|" + driverName + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 0;
            printData = startACommand +
                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Address|" +
                    "@" + topSpace + ",140:PE203,HMULT1,VMULT1|:|" +
                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|" + getAddress(paymentPdfDO) + "|" + endCommand;
            lp.write(printData.getBytes());
            Thread.sleep(100);
            topSpace = 20;
            printData = startACommand +
                    "@" + topSpace + ",1:HLINE,Length820,Thick2|\n" +
                    "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
                    "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
                    "\n";

            printData = printData +
                    "@" + (topSpace + 5) + ",10:PE203,HMULT1,VMULT1|Amount|\n" +
                    "@" + (topSpace + 5) + ",170:PE203,HMULT1,VMULT1|:|\n" +
                    "@" + (topSpace + 5) + ",190:PE203,HMULT1,VMULT1|AED " + paymentPdfDO.amount + "|\n";
            topSpace = topSpace + 40;
            printData = printData +
                    "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
                    "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
                    "\n";
            try {
                double amt = paymentPdfDO.amount;
                int afterDcimalVal = PDFOperations.getInstance().anyMethod(amt);

                printData = printData +
                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1|The Sum of |\n" +
                        "@" + topSpace + ",170:PE203,HMULT1,VMULT1|:|\n" +
                        "@" + topSpace + ",190:PE203,HMULT1,VMULT1|AED " + getAmountInLetters(amt, false) + " and " + getAmountInLetters(afterDcimalVal, true) + "" + "|\n" +
                        "\n" + "\n";

                topSpace = topSpace + 40;
                String paymentType = "";
                if (!TextUtils.isEmpty(paymentPdfDO.chequeNumber)) {
                    paymentType = "By Cheque";
                } else {
                    paymentType = "By Cash";
                }
                printData = printData +
                        "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
                        "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
                        "\n";


                if (paymentType.equalsIgnoreCase("By Cheque")) {
                    printData = printData +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Payment Method|\n" +
                            "@" + topSpace + ",170:PE203,HMULT1,VMULT1|:|\n" +
                            "@" + topSpace + ",190:PE203,HMULT1,VMULT1|" + paymentType + "|\n" +
                        "@" + topSpace + ",400:PE203,HMULT1,VMULT1|Bank : |\n" +
                        "@" + topSpace + ",500:PE203,HMULT1,VMULT1|" + paymentPdfDO.bank + "|\n" +
                            "\n";
                }else {
                    printData = printData +
                            "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Payment Method|\n" +
                            "@" + topSpace + ",170:PE203,HMULT1,VMULT1|:|\n" +
                            "@" + topSpace + ",190:PE203,HMULT1,VMULT1|" + paymentType + "|\n" +
//                        "@" + topSpace + ",400:PE203,HMULT1,VMULT1|Bank : |\n" +
//                        "@" + topSpace + ",580:PE203,HMULT1,VMULT1|" + paymentPdfDO.bank + "|\n" +
                            "\n";
                }


                printData = printData +
                        "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
                        "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
                        "\n";
                if (paymentType.equalsIgnoreCase("By Cheque")) {
                    topSpace = 40 + topSpace;
                    if (paymentPdfDO.chequeDate.length() > 0) {
                        String aMonth = paymentPdfDO.chequeDate.substring(4, 6);
                        String ayear = paymentPdfDO.chequeDate.substring(0, 4);
                        String aDate = paymentPdfDO.chequeDate.substring(Math.max(paymentPdfDO.chequeDate.length() - 2, 0));
                        if (!TextUtils.isEmpty(paymentPdfDO.chequeNumber)) {
                            paymentPdfDO.chequeNumber = paymentPdfDO.chequeNumber.substring(0, Math.min(paymentPdfDO.chequeNumber.length(), 20));
                            printData = printData +
                                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Cheque No.|\n" +
                                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|:|\n" +
                                    "@" + topSpace + ",190:PE203,HMULT1,VMULT1|" + paymentPdfDO.chequeNumber + "|\n" +
                                    "@" + topSpace + ",400:PE203,HMULT1,VMULT1|Cheque Date: |\n" +
                                    "@" + topSpace + ",580:PE203,HMULT1,VMULT1|" + aDate + "/" + aMonth + "/" + ayear + "|\n" +
                                    "\n";
                        } else {
                            printData = printData +
                                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Cheque No.|\n" +
                                    "@" + topSpace + ",170:PE203,HMULT1,VMULT1|:|\n" +
                                    "@" + topSpace + ",190:PE203,HMULT1,VMULT1|" + paymentPdfDO.chequeNumber + "|\n" +
                                    "@" + topSpace + ",400:PE203,HMULT1,VMULT1|Cheque Date: |\n" +
                                    "@" + topSpace + ",580:PE203,HMULT1,VMULT1|" + " " + "|\n" +
                                    "\n";
                        }
                        printData = printData +
                                "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
                                "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
                                "\n";
                    }
                }
                topSpace = 40 + topSpace;
                printData = printData +
                        "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
                        "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
                        "\n";

                String pId = "";
                if (paymentPdfDO.invoiceDos.size() > 1) {
                    for (int i = 0; i < paymentPdfDO.invoiceDos.size(); i++) {
                        if(i==paymentPdfDO.invoiceDos.size()-1){
                            pId = pId + paymentPdfDO.invoiceDos.get(i).invoiceNumber + "";
                        }else {
                            pId = pId + paymentPdfDO.invoiceDos.get(i).invoiceNumber + " , ";
                        }

                    }
                    String pid1="";
                    String pid2="";
                    String pid3="";
                    String pid4="";
                    String pid5="";
                    String pid6="";
                    String pid7="";
                    String pid8="";
                    String pid9="";
                    String pid10="";



                    if (pId.length() > 60) {
                        pid1 = pId.substring(0, 60);
                        pid2 = pId.substring(60);
                        printData = printData +
                                "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Being Paid Against|\n" +
                                "@" + topSpace + ",200:PE203,HMULT1,VMULT1|:|\n" +
                                "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pid1 + "|\n" +
                                "\n";
                        topSpace=topSpace+20;


                        if (pid2.length() > 60) {
                            pid3 = pid2.substring(0, 60);
                            pid4 = pid2.substring(60);
                            printData = printData +
                                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1||\n" +
                                    "@" + topSpace + ",200:PE203,HMULT1,VMULT1||\n" +
                                    "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pid3 + "|\n" +
                                    "\n";

                            topSpace=topSpace+20;
                            printData = printData +
                                    "@" + topSpace + ",2:VLINE,Length40,Thick2|\n" +
                                    "@" + topSpace + ",820:VLINE,Length40,Thick2|\n" +
                                    "\n";
                            if (pid4.length() > 60) {
                                pid5 = pid4.substring(0, 60);
                                pid6= pid4.substring(60);
                                printData = printData +
                                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1||\n" +
                                        "@" + topSpace + ",200:PE203,HMULT1,VMULT1||\n" +
                                        "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pid5 + "|\n" +
                                        "\n";
                                topSpace=topSpace+20;

                                printData = printData +
                                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1||\n" +
                                        "@" + topSpace + ",200:PE203,HMULT1,VMULT1||\n" +
                                        "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pid6 + "|\n" +
                                        "\n";
                            }else {
                                printData = printData +
                                        "@" + topSpace + ",10:PE203,HMULT1,VMULT1||\n" +
                                        "@" + topSpace + ",200:PE203,HMULT1,VMULT1||\n" +
                                        "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pid4 + "|\n" +
                                        "\n";
                            }


                        }else {
                            printData = printData +
                                    "@" + topSpace + ",10:PE203,HMULT1,VMULT1||\n" +
                                    "@" + topSpace + ",200:PE203,HMULT1,VMULT1||\n" +
                                    "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pid2 + "|\n" +
                                    "\n";
                        }

                    } else {
                        printData = printData +
                                "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Being Paid Against|\n" +
                                "@" + topSpace + ",200:PE203,HMULT1,VMULT1|:|\n" +
                                "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + pId + "|\n" +
                                "\n";
                    }

                } else {
                    if (paymentPdfDO.invoiceDos.size() > 0 && paymentPdfDO.invoiceDos.get(0).invoiceNumber.length() > 0) {
                        printData = printData +
                                "@" + topSpace + ",10:PE203,HMULT1,VMULT1|Being Paid Against |\n" +
                                "@" + topSpace + ",200:PE203,HMULT1,VMULT1|:|\n" +
                                "@" + topSpace + ",220:PE203,HMULT1,VMULT1|" + paymentPdfDO.invoiceDos.get(0).invoiceNumber + "|\n" +
                                "\n";
                    }
                }

                topSpace = topSpace + 40;
                printData = printData +
                        "@" + topSpace + ",1:HLINE,Length820,Thick2|\n";

                PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);
                String note = "";
                if (podDo != null && !TextUtils.isEmpty(podDo.getNotes())) {
                    note = podDo.getNotes();
                }
                printData = printData +
                        "@" + topSpace + ",1:VLINE,Length120,Thick2|\n" +
                        "@" + (topSpace + 5) + ",12:PE203,HMULT1,VMULT1|Remarks :-  " + note + "|" +
                        "@" + topSpace + ",820:VLINE,Length120,Thick2|\n" +
                        "\n";
//
                topSpace = topSpace + 120;//tvs = 50+40 = 90,

                printData = printData + "@" + topSpace + ",1:HLINE,Length820,Thick2|\n";
                printData = printData + endCommand;

                lp.write(printData.getBytes());
                Thread.sleep(100);
                lp.newLine(2);

//                addQRCode(paymentPdfDO.paymentNumber);

            } catch (PrinterException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            printDynamicFooter(paymentPdfDO, lp);
            printQRCode(context, lp, paymentPdfDO.paymentNumber);
//            Bitmap qrCode = Util.encodeAsBitmap(context, paymentPdfDO.paymentNumber);
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            Bitmap b = Bitmap.createScaledBitmap(qrCode, 120, 120, false);
//            b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
//            byte[] byteArray = stream.toByteArray();
//            String encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
//
//            if (encodedImage != null) {
//
//                Thread.sleep(100);
//                lp.writeGraphicBase64(encodedImage, LinePrinter.GraphicRotationDegrees.DEGREE_0,
//                        280,  // Offset in printhead dots from the left of the page
//                        150, // Desired graphic width on paper in printhead dots
//                        150); // Desired graphic height on paper in printhead dots
//            }

            lp.newLine(7);
            sResult = "Number of bytes sent to printer: " + lp.getBytesWritten();
        } catch (BadPrinterStateException ex) {
            // Stop listening for printer events.
            lp.removePrintProgressListener(progressListener);
            sResult = "Printer error detected: " + ex.getMessage() + ". Please correct the error and try again.";
        } catch (LinePrinterException ex) {
            sResult = "LinePrinterException: " + ex.getMessage();
        } catch (Exception ex) {
            if (ex.getMessage() != null)
                sResult = "Unexpected exception: " + ex.getMessage();
            else
                sResult = "Unexpected exception.";
        } finally {
            ((BaseActivity) context).hideLoader();
            if (lp != null) {
                try {
                    lp.disconnect();  // Disconnects from the printer
                    lp.close();  // Releases resources
                } catch (Exception ex) {
                }
            }
        }
    }

    public class BadPrinterStateException extends Exception {
        static final long serialVersionUID = 1;

        private BadPrinterStateException(String message) {
            super(message);
        }
    }

    private String getAddress(ActiveDeliveryMainDO activeDeliveryMainDO) {
        String postal = activeDeliveryMainDO.customerStreet;
        String countryName = activeDeliveryMainDO.customerLandMark;
        String town = activeDeliveryMainDO.customerTown;
        String city = activeDeliveryMainDO.customerCity;
        String postalcode = activeDeliveryMainDO.customerPostalCode;

        String finalString = "";

        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(countryName)) {
            finalString += countryName + ", ";
        }
        if (!TextUtils.isEmpty(postalcode)) {
            finalString += postalcode;
        }
        return finalString;
    }

    private String getAddress(CylinderIssueMainDO activeDeliveryMainDO) {


        String postal = activeDeliveryMainDO.customerStreet;
        String countryName = activeDeliveryMainDO.customerLandMark;
        String town = activeDeliveryMainDO.customerTown;
        String city = activeDeliveryMainDO.customerCity;
        String postalcode = activeDeliveryMainDO.customerPostalCode;

        String finalString = "";

        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(countryName)) {
            finalString += countryName + ", ";
        }
        if (!TextUtils.isEmpty(postalcode)) {
            finalString += postalcode;
        }
        return finalString;
    }

    private String getAddress(PaymentPdfDO activeDeliveryMainDO) {


        String postal = activeDeliveryMainDO.customerStreet;
        String countryName = activeDeliveryMainDO.customerLandMark;
        String town = activeDeliveryMainDO.customerTown;
        String city = activeDeliveryMainDO.customerCity;
        String postalcode = activeDeliveryMainDO.customerPostalCode;

        String finalString = "";

        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(countryName)) {
            finalString += countryName + ", ";
        }
        if (!TextUtils.isEmpty(postalcode)) {
            finalString += postalcode;
        }
        return finalString;
    }

    private String getAddress(CreateInvoicePaymentDO activeDeliveryMainDO) {


        String postal = activeDeliveryMainDO.customerStreet;
        String countryName = activeDeliveryMainDO.customerLandMark;
        String town = activeDeliveryMainDO.customerTown;
        String city = activeDeliveryMainDO.customerCity;
        String postalcode = activeDeliveryMainDO.customerPostalCode;

        String finalString = "";

        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(countryName)) {
            finalString += countryName + ", ";
        }
        if (!TextUtils.isEmpty(postalcode)) {
            finalString += postalcode;
        }
        return finalString;
    }

    private String getRegisteredAddress(CreateInvoicePaymentDO invoiceDO) {

        String street, landMark, town, postal, city, countryName;
        street = invoiceDO.siteAddress1;
        landMark = invoiceDO.siteAddress2;
        town = invoiceDO.siteAddress3;
        city = invoiceDO.siteCity;
        postal = invoiceDO.sitePostalCode;
        countryName = invoiceDO.siteCountry;
        String finalString = "";

        if (!TextUtils.isEmpty(street)) {
            finalString += street + ", ";
        }
        if (!TextUtils.isEmpty(landMark)) {
            finalString += landMark + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
        return finalString + countryName;
    }

    private String getAmountInLetters(double amount, boolean fills) {
        if (fills) {
            return new NumberToWord().convert((int) amount) + " Fills";
        }
        return new NumberToWord().convert((int) amount);
        //return NumberToWords.getNumberToWords().getTextFromNumbers(amount);
    }
    private double getTotalAmount(CreateInvoicePaymentDO invoicePaymentDO) {
        double totalAmount = 0.0f;
        try {
            /*totalAmount = getTotalDiscount(pdfInvoiceDos) -
                    getTotalGrossAmount(pdfInvoiceDos) +
                    getTotalExlTax(pdfInvoiceDos) -
                    getTotalVat(pdfInvoiceDos);*/
            return Double.parseDouble(invoicePaymentDO.includingTax);

        } catch (Exception e) {
            return totalAmount;
        }

    }
}
