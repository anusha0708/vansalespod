package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class VanStockDetailsDO implements Serializable {

    public String vehicleCapacity = "";
    public String vehicleCapacityUnit = "";
    public String vehicleVolume = "";
    public String vehicleVolumeUnit = "";
    public String totalScheduledStockQty = "";
    public String totalScheduledStockVoulme = "";
    public String totalScheduledStockUnits= "";
    public String totalNonScheduledStockQty = "";
    public String totalNonScheduledStockVoulme = "";
    public String totalNonScheduledStockUnits= "";
    public String pickUpStock= "";
    public String dropStock= "";

    public ArrayList<StockItemDo> stockItemList = new ArrayList<>();

    @Override
    public String toString() {
        return "VanStockDetailsDO{" +
                "vehicleCapacity='" + vehicleCapacity + '\'' +
                ", vehicleCapacityUnit='" + vehicleCapacityUnit + '\'' +
                ", vehicleVolume='" + vehicleVolume + '\'' +
                ", vehicleVolumeUnit='" + vehicleVolumeUnit + '\'' +
                ", totalScheduledStockQty='" + totalScheduledStockQty + '\'' +
                ", totalScheduledStockVoulme='" + totalScheduledStockVoulme + '\'' +
                ", totalScheduledStockUnits='" + totalScheduledStockUnits + '\'' +
                ", totalNonScheduledStockQty='" + totalNonScheduledStockQty + '\'' +
                ", totalNonScheduledStockVoulme='" + totalNonScheduledStockVoulme + '\'' +
                ", totalNonScheduledStockUnits='" + totalNonScheduledStockUnits + '\'' +
                ", stockItemList=" + stockItemList +
                '}';
    }
}
