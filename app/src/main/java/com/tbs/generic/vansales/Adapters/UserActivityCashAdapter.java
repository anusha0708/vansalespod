package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.CashDO;
import com.tbs.generic.vansales.R;

import java.util.ArrayList;

public class UserActivityCashAdapter extends RecyclerView.Adapter<UserActivityCashAdapter.MyViewHolder> {

    private ArrayList<CashDO> cashDOS;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRecieptNumber, tvCustomerName;
        private TextView tvAmount;

        public MyViewHolder(View view) {
            super(view);
            tvAmount = view.findViewById(R.id.tvAmount);
            tvRecieptNumber = view.findViewById(R.id.tvRecieptNumber);
            tvCustomerName = view.findViewById(R.id.tvCustomerName);



        }
    }


    public UserActivityCashAdapter(Context context, ArrayList<CashDO> siteDOS) {
        this.context = context;
        this.cashDOS = siteDOS;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_data_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final CashDO cashDO = cashDOS.get(position);

        String amount = String.valueOf(cashDO.amount);

        holder.tvRecieptNumber.setText(cashDO.paymentId);
        holder.tvCustomerName.setText(cashDO.customerName);
        holder.tvAmount.setText(""+amount);




        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });


    }

    @Override
    public int getItemCount() {
        return cashDOS.size();
    }

}
