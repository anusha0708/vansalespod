package com.tbs.generic.vansales.common

import android.app.Application
import android.content.Context
import androidx.lifecycle.ProcessLifecycleOwner
import com.devs.acr.AutoErrorReporter
import com.tbs.generic.vansales.utils.AppLifecycleListener
import com.tbs.generic.vansales.utils.AppPrefs


/*
 * Created by developer on 22/2/19.
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        AppPrefs.initPrefs(this)
//        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "font/montserrat_regular.ttf");
//        AutoErrorReporter.get(this)
//                .setEmailAddresses("anusha.v@tema-systems.com")
//                .setEmailSubject("Crash Report of Vansales(POD)")
//                .start();

        // register observer
        ProcessLifecycleOwner.get().lifecycle.addObserver(AppLifecycleListener())

    }

}