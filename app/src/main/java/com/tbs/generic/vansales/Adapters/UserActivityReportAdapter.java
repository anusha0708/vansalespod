package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.TransactionChequeDO;
import com.tbs.generic.vansales.R;

import java.util.ArrayList;

public class UserActivityReportAdapter extends RecyclerView.Adapter<UserActivityReportAdapter.MyViewHolder> {

    private ArrayList<TransactionChequeDO> siteDOS;
    private Context context;
    private String customerId;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRecieptNumber, tvCustomerName;
        private TextView tvAmount;

        public MyViewHolder(View view) {
            super(view);
            tvAmount = view.findViewById(R.id.tvAmount);
            tvRecieptNumber = view.findViewById(R.id.tvRecieptNumber);
            tvCustomerName = view.findViewById(R.id.tvCustomerName);



        }
    }


    public UserActivityReportAdapter(Context context, ArrayList<TransactionChequeDO> siteDOS, String customerID) {
        this.context = context;
        this.siteDOS = siteDOS;
        this.customerId = customerID;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_data_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final TransactionChequeDO unPaidInvoiceDO = siteDOS.get(position);

        String amount = String.valueOf(unPaidInvoiceDO.chequeAmount);

        holder.tvRecieptNumber.setText(unPaidInvoiceDO.chequePayment);
        holder.tvCustomerName.setText(unPaidInvoiceDO.chequeCustomer);
        holder.tvAmount.setText(""+amount);




        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });


    }

    @Override
    public int getItemCount() {
        return siteDOS.size();
    }

}
