package com.tbs.generic.vansales.pdfs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.zxing.WriterException;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.CreateInvoicePaymentDO;
import com.tbs.generic.vansales.Model.PdfInvoiceDo;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.pdfs.utils.PDFConstants;
import com.tbs.generic.vansales.pdfs.utils.PDFOperations;
import com.tbs.generic.vansales.pdfs.utils.PreviewActivity;
import com.tbs.generic.vansales.utils.CalendarUtils;
import com.tbs.generic.vansales.utils.NumberToWord;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by VenuAppasani on 04-11-2018.
 * Copyright (C) 2018 TBS - All Rights Reserved
 */
public class BGInvoicePdf {

    private static Context context;
    private Document document = null;
    private PdfPTable parentTable;

    private Font normalFont, boldFont;

    private BGInvoicePdf() {

    }

    public static BGInvoicePdf getBuilder(Context mContext) {
        //create pdf here
        context = mContext;
        return new BGInvoicePdf();
    }

    private CreateInvoicePaymentDO createInvoicePaymentDO;

    @SuppressLint("StaticFieldLeak")
    public BGInvoicePdf build(CreateInvoicePaymentDO createInvoicePaymentDO, String type) {
        this.createInvoicePaymentDO = createInvoicePaymentDO;
        Log.d("crate--->", createInvoicePaymentDO + "");
        new AsyncTask<Void, Void, Void>() {

            ProgressDialog progressDialog = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setMessage(context.getResources().getString(R.string.invoice_generate));
//                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    parentTable = new PdfPTable(1);
                    parentTable.setWidthPercentage(100);
                    init(type);

                    addHeaderLogo();

                    addHeaderLabel();
                    addCustomerDetailsToPdf();
//                    shipmentDetailsToPdf(); using in above method
                    addProductsTableLabels();
                    addProductsToPdf();
                    addSignature(createInvoicePaymentDO.signature);
                    addFooterToPdf();
                    addQRCode();

                    document.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                progressDialog.cancel();
                sendPDfewDoc();
            }
        }.execute();
        return this;
    }

    @SuppressLint("StaticFieldLeak")
    public BGInvoicePdf createHistoryPDF(CreateInvoicePaymentDO createInvoicePaymentDO, String type) {
        this.createInvoicePaymentDO = createInvoicePaymentDO;
        Log.d("create--->", createInvoicePaymentDO + "");
        new AsyncTask<Void, Void, Void>() {

            ProgressDialog progressDialog = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setMessage(context.getString(R.string.preparing_invoice_please_wait));
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    parentTable = new PdfPTable(1);
                    parentTable.setWidthPercentage(100);
                    init(type);

                    addHeaderLogo();

                    addHeaderLabel();
                    addCustomerDetailsToPdf();
//                    shipmentDetailsToPdf(); using in above method
                    addProductsTableLabels();
                    addProductsToPdf();
                    addSignature(createInvoicePaymentDO.signature);

                    addFooterToPdf();
                    addQRCode();

                    document.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressDialog.cancel();


//                Intent intent = new Intent(context.getApplicationContext(), com.tbs.brothersgas.haadhir.history.PreviewActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                intent.putExtra("KEY", PDFConstants.BG_INVOICE_PDF_NAME);
//                intent.putExtra("ID",createInvoicePaymentDO.invoiceNumber);
//
//                context.startActivity(intent);
            }
        }.execute();
        return this;
    }

    @SuppressLint("StaticFieldLeak")
    public BGInvoicePdf createPDF(CreateInvoicePaymentDO createInvoicePaymentDO, String type) {
        this.createInvoicePaymentDO = createInvoicePaymentDO;
        Log.d("create--->", createInvoicePaymentDO + "");
        new AsyncTask<Void, Void, Void>() {

            ProgressDialog progressDialog = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setMessage(context.getString(R.string.preparing_invoice_please_wait));
                progressDialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    parentTable = new PdfPTable(1);
                    parentTable.setWidthPercentage(100);
                    init(type);

                    addHeaderLogo();

                    addHeaderLabel();
                    addCustomerDetailsToPdf();
//                    shipmentDetailsToPdf(); using in above method
                    addProductsTableLabels();
                    addProductsToPdf();
                    addSignature(createInvoicePaymentDO.signature);

                    addFooterToPdf();
                    addQRCode();

                    document.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressDialog.cancel();


                Intent intent = new Intent(context.getApplicationContext(), PreviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("KEY", PDFConstants.BG_INVOICE_PDF_NAME);
                context.startActivity(intent);
            }
        }.execute();
        return this;
    }

    private void init(String type) {
        boldFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);
        normalFont = new Font(Font.FontFamily.COURIER, 10.0f, Font.NORMAL, BaseColor.BLACK);
        if (type.equalsIgnoreCase("Preview")) {
            normalFont = boldFont;
        }
        File file = new File(Util.getAppPath(context) + "BGInvoice.pdf");
        if (file.exists())
            file.delete();
        try {
            file.createNewFile();
            document = new Document();
            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(file));
            // Open to write
            document.open();
            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("TBS");
            document.addCreator("Kishore Ganji");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addHeaderLogo() {
        try {
            document.add(PDFOperations.getInstance().
                    getHeadrLogoIOSCertificateLogo(context, document, createInvoicePaymentDO.companyCode));
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }


    private void addHeaderLabel() {
        try {
//            Font headerLabelFont = new Font(Font.FontFamily.COURIER, 11.0f, Font.BOLD, BaseColor.BLACK);
//            Paragraph headerLabel = new Paragraph(PDFConstants.HEADER_TEXT_BG_INVOICE, headerLabelFont);
//            headerLabel.setAlignment(Element.ALIGN_CENTER);
//            headerLabel.setPaddingTop(10);
//            document.add(headerLabel);
//            addEmptySpaceLine(1);

            Font headerLabelFont = new Font(Font.FontFamily.COURIER, 11.0f, Font.BOLD, BaseColor.BLACK);
//            Paragraph headerLabel = new Paragraph(PDFConstants.HEADER_1, headerLabelFont);
            Paragraph headerLabel = new Paragraph(createInvoicePaymentDO.supplierName, headerLabelFont);

            headerLabel.setAlignment(Element.ALIGN_CENTER);
            headerLabel.setPaddingTop(10);
            document.add(headerLabel);
//            Paragraph headerLabel1 = new Paragraph(PDFConstants.HEADER_2, headerLabelFont);

            Paragraph headerLabel1 = new Paragraph(context.getString(R.string.bg_invoice_name), headerLabelFont);
            headerLabel1.setAlignment(Element.ALIGN_CENTER);
            headerLabel1.setPaddingTop(10);
            document.add(headerLabel1);
            addEmptySpaceLine(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCustomerDetailsToPdf() {
        try {

            PdfPTable headerParentTable = new PdfPTable(1);
            headerParentTable.setPaddingTop(10f);
            headerParentTable.setWidthPercentage(100);

            float[] columnWidths = {3, 0.3f, 5.5f, 4f, 0.3f, 2.5f};
            PdfPTable headerTable = new PdfPTable(columnWidths);
            headerTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            cellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellOne1 = new PdfPCell();
            cellOne1.setBorder(Rectangle.NO_BORDER);

            PdfPCell cellTwo = new PdfPCell();
            cellTwo.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree = new PdfPCell();
            cellThree.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellThree1 = new PdfPCell();
            cellThree1.setBorder(Rectangle.NO_BORDER);
            PdfPCell cellFour = new PdfPCell();
            cellFour.setBorder(Rectangle.NO_BORDER);


            cellOne.addElement(new Phrase(context.getString(R.string.invoice_number), boldFont));
            cellOne.addElement(new Phrase(context.getString(R.string.customer_name), boldFont));
            cellOne.addElement(new Phrase(context.getString(R.string.customer_trn), boldFont));
//            cellOne.addElement(new Phrase(PDFConstants.PAYMENT_TERM, boldFont));

            cellOne.addElement(new Phrase(context.getString(R.string.customer_address), boldFont));

            cellOne1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            cellOne1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            cellOne1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            cellOne1.addElement(new Phrase(context.getString(R.string.colon), normalFont));

            cellTwo.addElement(new Phrase(createInvoicePaymentDO.invoiceNumber, normalFont));
            cellTwo.addElement(new Phrase(createInvoicePaymentDO.customerDescription, normalFont));
            if (createInvoicePaymentDO.customerTrn.isEmpty()) {
                cellTwo.addElement(new Phrase("                     ", normalFont));

            } else {

                cellTwo.addElement(new Phrase(createInvoicePaymentDO.customerTrn, normalFont));
            }
//            cellTwo.addElement(new Phrase(createInvoicePaymentDO.paymentTerm, normalFont));

            cellTwo.addElement(new Phrase(getAddress(true), normalFont));

            cellThree.addElement(new Phrase(context.getString(R.string.date_pdf), boldFont));
            cellThree.addElement(new Phrase(context.getString(R.string.time_pdf), boldFont));
            cellThree.addElement(new Phrase(context.getString(R.string.user_id), boldFont));
            cellThree.addElement(new Phrase(context.getString(R.string.user_name), boldFont));

            cellThree1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            cellThree1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            cellThree1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            cellThree1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            if (createInvoicePaymentDO.createdDate != null && createInvoicePaymentDO.createdDate.length() > 0) {
                String dMonth = createInvoicePaymentDO.createdDate.substring(4, 6);
                String dyear = createInvoicePaymentDO.createdDate.substring(0, 4);
                String dDate = createInvoicePaymentDO.createdDate.substring(Math.max(createInvoicePaymentDO.createdDate.length() - 2, 0));

                cellFour.addElement(new Phrase(dDate + "-" + dMonth + "-" + dyear, normalFont));
            } else {
                cellFour.addElement(new Phrase(CalendarUtils.getPDFDate(context), normalFont));

            }
            if (createInvoicePaymentDO.createdTime != null && createInvoicePaymentDO.createdTime.length() > 0) {
                cellFour.addElement(new Phrase(createInvoicePaymentDO.createdTime, normalFont));

            }
//            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, ""), normalFont));
//            cellFour.addElement(new Phrase(((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, ""), normalFont));
            cellFour.addElement(new Phrase(createInvoicePaymentDO.createUserID, normalFont));
            cellFour.addElement(new Phrase(createInvoicePaymentDO.createUserName, normalFont));
            headerTable.addCell(cellOne);
            headerTable.addCell(cellOne1);
            headerTable.addCell(cellTwo);
            headerTable.addCell(cellThree);
            headerTable.addCell(cellThree1);
            headerTable.addCell(cellFour);

            PdfPCell pdfPCell = new PdfPCell();
            pdfPCell.addElement(headerTable);


            float[] shipmentColumnsWidth = {3, 0.3f, 10};
            PdfPTable shipmentTable = new PdfPTable(shipmentColumnsWidth);
            shipmentTable.setWidthPercentage(100);
            PdfPCell shipCellOne = new PdfPCell();
            shipCellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell shipCellOne1 = new PdfPCell();
            shipCellOne1.setBorder(Rectangle.NO_BORDER);
            PdfPCell shipCellTwo = new PdfPCell();
            shipCellTwo.setBorder(Rectangle.NO_BORDER);


            shipCellOne.addElement(new Phrase(context.getString(R.string.supplier_name), boldFont));
            shipCellOne.addElement(new Phrase(context.getString(R.string.supplier_trn), boldFont));
            shipCellOne.addElement(new Phrase(context.getString(R.string.registered_address), boldFont));

            shipCellOne1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            shipCellOne1.addElement(new Phrase(context.getString(R.string.colon), normalFont));
            shipCellOne1.addElement(new Phrase(context.getString(R.string.colon), normalFont));

            shipCellTwo.addElement(new Phrase(createInvoicePaymentDO.siteDescription, normalFont));
            if (createInvoicePaymentDO.supplierTrn.isEmpty()) {
                cellTwo.addElement(new Phrase("                     ", normalFont));

            } else {

                shipCellTwo.addElement(new Phrase(createInvoicePaymentDO.supplierTrn, normalFont));
            }
            shipCellTwo.addElement(new Phrase(getAddress(false), normalFont));
            shipmentTable.addCell(shipCellOne);
            shipmentTable.addCell(shipCellOne1);
            shipmentTable.addCell(shipCellTwo);
            shipmentTable.addCell(getBorderlessCell("", Element.ALIGN_CENTER));

            pdfPCell.addElement(shipmentTable);
            headerParentTable.addCell(pdfPCell);
            document.add(headerParentTable);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void shipmentDetailsToPdf() {

        try {

            float[] shipmentColumnsWidth = {2, 10};
            PdfPTable shipmentTable = new PdfPTable(shipmentColumnsWidth);
            shipmentTable.setWidthPercentage(100);
            PdfPCell shipCellOne = new PdfPCell();
            shipCellOne.setBorder(Rectangle.NO_BORDER);
            PdfPCell shipCellTwo = new PdfPCell();
            shipCellTwo.setBorder(Rectangle.NO_BORDER);


            shipCellOne.addElement(new Phrase(""+context.getString(R.string.supplier_name), boldFont));
            shipCellOne.addElement(new Phrase(context.getString(R.string.supplier_trn), boldFont));
            shipCellOne.addElement(new Phrase(context.getString(R.string.registered_address), boldFont));
            shipCellOne.addElement(new Phrase("  \n"));

            shipCellTwo.addElement(new Phrase(" : " + ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.SITE_NAME, ""), normalFont));
            shipCellTwo.addElement(new Phrase(" : " + createInvoicePaymentDO.supplierTrn, normalFont));
            shipCellTwo.addElement(new Phrase(" : " + createInvoicePaymentDO.customerStreet + ", " + createInvoicePaymentDO.customerLandMark + ", P.O. Box "
                    + createInvoicePaymentDO.customerPostalCode + ", " + createInvoicePaymentDO.customerCity + ".", normalFont));
            shipmentTable.addCell(shipCellOne);
            shipmentTable.addCell(shipCellTwo);

            document.add(shipmentTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addProductsTableLabels() {
        try {

            float[] productColumnsWidth = {2.5f, 10, 3, 4, 4};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);
            PdfPCell cellOne = new PdfPCell();
            PdfPCell cellTwo = new PdfPCell();
            PdfPCell cellThree = new PdfPCell();
            PdfPCell cellFour = new PdfPCell();
            PdfPCell cellFive = new PdfPCell();

            Paragraph pOne = new Paragraph(context.getString(R.string.sr_no), boldFont);
            pOne.setAlignment(Element.ALIGN_CENTER);
            cellOne.addElement(pOne);

            Paragraph pTwo = new Paragraph(context.getString(R.string.particulars), boldFont);
            pTwo.setAlignment(Element.ALIGN_LEFT);
            cellTwo.addElement(pTwo);

            Paragraph pThree = new Paragraph(context.getString(R.string.qty), boldFont);
            pThree.setAlignment(Element.ALIGN_CENTER);
            cellThree.addElement(pThree);

            Paragraph pFour = new Paragraph(context.getString(R.string.unit_price), boldFont);
            pFour.setAlignment(Element.ALIGN_CENTER);
            cellFour.addElement(pFour);

            Paragraph pFive = new Paragraph(context.getString(R.string.amount_aed), boldFont);
            pFive.setAlignment(Element.ALIGN_CENTER);
            cellFive.addElement(pFive);


            productsTable.addCell(cellOne);
            productsTable.addCell(cellTwo);
            productsTable.addCell(cellThree);
            productsTable.addCell(cellFour);
            productsTable.addCell(cellFive);

            document.add(productsTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addProductsToPdf() {

        try {

            float[] productColumnsWidth = {2.5f, 10, 3, 4, 4};
            PdfPTable productsTable = new PdfPTable(productColumnsWidth);
            productsTable.setWidthPercentage(100);

            if (createInvoicePaymentDO.pdfInvoiceDos != null && createInvoicePaymentDO.pdfInvoiceDos.size() > 0) {

                for (int i = 0; i < createInvoicePaymentDO.pdfInvoiceDos.size(); i++) {
                    PdfInvoiceDo pdfInvoiceDo = createInvoicePaymentDO.pdfInvoiceDos.get(i);
                    PdfPCell cellOne = new PdfPCell();
                    PdfPCell cellTwo = new PdfPCell();
                    PdfPCell cellThree = new PdfPCell();
                    PdfPCell cellFour = new PdfPCell();
                    PdfPCell cellFive = new PdfPCell();

                    cellOne.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prOne = new Paragraph("" + (i + 1), normalFont);
                    prOne.setAlignment(Element.ALIGN_CENTER);
                    cellOne.addElement(prOne);
                    cellOne.addElement(new Phrase("\n"));
                    cellOne.addElement(new Phrase("\n"));

                    cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cellTwo.addElement(new Phrase("" + pdfInvoiceDo.productDesccription, normalFont));
                    cellTwo.addElement(new Phrase(context.getString(R.string.discount), normalFont));
                    cellTwo.addElement(new Phrase(context.getString(R.string.vat_pdf) + createInvoicePaymentDO.taxRate + "%", normalFont));

                    cellThree.setHorizontalAlignment(Element.ALIGN_CENTER);
                    Paragraph prThree = new Paragraph("" + pdfInvoiceDo.deliveredQunatity + " " + pdfInvoiceDo.quantityUnits, normalFont);
                    prThree.setAlignment(Element.ALIGN_CENTER);
                    cellThree.addElement(prThree);
                    cellThree.addElement(new Phrase("\n"));
                    cellThree.addElement(new Phrase("\n"));

                    cellFour.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    Paragraph prFour = new Paragraph("" + pdfInvoiceDo.grossPrice, normalFont);
                    prFour.setAlignment(Element.ALIGN_RIGHT);
                    cellFour.addElement(prFour);
                    cellFour.addElement(new Phrase("\n"));
                    cellFour.addElement(new Phrase("\n"));

                    cellFive.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    Paragraph prFive = new Paragraph("" + pdfInvoiceDo.amount, normalFont);
                    prFive.setAlignment(Element.ALIGN_RIGHT);
                    cellFive.addElement(prFive);
                    Paragraph prFive1 = new Paragraph("" + pdfInvoiceDo.discount, normalFont);
                    prFive1.setAlignment(Element.ALIGN_RIGHT);
                    cellFive.addElement(prFive1);
                    Paragraph prFive2 = new Paragraph("" + pdfInvoiceDo.vatPercentage, normalFont);
                    prFive2.setAlignment(Element.ALIGN_RIGHT);
                    cellFive.addElement(prFive2);


                    productsTable.addCell(cellOne);
                    productsTable.addCell(cellTwo);
                    productsTable.addCell(cellThree);
                    productsTable.addCell(cellFour);
                    productsTable.addCell(cellFive);
                }
                document.add(productsTable);

                float[] productColumnsWidth1 = {2.5f, 10, 3, 4, 4};
                PdfPTable totalTable = new PdfPTable(productColumnsWidth1);
                totalTable.setWidthPercentage(100);

                PdfPCell cellOne = new PdfPCell();
                PdfPCell cellTwo = new PdfPCell();
                PdfPCell cellThree = new PdfPCell();
                PdfPCell cellFour = new PdfPCell();
                PdfPCell cellFive = new PdfPCell();

                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));
                cellOne.addElement(new Phrase("\n"));

                cellTwo.setHorizontalAlignment(Element.ALIGN_LEFT);
                cellTwo.addElement(new Phrase(context.getString(R.string.total_gross), normalFont));
                cellTwo.addElement(new Phrase(context.getString(R.string.discount) , normalFont));
                cellTwo.addElement(new Phrase(context.getString(R.string.total_vat) , normalFont));
                cellTwo.addElement(new Phrase(context.getString(R.string.vat) , normalFont));
                cellTwo.addElement(new Phrase(context.getString(R.string.total) , normalFont));

                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase("\n"));
                cellThree.addElement(new Phrase(""));

                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase("\n"));
                cellFour.addElement(new Phrase(""));

                cellFive.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cellFive.addElement(getParagraph("" + /*getTotalGrossAmount(createInvoicePaymentDO.pdfInvoiceDos)*/createInvoicePaymentDO.totalGrossAmount, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.totalDiscount, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.excludingTax, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.totalTax, Element.ALIGN_RIGHT));
                cellFive.addElement(getParagraph("" + createInvoicePaymentDO.includingTax, Element.ALIGN_RIGHT));

                totalTable.addCell(cellOne);
                totalTable.addCell(cellTwo);
                totalTable.addCell(cellThree);
                totalTable.addCell(cellFour);
                totalTable.addCell(cellFive);

                document.add(totalTable);

                PdfPTable noteTable = new PdfPTable(1);
                noteTable.setWidthPercentage(100);
                double totalAmount = getTotalAmount(createInvoicePaymentDO.pdfInvoiceDos);

                int afterDcimalVal = PDFOperations.getInstance().anyMethod(totalAmount);

                PdfPCell noteCell = new PdfPCell(new Phrase(context.getString(R.string.aed)+ getAmountInLetters(totalAmount) + context.getString(R.string.and)+ getAmountInLetters(afterDcimalVal) +context.getString(R.string.cents), boldFont));
//                PdfPCell noteCell = new PdfPCell(new Phrase("AED : Eighteen Thousand Eight Hundred Forty Seven and 50/100 Only-", boldFont));
                noteCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                noteCell.setPadding(10);
                noteTable.addCell(noteCell);

                document.add(noteTable);
            }
            addEmptySpaceLine(1);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getDecimalAmount(String amount) {
        try {
            DecimalFormat df = new DecimalFormat("0.00");
            return df.format(amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private double getTotalGrossAmount(ArrayList<PdfInvoiceDo> pdfInvoiceDos) {
        double grossAmount = 0.0f;
        try {
            for (int i = 0; i < pdfInvoiceDos.size(); i++) {
                grossAmount = grossAmount + Double.parseDouble(pdfInvoiceDos.get(i).grossPrice);
            }
        } catch (Exception e) {
            return grossAmount;
        }

        return grossAmount;
    }

    private double getTotalDiscount(ArrayList<PdfInvoiceDo> pdfInvoiceDos) {
        double totalDiscountAmount = 0.0f;
        try {
            for (int i = 0; i < pdfInvoiceDos.size(); i++) {
                totalDiscountAmount = totalDiscountAmount + Double.parseDouble(pdfInvoiceDos.get(i).excludingTax);
            }
        } catch (Exception e) {
            return totalDiscountAmount;
        }

        return totalDiscountAmount;
    }

    private double getTotalExlTax(ArrayList<PdfInvoiceDo> pdfInvoiceDos) {
        double totalExlTax = 0.0f;
        try {
            for (int i = 0; i < pdfInvoiceDos.size(); i++) {
                totalExlTax = totalExlTax + Double.parseDouble(pdfInvoiceDos.get(i).excludingTax);
            }
        } catch (Exception e) {
            return totalExlTax;
        }

        return totalExlTax;
    }

    private double getTotalVat(ArrayList<PdfInvoiceDo> pdfInvoiceDos) {
        double totalVat = 0.0f;
        try {
            for (int i = 0; i < pdfInvoiceDos.size(); i++) {
                totalVat = totalVat + Double.parseDouble(pdfInvoiceDos.get(i).excludingTax);
            }
        } catch (Exception e) {
            return totalVat;
        }

        return totalVat;
    }

    private double getTotalAmount(ArrayList<PdfInvoiceDo> pdfInvoiceDos) {
        double totalAmount = 0.0f;
        try {
            /*totalAmount = getTotalDiscount(pdfInvoiceDos) -
                    getTotalGrossAmount(pdfInvoiceDos) +
                    getTotalExlTax(pdfInvoiceDos) -
                    getTotalVat(pdfInvoiceDos);*/
            return Double.parseDouble(createInvoicePaymentDO.includingTax);

        } catch (Exception e) {
            return totalAmount;
        }

    }

    private String getAmountInLetters(double amount) {
        return new NumberToWord().convert((int) amount);
        //return NumberToWords.getNumberToWords().getTextFromNumbers(amount);
    }

    private void addEmptySpaceLine(int noOfLines) {
        try {
            PdfPTable emptyTable = new PdfPTable(1);
            emptyTable.setWidthPercentage(100);
            for (int i = 0; i < noOfLines; i++) {
                PdfPCell emptyCell = new PdfPCell();
                emptyCell.setBorder(Rectangle.NO_BORDER);
                emptyCell.addElement(new Paragraph("\n"));
                emptyTable.addCell(emptyCell);
            }
            document.add(emptyTable);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private Paragraph getParagraph(String content, int alignment) {
        Paragraph p1 = new Paragraph(content, normalFont);
        p1.setAlignment(alignment);
        return p1;
    }

    private void addQRCode() {
        try {
//

            float[] productColumnsWidth1 = {2, 6};
            PdfPTable sig = new PdfPTable(productColumnsWidth1);
            sig.setWidthPercentage(100);
            Image image = getQRCODE();
            if (image != null) {
                image.setBorder(Rectangle.NO_BORDER);
                PdfPCell pdfPCell = new PdfPCell();
                pdfPCell.setBorder(Rectangle.NO_BORDER);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
                pdfPCell.addElement(image);
                sig.addCell(pdfPCell);
                PdfPCell pCell = new PdfPCell();
                pCell.setBorder(Rectangle.NO_BORDER);
                sig.addCell(pCell);
                document.add(sig);

            }

            PdfPTable custTable = new PdfPTable(1);
            custTable.setWidthPercentage(100);


            PdfPCell pdfPCellSig = new PdfPCell(new Phrase("          "+context.getString(R.string.scan_here), normalFont));
            pdfPCellSig.setBorder(Rectangle.NO_BORDER);
            pdfPCellSig.setHorizontalAlignment(Element.ALIGN_LEFT);
            custTable.addCell(pdfPCellSig);
            document.add(custTable);

            addEmptySpaceLine(1);


        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addSignature(String sign) {
        try {
//

            float[] productColumnsWidth1 = {2, 6};
            PdfPTable sig = new PdfPTable(productColumnsWidth1);
            sig.setWidthPercentage(100);
            Image image = PDFOperations.getInstance().getInvoiceSignatureFromFile(sign);
            if (image != null) {
                image.setBorder(Rectangle.NO_BORDER);
                PdfPCell pdfPCell = new PdfPCell();
                pdfPCell.setBorder(Rectangle.NO_BORDER);
                pdfPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                pdfPCell.setVerticalAlignment(Element.ALIGN_CENTER);
                pdfPCell.addElement(image);
                sig.addCell(pdfPCell);
                PdfPCell pCell = new PdfPCell();
                pCell.setBorder(Rectangle.NO_BORDER);
                sig.addCell(pCell);
                document.add(sig);
            }

            PdfPTable custTable = new PdfPTable(1);
            custTable.setWidthPercentage(100);


            PdfPCell pdfPCellSig = new PdfPCell(new Phrase(context.getString(R.string.customer_sign), normalFont));
            pdfPCellSig.setBorder(Rectangle.NO_BORDER);
            pdfPCellSig.setHorizontalAlignment(Element.ALIGN_LEFT);
            custTable.addCell(pdfPCellSig);
            document.add(custTable);

            addEmptySpaceLine(1);


        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void addFooterToPdf() {
        try {

//            Paragraph signPara = new Paragraph("                            ");
//            document.add(signPara);//user signature
//            PdfPTable custTable3 = new PdfPTable(1);
//            custTable3.setWidthPercentage(100);
//            PdfPCell pdf = new PdfPCell(new Phrase(context.getString(R.string.delivery_remarks)+" "+context.getString(R.string.colon) + createInvoicePaymentDO.deliveryRemarks, boldFont));
//
//            pdf.setBorder(Rectangle.NO_BORDER);
//            pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//            custTable3.addCell(pdf);
//
//            PdfPTable custTable1 = new PdfPTable(1);
//            custTable1.setWidthPercentage(100);
//            PdfPCell pdf1 = new PdfPCell(new Phrase(context.getString(R.string.inv_remarks)+" "+context.getString(R.string.colon)  + createInvoicePaymentDO.remarks, boldFont));
//
//            pdf1.setBorder(Rectangle.NO_BORDER);
//            pdf1.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//            custTable1.addCell(pdf1);

            PdfPTable custTable2 = new PdfPTable(1);
            custTable2.setWidthPercentage(100);
            PdfPCell pdfCe = new PdfPCell(new Phrase(context.getString(R.string.computer_generated), boldFont));
//            PdfPCell pdfCe = new PdfPCell(new Phrase("THIS IS COMPUTER GENERATED DOCUMENT AND DOES NOT REQUIRE SIGNATURE", boldFont));

            pdfCe.setBorder(Rectangle.NO_BORDER);
            pdfCe.setHorizontalAlignment(Element.ALIGN_LEFT);

            custTable2.addCell(pdfCe);
//            document.add(custTable3);
//            document.add(custTable1);
            document.add(custTable2);

            document.add(getBorderlessCell("", Element.ALIGN_CENTER));

            float[] regColWidth = {2.2f, 7};
            PdfPTable table = new PdfPTable(regColWidth);
            table.setWidthPercentage(100);

            PdfPCell regCell = new PdfPCell(new Phrase(context.getString(R.string.registered_address_label), boldFont));
            regCell.setHorizontalAlignment(Rectangle.LEFT);
            regCell.setBorder(Rectangle.NO_BORDER);

            PdfPCell regCell2 = new PdfPCell(new Phrase(getAddress(false), normalFont));
            regCell2.setHorizontalAlignment(Rectangle.LEFT);
            regCell2.setBorder(Rectangle.NO_BORDER);

            table.addCell(regCell);
            table.addCell(regCell2);
            document.add(table);

            document.add(getBorderlessCell("", Element.ALIGN_CENTER));

            float[] regAdd = {5, 5};
            PdfPTable regAddTable = new PdfPTable(regAdd);
            regAddTable.setWidthPercentage(100);

            PdfPCell regAddCell = new PdfPCell();
            regAddCell.setHorizontalAlignment(Rectangle.LEFT);
            regAddCell.setBorder(Rectangle.NO_BORDER);
            regAddCell.addElement(new Phrase(context.getString(R.string.site_mob) + createInvoicePaymentDO.siteMobile, normalFont));
            regAddCell.addElement(new Phrase(context.getString(R.string.email_pdf) + createInvoicePaymentDO.siteEmail2, normalFont));

            PdfPCell regAddCell2 = new PdfPCell();
            regAddCell2.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
            regAddCell2.setBorder(Rectangle.NO_BORDER);
            regAddCell2.addElement(new Phrase(context.getString(R.string.fax)+ createInvoicePaymentDO.siteFax, normalFont));
            regAddCell2.addElement(new Phrase(context.getString(R.string.website) + createInvoicePaymentDO.siteWebEmail, normalFont));

            regAddTable.addCell(regAddCell);
            regAddTable.addCell(regAddCell2);

            document.add(regAddTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void sendPDfewDoc() {
//        String shipmentType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "");
//        if (shipmentType.equalsIgnoreCase("Scheduled")) {
//            ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(context).getActiveDeliveryMainDo(context);
//            if (activeDeliverySavedDo.invoiceEmail.length() > 0) {
//                PDFOperations.getInstance().sendpdfMail(context,
//                        activeDeliverySavedDo.invoiceEmail,
//                        activeDeliverySavedDo.customerDescription,
//                        PDFConstants.BG_INVOICE_PDF_NAME, PDFConstants.BG_INVOCE_NAME);
//            } else {
//                ((BaseActivity) context).showToast("Please provide email address");
//            }
//
//        } else {
//
//            CustomerDo customerDo = StorageManager.getInstance(context).getCurrentSpotSalesCustomer(context);
//            if (customerDo.invoiceEmail.length() > 0) {
//                PDFOperations.getInstance().sendpdfMail(context,
//                        customerDo.invoiceEmail,
//                        customerDo.customerName,
//                        PDFConstants.BG_INVOICE_PDF_NAME, PDFConstants.BG_INVOCE_NAME);
//            } else {
//                ((BaseActivity) context).showToast("Please provide email address");
//            }
//
//        }
        if (createInvoicePaymentDO != null && createInvoicePaymentDO.email != null && !createInvoicePaymentDO.email.isEmpty()) {
            PDFOperations.getInstance().sendpdfMail(context,
                    createInvoicePaymentDO.email,
                    createInvoicePaymentDO.customerDescription,
                    PDFConstants.BG_INVOICE_PDF_NAME, context.getString(R.string.bg_invoice_name));
        } else {
            ((BaseActivity) context).showToast(context.getString(R.string.please_provide_email_address));
        }


    }

    private PdfPCell getBorderlessCell(String elementName, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(elementName, normalFont));
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    private String getAddress(boolean isCustomer) {

        String street, landMark, town, postal, city, countryName;
        if (isCustomer) {
            street = createInvoicePaymentDO.customerStreet;
            landMark = createInvoicePaymentDO.customerLandMark;
            town = createInvoicePaymentDO.customerTown;
            postal = createInvoicePaymentDO.customerPostalCode;
            city = createInvoicePaymentDO.customerCity;
            countryName = createInvoicePaymentDO.countryname;
        } else {
            street = createInvoicePaymentDO.siteAddress1;
            landMark = createInvoicePaymentDO.siteAddress2;
            town = createInvoicePaymentDO.siteAddress3;
            city = createInvoicePaymentDO.siteCity;
            postal = "";
            countryName = createInvoicePaymentDO.siteCountry;
        }
        String finalString = "";

        if (!TextUtils.isEmpty(street)) {
            finalString += street + ", ";
        }
        if (!TextUtils.isEmpty(landMark)) {
            finalString += landMark + ", ";
        }
        if (!TextUtils.isEmpty(town)) {
            finalString += town + ", ";
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += city + ", ";
        }
        if (!TextUtils.isEmpty(postal)) {
            finalString += postal + ", ";
        }
//        if (!TextUtils.isEmpty(countryName)) {
//            finalString += countryName;
//        }

        return finalString;
    }

    public Image getQRCODE() {

        try {

            try {
                Bitmap qrCode = Util.encodeAsBitmap(context, createInvoicePaymentDO.invoiceNumber);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                qrCode = getResizedBitmap(qrCode, 200);

                qrCode.compress(Bitmap.CompressFormat.PNG, 30, stream);
                return Image.getInstance(stream.toByteArray());
            } catch (WriterException e) {
                e.printStackTrace();
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BadElementException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Bitmap getResizedBitmap(Bitmap image, int size) {

        return Bitmap.createScaledBitmap(image, 250, 250, true);
    }

}
