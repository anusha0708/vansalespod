package com.tbs.generic.vansales.listeners;
/*
 * Created by developer on 11/1/18.
 */

/**
 * The interface String listner.
 */
public interface StringListner {
    /**
     * Gets prase string.
     *
     * @param value the value
     */
    void getString(String value);
}
