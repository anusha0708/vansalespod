package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.CreateDeliveryDO;
import com.tbs.generic.vansales.Model.CreateDeliveryMainDO;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CreatePurchaseReturnRequest extends AsyncTask<String, Void, Boolean> {

    private CreateDeliveryMainDO createDeliveryMainDO;
    private CreateDeliveryDO createDeliveryDO;
    private Context mContext;
    private String purchaseReciept, site, location, lattitude, longitude, address, signature;
    ArrayList<ActiveDeliveryDO> createDeliveryDoS;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    static int networkTimeOut = 60 * 1000;
    String routingId;
    int type;

    public CreatePurchaseReturnRequest(String lattitudE, String longitudE, String addresS, String purchaseRecieptt, ArrayList<ActiveDeliveryDO> createDeliveryDOS, Context mContext) {

        this.mContext = mContext;
        this.purchaseReciept = purchaseRecieptt;
        this.createDeliveryDoS = createDeliveryDOS;
        this.lattitude = lattitudE;
        this.longitude = longitudE;
        this.address = addresS;

    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    public void signature(@Nullable String signature) {
        this.signature = signature;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, CreateDeliveryMainDO createDeliveryMainDO, String msg);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
       PodDo podDo = StorageManager.getInstance(mContext).getDepartureData(mContext);
        String nonSheduledId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
        String vrID = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_XSTKVCR", nonSheduledId);
            jsonObject.put("I_XVCRNUM", vrID);

            jsonObject.put("I_YPNHNUM", purchaseReciept);
            jsonObject.put("I_YSIGNATURE", podDo.getSignatureEncode());
            if (!TextUtils.isEmpty(signature)) {
                jsonObject.put("I_YSIGNATURE", signature);
            }
            JSONArray jsonArray = new JSONArray();
            if (podDo.getCapturedImagesListBulk() != null && podDo.getCapturedImagesListBulk().size() > 0) {
                for (int i = 0; i < podDo.getCapturedImagesListBulk().size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("I_YIMAGE", podDo.getCapturedImagesListBulk().get(i));

                    jsonArray.put(i, jsonObject1);
                }
                jsonObject.put("GRP2", jsonArray);
            }
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.CREATE_PURCHASE_RETURN, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }


    String message = "";

    public boolean parseXML(String xmlString) {
        System.out.println("create delivery xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            createDeliveryMainDO = new CreateDeliveryMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        createDeliveryMainDO.createDeliveryDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        createDeliveryDO = new CreateDeliveryDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("I_YSOLD")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.customerId = text;

                            }

                        } else if (attribute.equalsIgnoreCase("I_YSALFCY")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.site = text;

                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDLNUM")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.deliveryNumber = text;

                            }


                        } else if (attribute.equalsIgnoreCase("O_YMESSAGE")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.message = text;

                            }


                        } else if (attribute.equalsIgnoreCase("O_YSTATUS")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.status = Integer.parseInt(text);

                            }


                        } else if (attribute.equalsIgnoreCase("O_YITMTYP")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.productGroupType = Integer.parseInt(text);

                            }


                        } else if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if (text.length() > 0) {
                                createDeliveryMainDO.flag = Integer.parseInt(text);

                            }


                        } else if (attribute.equalsIgnoreCase("I_YITMREF")) {
                            if (text.length() > 0) {
                                createDeliveryDO.item = text;
                            }

                        } else if (attribute.equalsIgnoreCase("I_YPRICE")) {
                            if (text.length() > 0) {
                                createDeliveryDO.price = Double.parseDouble(text);
                            }

                        } else if (attribute.equalsIgnoreCase("I_YQTY")) {
                            if (text.length() > 0) {
                                createDeliveryDO.quantity = Integer.parseInt(text);
                            }

                        }
                        text = "";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        createDeliveryMainDO.createDeliveryDOS.add(createDeliveryDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        ProgressTask.getInstance().showProgress(mContext, false, mContext.getString(R.string.retreving_details));
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, createDeliveryMainDO, ServiceURLS.message);
        }
    }
}