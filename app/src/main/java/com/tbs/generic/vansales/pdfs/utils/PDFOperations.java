package com.tbs.generic.vansales.pdfs.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Base64;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.mail.TBSMailBG;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/*
 * Created by developer on 27/1/19.
 */
public class PDFOperations {
    private static final PDFOperations ourInstance = new PDFOperations();

    public static PDFOperations getInstance() {
        return ourInstance;
    }

    private PDFOperations() {
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, 150, 100, true);
    }

    public Image getSignatureFromFile(String sign) {
        File file = new File(Environment.getExternalStorageDirectory().getPath() + "/UserInvoiceSignature/signature.png");
        if (!TextUtils.isEmpty(sign)) {
            try {
                FileInputStream ims = new FileInputStream(file);

                byte[] decodedString = Base64.decode(sign, Base64.DEFAULT);

                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//            Bitmap bmp1 = BitmapFactory.decodeStream(ims);
                Bitmap bmp = PDFOperations.getInstance().getResizedBitmap(decodedByte, 250);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                return Image.getInstance(stream.toByteArray());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (BadElementException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public Bitmap getSignBitmap(String sign) {
        if (!TextUtils.isEmpty(sign)) {
            try {

                byte[] decodedString = Base64.decode(sign, Base64.DEFAULT);

                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//            Bitmap bmp1 = BitmapFactory.decodeStream(ims);
                Bitmap bmp = PDFOperations.getInstance().getResizedBitmap(decodedByte, 250);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                return bmp;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public Image getInvoiceSignatureFromFile(String sign) {
        File file = new File(Environment.getExternalStorageDirectory().getPath() + "/UserInvoiceSignature/signature.png");
        if (!TextUtils.isEmpty(sign)) {
            try {
                FileInputStream ims = new FileInputStream(file);

                byte[] decodedString = Base64.decode(sign, Base64.DEFAULT);

                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//            Bitmap bmp1 = BitmapFactory.decodeStream(ims);
                Bitmap bmp = PDFOperations.getInstance().getResizedBitmap(decodedByte, 250);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                return Image.getInstance(stream.toByteArray());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (BadElementException e) {
                e.printStackTrace();
            }
        }
        return null;

    }

    public PdfPTable getHeadrLogoIOSCertificateLogo(Context context, Document document, String companyCode) {
        float[] columnWidths = {11, 1f};
        PdfPTable headerTable = new PdfPTable(columnWidths);
        headerTable.setPaddingTop(10);
        headerTable.setWidthPercentage(100);
        PdfPCell cellOne = new PdfPCell();
        cellOne.setBorder(Rectangle.NO_BORDER);
        PdfPCell cellTwo = new PdfPCell();
        cellTwo.setBorder(Rectangle.NO_BORDER);

        Paragraph p = new Paragraph(new Chunk(addLogo(context, document, companyCode), 20, 0, true));
        p.setAlignment(Element.ALIGN_CENTER);
        cellOne.addElement(p);
        cellOne.setFixedHeight(100f);


        //cellTwo.addElement(addISOLogo(context));
        cellTwo.setPadding(-5f);
        cellTwo.setPaddingTop(10f);
        cellTwo.setVerticalAlignment(Element.ALIGN_TOP);
        headerTable.addCell(cellOne);
        headerTable.addCell(cellTwo);
        return headerTable;
    }

    private Image addISOLogo(Context context) {
        try {
            InputStream ims = context.getAssets().open(PDFConstants.ISO_LOGO_NAME);
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image image = Image.getInstance(stream.toByteArray());
            image.setBorder(Rectangle.NO_BORDER);
            image.setPaddingTop(15f);

//            image.setScaleToFitHeight(true);
//            image.scalePercent(100,100);
            //image.setAlignment(Element.ALIGN_CENTER);
            //document.add(image);
            return image;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Image addLogo(Context context, Document document, String companyCode) {
        try {
            int indentation = 0;
            String logo = "";
            String img = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.COMPANY_IMG, "");
            if (img != null) {

                    byte[] decodedString = android.util.Base64.decode(img, android.util.Base64.DEFAULT);
                    Bitmap bmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Image image = Image.getInstance(stream.toByteArray());
                    image.setBorder(Rectangle.NO_BORDER);
                    image.setAlignment(Element.ALIGN_CENTER);
                    float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                            - document.rightMargin() - indentation) / image.getWidth()) * 100;
                    image.scalePercent(scaler);

                return image;

            }else {
                Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.tema_logo);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); // bm is the bitmap object

                Image image = Image.getInstance(baos.toByteArray());
                image.setBorder(Rectangle.NO_BORDER);
                image.setAlignment(Element.ALIGN_CENTER);
                float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                        - document.rightMargin() - indentation) / image.getWidth()) * 100;
                image.scalePercent(scaler);

                return image;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public String getFormatedDate(String actualtDate) {
        String aMonth;
        String ayear;
        String aDate;
        try {
            aMonth = actualtDate.substring(4, 6);
            ayear = actualtDate.substring(0, 4);
            aDate = actualtDate.substring(Math.max(actualtDate.length() - 2, 0));
            return aDate + "/" + aMonth + "/" + ayear;
        } catch (Exception e) {
        }
        return actualtDate;
    }


    public PdfPTable addFooterWithImage(Context context, Document document) {

        PdfPTable pdfPTable = new PdfPTable(1);
        pdfPTable.setWidthPercentage(100);
        pdfPTable.setPaddingTop(10f);

        pdfPTable.getDefaultCell().setFixedHeight(80);
        pdfPTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        pdfPTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        pdfPTable.addCell(addFooterImage(context, document));

        return pdfPTable;

    }

    public Image addFooterImage(Context context, Document document) {
        try {
            int indentation = 0;
            InputStream ims = context.getAssets().open("c_c.png");
            Bitmap bmp = BitmapFactory.decodeStream(ims);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image image = Image.getInstance(stream.toByteArray());
            image.setBorder(Rectangle.NO_BORDER);
            image.setAlignment(Element.ALIGN_CENTER);
            //fitting image into pdf
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - indentation) / image.getWidth()) * 100;
            image.scalePercent(scaler);
            //image.scaleToFit(image.getWidth(), image.getHeight());
            //image.scalePercent(10);
            //image.setAlignment(Element.ALIGN_CENTER);
            //document.add(image);
            return image;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void sendpdfMail(Context context,
                            String email,
                            String userName,
                            String pdfName,
                            String message) {
        TBSMailBG.newBuilder(context, message)
//                .withUsername("online@brothersgas.ae")
//                .withPassword("Cam52743@2018")
                .withUsername("androidsupport@tema-systems.com")
                .withPassword("Aspt@TBS!")
                .withMailto(email)
                .withSubject(message)
//        Dear TEMA Testing,
//
//                Thank you for your business, Please find the attached document for the copy of Sales Invoice.
                .withBody("Dear " + userName + ",\n\nThank you for your business, Please find the attached document for the copy of " + message)
                .withAttachments(Util.getAppPath(context) + pdfName)
                .send();
    }

    public int anyMethod(double a) {
        //if the number has two digits after the decimal point.
        return (int) ((a + 0.001) * 100) % 100;
    }
}
