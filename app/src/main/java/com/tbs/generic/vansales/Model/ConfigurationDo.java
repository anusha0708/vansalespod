package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class ConfigurationDo implements Serializable {
    public String company = "";
    public String ipAddress = "";
    public String portNumber = "";
    public String poolAlias = "";
    public String userName = "";
    public String password = "";
    public String companyImg = "";
    public String companyDescription = "";

}
