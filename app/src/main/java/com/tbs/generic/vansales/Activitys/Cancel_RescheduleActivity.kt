package com.tbs.generic.vansales.Activitys

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.BitmapFactory
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.tbs.generic.vansales.Adapters.ReasonListAdapter
import com.tbs.generic.vansales.Model.ReasonMainDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.CancelOrRescheduleRequest
import com.tbs.generic.vansales.Requests.CancelRescheduleApprovalStatusRequest
import com.tbs.generic.vansales.Requests.NewRescheduleRequest
import com.tbs.generic.vansales.Requests.ReasonsListRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.LogUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.cancel_reschedule_data.*
import kotlinx.android.synthetic.main.include_signature.btnSignature
import java.text.DecimalFormat
import java.util.*


//
class Cancel_RescheduleActivity : BaseActivity() {

    var status = ""

    var cday: Int = 0
    var cmonth: Int = 0
    var cyear: Int = 0
    lateinit var tvSelection: TextView
    lateinit var etComments: EditText
    lateinit var tvDatee: TextView
     var signature: String=""

    lateinit var dialog: Dialog
    lateinit var llSelectReason: LinearLayout
    lateinit var date: String
    lateinit var comments: String
    lateinit var reason: String
    private var reasonMainDO: ReasonMainDO = ReasonMainDO()
    lateinit var btnConfirm: Button
    lateinit var btnRequestApproval: Button
    var activityType: Int = 0

    lateinit var tvApprovalStatus: TextView
    private val TAG = "RescheduleActivity"
    var type: Int = 1
    var modifyType: Int = 6
    override fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.cancel_reschedule_data, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        hideKeyBoard(llBody)
        btnSignature.setOnClickListener {
            val intent = Intent(this, CommonSignatureActivity::class.java)
            startActivityForResult(intent, 9907)
        }
        toolbar.setNavigationOnClickListener {

            LogUtils.debug("PAUSE", "tes")
            var data = tvApprovalStatus.text.toString()
            var comments = etComments.text.toString()
            var reason = tvSelection.text.toString()
            var date = tvDatee.text.toString()

            var intent = Intent()
            intent.putExtra("data", data + "," + comments + "," + reason + "," + date + "," + type)
            setResult(123, intent)
            finish()
        }
    }


    override fun initializeControls() {
        tvScreenTitle.setText(R.string.cancel_reschedule)
        ivRefresh.visibility = View.GONE
        llSelectReason = findViewById<LinearLayout>(R.id.llSelectReason)
        etComments = findViewById<EditText>(R.id.etComments)
        tvDatee = findViewById<TextView>(R.id.tvDate)
        tvSelection = findViewById<TextView>(R.id.tvSelection)
        btnConfirm = findViewById<Button>(R.id.btnConfirm)
        btnRequestApproval = findViewById<Button>(R.id.btnRequestApproval)
        tvApprovalStatus = findViewById<TextView>(R.id.tvApprovalStatus)
        var yourRadioGroup = findViewById<RadioGroup>(R.id.radioGroup)
        var rbCacel = findViewById<RadioButton>(R.id.rbCacel)
        var rbReschedule = findViewById<RadioButton>(R.id.rbReschedule)
        if (intent.hasExtra("TYPE")) {
            activityType = intent.extras?.getInt("TYPE")!!
        }
        if(activityType==1){
            radioGroup.visibility=View.GONE
            tvScreenTitle.setText(R.string.reschedule)

            rbReschedule.isChecked = true
            tvApprovalStatus.visibility = View.GONE
            btnSignature.visibility=View.GONE
            imv_signatur.visibility=View.GONE

            etComments.setText("")
            tvDatee.isClickable = true
            tvDatee.isEnabled = true
            rbReschedule.isChecked = true
            type = 2
            modifyType = 5
            tvDatee.text = ""
            tvDatee.hint = getString(R.string.select_date)

            tvSelection.hint = getString(R.string.select_reason)
            tvSelection.text = ""
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirm.isClickable = false
            btnConfirm.isEnabled = false
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false

        }else{
            rbCacel.isChecked = true

        }

        if (rbCacel.isChecked == true) {
            tvDatee.isClickable = false
            tvDatee.isEnabled = false
        } else {
            tvDatee.isClickable = true
            tvDatee.isEnabled = true
        }
        var approval = tvApprovalStatus.text.toString()
        status = preferenceUtils.getStringFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL, "")
        if(activityType!=1){
            if (approval.equals("NA")) {
                tvApprovalStatus.text = getString(R.string.na)

            } else {
                var podDo = StorageManager.getInstance(this).getDepartureData(this)
                if (!podDo.rescheduleStatus!!.get(0).equals("")) {
                    tvSelection.text = "" + podDo.rescheduleStatus!![2]
                    etComments.setText("" + podDo.rescheduleStatus!![1])
                    tvDatee.text = "" + podDo.rescheduleStatus!![3]
                    if (podDo.rescheduleStatus!!.get(4).equals("1")) {
                        rbCacel.isChecked = true
                    } else {
                        rbReschedule.isChecked = true
                    }
                    if (podDo.rescheduleStatus?.get(0)!!.contains(resources.getString(R.string.request), true)) {

                        tvApprovalStatus.visibility = View.VISIBLE
                        tvApprovalStatus.text = "" + podDo.rescheduleStatus!![0]
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnConfirm.isClickable = false
                        btnConfirm.isEnabled = false
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false

                    } else if (podDo.rescheduleStatus?.get(0)!!.contains(resources.getString(R.string.approved), true)) {
                        tvApprovalStatus.visibility = View.VISIBLE
                        tvApprovalStatus.text = "" + podDo.rescheduleStatus!![0]

                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true

                    } else {
                        tvApprovalStatus.visibility = View.VISIBLE

                        tvApprovalStatus.text = status
                        tvApprovalStatus.text = "" + podDo.rescheduleStatus!![0]

                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnConfirm.isClickable = false
                        btnConfirm.isEnabled = false
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                    }

                } else {
                    tvApprovalStatus.visibility = View.VISIBLE

                    tvApprovalStatus.text = status
                    var c = Calendar.getInstance()
                    var mYear = c.get(Calendar.YEAR)
                    var mMonth = c.get(Calendar.MONTH)

                    var mDay = c.get(Calendar.DAY_OF_MONTH)
                    if(activityType==1){
                        tvDatee.text = ""

                    }else{
                        tvDatee.text = CalendarUtils.getPDFDate(this)

                    }
                    var data = preferenceUtils.getStringFromPreference(PreferenceUtils.REASON, "")
                    tvSelection.text = "" + data
                    data = tvSelection.text.toString()

                    btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnConfirm.isClickable = false
                    btnConfirm.isEnabled = false
                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false
                }

            }
        }

        yourRadioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.rbCacel -> {
                        rbCacel.isChecked = true
                        btnSignature.visibility=View.VISIBLE
                        type = 1
                        modifyType = 6
                        tvSelection.hint = getString(R.string.select_reason)
                        tvSelection.text = ""
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnConfirm.isClickable = false
                        btnConfirm.isEnabled = false
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                        etComments.setText("")
                        tvApprovalStatus.visibility = View.GONE
                        tvDatee.isClickable = false
                        tvDatee.isEnabled = false
                        tvDatee.text = "" + CalendarUtils.getPDFDate(applicationContext)
                        imv_signatur.visibility=View.VISIBLE

                    }
                    R.id.rbReschedule -> {
                        tvApprovalStatus.visibility = View.GONE
                        btnSignature.visibility=View.GONE
                        imv_signatur.visibility=View.GONE

                        etComments.setText("")
                        tvDatee.isClickable = true
                        tvDatee.isEnabled = true
                        rbReschedule.isChecked = true
                        type = 2
                        modifyType = 5
                        tvDatee.text = ""
                        tvDatee.hint = getString(R.string.select_date)

                        tvSelection.hint = getString(R.string.select_reason)
                        tvSelection.text = ""
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnConfirm.isClickable = false
                        btnConfirm.isEnabled = false
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                    }

                }
            }
        })


        tvDatee.setOnClickListener {
            Util.preventTwoClick(it)
            val calend = Calendar.getInstance()
            calend.add(Calendar.DAY_OF_MONTH, 1)
            val datePicker = DatePickerDialog(this@Cancel_RescheduleActivity, dateSetListener, calend.get(Calendar.YEAR), calend.get(Calendar.MONTH), calend.get(Calendar.DAY_OF_MONTH))
            datePicker.datePicker.minDate = calend.timeInMillis
            datePicker.show()
        }


        llSelectReason.setOnClickListener {
            Util.preventTwoClick(it)
            //            reasonDialog()

            tvSelection.performClick()
        }

        tvSelection.setOnClickListener {
            Util.preventTwoClick(it)
            tvApprovalStatus.visibility = View.GONE


            reasonDialog()
        }
        ivRefresh.setOnClickListener {
            Util.preventTwoClick(it)
            comments = etComments.text.toString().trim()
            date = tvDatee.text.toString().trim()
            reason = tvSelection.text.toString().trim()
            if (date.isEmpty()) {
                showToast(resources.getString(R.string.please_select_date))
            } else if (reason.isEmpty()) {
                showToast(resources.getString(R.string.please_select_reason))
            } else {
                if (tvApprovalStatus.text.toString().contains(resources.getString(R.string.request_sent))) {
                    preferenceUtils.saveString(PreferenceUtils.RESCHEDULE_APPROVAL, tvApprovalStatus.text.toString())

                    updateStatus()
                } else if (tvApprovalStatus.text.toString().contains(getString(R.string.statue_requested))) {
                    preferenceUtils.saveString(PreferenceUtils.RESCHEDULE_APPROVAL, tvApprovalStatus.text.toString())

                    updateStatus()

                }
            }
        }
        btnConfirm.setOnClickListener {
            Util.preventTwoClick(it)
            comments = etComments.text.toString().trim()
            date = tvDatee.text.toString().trim()
            reason = tvSelection.text.toString().trim()

            if (date.isEmpty()) {
                showToast(resources.getString(R.string.please_select_date))
            } else if (reason.isEmpty()) {
                showToast(resources.getString(R.string.please_select_reason))
            } else {

                if (Util.isNetworkAvailable(this)) {
                    //   cancel()
                    cancelOrResheduleApproval("")
                } else {
                    showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

                }


            }
        }
        btnRequestApproval.setOnClickListener {
            Util.preventTwoClick(it)
            comments = etComments.text.toString().trim()
            date = tvDatee.text.toString().trim()
            reason = tvSelection.text.toString().trim()

            if (date.isEmpty()) {
                showToast(getString(R.string.please_select_date))
            } else if (reason.isEmpty()) {
                showToast(getString(R.string.please_select_reason))
            } else {
                cancelOrResheduleApproval("")
            }
        }
    }

    var dateSetListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year

        val formatter = DecimalFormat("00")
        val montH = formatter.format(cmonth)
        val daY = formatter.format(cday)

        tvDatee.text = "" + daY + "/" + montH + "/" + year

        var dateFormat = "" + year + montH + daY
        preferenceUtils.saveString(PreferenceUtils.DATE_FORMAT, dateFormat)

    }
//
//    fun reschedule() {
//        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
//        comments = etComments.text.toString().trim()
//        date = tvDatee.text.toString().trim()
//        reason = tvSelection.text.toString().trim()
//
//        var splited = date.replace("/", "")
//        var aDate = splited.substring(0, 2)
//        var aMonth = splited.substring(2, 4)
//        var ayear = splited.substring(4, 8)
//        var code = preferenceUtils.getStringFromPreference(PreferenceUtils.Reason_CODE, "")
//
//        if (type == 1) {
//            showLoader()
//            val siteListRequest = NewRescheduleRequest(shipmentId, type, CalendarUtils.getDate(), code, comments, this@Cancel_RescheduleActivity)
//            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO, msg ->
//                hideLoader()
//                if (isError) {
//                    if (!msg.isNullOrEmpty()) {
//                        showAlert(msg)
//
//                    } else {
//                        showAlert(getString(R.string.server_error))
//                    }
//                } else {
//                    if (unPaidInvoiceMainDO != null) {
//                        if (unPaidInvoiceMainDO.flag == 2) {
//                            setResult(511)
//                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "NOT MANDATORY")
//                            showToast(unPaidInvoiceMainDO.successFlag)
//                            finish()
//                        } else {
//                            showToast(unPaidInvoiceMainDO.successFlag)
//
//                        }
//
//                    } else {
//                        if (!msg.isNullOrEmpty()) {
//                            showAlert(msg)
//
//                        } else {
//                            showAlert(getString(R.string.server_error))
//                        }
//                    }
//                }
//            }
//
//            siteListRequest.execute()
//        } else {
//            val siteListRequest = CancelOrRescheduleRequest(shipmentId, type, ayear + aMonth + aDate, code, comments, this@Cancel_RescheduleActivity)
//            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO, msg ->
//                hideLoader()
//                if (isError) {
//                    if (!msg.isNullOrEmpty()) {
//                        showAlert(msg)
//
//                    } else {
//                        showAlert(getString(R.string.server_error))
//                    }
//                } else {
//                    if (unPaidInvoiceMainDO != null) {
//                        if (unPaidInvoiceMainDO.flag == 2) {
//                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "NOT MANDATORY")
//                            showToast(unPaidInvoiceMainDO.successFlag)
//
//                            setResult(511)
//                            finish()
//                        } else {
//                            if (!msg.isNullOrEmpty()) {
//                                showAlert(msg)
//
//                            } else {
//                                showToast(unPaidInvoiceMainDO.successFlag)
//                            }
//                        }
//                    }
//                }
//
//
//            }
//
//            siteListRequest.execute()
//        }
//
//    }
    fun cancel(type :Int) {
        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        comments = etComments.text.toString().trim()
        date = tvDatee.text.toString().trim()
        reason = tvSelection.text.toString().trim()

        var splited = date.replace("/", "")
        var aDate = splited.substring(0, 2)
        var aMonth = splited.substring(2, 4)
        var ayear = splited.substring(4, 8)
        var code = preferenceUtils.getStringFromPreference(PreferenceUtils.Reason_CODE, "")

        if (type == 1) {
            val siteListRequest = CancelOrRescheduleRequest(signature,shipmentId, type, CalendarUtils.getDate(), code, comments, this@Cancel_RescheduleActivity)
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO, msg ->
                hideLoader()
                if (isError) {
                    if (!msg.isNullOrEmpty()) {
                        showAlert(msg)

                    } else {
                        showAlert(getString(R.string.server_error))
                    }
                } else {
                    if (unPaidInvoiceMainDO != null) {
                        if (unPaidInvoiceMainDO.flag == 2) {
                            preferenceUtils.removeFromPreference(PreferenceUtils.Reason_OTH)
                            preferenceUtils.removeFromPreference(PreferenceUtils.Reason_CODE)

                            setResult(511)
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "NOT MANDATORY")
                            showToast(unPaidInvoiceMainDO.successFlag)
                            finish()
                        } else {
                            showToast(unPaidInvoiceMainDO.successFlag)

                        }

                    } else {
                        if (!msg.isNullOrEmpty()) {
                            showAlert(msg)

                        } else {
                            showAlert(getString(R.string.server_error))
                        }
                    }
                }
            }

            siteListRequest.execute()
        } else {
            val siteListRequest = CancelOrRescheduleRequest(signature,shipmentId, type, ayear + aMonth + aDate, code, comments, this@Cancel_RescheduleActivity)
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO, msg ->
                hideLoader()
                if (isError) {
                    if (!msg.isNullOrEmpty()) {
                        showAlert(msg)

                    } else {
                        showAlert(getString(R.string.server_error))
                    }
                } else {
                    if (unPaidInvoiceMainDO != null) {
                        if (unPaidInvoiceMainDO.flag == 2) {
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE_RESHEDULE, "NOT MANDATORY")
                            preferenceUtils.removeFromPreference(PreferenceUtils.Reason_OTH)

                            showToast(unPaidInvoiceMainDO.successFlag)

                            setResult(511)
                            finish()
                        } else {
                            if (!msg.isNullOrEmpty()) {
                                showAlert(msg)

                            } else {
                                showToast(unPaidInvoiceMainDO.successFlag)
                            }
                        }
                    }
                }


            }

            siteListRequest.execute()
        }

    }
    fun reasonDialog() {
        dialog = Dialog(this, R.style.NewDialog)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.simple_list_dialog)
        val window = dialog.window
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        }
        var recyclerView = dialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
//        if (reasonMainDO != null && reasonMainDO.reasonDOS != null && reasonMainDO.reasonDOS.size > 0) {
//            val siteAdapter = ReasonListAdapter(this@Cancel_RescheduleActivity, reasonMainDO.reasonDOS)
//            recyclerView.setAdapter(siteAdapter)
//        } else {
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = ReasonsListRequest(type, this@Cancel_RescheduleActivity)
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null) {
                    if (isError) {
                        showAppCompatAlert("", getString(R.string.no_reasong_found_at_this_movment), getString(R.string.ok), getString(R.string.cancel), "", false)
                    } else {
                        reasonMainDO = unPaidInvoiceMainDO
                        val siteAdapter = ReasonListAdapter(1,this@Cancel_RescheduleActivity, reasonMainDO.reasonDOS)
                        recyclerView.adapter = siteAdapter
                    }
                } else {
                    hideLoader()
                    showAppCompatAlert("", getString(R.string.no_reasong_found_at_this_movment), getString(R.string.ok), getString(R.string.cancel), "", false)

                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }





        dialog.show()


    }

    fun updateReqApprButton(selectedreason: String) {
        if (dialog != null) {
            dialog.dismiss()
        }
        tvSelection.text = selectedreason
        if(rbCacel.isChecked){
            if(signature.isNotEmpty()){
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                btnConfirm.isClickable = true
                btnConfirm.isEnabled = true
            }else{
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirm.isClickable = false
                btnConfirm.isEnabled = false
            }
        }else{
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
            btnConfirm.isClickable = true
            btnConfirm.isEnabled = true
        }



    }

    fun cancelOrResheduleApproval(rejected: String) {
        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

        var formatdate = preferenceUtils.getStringFromPreference(PreferenceUtils.DATE_FORMAT, "")

        if (modifyType == 6) {
            formatdate = CalendarUtils.getDate()
            cancel(1)
        } else {
            comments = etComments.text.toString().trim()
            date = tvDatee.text.toString().trim()
            reason = tvSelection.text.toString().trim()
            if (!rejected.isEmpty()) {
                var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

                formatdate = activeDeliverySavedDo.shipmentDate
                modifyType = 2
            }
            if (Util.isNetworkAvailable(this)) {
                showLoader()
                var reason = preferenceUtils.getStringFromPreference(PreferenceUtils.Reason_CODE, "")

                val siteListRequest = NewRescheduleRequest(shipmentId, modifyType, formatdate, reason, comments, this@Cancel_RescheduleActivity)
                siteListRequest.setOnResultListener { isError, approveDO, msg ->
                    hideLoader()

                    if (isError) {
                        if (!msg.isNullOrEmpty()) {
                            showAlert(msg)

                        } else {
                            showAlert(getString(R.string.server_error))
                        }
                    } else {
                        hideLoader()
                        if (approveDO != null) {
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
                            if (approveDO.flag == 20) {

                                cancel(2)

                            } else {
                                if (!msg.isNullOrEmpty()) {
                                    showAlert(msg)

                                } else {
                                    showAlert(getString(R.string.server_error))
                                }
                            }
                        } else {
                            if (!msg.isNullOrEmpty()) {
                                showAlert(msg)

                            } else {
                                showAlert(getString(R.string.server_error))
                            }
                        }

//                    if (approveDO.status.equals("Requested")) {
////                        Toast.makeText(this@Cancel_RescheduleActivity, "Success", Toast.LENGTH_SHORT).show()
//                        tvApprovalStatus.setVisibility(View.VISIBLE)
//                        tvApprovalStatus.setText("Status : " + approveDO.status)
//
//                    } else {
////                        Toast.makeText(this@Cancel_RescheduleActivity, "Failed", Toast.LENGTH_SHORT).show()
//
//                    }

                    }


                }

                siteListRequest.execute()
            } else {
                showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

            }
        }


    }

    fun updateStatus() {
        var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

        comments = etComments.text.toString().trim()
        date = tvDatee.text.toString().trim()
        reason = tvSelection.text.toString().trim()
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = CancelRescheduleApprovalStatusRequest(shipmentId, this)
            siteListRequest.setOnResultListener { isError, approvalDO ->
                hideLoader()
                if (isError) {
                    hideLoader()
                    tvApprovalStatus.visibility = View.VISIBLE
                    tvApprovalStatus.text = getString(R.string.statues_request_not_sent)
                } else {
                    hideLoader()
                    if (approvalDO.status != null) {
                        tvApprovalStatus.text = getString(R.string.status) + " : " + approvalDO.status

                        if (approvalDO.status.equals(getString(R.string.approved), true)) {
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnConfirm.isClickable = true
                            btnConfirm.isEnabled = true
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false

                        } else if (approvalDO.status.equals(getString(R.string.rejected), true)) {
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnConfirm.isClickable = true
                            btnConfirm.isEnabled = true
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false

                        } else {
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnConfirm.isClickable = false
                            btnConfirm.isEnabled = false
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false

                        }


                    } else {
                        showToast(getString(R.string.server_error))

                    }
                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }


    }

    override fun onBackPressed() {

        LogUtils.debug("PAUSE", "tes")
        var data = tvApprovalStatus.text.toString()
        var comments = etComments.text.toString()
        var reason = tvSelection.text.toString()
        var date = tvDatee.text.toString()


        var intent = Intent()
        intent.putExtra("data", data + "," + comments + "," + reason + "," + date + "," + type)


        setResult(123, intent)
        finish()
        super.onBackPressed()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == 9907) {
            signature = data!!.getStringExtra("Signature")
            Log.d("Signature----->", signature)

            val decodedString = android.util.Base64.decode(signature, android.util.Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            imv_signatur.visibility = View.VISIBLE
            imv_signatur?.setImageBitmap(decodedByte)

        }
        if(tvSelection.text.toString().isNotEmpty()&&signature.isNotEmpty()){
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
            btnConfirm.isClickable = true
            btnConfirm.isEnabled = true
        }
    }
}