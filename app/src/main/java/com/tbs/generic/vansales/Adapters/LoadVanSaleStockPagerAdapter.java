package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/15/2018.
 */

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.fragments.LoadStockNonScheduledFragmment;
import com.tbs.generic.vansales.fragments.LoadStockScheduledFragmment;

import java.util.ArrayList;

public class LoadVanSaleStockPagerAdapter extends FragmentStatePagerAdapter  {

    private Context context;
    private ArrayList<LoadStockDO> scheduleDos;
    private ArrayList<LoadStockDO> nonScheduleDos;

    public LoadVanSaleStockPagerAdapter(Context context, ArrayList<LoadStockDO> scheduleDos, ArrayList<LoadStockDO> nonScheduleDos, FragmentManager fm) {
        super(fm);
        this.context = context;
        this.scheduleDos = scheduleDos;
        this.nonScheduleDos = nonScheduleDos;
//        boolean isProductExisted = false;
//
//        if(availableDOS.size()==0){
//            availableDOS=nonScheduleDos;
//            for (int i=0; i<scheduleDos.size(); i++) {
//            for (int k=0; i<availableDOS.size(); k++) {
//                    if (scheduleDos.get(i).product.equalsIgnoreCase(availableDOS.get(k).product)) {
//                        availableDOS.get(k).quantity= availableDOS.get(k).quantity+scheduleDos.get(i).quantity;
//                        isProductExisted = true;
//                        break;
//                    }
//                }
//                if (isProductExisted) {
//                    isProductExisted = false;
//                    continue;
//                } else {
//                    availableDOS.add(scheduleDos.get(i));
//                }
//            }
//        }
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
//            case 0:
//                AvailableLoadStockFragmment availableLoadStockFragmment = new AvailableLoadStockFragmment();
//                Bundle bundle2 = new Bundle();
//                bundle2.putSerializable("availableData", availableDOS);
////                bundle2.putSerializable("NonScheduleLoadData", nonScheduleDos);
//
//                availableLoadStockFragmment.setArguments(bundle2);
//                return availableLoadStockFragmment;
            case 0:
                LoadStockScheduledFragmment loadStockScheduledFragmment = new LoadStockScheduledFragmment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("LoadData", scheduleDos);
                loadStockScheduledFragmment.setArguments(bundle);
                return loadStockScheduledFragmment;
            case 1:
                LoadStockNonScheduledFragmment nonScheduledFragmment = new LoadStockNonScheduledFragmment();
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("LoadData", nonScheduleDos);
                nonScheduledFragmment.setArguments(bundle1);
                return nonScheduledFragmment;

        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


}
