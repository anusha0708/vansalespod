package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.*
import com.darsh.multipleimageselect.activities.AlbumSelectActivity
import com.darsh.multipleimageselect.models.Image
import com.tbs.generic.vansales.Adapters.FilesPreviewAdapter
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.listeners.StringListner
import com.tbs.generic.vansales.utils.LogUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.include_image_caputure.*
import kotlinx.android.synthetic.main.include_signature.*
import kotlinx.android.synthetic.main.include_signature.btnSignature
import kotlinx.android.synthetic.main.include_signature.imv_signature
import kotlinx.android.synthetic.main.misc_stop_scheduled_capture_reciept.*
import kotlinx.android.synthetic.main.pod_screen_1.*
import kotlinx.android.synthetic.main.user_aactivity_report.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import kotlin.collections.ArrayList


//
class MiscStopScheduledCaptureDeliveryActivity : BaseActivity() {
    lateinit var podDo: PodDo

    private var signature: String? = null

    private var type: Int = 1

    lateinit var btnRequestApproval: Button
    lateinit var tvDetails: TextView

    lateinit var btnConfirm: Button
    lateinit var tvApprovalStatus: TextView
    private var shipmentType: String = ""
    var status = "";
    var customerId = "";
    lateinit var dialog: Dialog
    private var shipmentId: String = ""
    var pickupDropType: String = "";
    lateinit var customerDo: CustomerDo
    lateinit var activeDeliverySavedDo: ActiveDeliveryMainDO
    lateinit var ShipmentType: String
    var customer = ""
    private var fileDetailsList: java.util.ArrayList<FileDetails>? = null
    private lateinit var filesPreviewAdapter: FilesPreviewAdapter
    var nonBgType = 0

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.misc_stop_scheduled_capture_reciept, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        hideKeyBoard(llBody)
        toolbar.setNavigationOnClickListener {

            if (isSentForApproval) {
                showToast(getString(R.string.please_process_order))
            } else {
                LogUtils.debug("PAUSE", "tes")
                var data = tvApprovalStatus.text.toString()
                var intent = Intent()
                intent.putExtra("Status", data)
                var args = Bundle();
                intent.putExtra("BUNDLE", args);
                setResult(56, intent)
                finish()


            }
        }

        initializeControls()
        activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
        ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
        podDo = StorageManager.getInstance(this).getDepartureData(this);

        if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
            customer = customerDo.customer
        } else {
            customer = activeDeliverySavedDo.customer
        }
        var pickFlag = preferenceUtils.getIntFromPreference(PreferenceUtils.PICKUP_DROP_FLAG, 0)
        if (pickFlag == 1) {
            pickupDropType = "Pickup"
        } else {
            pickupDropType = "Drop"

        }
        shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
        customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
        activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        tvDetails.setText(activeDeliverySavedDo.customer + " - " + activeDeliverySavedDo.customerDescription + "\n"
                + "Document - " + shipmentId + "\nType - " + pickupDropType
        )


        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras?.getString("CustomerId").toString()
        }

        tvScreenTitle.setText(getString(R.string.confirm_products))
        ivAdd.setVisibility(View.GONE)
        ivRefresh.setVisibility(View.GONE)
        btnRequestApproval.visibility = GONE
        podDo = StorageManager.getInstance(this).getDepartureData(this);


        if (podDo.deliveryStatus?.get(0)!!.contains("Requested", true)) {
            tvApprovalStatus.setVisibility(VISIBLE)
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirm.isClickable = false
            btnConfirm.isEnabled = false
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            tvApprovalStatus.setText(podDo.deliveryStatus?.get(0))
        } else if (podDo.deliveryStatus?.get(0)!!.contains("Approved", true)) {
            tvApprovalStatus.setVisibility(VISIBLE)

            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
            btnConfirm.isClickable = true
            btnConfirm.isEnabled = true
            tvApprovalStatus.setText(podDo.deliveryStatus?.get(0))

        } else {
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
            btnRequestApproval.isClickable = true
            btnRequestApproval.isEnabled = true
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
            btnConfirm.isClickable = true
            btnConfirm.isEnabled = true
            tvApprovalStatus.setText(podDo.deliveryStatus?.get(0))
        }


        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
            btnConfirm.setText(getString(R.string.validate_misc_stop))
        } else {
            btnConfirm.setText(getString(R.string.create_delivery))
        }
        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
            if (tvApprovalStatus.text.toString().contains("Requested")) {
                btnConfirm.setText(getString(R.string.validate_misc_stop))
                btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnConfirm.isClickable = false
                btnConfirm.isEnabled = false
                btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnRequestApproval.isClickable = false
                btnRequestApproval.isEnabled = false
            } else {
                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                    btnConfirm.setText(getString(R.string.validate_misc_stop))
                    btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnConfirm.isClickable = false
                    btnConfirm.isEnabled = false
//                    cbNonBGSelected.setVisibility(GONE)
                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false
                } else {
                    btnConfirm.setText(getString(R.string.validate_misc_stop))
                    btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                    btnConfirm.isClickable = true
                    btnConfirm.isEnabled = true
//                    cbNonBGSelected.setVisibility(VISIBLE)

                    btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                    btnRequestApproval.isClickable = false
                    btnRequestApproval.isEnabled = false
                }

            }

        } else {

            btnConfirm.setText(getString(R.string.create_delivery))
            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnConfirm.isClickable = false
            btnConfirm.isEnabled = false
            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnRequestApproval.isClickable = false
            btnRequestApproval.isEnabled = false
            ivRefresh.setVisibility(View.GONE)
            btnRequestApproval.setVisibility(View.GONE)
        }
        llMscnoteFab.setOnClickListener {
            var intent = Intent(this@MiscStopScheduledCaptureDeliveryActivity, PODNotesActivity::class.java);
            startActivityForResult(intent, 140);
        }
        ivAdd.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this, ScheduledAddProductActivity::class.java);
            intent.putExtra("FROM", "SPOTSALES")
            startActivityForResult(intent, 1);
        }

        btnRequestApproval.setOnClickListener {
            Util.preventTwoClick(it)
            requestApproval()
        }

        btnConfirm.setOnClickListener {
            Util.preventTwoClick(it)
            if(signature.isNullOrEmpty()){
                showToast(resources.getString(R.string.please_do_sig))

            }else{

                if (!lattitudeFused.isNullOrEmpty()) {
                    stopLocationUpdates()
                    hideLoader()
                    creation()

                } else {
                    showLoader()
                    stopLocationUpdates()
                    location(ResultListner { `object`, isSuccess ->
                        if (isSuccess) {
                            hideLoader()
                            stopLocationUpdates()
                            creation()
                        }
                    })
                }
            }



        }
        imagesSetUp()
        btnAddImages.setOnClickListener {
            Util.preventTwoClick(it)

            val intent = Intent(this, AlbumSelectActivity::class.java)
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 1)
            if (fileDetailsList!!.size > 0 && fileDetailsList!!.size <= 4) {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4 - fileDetailsList!!.size)
            } else {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4)
            }
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_FROM, true)
            startActivityForResult(intent, 888)
        }

//        ll_signature.visibility = VISIBLE
        btnSignature.setOnClickListener {
            val intent = Intent(this, CommonSignatureActivity::class.java)
            startActivityForResult(intent, 9907)
        }
    }

    private fun creation() {
        podDo.creDocLattitude = lattitudeFused
        podDo.creDOCLongitude = longitudeFused
        saveDepartureData()
        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true)) {
            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {

                updateQtyToServer("METER")
                //  updateMeterQtyToServer()
            } else {

                updateQtyToServer("")
            }
        } else {

        }
    }

    private fun saveDepartureData() {
        StorageManager.getInstance(this).saveDepartureData(this, podDo);
    }

    private fun imagesSetUp() {

        fileDetailsList = java.util.ArrayList()
        filesPreviewAdapter = FilesPreviewAdapter(this, fileDetailsList, StringListner {

            if (fileDetailsList!!.size < 4) {
                btnAddImages.visibility = VISIBLE

            } else {
                btnAddImages.visibility = GONE
            }

            if (fileDetailsList!!.size >= 1) {
                recycleviewImages.visibility = VISIBLE
            } else {
                recycleviewImages.visibility = GONE
            }
        })
        recycleviewImages.adapter = filesPreviewAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 140 && resultCode == Activity.RESULT_OK) {
            val returnString = data!!.getStringExtra(resources.getString(R.string.notes_remarks))
            LogUtils.debug(resources.getString(R.string.notes_remarks), returnString)
            podDo.notes = returnString;
            saveDepartureData()
        }
        if (resultCode == RESULT_OK && requestCode == 888) {
            val images = data?.getParcelableArrayListExtra<Image>(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_IMAGES)

            if (images != null) {
                Log.d("image-->", images.size.toString() + "----" + images)

                /* try {
                     val fis = FileInputStream(AppConstants.file1)
                     val bitmap = BitmapFactory.decodeStream(fis)
                     val baos = ByteArrayOutputStream()
                     bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                     val bytes = baos.toByteArray()
                     val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                     preferenceUtils.saveString(PreferenceUtils.CAPTURE, "CAPTURE")
                     LogUtils.debug("PIC", encImage)
                 } catch (e: Exception) {
                     e.printStackTrace()
                 }*/


                var i = 0
                val l = images.size
                while (i < l) {
                    val file1 = File(images[i].path)
                    val fileDetails = FileDetails()
                    fileDetails.fileName = file1.name
                    fileDetails.filePath = file1.absolutePath
                    fileDetailsList?.add(0, fileDetails)
                    i++
                }
                filesPreviewAdapter.notifyDataSetChanged()

                if (fileDetailsList!!.size < 4) {
                    btnAddImages.visibility = View.VISIBLE
                } else {
                    btnAddImages.visibility = View.GONE
                }

                if (fileDetailsList!!.size >= 1) {
                    recycleviewImages.visibility = View.VISIBLE
                } else {
                    recycleviewImages.visibility = View.GONE
                }
            }
        }
        if (resultCode == RESULT_OK && requestCode == 9907) {
            signature = data!!.getStringExtra("Signature")
            Log.d("Signature----->", signature)
            var rating = data!!.getStringExtra("Rating")
            podDo.departureRating = rating
            saveDepartureData()
            val decodedString = android.util.Base64.decode(signature, android.util.Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            imv_signature.visibility = VISIBLE
            imv_signature?.setImageBitmap(decodedByte)

        }
    }


    private fun updateQtyToServer(id: String) {
        if (id.equals("METER")) {
            updateMeterQtyToServer()
        } else {
            customerAuthorization(customer, 19, ResultListner { `object`, isSuccess ->
                if (isSuccess) {
                    createReturn()
                } else {
                    showAlert(resources.getString(R.string.not_authorized))

                }
            })


        }

    }

    private fun updateQtyConfirmationToServer() {
        if (Util.isNetworkAvailable(this)) {

            val request = ConfirmationQtyRequest(shipmentId, ArrayList(), this@MiscStopScheduledCaptureDeliveryActivity)
            request.setOnResultListener { isError, approveDO ->
                hideLoader()

                if (approveDO != null) {
                    if (isError) {
                        isSentForApproval = false

                        showAppCompatAlert("", getString(R.string.please_wait_for_conf_approval), getString(R.string.ok), getString(R.string.cancel), "", false)

                    } else {
                        if (approveDO.flag == 2) {
                            showToast(getString(R.string.updated_successfully))
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                val intent = Intent(this@MiscStopScheduledCaptureDeliveryActivity, SignatureActivity::class.java);
                                startActivityForResult(intent, 11)
                            } else {

                                val intent = Intent(this@MiscStopScheduledCaptureDeliveryActivity, SignatureActivity::class.java)
                                startActivityForResult(intent, 11)

                            }

                        } else if (approveDO.flag == 1) {
                            showToast(getString(R.string.shipment_already_validate))
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                val intent = Intent(this@MiscStopScheduledCaptureDeliveryActivity, SignatureActivity::class.java);
                                startActivityForResult(intent, 11)
                            } else {

                                val intent = Intent(this@MiscStopScheduledCaptureDeliveryActivity, SignatureActivity::class.java)
                                startActivityForResult(intent, 11)
                            }

                        } else {
                            showAppCompatAlert("", getString(R.string.please_wait_for_conf_approval), getString(R.string.ok), getString(R.string.cancel), "", false)
                            isSentForApproval = false

                        }
                    }
                } else {
                    showAppCompatAlert("", getString(R.string.please_wait_for_conf_approval), getString(R.string.ok), getString(R.string.cancel), "", false)
                    isSentForApproval = false

                }
            }
            request.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }

    }

    private fun updateMeterQtyToServer() {
        if (Util.isNetworkAvailable(this)) {

            val siteListRequest = ProductsModifyRequest(0, shipmentId, ArrayList(), this@MiscStopScheduledCaptureDeliveryActivity)
            siteListRequest.setOnResultListener { isError, approveDO, msg ->
                hideLoader()
                if (isError) {
                    Log.e("RequestAproval", "Response : " + isError)
                    isSentForApproval = false
                    updateQtyConfirmationToServer()

                } else {
                    if (approveDO != null) {
                        if (approveDO.flag == 4) {
                            updateQtyConfirmationToServer()

                        } else {
                            isSentForApproval = false
                            updateQtyConfirmationToServer()

                        }
                    } else {
                        updateQtyConfirmationToServer()

                    }


                }

            }
            siteListRequest.execute()

//            val request = UpdateRunQtyRequest(shipmentId, shipmentProductsList, this@ScheduledCaptureDeliveryActivity)
//            request.setOnResultListener { isError, approveDO ->
//                hideLoader()
//                //  updateTheProductsInLocal()
//                if (approveDO != null) {
//                    if (isError) {
//
//
//                        showAppCompatAlert("", " please try again.", "Ok", "Cancel", "", false)
//
//                    } else {
//                        if (approveDO != null) {
//                            if (approveDO.flag != null) {
//                                updateQtyConfirmationToServer()
//
////                            updateTheProductsInLocal()
//
//                                showToast("Updated Successfully")
//                            }
//                        } else {
//                            isSentForApproval = false
//
//                        }
//                    }
//                }
//            }
//            request.execute()
//


        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }

    }

    lateinit var loadStockDO: ActiveDeliveryDO;
    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)

        if (from.equals(getString(R.string.success), true)) {

            val intent = Intent()
            intent.putExtra("CRELAT", lattitudeFused)
            intent.putExtra("CRELON", longitudeFused)
            intent.putExtra("RATING", podDo!!.departureRating)
            intent.putExtra("NOTES", podDo!!.notes)

            setResult(11, intent)
            finish()
        }
    }


    override fun initializeControls() {
        btnRequestApproval = findViewById<View>(R.id.btnRequestApproval) as Button
        btnConfirm = findViewById<View>(R.id.btnConfirm) as Button
        tvApprovalStatus = findViewById(R.id.tvApprovalStatus) as TextView
        tvDetails = findViewById(R.id.tvDetails) as TextView

        var approval = tvApprovalStatus.text.toString()
        status = preferenceUtils.getStringFromPreference(PreferenceUtils.PRODUCT_APPROVAL, "")
        if (approval.equals("NA")) {
            tvApprovalStatus.setText(getString(R.string.na))

        } else {
            tvApprovalStatus.setText(status)

        }

        ivRefresh.setOnClickListener {
            Util.preventTwoClick(it)
            if (tvApprovalStatus.getText().toString().contains("Request")) {
                preferenceUtils.saveString(PreferenceUtils.PRODUCT_APPROVAL, tvApprovalStatus.getText().toString())
                updateStatus()
            }
        }
    }

    private fun requestApproval() {
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = ProductsModifyRequest(4, shipmentId, ArrayList(), this@MiscStopScheduledCaptureDeliveryActivity)
            siteListRequest.setOnResultListener { isError, approveDO, msg ->
                hideLoader()
                if (approveDO != null) {
                    if (isError) {
                        Log.e("RequestAproval", "Response : " + isError)
                    } else {
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false
                        if (approveDO.flag == 4) {
                            tvApprovalStatus.setText(getString(R.string.statue_requested))
                            isSentForApproval = true;
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnRequestApproval.isClickable = false
                            btnRequestApproval.isEnabled = false
                        } else {
                            Toast.makeText(this@MiscStopScheduledCaptureDeliveryActivity, "Please send request again", Toast.LENGTH_SHORT).show()
                            btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_green))
                            btnRequestApproval.isClickable = true
                            btnRequestApproval.isEnabled = true
                            tvApprovalStatus.setText(getString(R.string.statues_request_not_sent))
                        }

                    }
                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), "OK", "", "", false)

        }


    }

    private fun setUpImagesAs64Bit() {
        podDo = StorageManager.getInstance(this).getDepartureData(this);

        for (list in this.fileDetailsList!!) {
            try {
                val fis = FileInputStream(File(list.filePath))
                val bitmap = BitmapFactory.decodeStream(fis)
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val bytes = baos.toByteArray()
                val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                LogUtils.debug("PIC-->", encImage)
                podDo.capturedImagesListBulk!!.add(encImage)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        saveDepartureData()
    }

    private fun createReturn() {
        setUpImagesAs64Bit()

        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = CreateMiscStopRequest(lattitudeFused, longitudeFused, addressFused, shipmentId, ArrayList(), this@MiscStopScheduledCaptureDeliveryActivity)
            siteListRequest.signature(signature)
            siteListRequest.setOnResultListener { isError, approveDO, msg ->
                hideLoader()
                if (isError) {
                    if (msg.isNotEmpty()) {
                        showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)

                    } else {
                        showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)

                    }
                } else {
                    if (approveDO != null) {
                        if (approveDO.flag == 2) {
                            btnConfirm.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnConfirm.isClickable = false
                            btnConfirm.isEnabled = false
                            showAppCompatAlert(getString(R.string.info), getString(R.string.mis_stop), getString(R.string.ok), "", getString(R.string.success), false)

                        } else {
                            if (msg.isNotEmpty()) {
                                showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)

                            } else {
                                showToast(getString(R.string.mis_not_avail))

                            }
                        }
                    } else {
                        if (msg.isNotEmpty()) {
                            showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)

                        } else {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)

                        }
                    }
                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }


    }

    private var isSentForApproval: Boolean = false
    private fun updateStatus() {
        val siteListRequest = CancelRescheduleApprovalStatusRequest(shipmentId, this)
        siteListRequest.setOnResultListener { isError, approvalDO ->
            hideLoader()
            if (approvalDO != null) {
                if (isError) {
                    Toast.makeText(this@MiscStopScheduledCaptureDeliveryActivity, "Approval under progress", Toast.LENGTH_SHORT).show()
                } else {
                    if (approvalDO.status.equals("Approved", true)) {
                        isSentForApproval = true;
                        tvApprovalStatus.setText(getString(R.string.status) + " : " + approvalDO.status)
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false


                    } else if (approvalDO.status.equals("Rejected", true)) {
                        isSentForApproval = true;
                        tvApprovalStatus.setText(getString(R.string.status) + " : " + approvalDO.status)
                        btnConfirm.setBackgroundColor(resources.getColor(R.color.md_green))
                        btnConfirm.isClickable = true
                        btnConfirm.isEnabled = true
                        btnRequestApproval.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                        btnRequestApproval.isClickable = false
                        btnRequestApproval.isEnabled = false

                    } else {
                        Toast.makeText(this@MiscStopScheduledCaptureDeliveryActivity, "Approval under progress", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        siteListRequest.execute()
    }

    override fun onBackPressed() {

        if (isSentForApproval) {
            showToast(getString(R.string.please_process_order))
        } else {
            LogUtils.debug("PAUSE", "tes")
            var data = tvApprovalStatus.text.toString()
            var intent = Intent()
            intent.putExtra("Status", data)
            setResult(56, intent)
            finish()
            super.onBackPressed()
        }

    }


    override fun onDestroy() {
        super.onDestroy()
        hideLoader()
    }


}