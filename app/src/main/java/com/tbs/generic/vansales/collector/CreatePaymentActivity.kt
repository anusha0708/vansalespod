package com.tbs.generic.vansales.collector

import android.annotation.TargetApi
import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.Adapters.BankListAdapter
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.pdfs.ReceiptPDF
import com.tbs.generic.vansales.prints.PrinterConnection
import com.tbs.generic.vansales.prints.PrinterConstants
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.LogUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.io.*
import java.util.*


class CreatePaymentActivity : BaseActivity() {

    lateinit var etChequeNo: EditText
    lateinit var tvBankSelection: TextView
    lateinit var dialog: Dialog
    lateinit var bankDialog: Dialog
    lateinit var etAmount: EditText
    private var calender: Calendar = Calendar.getInstance()
    lateinit var podDo: PodDo
    var encImage = ""
    lateinit var createPDFpaymentDO: PaymentPdfDO
    private var selectedInvoiceDOs = ArrayList<UnPaidInvoiceDO>()
    private var selectedCustomerDOs = ArrayList<CustomerDo>()

    private lateinit var ivCapturedPic: ImageView
    private lateinit var ivCamera: ImageView
    private lateinit var llChequeDate: LinearLayout
    private lateinit var llChequeNo: LinearLayout
    private lateinit var llView: LinearLayout
    private lateinit var llBankList: LinearLayout

    lateinit var btnPayments: Button
    lateinit var tvSelection: TextView
    private lateinit var cheque: String
    private lateinit var bank: String
    private lateinit var amount: String
    private lateinit var tvCurrency: TextView
    private lateinit var tvSelectDate: TextView
    private lateinit var createPaymentDo: CreatePaymentDO
    private lateinit var unpaidMainDo: UnPaidInvoiceMainDO
    private lateinit var llCapture: LinearLayout
    private lateinit var siteListRequest: UnPaidInvoicesListRequest
    private lateinit var siteListRequest2: CreateMiscelleneousPaymentRequest

    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    private var type: Int = 10
    private var paymentType: Int = 0

    private var fromId = 0
    private var totalAmount = ""

    private lateinit var bitmap: Bitmap
    private lateinit var chequeDate: String
    private lateinit var cid: String
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 200

    override fun initialize() {
        val llCategories = layoutInflater.inflate(R.layout.create_payments_screen, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        toolbar.setNavigationIcon(R.drawable.back)
        changeLocale()
        toolbar.setNavigationOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
        disableMenuWithBackButton()
        initializeControls()
        totalAmount = preferenceUtils.getStringFromPreference(PreferenceUtils.CREDIT_INVOICE_AMOUNT, "")

        if (intent.hasExtra("Sales")) {
            fromId = intent!!.extras!!.getInt("Sales")
        }
        if (fromId == 1) {
            llView.visibility = View.GONE
            etAmount.setText(totalAmount)

        } else {
            llView.visibility = View.VISIBLE
            val data = preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, "")
            tvSelection.text = "" + data
            etAmount.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_AMOUNT, ""))

        }
        podDo = StorageManager.getInstance(this).getDepartureData(this)
        if (!podDo.payment.equals("")) {

//            btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            btnPayments.setClickable(false)
//            btnPayments.setEnabled(false)

            var selectedId = tvSelection.text.toString().trim()


            var invoiceId = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID, "")
            if (invoiceId.length > 0 && selectedId.equals(invoiceId)) {
//                btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//                btnPayments.setClickable(false)
//                btnPayments.setEnabled(false)

            } else {
                btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
                btnPayments.isClickable = true
                btnPayments.isEnabled = true
            }
        } else {
            btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
            btnPayments.isClickable = true
            btnPayments.isEnabled = true
        }
//
//        if (!podDo.payment.equals("")) {
//
//            btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//            btnPayments.setClickable(false)
//            btnPayments.setEnabled(false)
//            var selectedId= tvSelection.text.toString().trim()
//
//            var deliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID, "")
//            if(deliveryId.length>0&&selectedId.equals(deliveryId)){
//                btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
//                btnPayments.setClickable(false)
//                btnPayments.setEnabled(false)
//
//            }else{
//                btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
//                btnPayments.setClickable(true)
//                btnPayments.setEnabled(true)
//            }
//        }else{
//            btnPayments.setBackgroundColor(resources.getColor(R.color.md_green))
//            btnPayments.setClickable(true)
//            btnPayments.setEnabled(true)
//        }


        bitmap = BitmapFactory.decodeResource(resources, R.drawable.gallary)
        cmonth = calender.get(Calendar.MONTH)
        cday = calender.get(Calendar.YEAR)
        cday = calender.get(Calendar.DAY_OF_MONTH)


        ivCamera.setOnClickListener {
            insertDummyContactWrapper()

            try {
                val file = File(Environment.getExternalStorageDirectory(), "BrothersGas_" + System.currentTimeMillis().toString() + ".png")
                if (file.exists()) {
                    file.delete()
                }
                file.createNewFile()
                val camIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                val uri = Uri.fromFile(file)
                camIntent.putExtra("return-data", true)
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)

                    val camIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    AppConstants.file2 = File(Environment.getExternalStorageDirectory(), "vansales_" + System.currentTimeMillis().toString() + ".png")
                    val uri = Uri.fromFile(AppConstants.file2)
                    camIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
                    camIntent.putExtra("return-data", true)
                    startActivityForResult(camIntent, 111)
                } else {
                    val file = File(uri.path)
                    val photoUri = FileProvider.getUriForFile(applicationContext, applicationContext.packageName + ".fileprovider", file)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivityForResult(camIntent, 199)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }


        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun insertDummyContactWrapper() {
        val permissionsNeeded = ArrayList<String>()
        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera")
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage")
        if (permissionsList.size > 0) {
            requestPermissions(permissionsList.toArray(arrayOfNulls<String>(permissionsList.size)),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
            return
        }
    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false
            }
        }
        return true
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 111 && resultCode == Activity.RESULT_OK) {

            if (data!!.data == null) {

                val fis = FileInputStream(AppConstants.file2)
                val bitmap = BitmapFactory.decodeStream(fis)
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val bytes = baos.toByteArray()
                val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                ivCapturedPic.setImageBitmap(bitmap)
                preferenceUtils.saveString(PreferenceUtils.CAPTURE, "CAPTURE")
                LogUtils.debug("PIC", encImage)
//                StorageManager.getInstance(this).saveDepartureData(this, podDo)
            } else {

                try {
//                val ivCamera = findViewById<ImageView>(R.id.ivCamera)
                    val fis = FileInputStream(AppConstants.file2)
                    val bitmap = BitmapFactory.decodeStream(fis)
                    val baos = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    val bytes = baos.toByteArray()
                    val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                    ivCapturedPic.setImageBitmap(bitmap)
                    preferenceUtils.saveString(PreferenceUtils.CAPTURE, encImage)
//                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                    LogUtils.debug("PIC", encImage)

                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }

            }
        } else if (requestCode == 199 && resultCode == Activity.RESULT_OK) {
            if (data!!.data == null) {
                bitmap = data.extras?.get("data") as Bitmap
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 75, baos)
                val b = baos.toByteArray()
                val encImage = Base64.encodeToString(b, Base64.DEFAULT)
                preferenceUtils.saveString(PreferenceUtils.CAPTURE, encImage)

                ivCapturedPic.setImageBitmap(bitmap)
            } else {
                bitmap = MediaStore.Images.Media.getBitmap(contentResolver, data.data)
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 75, baos)
                val b = baos.toByteArray()
                val encImage = Base64.encodeToString(b, Base64.DEFAULT)
                preferenceUtils.saveString(PreferenceUtils.CAPTURE, encImage)

                ivCapturedPic.setImageBitmap(bitmap)
            }
        }
    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.payment)
        tvSelection = findViewById<TextView>(R.id.tvSelection)
        llView = findViewById<LinearLayout>(R.id.llView)
        tvBankSelection = findViewById<TextView>(R.id.tvBankSelection)
        llBankList = findViewById<LinearLayout>(R.id.llBankList)
        etChequeNo = findViewById<EditText>(R.id.etChequeNo)
        etAmount = findViewById<EditText>(R.id.etAmount)
        tvCurrency = findViewById<TextView>(R.id.tvCurrency)
        tvSelectDate = findViewById<TextView>(R.id.tvSelectDate)
        btnPayments = findViewById<Button>(R.id.btnPayments)
        ivCapturedPic = findViewById<ImageView>(R.id.ivCapturedPic)
        ivCamera = findViewById<ImageView>(R.id.ivCamera)
        llChequeDate = findViewById<LinearLayout>(R.id.llChequeDate)
        llChequeNo = findViewById<LinearLayout>(R.id.llChequeNo)
        llCapture = findViewById<LinearLayout>(R.id.llCapture)
        val yourRadioGroup = findViewById<RadioGroup>(R.id.radioGroup)
        val rbCash = findViewById<RadioButton>(R.id.rbCash)
        val rbCheque = findViewById<RadioButton>(R.id.rbCheque)
        rbCash.isChecked = true
        clickOnCash(rbCash)
        ivCapturedPic.setOnClickListener {
            if (bitmap != null) {
                showChequePreview(bitmap)
            }
        }


        paymentType = preferenceUtils.getIntFromPreference(PreferenceUtils.PAYMENT_TYPE, 0)
        var currency = preferenceUtils.getStringFromPreference(PreferenceUtils.CURRENCY, "")
        tvCurrency.text = ""+currency
        insertDummyContactWrapper()

        if (paymentType == 1) {
            rbCash.isChecked = true
            rbCheque.isClickable = false
            rbCheque.isEnabled = false
            clickOnCash(rbCash)

        } else if (paymentType == 2) {
            rbCheque.isChecked = true
            rbCash.isClickable = false
            rbCash.isEnabled = false
            clickOnCheque(rbCheque)

        } else {
            rbCash.isChecked = true
            rbCash.isClickable = true
            rbCash.isEnabled = true
            rbCheque.isClickable = true
            rbCheque.isEnabled = true
        }

        yourRadioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.rbCash -> {
                        clickOnCash(rbCash)

                    }
                    R.id.rbCheque -> {
                        clickOnCheque(rbCheque)
                    }

                }
            }
        })


        val chequeValue = preferenceUtils.getIntFromPreference(PreferenceUtils.CHEQUE_DATE, 0)
        val datePicker = DatePickerDialog(this@CreatePaymentActivity, datePickerListener, cyear, cmonth, cday)
        datePicker.datePicker.minDate = System.currentTimeMillis() - 1000

        tvSelectDate.setOnClickListener {
            if (!datePicker.isShowing) {
                val c1 = Calendar.getInstance()

                c1.set(Calendar.DAY_OF_MONTH, c1.get(Calendar.DAY_OF_MONTH))
                c1.add(Calendar.MONTH, -6)
                c1.set(Calendar.YEAR, c1.get(Calendar.YEAR))
                datePicker.datePicker.minDate = c1.time.time
                datePicker.show()
            }
        }

        cheque = etChequeNo.text.toString().trim()
        bank = tvBankSelection.text.toString().trim()
        amount = etAmount.text.toString().trim()

        btnPayments.setOnClickListener {


            amount = etAmount.text.toString().trim()
            cheque = etChequeNo.text.toString().trim()
            bank = tvBankSelection.text.toString().trim()
            val data = tvSelection.text.toString()
            if (type == 10) {
                when {
//                    data.isEmpty() -> showToast("please select invoice")
                    amount.isEmpty() -> showToast("please enter amount")
                    else -> customerAuthorization(0)
                }

            } else {
                val value = preferenceUtils.getStringFromPreference(PreferenceUtils.CAPTURE, "")

                when {

//                    data.isEmpty() -> showToast("please select invoice")
                    cheque.isEmpty() -> showToast("please enter cheque number")
                    chequeDate.isEmpty() -> showToast("please select date")
                    amount.isEmpty() -> showToast("please enter amount")
                    bank.isEmpty() -> showToast("please select bank")
                    value.isEmpty() -> showToast("please capture image")


                    else -> customerAuthorization(1)
                }

            }
        }

//        tvSelection.setOnClickListener {
//            selectInvoiceList()
//        }
        tvBankSelection.setOnClickListener {
            selectBankList()
        }
    }

    private fun clickOnCash(rbCash: RadioButton) {
        type = 10
        rbCash.isChecked = true
        etChequeNo.isEnabled = false
        etChequeNo.isClickable = false
        llChequeDate.visibility = View.GONE
        llChequeNo.visibility = View.GONE
        llCapture.visibility = View.GONE
        llBankList.visibility = View.GONE

        etChequeNo.setText("")
        etChequeNo.hint = getString(R.string.cheque_number)
        tvSelectDate.isEnabled = false
        tvSelectDate.isClickable = false
        tvSelectDate.text = ""
        chequeDate = ""
        tvSelectDate.hint = getString(R.string.cheque_date)
        tvBankSelection.isEnabled = true
        tvBankSelection.isClickable = true
        tvBankSelection.text = ""
        tvBankSelection.hint = getString(R.string.select_bank)
        ivCapturedPic.setBackgroundColor(resources.getColor(android.R.color.transparent))
        ivCapturedPic.setImageResource(R.drawable.gallary)
        ivCamera.isClickable = false
        ivCamera.isEnabled = false
        ivCapturedPic.isClickable = false
        ivCapturedPic.isEnabled = false
    }

    private fun customerAuthorization(type: Int) {
        if (intent.hasExtra("CODE")) {
            cid = intent.extras?.getString("CODE").toString()
        }

        if (type == 0) {
            if (cid!=null&&cid.length > 0) {
                paymentCreation()

            } else {
//                customerManagement("", 13)
                paymentCreation()

            }
        } else {
            if (cid!=null&&cid.length > 0) {
                paymentCreation()

            } else {
//                customerManagement("", 14)
                paymentCreation()

            }
        }
    }

    private fun clickOnCheque(rbCheque: RadioButton) {
        rbCheque.isChecked = true
        type = 20
        tvSelectDate.isEnabled = true
        tvSelectDate.isClickable = true
        tvSelectDate.text = ""
        chequeDate = ""
        tvSelectDate.hint = getString(R.string.cheque_date)
        llChequeDate.visibility = View.VISIBLE
        llCapture.visibility = View.VISIBLE
        llBankList.visibility = View.VISIBLE

        llChequeNo.visibility = View.VISIBLE
        etChequeNo.isEnabled = true
        etChequeNo.isClickable = true
        etChequeNo.setText("")
        etChequeNo.hint = getString(R.string.cheque_number)
        tvBankSelection.isEnabled = true
        tvBankSelection.isClickable = true
        tvBankSelection.text = ""
        tvBankSelection.hint = getString(R.string.select_bank)
        ivCamera.isClickable = true
        ivCamera.isEnabled = true
        ivCapturedPic.isClickable = true
        ivCapturedPic.isEnabled = true
    }

    private fun showChequePreview(bitmap: Bitmap) {
        if (bitmap != null) {
            val dialog = Dialog(this, R.style.NewDialog)
            dialog.setContentView(R.layout.cheque_img_preview_dialog)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
            val tvCloseable = dialog.findViewById(R.id.tvClose) as TextView
            val ivChequePreview = dialog.findViewById(R.id.ivChequePreview) as ImageView

            ivChequePreview.setImageBitmap(bitmap)
            tvCloseable.setOnClickListener {
                dialog.dismiss()
            }

            dialog.show()
        } else {
            showToast("Please capture pic for cheque")
        }
    }

    private val datePickerListener: DatePickerDialog.OnDateSetListener = OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        chequeDate = "" + cyear + monthString + dayString
        tvSelectDate.text = "" + dayString + " - " + monthString + " - " + cyear
    }

    private fun paymentCreation() {
        var payId = ""
        var id = tvSelection.text.toString()
        amount = etAmount.text.toString().trim()
        if (intent.hasExtra("SINGLE")) {
            payId = intent!!.extras!!.getString("SINGLE").toString()
        }
        if (intent.hasExtra("selectedInvoiceDOs")) {
            selectedInvoiceDOs = intent.getSerializableExtra("selectedInvoiceDOs") as ArrayList<UnPaidInvoiceDO>

        }
        if (intent.hasExtra("selectedCustomerDOs")) {
            selectedCustomerDOs = intent.getSerializableExtra("selectedCustomerDOs") as ArrayList<CustomerDo>
        }
        if (selectedInvoiceDOs != null && selectedInvoiceDOs.size > 0) {
          val siteMultipleListRequest = CreateMultiplePaymentRequest(selectedInvoiceDOs, cheque, chequeDate, bank, java.lang.Double.parseDouble(amount), type, encImage, this@CreatePaymentActivity)
            siteMultipleListRequest.setOnResultListener { isError, createPaymentDO ->
                hideLoader()
                if (createPaymentDO != null) {
                    createPaymentDo = createPaymentDO
                    if (isError) {
                        showAppCompatAlert("Error", "Payment Not Created", "Ok", "", "Error", false)
                    } else {
                        if (createPaymentDO.paymentNumber.length > 0) {
                            preferenceUtils.saveString(PreferenceUtils.PAYMENT_INVOICE_ID, createPaymentDO.invoiceNumber)
                            preferenceUtils.saveString(PreferenceUtils.PAYMENT_ID, createPaymentDO.paymentNumber)
//                            showAppCompatAlert("Success", "Payment Created - " + createPaymentDO.paymentNumber, "Ok", "", "SUCCESS", false)
                            showToast("Payment Created - " + createPaymentDO.paymentNumber)

//                            preparePaymentCreation();
                            preParePaymentCreation()
                            podDo.payment = CalendarUtils.getCurrentDate(this)
                            StorageManager.getInstance(this).saveDepartureData(this, podDo)
                            btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnPayments.isClickable = false
                            btnPayments.isEnabled = false

                        } else if (createPaymentDO.message.length > 0) {
                            showAppCompatAlert("Info!", " " + createPaymentDO.message, "Ok", "", "", false)

                        } else {
                            showAppCompatAlert("Error", "Payment Not Created, please try again.", "Ok", "", "failed", false)

                        }
                        // Toast.makeText(this@CreatePaymentActivity, "Payment Created", Toast.LENGTH_SHORT).show()

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                    }
                } else {
                    showAppCompatAlert("Error", "Payment Not Created", "Ok", "", "fail", false)
                }

            }

            siteMultipleListRequest.execute()

        } else {
            if (payId.length > 0&&selectedCustomerDOs.size<2) {
            val    siteListRequest = CreateMiscelleneousPaymentRequest(selectedCustomerDOs.get(0).customer, cheque, chequeDate, bank, java.lang.Double.parseDouble(amount), type, encImage, this@CreatePaymentActivity)
                siteListRequest.setOnResultListener { isError, createPaymentDO,msg ->
                    hideLoader()
                    if (createPaymentDO != null) {
                        createPaymentDo = createPaymentDO
                        if (isError) {
                            if (msg.isNotEmpty()) {
                                showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                            } else {
                                showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.error), false)

                            }                        } else {
                            if (createPaymentDO.paymentNumber.length > 0) {
                                preferenceUtils.saveString(PreferenceUtils.PAYMENT_INVOICE_ID, createPaymentDO.invoiceNumber)
                                preferenceUtils.saveString(PreferenceUtils.PAYMENT_ID, createPaymentDO.paymentNumber)
//                            showAppCompatAlert("Success", "Payment Created - " + createPaymentDO.paymentNumber, "Ok", "", "SUCCESS", false)
                                showToast("Payment Created - " + createPaymentDO.paymentNumber)

//                            preparePaymentCreation();
                                preParePaymentCreation()
                                podDo.payment = CalendarUtils.getCurrentDate(this)
                                StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                btnPayments.isClickable = false
                                btnPayments.isEnabled = false

                            } else if (createPaymentDO.message.length > 0) {
                                showAppCompatAlert("Info!", " " + createPaymentDO.message, "Ok", "", "", false)

                            } else {
                                showAppCompatAlert("Error", "Payment Not Created, please try again.", "Ok", "", "failed", false)

                            }
                            // Toast.makeText(this@CreatePaymentActivity, "Payment Created", Toast.LENGTH_SHORT).show()

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                        }
                    } else {
                        showAppCompatAlert("Error", "Payment Not Created", "Ok", "", "fail", false)
                    }

                }

                siteListRequest.execute()
            } else {

                    siteListRequest2 = CreateMiscelleneousPaymentRequest(selectedCustomerDOs.get(0).customer, cheque, chequeDate, bank, java.lang.Double.parseDouble(amount), type, encImage, this@CreatePaymentActivity)

                siteListRequest2.setOnResultListener { isError, createPaymentDO,msg ->
                    hideLoader()
                    if (createPaymentDO != null) {
                        createPaymentDo = createPaymentDO
                        if (isError) {
                            if (msg.isNotEmpty()) {
                                showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                            } else {
                                showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.error), false)

                            }                        } else {
                            if (createPaymentDO.paymentNumber.length > 0) {
                                preferenceUtils.saveString(PreferenceUtils.PAYMENT_INVOICE_ID, createPaymentDO.invoiceNumber)
                                preferenceUtils.saveString(PreferenceUtils.PAYMENT_ID, createPaymentDO.paymentNumber)
//                            showAppCompatAlert("Success", "Payment Created - " + createPaymentDO.paymentNumber, "Ok", "", "SUCCESS", false)
                                showToast("Payment Created - " + createPaymentDO.paymentNumber)

//                            preparePaymentCreation();
                                preParePaymentCreation()
                                podDo.payment = CalendarUtils.getCurrentDate(this)
                                StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                btnPayments.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                                btnPayments.isClickable = false
                                btnPayments.isEnabled = false

                            } else if (createPaymentDO.message.length > 0) {
                                showAppCompatAlert("Info!", " " + createPaymentDO.message, "Ok", "", "", false)

                            } else {
                                showAppCompatAlert("Error", "Payment Not Created, please try again.", "Ok", "", "failed", false)

                            }
                            // Toast.makeText(this@CreatePaymentActivity, "Payment Created", Toast.LENGTH_SHORT).show()

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                        }
                    } else {
                        showAppCompatAlert("Error", "Payment Not Created", "Ok", "", "fail", false)
                    }

                }

                siteListRequest2.execute()
            }
        }

    }



    fun selectBankList() {
        bankDialog = Dialog(this, R.style.NewDialog)
        bankDialog.setCancelable(true)
        bankDialog.setCanceledOnTouchOutside(true)
        bankDialog.setContentView(R.layout.simple_list_dialog)
        val window = bankDialog.window
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
        }
        var data = preferenceUtils.getStringFromPreference(PreferenceUtils.Reason_Payment, "")
//            tvInvoiceId.setText("" + data)
//            tvInvoiceId.setOnClickListener({
//                preferenceUtils.saveString(PreferenceUtils.SITE_NAME, tvInvoiceId.text.toString())
//                tvSelection.text = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.SITE_NAME, "")
//                dialog.dismiss()
//            })
        var site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")

        var recyclerView = bankDialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        val siteListRequest = BankListRequest(site, this@CreatePaymentActivity)
        siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
            hideLoader()
            if (unPaidInvoiceMainDO != null) {
                if (isError) {
                    bankDialog.dismiss()
                    showToast(getString(R.string.server_error))                } else {
                    if(unPaidInvoiceMainDO!=null&&unPaidInvoiceMainDO.bankDOS.size>0){
                        var siteAdapter = BankListAdapter(this@CreatePaymentActivity, unPaidInvoiceMainDO.bankDOS,3)
                        recyclerView.setAdapter(siteAdapter)
                    }else{
                        bankDialog.dismiss()
                        showToast(getString(R.string.server_error))
                    }
                }
            } else {
                bankDialog.dismiss()
                showToast(getString(R.string.server_error))
            }

        }

        siteListRequest.execute()


        bankDialog.show()

    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            finish()
        }
    }

    override fun onButtonNoClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            finish()
        }
    }

    private fun preparePaymentCreation() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
//        id = "SD-U101-19000886";
        if (id.length > 0) {
            val siteListRequest = PDFPaymentDetailsRequest(id, this)
            val listener = PDFPaymentDetailsRequest.OnResultListener { isError, createPDFInvoiceDO ->
                hideLoader()
                if (createPDFInvoiceDO != null) {
                    if (isError) {

                        showToast("Unable to send Email")

                    } else {
                        createPDFpaymentDO = createPDFInvoiceDO
                        if (createPaymentDo.email == 10) {
                            createPaymentPDF(createPDFInvoiceDO)
                        }
                        if (createPaymentDo.print == 10) {
                            printDocument(createPDFpaymentDO, PrinterConstants.PrintPaymentReport)
                        }

                    }
                } else {
                    showToast("Unable to send Email")
                }
            }
            siteListRequest.setOnResultListener(listener)
            siteListRequest.execute()
        } else {
            showToast("Unable to send Email")

        }

    }

    private fun preParePaymentCreation() {
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
//        var id ="RMRC-U1011900056"
        if (id.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val siteListRequest = PDFPaymentDetailsRequest(id, this)
                siteListRequest.setOnResultListener { isError, createPDFInvoiceDO ->
                    hideLoader()
                    if (createPDFInvoiceDO != null) {
                        if (isError) {
                            showAppCompatAlert("Error", "Payment pdf api error", "Ok", "", "", false)
                        } else {
                            createPDFpaymentDO = createPDFInvoiceDO
                            if (createPDFInvoiceDO != null) {
                                ReceiptPDF(this).prepareCollectorPDF(createPDFInvoiceDO, "Collector")
                            } else {
                                //Display error message
                            }
                        }
                    } else {
                        showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                    }
                }
                siteListRequest.execute()
            } else {
                showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)
            }


        } else {
            showAppCompatAlert("Error", "No Payment Id found", "Ok", "", "", false)

        }

    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@CreatePaymentActivity)
        if (printConnection.isBluetoothEnabled) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().bondedDevices
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.name
                val mac = device.address // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                    StorageManager.getInstance(this).savePrinterMac(this, mac)
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        AppConstants.file3 = null

        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun createPaymentPDF(createPDFInvoiceDO: PaymentPdfDO) {
        if (createPDFInvoiceDO != null) {
            if (createPaymentDo.email == 10) {
                ReceiptPDF(this).createReceiptPDF(createPDFInvoiceDO, "Collector")

            }
            return

        } else {
            //Display error message
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        setResult(1, intent)
        finish()
        super.onBackPressed()

    }


}