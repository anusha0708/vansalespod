package com.tbs.generic.vansales.Activitys

import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.R

//
class CurrentVanSaleStockActivity : BaseActivity() {

    override fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = layoutInflater.inflate(R.layout.current_van_sales_stock, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.current_stock)

    }
}