//package com.tbs.generic.vansales.Adapters;
//
///**
// * Created by sandy on 2/7/2018.
// */
//
//import android.content.Context;
//import android.content.Intent;
//import androidx.recyclerview.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.tbs.generic.vansales.Activitys.PriceItemsActivity;
//import com.tbs.generic.vansales.Model.PriceDO;
//import com.tbs.generic.vansales.R;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class PriceAdapter extends RecyclerView.Adapter<PriceAdapter.MyViewHolder>  {
//
//    private List<PriceDO> priceDOS;
//    private Context context;
//
//    public void refreshAdapter(ArrayList<PriceDO> priceDOS) {
//        this.priceDOS = priceDOS;
//        notifyDataSetChanged();
//    }
//
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//        public TextView tvSiteName, tvSiteId;
//        private LinearLayout llDetails;
//
//        public MyViewHolder(View view) {
//            super(view);
//            llDetails = view.findViewById(R.id.llDetails);
//            tvSiteName = view.findViewById(R.id.tvName);
//            tvSiteId = view.findViewById(R.id.tvPriceId);
//
//
//
//        }
//    }
//
//
//    public PriceAdapter(Context context, List<PriceDO> siteDOS) {
//        this.context = context;
//        this.priceDOS = siteDOS;
//    }
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.price_data, parent, false);
//
//        return new MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(final MyViewHolder holder, int position) {
//
//        final PriceDO siteDO = priceDOS.get(position);
//        holder.tvSiteName.setText(siteDO.siteName);
//        holder.tvSiteId.setText("" + siteDO.siteId);
//
//        holder.llDetails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent intent = new Intent(context, PriceItemsActivity.class);
//                intent.putExtra("Code", siteDO.siteId);
//                context.startActivity(intent);
//
//
//            }
//        });
//
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return priceDOS.size();
//    }
//
//}
