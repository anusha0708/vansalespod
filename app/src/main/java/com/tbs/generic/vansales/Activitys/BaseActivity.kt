package com.tbs.generic.vansales.Activitys

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.text.TextUtils
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.webkit.PermissionRequest
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import com.tbs.generic.pod.Activitys.MasterDataActivity
import com.tbs.generic.vansales.ActionBarDrawerToggle
import com.tbs.generic.vansales.DrawerArrowDrawable
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.AuthorizationsRequest
import com.tbs.generic.vansales.Requests.LogoutTimeCaptureRequest
import com.tbs.generic.vansales.collector.TransactionListActivity
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.common.FireBaseOperations
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.AuthorizationListner
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.receivers.DateChangedReceiver
import com.tbs.generic.vansales.utils.*
import java.text.DateFormat
import java.util.*


abstract class BaseActivity : AppCompatActivity() {
    private lateinit var myReceiver: DateChangedReceiver
    val childList = HashMap<MenuModel, List<MenuModel>>()
    val headerList = ArrayList<MenuModel>()
    private var expandableListView: ExpandableListView? = null
    lateinit var ivMenu: ImageView
    lateinit var ivCustomerLocation: ImageView
    lateinit var authListener: AuthorizationListner
    lateinit var ivAdd: ImageView
    private var lastExpandedPosition = -1
    lateinit var toolbar: Toolbar
    lateinit var llBody: LinearLayout
    var llFooter: LinearLayout? = null
    var calendar = Calendar.getInstance()
    var display: TextView? = null
    var mDay: Int = 0
    var mMonth: Int = 0
    var mYear: Int = 0
    var lattitudeFused: String? = null
    var longitudeFused: String? = null
    var addressFused: String? = null
    private var mLastUpdateTime: String? = null
    private var myLocale: Locale? = null

    // location updates interval - 10sec
    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000

    private val REQUEST_CHECK_SETTINGS = 100


    // bunch of location related apis
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mSettingsClient: SettingsClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var mLocationSettingsRequest: LocationSettingsRequest? = null
    private var mLocationCallback: LocationCallback? = null
    private var mCurrentLocation: Location? = null

    // boolean flag to toggle the ui
    private var mRequestingLocationUpdates: Boolean? = null
    private lateinit var dialogLoader: Dialog
    private var number = ""
    lateinit var ivRefresh: ImageView

    lateinit var ivDone: ImageView

    lateinit var mDrawerLayout: DrawerLayout
    private val mDrawerList: ListView? = null
    private var context: Context? = null
    private val userId: String? = null
    private var orderCode: String? = null
    lateinit var flToolbar: FrameLayout
    private var mDrawerToggle: ActionBarDrawerToggle? = null
    private var drawerArrow: DrawerArrowDrawable? = null


    lateinit var preferenceUtils: PreferenceUtils
    lateinit var ivSearch: ImageView
    lateinit var svSearch: SearchView

    var ivPic: ImageView? = null

    //    lateinit   var ivBack: ImageView
    lateinit var tvScreenTitle: TextView
    lateinit var tvScreenTitleTop: TextView
    lateinit var tvScreenTitleBottom: TextView
    lateinit var tvDate: TextView
    lateinit var tvDriverCode: TextView
    lateinit var tvDriverName: TextView
    lateinit var tvRoutingId: TextView
    lateinit var tvCompanyCode: TextView
    lateinit var ivProfileArrow: ImageView
    lateinit var ivSelectAll: ImageView

    lateinit var tvShipments: TextView
    lateinit var tvVehicleCode: TextView
    lateinit var tvPlate: TextView
    lateinit var tvADT: TextView
    lateinit var tvDDT: TextView
    lateinit var tvStatus: TextView
    lateinit var llData: LinearLayout
    lateinit var llProfile: LinearLayout
    lateinit var llStatus: LinearLayout
    lateinit var llUserProfile: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.base_layout)

        context = this@BaseActivity
        //        inflater = this.getLayoutInflater();

        baseInitializeControls()

        preferenceUtils = PreferenceUtils(context)
        var id = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
        if (id.length > 0) {
            tvVehicleCode.visibility = View.VISIBLE
        }
        //        tvUsername.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.NAME, ""));
//        if (supportActionBar != null) {
//            supportActionBar!!.setDisplayShowHomeEnabled(true)
//            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
//            supportActionBar!!.setHomeButtonEnabled(true)
//            supportActionBar!!.setDisplayShowTitleEnabled(false)
//        }
//        setSupportActionBar(toolbar)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            supportActionBar!!.setHomeButtonEnabled(true)
            supportActionBar!!.setDisplayShowTitleEnabled(false)
        }

        tvScreenTitleTop.text = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID, "")
        tvScreenTitleBottom.text = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.USER_NAME, "")

        //        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //        getSupportActionBar().setHomeButtonEnabled(true);
        val data = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")

        if (data.length > 0) {
            tvRoutingId.visibility = View.VISIBLE
            llStatus.visibility = View.VISIBLE
            ivProfileArrow.visibility = View.VISIBLE
//            llData.setVisibility(View.VISIBLE)
//            tvDate.setVisibility(View.VISIBLE)

        } else {
            tvRoutingId.visibility = View.GONE
            tvDate.visibility = View.GONE
            llData.visibility = View.GONE
            ivProfileArrow.visibility = View.GONE

        }
        val siteId = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_NAME, "")
        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity)
        if (vehicleCheckInDo.scheduledRootId!!.isEmpty()) {
            tvDate.visibility = View.GONE
            tvRoutingId.visibility = View.GONE
        } else {
            tvDate.visibility = View.VISIBLE
            tvRoutingId.visibility = View.VISIBLE
        }

        tvDate.text = resources.getString(R.string.date_label) + preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE, "")
        tvRoutingId.text = resources.getString(R.string.vr_label) + preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
        tvDriverCode.text = resources.getString(R.string.driver_label) + preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "")
        tvVehicleCode.text = resources.getString(R.string.vehicle_code_label) + preferenceUtils.getStringFromPreference(PreferenceUtils.CVEHICLE_CODE, "")
        tvPlate.text = resources.getString(R.string.plate_label) + preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "")
        tvShipments.text = resources.getString(R.string.shipment_label) + preferenceUtils.getStringFromPreference(PreferenceUtils.CN_SHIPMENTS, "")
        tvADT.text = resources.getString(R.string.eta_label) + preferenceUtils.getStringFromPreference(PreferenceUtils.CA_DATE, "") + "  " + preferenceUtils.getStringFromPreference(PreferenceUtils.CA_TIME, "")
        tvDDT.text = resources.getString(R.string.etd_label) + preferenceUtils.getStringFromPreference(PreferenceUtils.CD_DATE, "") + "  " + preferenceUtils.getStringFromPreference(PreferenceUtils.CD_TIME, "")

        changeLocale()
        if (vehicleCheckInDo.checkInStatus.equals("", true)) {
            if (!vehicleCheckInDo.loadStock.equals("")) {
                tvStatus.text = vehicleCheckInDo.loadStock
            } else {
                tvStatus.text = resources.getString(R.string.logged_in)
            }
        } else {
            tvStatus.text = vehicleCheckInDo.checkInStatus
        }
        tvCompanyCode.text = "" + resources.getString(R.string.company_name)
        if (siteId.length > 0) {
            tvDriverName.visibility = View.VISIBLE
            tvDriverName.text = resources.getString(R.string.site) + siteId
        } else {
            tvDriverName.visibility = View.GONE
        }

        val image = preferenceUtils.getStringFromPreference(PreferenceUtils.PROFILE_PICTURE, "")
        if (image.isNotEmpty()) {
            val decodedString = android.util.Base64.decode(image, android.util.Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

            ivPic?.setImageBitmap(decodedByte)
        } else {
        }

        drawerArrow = object : DrawerArrowDrawable(this) {
            override fun isLayoutRtl(): Boolean {
                return false
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        }

        mDrawerToggle = object : ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close, drawerArrow) {

            override fun onDrawerOpened(drawerView: View) {
                //invalidateOptionsMenu()
                mDrawerLayout.closeDrawer(Gravity.LEFT)
                super.onDrawerOpened(drawerView)
            }

            override fun onDrawerClosed(drawerView: View) {
                this@BaseActivity.expandableListView!!.collapseGroup(this@BaseActivity.lastExpandedPosition)
                //invalidateOptionsMenu()
                mDrawerLayout.closeDrawer(Gravity.LEFT)
                super.onDrawerClosed(drawerView)
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                mDrawerLayout.closeDrawer(Gravity.LEFT)
                super.onDrawerSlide(drawerView, slideOffset)
            }

            override fun onDrawerStateChanged(newState: Int) {
                mDrawerLayout.closeDrawer(Gravity.LEFT)
                super.onDrawerStateChanged(newState)
            }
        }
        mDrawerLayout.addDrawerListener(mDrawerToggle!!)
        mDrawerToggle!!.syncState()
        toolbar.setNavigationOnClickListener {
            menuOperates()
        }
        //

        initialize()
        setTypeFace(AppConstants.MONTSERRAT_REGULAR_TYPE_FACE, llBody)


    }

    fun showAddItemDialog(resultListner: ResultListner) {
        try {
            var dialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(this).inflate(R.layout.item_custom, null)
//            applyFont(view, AppConstants.MONTSERRAT_REGULAR_TYPE_FACE)
            var etAdd = view.findViewById<View>(R.id.etAdd) as EditText

            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            val btnSubmit = view.findViewById<View>(R.id.btnSubmit) as Button




            btnSubmit.setOnClickListener {
                val itemName = etAdd.getText().toString().trim()

                if (itemName.equals("", ignoreCase = true)) {
                    showToast("Please Enter Reason")
                } else {

                    dialog.dismiss()
                    resultListner.onResultListner(itemName, true)

                }
            }
            dialog.setContentView(view)
            if (!dialog.isShowing())
                dialog.show()
        } catch (e: Exception) {
        }

    }

    fun showAddRatingDialog(resultListner: ResultListner) {
        try {
            var dialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(this).inflate(R.layout.rating, null)
//            applyFont(view, AppConstants.MONTSERRAT_REGULAR_TYPE_FACE)
            var ratingBar = view.findViewById<View>(R.id.ratingBar) as RatingBar

            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            val btnSubmit = view.findViewById<View>(R.id.btnSubmit) as Button




            btnSubmit.setOnClickListener {
                val itemName = ratingBar.rating.toDouble()

                if (itemName == 0.0) {
                    showToast("Please provide Rating")
                } else {

                    dialog.dismiss()
                    resultListner.onResultListner(itemName, true)

                }
            }
            dialog.setContentView(view)
            if (!dialog.isShowing())
                dialog.show()
        } catch (e: Exception) {
        }

    }

    fun showLoader() {
        runOnUiThread(RunShowLoader(resources.getString(R.string.loading_text)))
    }

    //Method to show loader with text
    fun showLoader(msg: String) {
        runOnUiThread(RunShowLoader(msg))
    }

    fun showToast(strMsg: String) {
        if (!strMsg.isEmpty()) {
            Toast.makeText(this@BaseActivity, strMsg, Toast.LENGTH_SHORT).show()
        }
    }

    fun showAlert(strMsg: String) {
        if (!strMsg.isEmpty()) {
            showAppCompatAlert(resources.getString(R.string.alert_message), strMsg, resources.getString(R.string.ok), "", "", true)
        }
    }

    fun showSnackbar(errorMsg: String) {

        val snackbar = Snackbar.make(llBody, errorMsg, Snackbar.LENGTH_SHORT)
        snackbar.show()

    }

    var alertDialog: AlertDialog.Builder? = null
    fun showAppCompatAlert(mTitle: String, mMessage: String, posButton: String, negButton: String?, from: String, isCancelable: Boolean) {
        if (alertDialog == null)
            alertDialog = AlertDialog.Builder(this@BaseActivity, R.style.AppCompatAlertDialogStyle)
        alertDialog!!.setTitle(mTitle)
        alertDialog!!.setMessage(mMessage)
        number = mMessage
        alertDialog!!.setPositiveButton(posButton) { dialog, which -> onButtonYesClick(from) }
        if (negButton != null && !negButton.equals("", ignoreCase = true))
            alertDialog!!.setNegativeButton(negButton) { dialog, which ->
                onButtonNoClick(from)
            }
        alertDialog!!.setCancelable(false)
        alertDialog!!.show()
    }

    open fun onButtonNoClick(from: String) {

    }

    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, id: Int) {
                        startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    }
                })
                .setNegativeButton("No", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, id: Int) {
                        dialog.cancel()
                    }
                })
        val alert = builder.create()
        alert.show()
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun insertContactWrapper() {
        val permissionsNeeded = ArrayList<String>()
        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add(resources.getString(R.string.camera))
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add(resources.getString(R.string.storage))
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add(resources.getString(R.string.location))
        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add(resources.getString(R.string.location))
        if (permissionsList.size > 0) {
            requestPermissions(permissionsList.toArray(arrayOfNulls<String>(permissionsList.size)),
                    124)
            return
        }
    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false
            }
        }
        return true
    }

    fun isLocationEnabled(context: Context): Boolean {
        var locationMode = 0
        val locationProviders: String
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE)
            } catch (e: Settings.SettingNotFoundException) {
                e.printStackTrace()
                return false
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED)
            return !TextUtils.isEmpty(locationProviders)
        }
    }

    fun updateLocationUI(resultListner: ResultListner) {
        if (mCurrentLocation != null) {

            lattitudeFused = mCurrentLocation!!.getLatitude().toString()
            longitudeFused = mCurrentLocation!!.getLongitude().toString()
            var gcd = Geocoder(getBaseContext(), Locale.getDefault());

            try {
                var addresses = gcd.getFromLocation(mCurrentLocation!!.getLatitude(),
                        mCurrentLocation!!.getLongitude(), 1);
                if (addresses.size > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    var cityName = addresses.get(0).getLocality();
                    addressFused = cityName
                    resultListner.onResultListner(true, true)

                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace();
            }

            stopLocationUpdates()
        }

    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates(resultListner: ResultListner) {
        mSettingsClient!!
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this) {

                    //                    Toast.makeText(applicationContext, "Started location updates!", Toast.LENGTH_SHORT).show()


                    mFusedLocationClient!!.requestLocationUpdates(mLocationRequest,
                            mLocationCallback, Looper.myLooper())

                    updateLocationUI(resultListner)
                }
                .addOnFailureListener(this) { e ->
                    val statusCode = (e as ApiException).statusCode
                    when (statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                val rae = e as ResolvableApiException
                                rae.startResolutionForResult(this@BaseActivity, REQUEST_CHECK_SETTINGS)
                            } catch (sie: IntentSender.SendIntentException) {
                            }

                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            val errorMessage = resources.getString(R.string.location_error)
                            Toast.makeText(this@BaseActivity, errorMessage, Toast.LENGTH_LONG).show()
                        }
                    }

                    updateLocationUI(resultListner)
                }
    }

    fun stopLocationUpdates() {
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient!!
                    .removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(this) {

                    }
        }
    }

    fun location(resultListner: ResultListner) {
        var boolean = isLocationEnabled(this)
        if (boolean == true) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            mSettingsClient = LocationServices.getSettingsClient(this)

            mLocationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    super.onLocationResult(locationResult)
                    // location is received
                    mCurrentLocation = locationResult!!.lastLocation
                    mLastUpdateTime = DateFormat.getTimeInstance().format(Date())

                    updateLocationUI(resultListner)
                }
            }

            mRequestingLocationUpdates = false

            mLocationRequest = LocationRequest()
            mLocationRequest!!.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
            mLocationRequest!!.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
            mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

            val builder = LocationSettingsRequest.Builder()
            builder.addLocationRequest(mLocationRequest!!)
            mLocationSettingsRequest = builder.build()

            Dexter.withActivity(this)
                    .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    .withListener(object : PermissionListener {
                        override fun onPermissionRationaleShouldBeShown(permission: com.karumi.dexter.listener.PermissionRequest?, token: PermissionToken?) {

                        }

                        override fun onPermissionGranted(response: PermissionGrantedResponse) {
                            mRequestingLocationUpdates = true
                            startLocationUpdates(resultListner)
                        }

                        override fun onPermissionDenied(response: PermissionDeniedResponse) {
                            if (response.isPermanentlyDenied()) {

                                showToast(resources.getString(R.string.loc_denied))
                                // open device settings when the permission is
                                // denied permanently
//                            openSettings()
                            }
                        }

                        fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
                            token.continuePermissionRequest()
                        }
                    }).check()
        } else {
            hideLoader()
            buildAlertMessageNoGps()
//            insertContactWrapper()
        }

    }

    open fun onButtonYesClick(from: String) {

    }


    fun hideLoader() {
        runOnUiThread {
            try {
                if (dialogLoader != null && dialogLoader.isShowing)
                    dialogLoader.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    fun hideKeyBoard(view: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    public fun customerAuthorization(id: String, type: Int, resultListner: ResultListner) {

        showLoader()
        if (Util.isNetworkAvailable(this)) {
            val request = AuthorizationsRequest(id, type, this)
            request.setOnResultListener { isError, customerDo ->
                if (isError) {
                    hideLoader()

                    showAppCompatAlert(resources.getString(R.string.error), resources.getString(R.string.server_error), resources.getString(R.string.ok), "", "", false)
                } else {
                    hideLoader()

                    if (customerDo.type == 2) {
                        resultListner.onResultListner(true, true)
                    } else {
//                            showAppCompatAlert("Error", resources.getString(R.string.server_error), resources.getString(R.string.ok), "", "", false)

                        resultListner.onResultListner(false, false)
                    }

                }

            }
            request.execute()
        } else {
            showAppCompatAlert(resources.getString(R.string.alert_message), resources.getString(R.string.internet_connection), resources.getString(R.string.ok), "", resources.getString(R.string.from_failure), false)
            hideLoader()

        }

    }

    private inner class RunShowLoader(private val strMsg: String) : Runnable {

        override fun run() {
            try {
                dialogLoader = Dialog(this@BaseActivity)
                dialogLoader.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialogLoader.setCancelable(false)
                dialogLoader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                dialogLoader.setContentView(R.layout.loader_custom)
                if (!dialogLoader.isShowing) {
                    dialogLoader.show()
                }
            } catch (e: Exception) {
                // dialogLoader = null
            }

        }
    }

    fun menuOperates() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT)
        } else {
            mDrawerLayout.openDrawer(Gravity.LEFT)
        }
    }


    protected fun disableMenuWithBackButton() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
    }

    abstract fun initialize()

    abstract fun initializeControls()

    private fun baseInitializeControls() {
        flToolbar = findViewById<View>(R.id.flToolbar) as FrameLayout
        llBody = findViewById<View>(R.id.llBody) as LinearLayout
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        expandableListView = findViewById<View>(R.id.expandableListView) as ExpandableListView
        ivMenu = findViewById<View>(R.id.ivMenu) as ImageView
        ivAdd = findViewById<View>(R.id.ivAdd) as ImageView
        ivCustomerLocation = findViewById<View>(R.id.ivCustomerLocation) as ImageView

        tvScreenTitle = findViewById<View>(R.id.tvScreenTitle) as TextView
        tvDriverName = findViewById<View>(R.id.tvDriverName) as TextView
        tvDriverCode = findViewById<View>(R.id.tvDriverCode) as TextView
        tvDate = findViewById<View>(R.id.tvDate) as TextView
        tvRoutingId = findViewById<View>(R.id.tvRoutingId) as TextView
        ivSearch = findViewById<View>(R.id.ivSearch) as ImageView
        tvScreenTitleTop = findViewById<View>(R.id.tvScreenTitleTop) as TextView
        tvScreenTitleBottom = findViewById<View>(R.id.tvScreenTitleBottom) as TextView
        ivPic = findViewById<View>(R.id.ivProfilePic) as ImageView
        svSearch = findViewById<View>(R.id.svSearch) as SearchView
        ivProfileArrow = findViewById<View>(R.id.ivProfileArrow) as ImageView
        tvCompanyCode = findViewById<View>(R.id.tvCompanyCode) as TextView
        ivRefresh = findViewById<View>(R.id.ivRefresh) as ImageView
        ivSelectAll = findViewById<View>(R.id.ivSelectAll) as ImageView

        tvVehicleCode = findViewById<View>(R.id.tvVehicleCode) as TextView
        tvPlate = findViewById<View>(R.id.tvPlate) as TextView
        tvShipments = findViewById<View>(R.id.tvShipments) as TextView
        tvADT = findViewById<View>(R.id.tvADT) as TextView
        tvDDT = findViewById<View>(R.id.tvDDT) as TextView

        tvStatus = findViewById<View>(R.id.tvStatus) as TextView
        llData = findViewById<View>(R.id.llData) as LinearLayout
        llProfile = findViewById<View>(R.id.llProfile) as LinearLayout
        llStatus = findViewById<View>(R.id.llStatus) as LinearLayout
        llUserProfile = findViewById<View>(R.id.llUserProfile) as LinearLayout


        mDrawerLayout = findViewById<View>(R.id.drawer_layout) as DrawerLayout


        llUserProfile.setOnClickListener {
            Util.preventTwoClick(it)
            val data = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            if (data.length > 0) {
                if (llData.visibility == View.VISIBLE) {
                    llData.visibility = View.GONE
                    ivProfileArrow.rotation = 270f
                } else {
                    llData.visibility = View.VISIBLE
                    ivProfileArrow.rotation = 90f
                }
            } else {
                tvRoutingId.visibility = View.GONE
                tvDate.visibility = View.GONE
                llData.visibility = View.GONE

            }
        }

        ivMenu.setOnClickListener {
            Util.preventTwoClick(it)

            val popup = PopupMenu(this@BaseActivity, ivMenu)
            popup.menuInflater.inflate(R.menu.popup_menu, popup.menu)
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    if (item.title.equals(resources.getString(R.string.logout))) {
                        logOut()


                    } else if (item.title.equals(resources.getString(R.string.activity_report))) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity)
                        val status = vehicleCheckInDo.checkInStatus
                        if (!status!!.isEmpty()) {
                            val intent = Intent(this@BaseActivity, TransactionListActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent)
                        } else {
                            showAppCompatAlert(resources.getString(R.string.alert_message), "Please Finish Check-in Process", resources.getString(R.string.ok), "", resources.getString(R.string.from_failure), false)

                        }

                    } else if (item.title.equals(resources.getString(R.string.user_activity_report))) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity)
                        val status = vehicleCheckInDo.checkInStatus
                        if (!status!!.isEmpty()) {
                            val intent = Intent(this@BaseActivity, UserActivityReportActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent)
                        } else {
                            showAppCompatAlert(resources.getString(R.string.alert_message), "Please Finish Check-in Process", resources.getString(R.string.ok), "", resources.getString(R.string.from_failure), false)

                        }

                    } else if (item.title.equals("Master Data")) {
                        val intent = Intent(this@BaseActivity, MasterDataActivity::class.java)
                        startActivity(intent)

                    } else if (item.title.equals("Available Stock")) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity)
                        val status = vehicleCheckInDo.checkInStatus
                        if (!status!!.isEmpty()) {
                            val intent = Intent(this@BaseActivity, AvailableStockActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent)
                        } else {
                            showAppCompatAlert(resources.getString(R.string.alert_message), "Please Finish Check-in Process", resources.getString(R.string.ok), "", resources.getString(R.string.from_failure), false)

                        }

                    } else if (item.title.equals("Vehicle Stock")) {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity)
                        val status = vehicleCheckInDo.checkInStatus
                        if (!status!!.isEmpty()) {
                            val intent = Intent(this@BaseActivity, NewAvailableStockActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent)
                        } else {
                            showAppCompatAlert(resources.getString(R.string.alert_message), "Please Finish Check-in Process", resources.getString(R.string.ok), "", resources.getString(R.string.from_failure), false)

                        }

                    } else {
                        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity)
                        val status = vehicleCheckInDo.checkInStatus
                        if (!status!!.isEmpty()) {
                            val intent = Intent(this@BaseActivity, CurrentVanSaleStockTabActivity::class.java)
                            intent.putExtra("ScreenTitle", "Current Van Sales Stock")
                            intent.putExtra("FLAG", 1)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent)
                        } else {
                            showAppCompatAlert(resources.getString(R.string.alert_message), "Please Finish Check-in Process", resources.getString(R.string.ok), "", resources.getString(R.string.from_failure), false)

                        }
                    }
//                        else if (item.title.equals("Settings")) {
//                             /* val intent = Intent(this@BaseActivity, PrintDocumentsActivity::class.java)
//                               startActivity(intent)*/
//                        } else if (item.title.equals("Messages")) {
//                            /*val intent = Intent(this@BaseActivity, DisplayBillDetailsActivity::class.java)
//                            startActivity(intent)*/
//                        } else if (item.title.equals("Events")) {
//                            /* val intent = Intent(this@BaseActivity, TBSMapsNavigationActivity::class.java)
//                             startActivity(intent)*/
//                        } else {
//                            Toast.makeText(this@BaseActivity, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show()
//                        }
                    return true

                }


            })
            popup.show()//showing popup menu

        }
    }

    fun refreshMenuAdapter() {
        val vehicleCheckInDo = StorageManager.getInstance(this@BaseActivity).getVehicleCheckInData(this@BaseActivity)
        if (vehicleCheckInDo.checkInStatus.equals("", true)) {
            if (!vehicleCheckInDo.loadStock.equals("")) {
                tvStatus.text = vehicleCheckInDo.loadStock
            } else {
                tvStatus.text = resources.getString(R.string.logged_in)
            }
        } else {
            tvStatus.text = vehicleCheckInDo.checkInStatus
        }
    }

    fun applyFont(root: View, font: String) {
        try {
            val fontName = fontStyleName(font)
            if (root is ViewGroup) {
                for (i in 0 until root.childCount)
                    applyFont(root.getChildAt(i), fontName)
            } else if (root is TextView) {
                root.typeface = Typeface.createFromAsset(assets, fontName)
            } else if (root is EditText) {
                root.typeface = Typeface.createFromAsset(assets, fontName)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun fontStyleName(name: String): String {
        var fontStyleName = ""
        when (name) {
            AppConstants.Goudy_Old_Style_Italic -> fontStyleName = "gillsansstd.otf"
            AppConstants.Goudy_Old_Style_Normal -> fontStyleName = "montserrat_regular.ttf"
            AppConstants.Goudy_Old_Style_Bold -> fontStyleName = "montserrat_bold.ttf"

            "" -> fontStyleName = "montserrat_regular.ttf"
        }
        return fontStyleName
    }


    override fun onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT)
        } else {
            super.onBackPressed()
        }

    }

    fun logOut() {
        logOutTimeCapture()
//        val siteListRequest = LogoutMultipleUsersRequest(driverId, date, date, time, this)
//        siteListRequest.setOnResultListener { isError, loginDo ->
//            hideLoader()
//            if (loginDo != null) {
//                if (isError) {
//                    hideLoader()
//                    showToast("you are unable to logout")
//                } else {
//                    hideLoader()
//                    if (loginDo.flag == 1) {
//                        logOutTimeCapture()
//
//
//                    } else {
//                        showToast("you are unable to logout")
//
//                    }
//
//                }
//            } else {
//                hideLoader()
//            }
//
//        }

//        siteListRequest.execute()
    }

    private fun logOutTimeCapture() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()

        val siteListRequest = LogoutTimeCaptureRequest(driverId, date, date, time, this)
        siteListRequest.setOnResultListener { isError, loginDo ->
            hideLoader()
            if (loginDo != null) {
                if (isError) {
                    hideLoader()
                    Toast.makeText(this@BaseActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
                } else {
                    hideLoader()
                    if (loginDo.flag == 2) {
                        showToast(resources.getString(R.string.logged_out))
//                        preferenceUtils.removeFromPreference(PreferenceUtils.DRIVER_ID)
//                        preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_SUCCESS)
//                        StorageManager.getInstance(this).deleteSpotSalesCustomerList(this)

                        val intent = Intent(this@BaseActivity, LoginActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(intent)

                    } else {
                        showToast(resources.getString(R.string.unable_logout))
                    }

                }
            } else {
                hideLoader()
            }

        }

        siteListRequest.execute()
    }

//    var trustManagers: Array<TrustManager>? = null
//
//    class _FakeX509TrustManager : X509TrustManager {
//        @Throws(CertificateException::class)
//        override fun checkClientTrusted(arg0: Array<X509Certificate>, arg1: String) {
//        }
//
//        @Throws(CertificateException::class)
//        override fun checkServerTrusted(arg0: Array<X509Certificate>, arg1: String) {
//        }
//
//        fun isClientTrusted(chain: Array<X509Certificate?>?): Boolean {
//            return true
//        }
//
//        fun isServerTrusted(chain: Array<X509Certificate?>?): Boolean {
//            return true
//        }
//
//        override fun getAcceptedIssuers(): Array<X509Certificate> {
//            return _AcceptedIssuers
//        }
//
//        companion object {
//            private val _AcceptedIssuers = arrayOf<X509Certificate>()
//        }
//    }

//    fun allowAllSSL() {
//        HttpsURLConnection
//                .setDefaultHostnameVerifier { hostname, session -> true }
//        var context: SSLContext? = null
//        if (trustManagers == null) {
//            trustManagers = arrayOf(_FakeX509TrustManager())
//        }
//        try {
//            context = SSLContext.getInstance("TLS")
//            context.init(null, trustManagers, SecureRandom())
//        } catch (e: NoSuchAlgorithmException) {
//        } catch (e: KeyManagementException) {
//        }
//        HttpsURLConnection.setDefaultSSLSocketFactory(context
//                ?.getSocketFactory())
//    }


    override fun onResume() {
        refreshMenuAdapter()
        FireBaseOperations.checkingDate(this)

        if (AppPrefs.getBoolean(AppPrefs.IS_TIME_CHANGED, false)) {
            checkDateAndTime()
        }
        super.onResume()
    }

    private fun checkDateAndTime() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle(resources.getString(R.string.setting_alert))
        builder.setMessage(resources.getString(R.string.date_change_alert))
        builder.setCancelable(false)
        //builder.setPositiveButton(resources.getString(R.string.ok), DialogInterface.OnClickListener(function = x))
        builder.setPositiveButton(resources.getString(R.string.adjust_date)) { dialog, which ->
            startActivityForResult(Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0)
        }
        builder.show()


    }

    fun setTypeFace(fontType: String, viewGroup: ViewGroup) {
        val count = viewGroup.childCount
        var v: View
        for (i in 0 until count) {
            v = viewGroup.getChildAt(i)
            if (v is TextView || v is Button)
                (v as TextView).typeface = getTypeFace(fontType)
            else if (v is ViewGroup)
                setTypeFace(fontType, v)
        }

    }

    private fun getTypeFace(fontType: String): Typeface {
        return Typeface.createFromAsset(assets, fontType)
    }
     fun getFormattedAmount(amount: Double): String? {
        try {
            return String.format("%,.2f", amount)
        } catch (ex: java.lang.Exception) {
            ex.fillInStackTrace()
        }
        return ""
    }
    fun changeLocale() {
        var lang = preferenceUtils.getStringFromPreference(PreferenceUtils.Locale_KeyValue, "")


        if (context == null) {
            return;
        }
        myLocale = Locale(lang)
        var config = Configuration()
        config.locale = myLocale;
        var resources = resources;
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }


}

