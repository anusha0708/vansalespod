package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class LoanReturnMainDO implements Serializable {

    public String site = "";
    public String customer = "";
    public Double openingQty         = 0.0;
    public int issuedQty =0;
    public int recievedQty=0;
    public int balanceQty=0;
    public String  message= "";
    public int status         = 0;
    public ArrayList<LoanReturnDO> loanReturnDOS = new ArrayList<>();

    public int flag                  = 0;
    public int flag2                 = 0;
    public int flag3                 = 0;
    public int flag4                 = 0;
    public int type;
}
