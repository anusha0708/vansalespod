package com.tbs.generic.vansales.prints;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.res.AssetManager;
import android.util.Base64;
import android.util.Log;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class PrinterConnection {

    private static final String TAG = "PrinterConnection";
    private static PrinterConnection printerConnection;
    private static Context context;
    private int from  = -1;
    private PrinterConnection(){

    }

    public static PrinterConnection getInstance(Context mContext){
        context = mContext;
//        if(printerConnection == null){
//        }
        printerConnection = new PrinterConnection();
        return printerConnection;
    }

    public void connectToPrinter(Object object, int from){
        this.from = from;
        readAssetFiles();
        startPrinting(object);
    }

    public boolean isBluetoothEnabled(){

        BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
        return bAdapter.isEnabled();
    }

    public void enableBluetooth(){
//        printerListDialog();
    }


    private void startPrinting(Object object){
        PrintConfigDo printConfigDo = new PrintConfigDo();
        printConfigDo.logoBase64 = PrinterConstants.base64LogoPng;
        printConfigDo.base64Footer = PrinterConstants.base64Footer;
        printConfigDo.jsonCmdAttribStr = jsonCmdAttribStr;
        printConfigDo.printerID  = StorageManager.getInstance(context).getPrinterMac(context);
        printConfigDo.printerName  = PrinterConstants.PRINTER_NAME;
        printConfigDo.printData = object;
        printConfigDo.printFrom = from;
        PodDo podDo = StorageManager.getInstance(context).getDepartureData(context);

        printConfigDo.signatureBase64 = podDo.getSignatureEncode();
        new PrintTask(context, printConfigDo).execute();
    }
    private String jsonCmdAttribStr = "";

    private void readAssetFiles(){
        InputStream input = null;
        ByteArrayOutputStream output = null;
        AssetManager assetManager = context.getAssets();
        String[] filesList = {"printer_profiles.JSON", "tema_logo.jpg", "tema_logo.jpg"};

        int fileIndex = 0;
        int initialBufferSize;

        try
        {
            for (String filename : filesList)
            {
                input = assetManager.open(filename);
                initialBufferSize = (fileIndex == 0) ? 80000 : 25000;

//                initialBufferSize = (fileIndex == 0) ? 8000 : 2500;
                output = new ByteArrayOutputStream(initialBufferSize);

                byte[] buf = new byte[1024];
                int len;
                while ((len = input.read(buf)) > 0)
                {
                    output.write(buf, 0, len);
                }
                input.close();
                input = null;

                output.flush();
                output.close();
                switch (fileIndex)
                {
                    case 0:
                        jsonCmdAttribStr = output.toString();
                        break;
                    case 1:
                        if(PrinterConstants.base64LogoPng.equalsIgnoreCase("")){
                            PrinterConstants.base64LogoPng = Base64.encodeToString(output.toByteArray(), Base64.DEFAULT);
                        }
                        break;
                    case 2:
                        if(PrinterConstants.base64Footer .equalsIgnoreCase("")){
                            PrinterConstants.base64Footer  = Base64.encodeToString(output.toByteArray(), Base64.DEFAULT);
                        }
                        break;
                }

                fileIndex++;
                output = null;
            }
        }
        catch (Exception ex)
        {
            Log.e(TAG, "Error reading asset file: " + filesList[fileIndex]);
        }
        finally
        {
            try
            {
                if (input != null)
                {
                    input.close();
                    input = null;
                }

                if (output != null)
                {
                    output.close();
                    output = null;
                }
            }
            catch (IOException e){}
        }
    }


    private void printDocument(Object object) {

    }

}
