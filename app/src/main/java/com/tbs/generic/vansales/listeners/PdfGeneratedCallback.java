package com.tbs.generic.vansales.listeners;

/**
 *Created by VenuAppasani on 04-11-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
public interface PdfGeneratedCallback {
    void onPdfGenerated();
}
