package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.CashDO;
import com.tbs.generic.vansales.Model.ChequeDO;
import com.tbs.generic.vansales.Model.CustomerReturnDO;
import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.Model.MiscStopDO;
import com.tbs.generic.vansales.Model.PickTicketDO;
import com.tbs.generic.vansales.Model.PurchaseReceiptDO;
import com.tbs.generic.vansales.Model.PurchaseReturnDO;
import com.tbs.generic.vansales.Model.SalesInvoiceDO;
import com.tbs.generic.vansales.Model.UserDeliveryDO;
import com.tbs.generic.vansales.Model.UserActivityReportMainDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class UserActivityReportRequest extends AsyncTask<String, Void, Boolean> {

    private UserActivityReportMainDO userActivityReportMainDO;
    private UserDeliveryDO userDeliveryDO;
    private MiscStopDO miscStopDO;
    private PurchaseReceiptDO purchaseReceiptDO;
    private PurchaseReturnDO purchaseReturnDO;
    private PickTicketDO pickTicketDO;
    private CustomerReturnDO customerReturnDO;

    private SalesInvoiceDO salesInvoiceDO;
    private CashDO cashDO;
    private ChequeDO chequeDO;
    private LoadStockDO loadStockDO;

    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    String transactionDate, endDATE;


    public UserActivityReportRequest(String date, Context mContext) {

        this.mContext = mContext;
        this.transactionDate = date;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, UserActivityReportMainDO userActivityReportMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        String site = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
        String routingID = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "");
        String transactionID = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
        String stransactionID = preferenceUtils.getStringFromPreference(PreferenceUtils.DOC_NUMBER, "");
        JSONObject jsonObject = new JSONObject();

        try {
            String ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, mContext.getResources().getString(R.string.checkin_non_scheduled));
            if (ShipmentType.equals(mContext.getResources().getString(R.string.checkin_non_scheduled))) {
                jsonObject.put("I_YSCRNUM", transactionID);

            } else {
                jsonObject.put("I_YSCRNUM", stransactionID);
            }
            jsonObject.put("I_YVRNUM", routingID);
            jsonObject.put("I_YUSR", id);
            jsonObject.put("I_YTRNDATE", transactionDate);
            jsonObject.put("I_YFCY", site);
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.USER_ACTIVITY_REPORT, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            userActivityReportMainDO = new UserActivityReportMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    attribute = xpp.getAttributeValue(null, "ID");
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP2")) {
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP2");
                        userActivityReportMainDO.userDeliveryDOS = new ArrayList<>();
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP3")) {
                        userActivityReportMainDO.purchaseReceiptDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP3");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP4")) {
                        userActivityReportMainDO.customerReturnDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP4");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP5")) {
                        userActivityReportMainDO.pickTicketDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP5");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP6")) {
                        userActivityReportMainDO.purchaseReturnDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP6");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP7")) {
                        userActivityReportMainDO.miscStopDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP7");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP8")) {
                        userActivityReportMainDO.salesInvoiceDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP8");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP9")) {
                        userActivityReportMainDO.cashDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP9");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP10")) {
                        userActivityReportMainDO.chequeDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP10");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP11")) {
                        userActivityReportMainDO.productDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP11");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP12")) {
                        userActivityReportMainDO.stockDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP12");
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP2")) {
                        userDeliveryDO = new UserDeliveryDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP3")) {
                        purchaseReceiptDO = new PurchaseReceiptDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP4")) {
                        customerReturnDO = new CustomerReturnDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP5")) {
                        pickTicketDO = new PickTicketDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP6")) {
                        purchaseReturnDO = new PurchaseReturnDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP7")) {
                        miscStopDO = new MiscStopDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP8")) {
                        salesInvoiceDO = new SalesInvoiceDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP9")) {
                        cashDO = new CashDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP10")) {
                        chequeDO = new ChequeDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP11")) {
                        loadStockDO = new LoadStockDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP12")) {
                        loadStockDO = new LoadStockDO();
                    }
//                    else if (startTag.equalsIgnoreCase("LIN")) {
//                        spotDeliveryDO = new SpotDeliveryDO();
//                        salesInvoiceDO = new SalesInvoiceDO();
//                        cashDO = new CashDO();
//                        chequeDO = new ChequeDO();
//
//                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("I_YUSR")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.userName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("I_YTRNDATE")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.transactionDate = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCPY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.companyCode = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCPYDES")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.companyDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("I_YFCY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.site = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHNUM")) {
                            if (text.length() > 0) {

                                userDeliveryDO.documentNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHBPC")) {
                            if (text.length() > 0) {

                                userDeliveryDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHBPCNAME")) {
                            if (text.length() > 0) {

                                userDeliveryDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHITM")) {
                            if (text.length() > 0) {

                                userDeliveryDO.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHITMDES")) {
                            if (text.length() > 0) {

                                userDeliveryDO.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHQTY")) {
                            if (text.length() > 0) {

                                userDeliveryDO.quantity = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHITMUN")) {
                            if (text.length() > 0) {

                                userDeliveryDO.productUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPTHNUM")) {
                            if (text.length() > 0) {

                                purchaseReceiptDO.documentNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPTHBPS")) {
                            if (text.length() > 0) {

                                purchaseReceiptDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPTHBPSNAME")) {
                            if (text.length() > 0) {

                                purchaseReceiptDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPTHITM")) {
                            if (text.length() > 0) {

                                purchaseReceiptDO.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPTHITMDES")) {
                            if (text.length() > 0) {

                                purchaseReceiptDO.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPTHQTY")) {
                            if (text.length() > 0) {

                                purchaseReceiptDO.quantity = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPTHITMUN")) {
                            if (text.length() > 0) {

                                purchaseReceiptDO.productUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSRHNUM")) {
                            if (text.length() > 0) {

                                customerReturnDO.documentNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSRHBPC")) {
                            if (text.length() > 0) {

                                customerReturnDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSRHBPCNAME")) {
                            if (text.length() > 0) {

                                customerReturnDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSRHITM")) {
                            if (text.length() > 0) {

                                customerReturnDO.product = text;

                            }

                        } else if (attribute.equalsIgnoreCase("O_YSRHITMDES")) {
                            if (text.length() > 0) {

                                customerReturnDO.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSRHQTY")) {
                            if (text.length() > 0) {

                                customerReturnDO.quantity = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSRHITMUN")) {
                            if (text.length() > 0) {

                                customerReturnDO.productUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPRHNUM")) {
                            if (text.length() > 0) {

                                pickTicketDO.documentNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPRHBPC")) {
                            if (text.length() > 0) {

                                pickTicketDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPRHBPCNAME")) {
                            if (text.length() > 0) {

                                pickTicketDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPRHITM")) {
                            if (text.length() > 0) {

                                pickTicketDO.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPRHITMDES")) {
                            if (text.length() > 0) {

                                pickTicketDO.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPRHQTY")) {
                            if (text.length() > 0) {

                                pickTicketDO.quantity = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPRHITMUN")) {
                            if (text.length() > 0) {

                                pickTicketDO.productUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPNHNUM")) {
                            if (text.length() > 0) {

                                purchaseReturnDO.documentNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPNHBPC")) {
                            if (text.length() > 0) {

                                purchaseReturnDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPNHBPCNAME")) {
                            if (text.length() > 0) {

                                purchaseReturnDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPNHITM")) {
                            if (text.length() > 0) {

                                purchaseReturnDO.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPNHITMDES")) {
                            if (text.length() > 0) {

                                purchaseReturnDO.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPNHQTY")) {
                            if (text.length() > 0) {

                                purchaseReturnDO.quantity = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPNHITMUN")) {
                            if (text.length() > 0) {

                                purchaseReturnDO.productUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YMISSTOPNUM")) {
                            if (text.length() > 0) {

                                miscStopDO.documentNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YMISSTOPBPC")) {
                            if (text.length() > 0) {

                                miscStopDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YMISSTOPBPCNAME")) {
                            if (text.length() > 0) {

                                miscStopDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHNUM")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.documentNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHBPC")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHBPCNAME")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHITM")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHITMDES")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHQTY")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.quantity = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHITMUN")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.productUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHAMT")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.amount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHCUR")) {
                            if (text.length() > 0) {

                                salesInvoiceDO.currency = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHPAYNUM")) {
                            if (text.length() > 0) {

                                cashDO.paymentId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHPAYBPC")) {
                            if (text.length() > 0) {

                                cashDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHPAYBPCNAME")) {
                            if (text.length() > 0) {

                                cashDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHPAYAMT")) {
                            if (text.length() > 0) {

                                cashDO.amount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHPAYCUR")) {
                            if (text.length() > 0) {

                                cashDO.currecncy = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQPAYNUM")) {
                            if (text.length() > 0) {

                                chequeDO.paymentId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQPAYBPC")) {
                            if (text.length() > 0) {

                                chequeDO.customerId = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQPAYBPCNAME")) {
                            if (text.length() > 0) {

                                chequeDO.customerName = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQPAYAMT")) {
                            if (text.length() > 0) {

                                chequeDO.amount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQPAYCUR")) {
                            if (text.length() > 0) {

                                chequeDO.currecncy = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDHTOTQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.deliveryTotalQty = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPRHTOTQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.pickTicketTotalQty = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSRHTOTQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.customerReturnTotalQty = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPNHTOTQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.purchaseReturnTotalQty = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPTHTOTQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.receiptTotalQty = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHTOTQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.invoiceTotalQuantity = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSIHTOTMAT")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.invoiceTotalAmount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCSHTOTPAYAMT")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.cashTotalAmount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCHQTOTPAYAMT")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.chequeTotalAmount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPENITM")) {
                            if (text.length() > 0) {

                                loadStockDO.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPENITMDES")) {
                            if (text.length() > 0) {

                                loadStockDO.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPENITMSAU")) {
                            if (text.length() > 0) {

                                loadStockDO.stockUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPENITMWEU")) {
                            if (text.length() > 0) {

                                loadStockDO.weightUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOPENITMQTY")) {
                            if (text.length() > 0) {

                                loadStockDO.quantity = Integer.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YTOTOPENQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.productsTotalQty = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YTOTENDQTY")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.stockTotalQty = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YENDITM")) {
                            if (text.length() > 0) {

                                loadStockDO.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YENDITMDES")) {
                            if (text.length() > 0) {

                                loadStockDO.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YENDITMSAU")) {
                            if (text.length() > 0) {

                                loadStockDO.stockUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YENDITWEU")) {
                            if (text.length() > 0) {

                                loadStockDO.weightUnit = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YENDITMQTY")) {
                            if (text.length() > 0) {

                                loadStockDO.quantity = Integer.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.flag = Integer.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YMESSAGE")) {
                            if (text.length() > 0) {

                                userActivityReportMainDO.message = text;
                            }

                        }
                        text = "";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP2")) {
                        userActivityReportMainDO.userDeliveryDOS.add(userDeliveryDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP3")) {
                        userActivityReportMainDO.purchaseReceiptDOS.add(purchaseReceiptDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP4")) {
                        userActivityReportMainDO.customerReturnDOS.add(customerReturnDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP5")) {
                        userActivityReportMainDO.pickTicketDOS.add(pickTicketDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP6")) {
                        userActivityReportMainDO.purchaseReturnDOS.add(purchaseReturnDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP7")) {
                        userActivityReportMainDO.miscStopDOS.add(miscStopDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP8")) {
                        userActivityReportMainDO.salesInvoiceDOS.add(salesInvoiceDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP9")) {
                        userActivityReportMainDO.cashDOS.add(cashDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP10")) {
                        userActivityReportMainDO.chequeDOS.add(chequeDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP11")) {
                        userActivityReportMainDO.productDOS.add(loadStockDO);
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP12")) {
                        userActivityReportMainDO.stockDOS.add(loadStockDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();
        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ((BaseActivity) mContext).hideLoader();

        //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, userActivityReportMainDO);
        }
    }
}