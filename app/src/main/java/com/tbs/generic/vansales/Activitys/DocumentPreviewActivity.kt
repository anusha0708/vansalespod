package com.tbs.generic.vansales.Activitys

import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.ActiveDeliveryRequest
import com.tbs.generic.vansales.pdfs.utils.PDFOperations
import com.tbs.generic.vansales.utils.PreferenceUtils
import kotlinx.android.synthetic.main.document_preview.*


//
class DocumentPreviewActivity : BaseActivity() {
    lateinit var activeDeliveryMainDO : ActiveDeliveryMainDO

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.document_preview, null) as ConstraintLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        initializeControls()

    }

    override fun initializeControls() {
        prepareDeliveryNoteCreation()
    }
    private fun getAddress(): String? {
        val street: String = activeDeliveryMainDO.customerStreet
        val landMark: String = activeDeliveryMainDO.customerLandMark
        val town: String = activeDeliveryMainDO.customerTown
        val city: String = activeDeliveryMainDO.customerCity
        val postal: String = activeDeliveryMainDO.customerPostalCode
        val countryName: String = activeDeliveryMainDO.countryName
        var finalString = ""
        if (!TextUtils.isEmpty(street)) {
            finalString += "$street, "
        }
        if (!TextUtils.isEmpty(landMark)) {
            finalString += "$landMark, "
        }
        tvScreenTitle.setText(resources.getString(R.string.preview))
        if (!TextUtils.isEmpty(town)) {
            finalString += "$town, "
        }
        if (!TextUtils.isEmpty(city)) {
            finalString += "$city, "
        }
        if (!TextUtils.isEmpty(postal)) {
            finalString += "$postal, "
        }
        //        if (!TextUtils.isEmpty(countryName)) {
//            finalString += countryName;
//        }
        return finalString
    }
    private fun prepareDeliveryNoteCreation() {
        var id = "";
        if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "").equals(resources.getString(R.string.checkin_scheduled))) {
            id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

        } else {
            id = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

        }


        if (id.length > 0) {
            showLoader()

            val driverListRequest = ActiveDeliveryRequest(id, this@DocumentPreviewActivity)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                if (activeDeliveryDo != null) {
                    if (isError) {
                        hideLoader()
                        showAppCompatAlert(getString(R.string.error), getString(R.string.delivery_note_pdf_error), getString(R.string.ok), "", "", false)
                    } else {
                        hideLoader()
                        activeDeliveryMainDO=activeDeliveryDo
                         if(activeDeliveryDo.activeDeliveryDOS.size>0) {
                             documenTNum.setText(activeDeliveryDo.shipmentNumber)
                             deliveredTo.setText(activeDeliveryDo.customerDescription)
                             userID.setText(activeDeliveryDo.createUserID)
                             user_name.setText(activeDeliveryDo.createUserName)
                             date.setText(activeDeliveryDo.createdDate)
                             time.setText(activeDeliveryDo.createdTime)
                             registered_supplier_address.setText(getAddress())
                             customerCode.setText(activeDeliveryDo.customer)
                             var vehiclCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")

                             vehicleCode.setText(vehiclCode)
                             namePlate.setText(activeDeliveryDo.customer)
                             receiverNum.setText(activeDeliveryDo.capturedNumber)
                             recieverName.setText(activeDeliveryDo.capturedName)
                             paymentTerm.setText(activeDeliveryDo.paymentTerm)

                             for (i in 0 until activeDeliveryDo.activeDeliveryDOS.size) {
                                 val view: View = layoutInflater.inflate(R.layout.content_row, null)
                                 val item_name = view.findViewById<View>(R.id.productDescription) as TextView
                                 val sno = view.findViewById<View>(R.id.sno) as TextView

                                 val quantity = view.findViewById<View>(R.id.qty) as TextView
                                 var s=i+1
                                 sno.setText(""+s)

                                 item_name.setText(activeDeliveryDo.activeDeliveryDOS.get(i).productDescription)
                                 quantity.setText(""+activeDeliveryDo.activeDeliveryDOS.get(i).orderedQuantity +" "+activeDeliveryDo.activeDeliveryDOS.get(i).unit)

                                 content.addView(view)
                             }
                             val image = PDFOperations.getInstance().getSignBitmap(activeDeliveryDo.signature)
                             progressBarCode.visibility=View.GONE
                             img_qr_code_image.setImageBitmap(image)
                         }
                    }
                } else {
                    showAppCompatAlert(getString(R.string.error), getString(R.string.server_erro), getString(R.string.ok), "", "", false)

                }
            }
            driverListRequest.execute()
        }else{
            showAlert(resources.getString(R.string.no_data_found))
        }
    }

}