package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.Model.LoanReturnDO;
import com.tbs.generic.vansales.Model.LoanReturnMainDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class LoanReturnRequest extends AsyncTask<String, Void, Boolean> {

    private LoanReturnMainDO loanReturnMainDO;
    private LoanReturnDO loanReturnDO;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;

    public LoanReturnRequest( Context mContext) {

        this.mContext = mContext;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, LoanReturnMainDO loanReturnMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String site  = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
        JSONObject jsonObject = new JSONObject();
        try {


            ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(mContext).getActiveDeliveryMainDo(mContext);
            CustomerDo customerDo = StorageManager.getInstance(mContext).getCurrentSpotSalesCustomer(mContext);


            String ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, mContext.getResources().getString(R.string.checkin_non_scheduled));
            if (ShipmentType.equals(mContext.getResources().getString(R.string.checkin_non_scheduled))) {

                jsonObject.put("I_YBPC", customerDo.customer);
                jsonObject.put("I_YFCY", site);
            } else {
                jsonObject.put("I_YBPC", activeDeliverySavedDo.customer);
                jsonObject.put("I_YFCY", site);
            }


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.LOAN_RETURN, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            loanReturnMainDO = new LoanReturnMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        loanReturnMainDO.loanReturnDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        loanReturnDO = new LoanReturnDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("I_YFCY")) {
                            loanReturnMainDO.site = text;


                        } else if (attribute.equalsIgnoreCase("I_YBPC")) {
                            loanReturnMainDO.customer = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_YSDHD")) {
                            loanReturnDO.shipmentNumber = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_YOQTY")) {
                            if(!text.isEmpty()){

                                loanReturnMainDO.openingQty = Double.valueOf(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YITM")) {
                            loanReturnDO.productName = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_YSDHCAT")) {
                            if(text.length()>0){


                                loanReturnDO.type = Integer.parseInt(text);
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_YDES")) {
                            loanReturnDO.productDescription = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_YQTY")) {
                            if(!text.isEmpty()){

                                loanReturnDO.qty = Double.parseDouble(text);
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_YWEI")) {
                            loanReturnDO.unit = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_YSDLIN")) {
                            loanReturnDO.lineNumber = Integer.parseInt(text);


                        }
                        text="";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        loanReturnMainDO.loanReturnDOS.add(loanReturnDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, loanReturnMainDO);
        }
    }
}