package com.tbs.generic.vansales.listeners;

public interface LoadStockListener {
    void updateCount();
}
