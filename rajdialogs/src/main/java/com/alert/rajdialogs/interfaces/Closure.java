package com.alert.rajdialogs.interfaces;

/**
 * Created by rajkumar on 21/08/17.
 */

public interface Closure {
    void exec();
}
